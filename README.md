## Arvie (Current: Laravel 6.*) 

### Setup

php artisan key:generate

composer install / update

php artisan storage:link

php artisan migrate --seed

php artisan vendor:publish --provider="Dacastro4\LaravelGmail\LaravelGmailServiceProvider"

### Demo Credentials

**User:** admin@admin.com  
**Password:** secret

### Setup Node for scrapping

sudo apt-get install npm

npm install

sudo apt-get -y install libnspr4 libnss3 libnss3-tools libatk1.0-0 libatk-bridge2.0-0 libxkbcommon-x11-0 libgtk-3-0 libgbm-dev

## If required chromium browser then install
sudo apt-get install chromium-browser
