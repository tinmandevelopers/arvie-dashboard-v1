<?php

namespace App\Classes\BookingEnquiry;

use App\Classes\General\GeneralCls;
use App\Classes\Sync\SyncCls;
use App\Models\Campground;
use App\Repositories\Api\BookingRepository;

class BookingEnquiryCls {

    protected $bookingRepository;

    public function __construct(BookingRepository $bookingRepository) {
        $this->bookingRepository = $bookingRepository;
    }

    /**
     *  class for create booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function storeBooking($postData) {
        try {
            $campgroundFound = Campground::find($postData['campground_id']);
            if ($campgroundFound) {
                if (isset($postData['details']) && $postData['details']) {
                    $postData['details'] = str_replace('"length":"No"', '"length":"1"', $postData['details']);
                }
                $verifyDetail = verify_availablity_data($postData);

                if ($verifyDetail['error']) {
                    $data = GeneralCls::setResponse('other_error', $verifyDetail['message']);
                    return $data;
                } else {
                    $bookingData = $this->bookingRepository->storeBooking($postData);
                    if (isset($bookingData['error']['message'])) {
                        return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
                    } else {
                        $data = GeneralCls::setResponse('success');
                        $data['response'] = $bookingData;
                        return $data;
                    }
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Campground Not Found.Invalid Campground ID.');
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for update booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function updateBooking($postData, $checktoken = false) {
        try {
            $bookingData = $this->bookingRepository->updateBooking($postData, $checktoken);
            
            $bookingPostData = array(
                'booking_id' => $postData['booking_id'],
                'status' => $postData['status']
            );
            logger('Call Site API >> ' . json_encode($bookingPostData));
            $updateServerBooking = SyncCls::updateBookingStatus($bookingPostData);
            logger('Site API Response >> ' . json_encode($updateServerBooking));
            
            if (isset($bookingData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $bookingData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for delete booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteBooking($postData) {

        try {
            $deleteBooking = $this->bookingRepository->deleteBooking($postData);
            if (isset($deleteBooking['error']['message'])) {
                return GeneralCls::setResponse('other_error', $deleteBooking['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $deleteBooking;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for Initiate booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function initiateBooking($postData) {
        try {
            $bookingData = $this->bookingRepository->initiateBooking($postData);
            if (isset($bookingData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $bookingData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for Initiate booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function detailBooking($postData, $checktoken = false) {
        try {
            $bookingData = $this->bookingRepository->detailBooking($postData, $checktoken);
            if (isset($bookingData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $bookingData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for Initiate booking enquiry 
     *
     * @return \Illuminate\Http\Response
     */
    public function storeFileBooking($postData) {
        try {
            $bookingData = $this->bookingRepository->storeFileBooking($postData);
            if (isset($bookingData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $bookingData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for check Booking Availability
     *
     * @return \Illuminate\Http\Response
     */
    public function checkBookingAvailability($postData) {
        try {
            $campgroundFound = Campground::find($postData['campground_id']);

            if ($campgroundFound) {
                $verifyDetail = verify_availablity_data($postData,'checkavailablity');
               
                if ($verifyDetail['error']) {
                    $data = GeneralCls::setResponse('other_error', $verifyDetail['message']);
                    return $data;
                } else {
                    $data = GeneralCls::setResponse('success');
                    $title = get_datasource_title($campgroundFound->url);
                    $data['title'] = $title;
                    if ($title == 'reserveamerica') {
                        $inDate = date_create($postData['check_in_date']);
                        $outDate = date_create($postData['check_out_date']);
                        $diff = date_diff($inDate, $outDate);
                        $days = $diff->format("%a");
                        $checkIn = str_replace('/', '-', $postData['check_in_date']);
                        $daysStr = '/campsites?arrivalDate=' . $checkIn . '&lengthOfStay=' . $days . '&availStartDate=' . $checkIn;
                        $campTxt = strtolower(str_replace(" ", "-", $campgroundFound->title));
                        $campname = str_replace("jaxrs-json/facilities", 'explore/' . $campTxt, $campgroundFound->url);
                        $campUrl = str_replace('?temp_facility_details_only=true', $daysStr, $campname);
                        $data['url'] = str_replace('&', '@@', $campUrl);
                        $data['url'] = str_replace("'", '', $data['url']);
                        $data['url'] = str_replace("(", '', $data['url']);
                        $data['url'] = str_replace(")", '', $data['url']);
                    } else {
                        $data['url'] = $campgroundFound->url;
                    }

                    // $data['url'] = $campgroundFound->url;
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Campground Not Found. Invalid Campground.');
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for Store booking Availability 
     *
     * @return \Illuminate\Http\Response
     */
    public function storeBookingAvailability($postData) {
        try {
            $bookingData = $this->bookingRepository->storeBookingAvailability($postData);
            if (isset($bookingData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $bookingData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

}
