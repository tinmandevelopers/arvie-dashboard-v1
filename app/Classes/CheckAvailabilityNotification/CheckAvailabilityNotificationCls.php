<?php

namespace App\Classes\CheckAvailabilityNotification;

use App\Classes\General\GeneralCls;
use App\Models\Campground;
use App\Repositories\Api\CheckAvailabilityNotificationRepository;
use DB;

class CheckAvailabilityNotificationCls
{

    protected $checkAvailabilityNotificationRepository;

    public function __construct(CheckAvailabilityNotificationRepository $checkAvailabilityNotificationRepository)
    {
        $this->checkAvailabilityNotificationRepository = $checkAvailabilityNotificationRepository;
    }

    /**
     *  class for create Check Availability Notification  
     *
     * @return \Illuminate\Http\Response
     */
    public function store($postData)
    {
        try {
            $campgroundFound = Campground::find($postData['campground_id']);
            if ($campgroundFound) {

                $custom = null;
                $user1 = null;
                $user2 = null;
                $user3 = null;
                if (isset($postData['duration']) && $postData['duration'] != '48_hour' && $postData['duration'] != '1_week' && $postData['duration'] != '1_month') {
                    return GeneralCls::setResponse('other_error', 'Duration is wrong.');
                }
                
                if (isset($postData['details']['search_type'])) {
                    if (isset($postData['details']['search_type']['custom_search'])) {
                        if (isset($postData['details']['custom'])) {
                            if ($postData['details']['custom']['towing_vehicle'] == 'Yes') {
                                if ($postData['details']['custom']['towing_length'] == "") {
                                    return GeneralCls::setResponse('other_error', 'Custom Search Towning length is required.');
                                }
                            }
                            if ($postData['details']['custom']['slide_out'] == 'Yes') {
                                if ($postData['details']['custom']['rear'] == "") {
                                    return GeneralCls::setResponse('other_error', 'Custom Search Slideout is required.');
                                }
                            }
                            $postData['details']['custom']['reservation_type'] = $postData['reservation_type'];
                            $postData['details']['custom']['adults'] = $postData['adults'];
                            $custom = $postData['details']['custom'];
                        }
                    }
                    if (isset($postData['details']['search_type']['user_1'])) {
                        if (isset($postData['details']['user_1'])) {
                            if ($postData['details']['user_1']['towing_vehicle'] == 'Yes') {
                                if ($postData['details']['user_1']['towing_length'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 1 Towning length is required.');
                                }
                            }
                            if ($postData['details']['user_1']['slide_out'] == 'Yes') {
                                if ($postData['details']['user_1']['rear'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 1 Search Slideout is required.');
                                }
                            }
                            $postData['details']['user_1']['reservation_type'] = $postData['reservation_type'];
                            $postData['details']['user_1']['adults'] = $postData['adults'];
                            $user1 = $postData['details']['user_1'];
                        }
                    }
                    if (isset($postData['details']['search_type']['user_2'])) {
                        if (isset($postData['details']['user_2'])) {
                            if ($postData['details']['user_2']['towing_vehicle'] == 'Yes') {
                                if ($postData['details']['user_2']['towing_length'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 2 Towning length is required.');
                                }
                            }
                            if ($postData['details']['user_2']['slide_out'] == 'Yes') {
                                if ($postData['details']['user_2']['rear'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 2 Search Slideout is required.');
                                }
                            }
                            $postData['details']['user_2']['reservation_type'] = $postData['reservation_type'];
                            $postData['details']['user_2']['adults'] = $postData['adults'];
                            $user2 = $postData['details']['user_2'];
                        }
                    }
                    if (isset($postData['details']['search_type']['user_3'])) {
                        if (isset($postData['details']['user_3'])) {
                            if ($postData['details']['user_3']['towing_vehicle'] == 'Yes') {
                                if ($postData['details']['user_3']['towing_length'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 3 Towning length is required.');
                                }
                            }
                            if ($postData['details']['user_3']['slide_out'] == 'Yes') {
                                if ($postData['details']['user_3']['rear'] == "") {
                                    return GeneralCls::setResponse('other_error', 'User 3 Search Slideout is required.');
                                }
                            }
                            $postData['details']['user_3']['reservation_type'] = $postData['reservation_type'];
                            $postData['details']['user_3']['adults'] = $postData['adults'];
                            $user3 = $postData['details']['user_3'];
                        }
                    }
                }

                if ($postData['duration'] == '48_hour'){
                    $date = date('Y-m-d H:i:s', strtotime('+48 hours'));
                    
                }else if($postData['duration'] == '1_week'){
                    $date = date('Y-m-d H:i:s', strtotime('+7 days'));
                    
                }elseif($postData['duration'] == '1_month'){
                    $date = date('Y-m-d H:i:s', strtotime('+1 month'));
                }
                
                $date1 = date('Y-m-d H:i:s');
                $timestamp1 = strtotime($date1);
                $timestamp2 = strtotime($date);
                $hour = (abs($timestamp2 - $timestamp1) / (60 * 60)) * (env('SOS_DURATION') ? env('SOS_DURATION') : 1);

                $inDate = date_create($postData['check_in_date']);
                $outDate = date_create($postData['check_out_date']);
                $dow = date("l", strtotime($postData['check_in_date']));

                $diff = date_diff($inDate, $outDate);
                $noOfDays = $diff->days;
                $currentTime = date("Y-m-d H:i:s");

                $location = '';
                if (isset($postData['location']) && isset($postData['location']) != '') {
                    $location = $postData['location'];
                } else {
                    $locationData = DB::select("select name from campgrounds_group where id = :id", ['id' => $campgroundFound->campground_group_id]);
                    $location = (count($locationData) > 0) ? $locationData[0]->name : '';
                }
                $data = [
                    'sold_out_search_id' => $postData['sold_out_search_id'],
                    'campground_id' => $postData['campground_id'],
                    'location' => $location,
                    'check_in_date' => $postData['check_in_date'],
                    'check_out_date' => $postData['check_out_date'],
                    'no_of_days' => $noOfDays,
                    'details' => null,
                    'result' => null,
                    'check_till' => $hour,
                    'next_schedule_date' => null,
                    'day_of_week' => $dow,
                    'elapsed_time' => null,
                    'positive_hit' => 0,
                    'status' => "Pending",
                    'enquiry_from' => $postData['enquiry_from'],
                    'duration' => $postData['duration'],
                    'booking_type' => $postData['booking_type'],
                ];

                if ($custom != null) {
                    $custom = json_encode($custom);
                    $data["user_type"] = "Custom";
                    $data['details'] = $custom;
                    $result = $this->checkAvailabilityNotificationRepository->store($data);
                }

                if ($user1 != null) {
                    $user1 = json_encode($user1);
                    $data["user_type"] = "User 1";
                    $data['details'] = $user1;
                    $result = $this->checkAvailabilityNotificationRepository->store($data);
                }

                if ($user2 != null) {
                    $user2 = json_encode($user2);
                    $data["user_type"] = "User 2";
                    $data['details'] = $user2;
                    $result = $this->checkAvailabilityNotificationRepository->store($data);
                }

                if ($user3 != null) {
                    $user3 = json_encode($user3);
                    $data["user_type"] = "User 3";
                    $data['details'] = $user3;
                    $result = $this->checkAvailabilityNotificationRepository->store($data);
                }
                if (isset($result['error']['message'])) {
                    return GeneralCls::setResponse('other_error', $result['error']['message']);
                } else {
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = $result;
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Campground Not Found.Invalid Campground ID.');
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    /**
     *  class for update Check Availability Notification
     *
     * @return \Illuminate\Http\Response
     */
    public function update($postData)
    {
        try {
            if ($postData['status'] == 'Expire') {

                $bookingData = $this->checkAvailabilityNotificationRepository->update($postData);
                if (isset($bookingData['error']['message'])) {
                    return GeneralCls::setResponse('other_error', $bookingData['error']['message']);
                } else {
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = $bookingData;
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse("other_error", "Wrong status value.");
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }
}
