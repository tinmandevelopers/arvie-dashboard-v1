<?php

namespace App\Classes\General;

use DateTime;
use DateTimeZone;
use Schema;
use DB;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GeneralCls {

    public static function stripRequest($data) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $data[$key] = self::stripRequest($value);
                } else {
                    $data[$key] = trim(strip_tags($value));
                }
            }
        }
        return $data;
    }

    public static function setResponse($type, $message = '') {
        switch (strtoupper($type)) {
            case 'SUCCESS':
                $code = 200;
                break;
            case 'NO_CONTENT':
                $code = 204;
                break;
            case 'VALIDATION_ERROR':
                $code = 422;
                break;
            case 'OTHER_ERROR':
                $code = 423;
                // $message = 'Something went wrong';
                break;
            case 'FORBIDDEN':
                $code = 403;
                break;
            case 'UNAUTHORIZED':
                $code = 401;
                break;
            case 'NOT_FOUND':
                $code = 404;
                break;
            case 'INACTIVE':
                $code = 409;
                break;
            default:
                break;
        }
        if ($code != 200) {
            $data['response'] = [];
        }
        $data['code'] = $code;
        if ($message != '') {
            $data['error'] = array('message' => $message);
        }

        return $data;
    }


    public static function removeOneLevel($objectData, $key) {
        $data1 = json_decode($objectData[$key], TRUE);
        unset($objectData[$key]);
        $data = array_merge($data1, $objectData);
        return $data;
    }


    public static function setWorkOrderResponse($type, $message = '', $orderData = array()) {

        switch (strtoupper($type)) {
            case 'SUCCESS':
                $code = 200;
                $status = 0;
                break;
            case 'NO_CONTENT':
                $code = 204;
                $status = 1;
                break;
            case 'VALIDATION_ERROR':
                $code = 422;
                $status = 1;
                break;
            case 'OTHER_ERROR':
                $code = 423;
                $status = 1;
                // $message = 'Something went wrong';
                break;
            case 'FORBIDDEN':
                $code = 403;
                $status = 1;
                break;
            case 'NOT_FOUND':
                $code = 404;
                $status = 1;
                break;
            case 'INACTIVE':
                $code = 409;
                $status = 1;
                break;
            default:
                break;
        }
        $data['transactionID'] = isset($orderData['transactionID']) ? $orderData['transactionID'] : '';
        $data['status'] = $status;
        $data['code'] = $code;
        if ($message != '') {
            $data['reason'] = $message;
        }
        return $data;
    }

    public static function getRandomNumber($len = "15") {
        $better_token = strtoupper(md5(uniqid(rand(), true)));
        $better_token_r = substr($better_token, 1, $len);
        return $better_token_r;
    }

    public static function changeDBDateFormat($date) {
        if ($date != '') {
            $dateFormat = config('settings.date_format');
            $dateObj = DateTime::createFromFormat($dateFormat, $date);
            $formated_date = $dateObj->format('Y-m-d');
        } else {
            $formated_date = "0000-00-00";
        }
        return $formated_date;
    }

    public static function findMinMaxDate($dateArr) {
        for ($i = 0; $i < count($dateArr); $i++) {
            if ($i == 0) {
                $max_date = date('Y-m-d', strtotime($dateArr[$i]));
                $min_date = date('Y-m-d', strtotime($dateArr[$i]));
            } else if ($i != 0) {
                $new_date = date('Y-m-d', strtotime($dateArr[$i]));
                if ($new_date > $max_date) {
                    $max_date = $new_date;
                } else if ($new_date < $min_date) {
                    $min_date = $new_date;
                }
            }
        }
        $data['min_date'] = $min_date;
        $data['max_date'] = $max_date;
        return $data;
    }

    public static function ago($datetime, $full = false) {
        //$now = new DateTime;
        $now = new DateTime("now", new DateTimeZone('Asia/Kolkata'));
        $ago = new DateTime($datetime);
        //print_r($ago);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function getUniqueLocation($location) {
        if ($location != "") {
            $locationArr = explode(",", $location);
            $filterArray = array_filter($locationArr);
            $uniqueArray = array_unique($filterArray);
            $result = array_map('trim', $uniqueArray);
            $return['location'] = implode(", ", $result);
            $return['count'] = count($uniqueArray);
        } else {
            $return['location'] = $location;
            $return['count'] = 0;
        }
        return $return;
    }

    public static function camelCase($apiResponseArray, $isSingleArr = false) {
        $finalArr = array();
        if (!$isSingleArr) {
            foreach ($apiResponseArray as $key => $val) {
                foreach ($val as $key1 => $testval) {
                    if (is_null($testval)) {
                        $apiResponseArray[$key][$key1] = "";
                    }
                }
            }
            foreach ($apiResponseArray as $key1 => $val1) {
                $keys = array_map(function ($i) {
                    $parts = explode('_', $i);
                    return array_shift($parts) . implode('', array_map('ucfirst', $parts));
                }, array_keys($val1));
                array_push($finalArr, array_combine($keys, $val1));
            }
        } else {
            $keys = array_map(function ($i) {
                $parts = explode('_', $i);
                return array_shift($parts) . implode('', array_map('ucfirst', $parts));
            }, array_values($apiResponseArray));
//            dd(array_combine($apiResponseArray,$keys));s
            $finalArr = $keys;
        }
        return $finalArr;
    }

    public static function underScoreCase($apiResponseArray, $isSingleArr = false) {
        $finalArr = array();
        if (!$isSingleArr) {

            foreach ($apiResponseArray as $key => $val) {
                foreach ($val as $key1 => $testval) {
                    if (is_null($testval)) {
                        $apiResponseArray[$key][$key1] = "";
                    }
                }
            }
            foreach ($apiResponseArray as $key1 => $val1) {
                $keys = strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", $string));
                array_push($finalArr, array_combine($keys, $val1));
            }
        } else {
            $keys = [];
            foreach ($apiResponseArray as $key1 => $val1) {
                $keys[strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", $key1))] = $val1;
            }
            $finalArr = $keys;
        }
        return $finalArr;
    }

    public static function replaceKeys($oldKey, $newKey, array $input) {
        $return = array();
        foreach ($input as $key => $value) {
            if ($key === $oldKey)
                $key = $newKey;

            if (is_array($value)) {
                $value = General::replaceKeys($oldKey, $newKey, $value);
            }

            $return[$key] = $value;
        }
        return $return;
    }

    public static function getEnumValues($table, $column) {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

    public static function getStyle() {
        $css = '<style type="text/css">.table {
                        width: 80%;
                        max-width: 100%;
                        margin-bottom: 20px;
                        margin-left: 50px;
                        }
                    .table > thead > tr > th,
                    .table > tbody > tr > th,
                    .table > tfoot > tr > th,
                    .table > thead > tr > td,
                    .table > tbody > tr > td,
                    .table > tfoot > tr > td {
                        padding: 8px;
                        line-height: 1.42857143;
                        vertical-align: top;
                        border-top: 1px solid #ddd;
                        text-align: center;
                    }
                    .table > thead > tr > th {
                        vertical-align: bottom;
                        border-bottom: 2px solid #ddd;
                    }
                    .table > caption + thead > tr:first-child > th,
                    .table > colgroup + thead > tr:first-child > th,
                    .table > thead:first-child > tr:first-child > th,
                    .table > caption + thead > tr:first-child > td,
                    .table > colgroup + thead > tr:first-child > td,
                    .table > thead:first-child > tr:first-child > td {
                        border-top: 0;
                    }
                    .table > tbody + tbody {
                        border-top: 2px solid #ddd;
                    }
                    .table .table {
                        background-color: #fff;
                    }
                    .table-bordered {
                        border: 1px solid #ddd;
                    }
                    .table-bordered > thead > tr > th,
                    .table-bordered > tbody > tr > th,
                    .table-bordered > tfoot > tr > th,
                    .table-bordered > thead > tr > td,
                    .table-bordered > tbody > tr > td,
                    .table-bordered > tfoot > tr > td {
                        border: 1px solid #ddd;
                    }
                    .table-bordered > thead > tr > th,
                    .table-bordered > thead > tr > td {
                        border-bottom-width: 2px;
                    }
</style>';
        return $css;
    }

    public static function getLatLongWiseAddress($lat, $lng) {
        $key = env('GMAP_KEY');
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false&key=' . $key;
        $json = @file_get_contents($url);
        $data = json_decode($json);
        if (isset($data->status)) {
            $status = $data->status;
            if ($status == "OK") {
                return $data->results[0]->formatted_address;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function kmFromTwoLatLong($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public static function checkFilepath($filepath) {
        if (env('STORAGE_TYPE') == 'S3') {
            if (!\Storage::disk('s3')->exists($filepath)) {
                return self::getDefaultImage();
            } else {
                $fileurl = \Storage::disk('s3')->url($filepath);

                return $fileurl;
            }
        } else {
            if (file_exists(self::public_path() . '/' . $filepath)) {
                return \URL::to("") . '/' . $filepath;
            } else {
                return self::getDefaultImage();
            }
        }
    }

    public static function removeFile($filepath) {
        if (env('STORAGE_TYPE') == 'S3') {

            if (\Storage::disk('s3')->exists($filepath)) {
                \Storage::disk('s3')->delete($filepath);
            }
        } else {
            if (file_exists($file_path)) {
                unlink($filepath);
            }
        }
        return true;
    }

    public static function getImage($image, $type, $id = 0) {
        $defaultImagePath = self::getDefaultImage();
        switch ($type) {
            case 'user':
                $checkImagePath = config('uploads.user.profile_image');
                break;
            case 'staff_member':
                $checkImagePath = config('uploads.staff_member');
                break;
            case 'common':
                $checkImagePath = config('uploads.common');
                break;
            case 'logo':
                $checkImagePath = config('uploads.logo');
                break;
            case 'form':
                $checkImagePath = config('uploads.form');
                break;
            default:
                return self::getDefaultImage();
                break;
        }
        if (!is_null($image) && !empty($image)) {
            $filepath = $checkImagePath . $image;

            return self::checkFilepath($filepath);
        } else {
            return $defaultImagePath;
        }
    }

    public static function getWorkOrderImage($image,$path,$agent_id,$order_id){
        $searchVal = array("{id}", "{agentid}");
        $replaceVal = array($order_id, $agent_id);
        $path = str_replace($searchVal, $replaceVal, $path);
        $filepath = $path . $image;
        return self::checkFilepath($filepath);
    }

    public static function removeImage($image, $type, $id = 0) {
        switch ($type) {
            case 'user':
                $checkImagePath = config('uploads.user.profile_image');
                break;
            case 'company_logo':
                $checkImagePath = config('uploads.user.company_logo');
                break;
            case 'common':
                $checkImagePath = config('uploads.common');
                break;
            case 'kyc_file':
                $checkImagePath = config('uploads.user.zip_files');
                break;
            case 'product':
                $checkImagePath = config('uploads.product');
                break;
            case 'tag':
                $checkImagePath = config('uploads.tag');
                break;
            case 'care_instructions':
                $checkImagePath = config('uploads.care_instructions');
                break;
        }
        if (!is_null($image) && !empty($image)) {
            $filepath = $checkImagePath . $image;
            return self::removeFile($filepath);
        }
    }

    public static function getDefaultImage() {
        $defaultImagepath = config('enum.defaultImage');
        return \URL::to("") . $defaultImagepath;
    }

    public static function public_path($path = null) {
        return rtrim(app()->basePath('public/' . $path), '/');
    }

    public static function characterReplace($array,$columns) {
        foreach ($array as $key => $value){
            if(in_array($key,$columns)){
                $array[$key]= preg_replace('/[!@#$%&*”:’;]/', ' ', $value); // Removes special chars.
            }
        }

        return $array;

    }
}