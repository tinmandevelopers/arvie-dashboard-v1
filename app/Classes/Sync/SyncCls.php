<?php

namespace App\Classes\Sync;

class SyncCls {

    protected $user;
    protected $web_url;

    public function __construct() {
        $this->web_url = env('WEB_URL');
    }

    public function pushCampground($postData) {
        try {
            $url = $this->web_url . '/add_new_room';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            return json_decode($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /*
     * SOS - Sold Out Search 
     */
    public static function updateStatusSOS($postData) {
        try {
            $url = env('WEB_URL') . '/update_sold_out_status';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            return json_decode($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /*
     * Booking Status
     */
    public static function updateBookingStatus($postData) {
        try {
            $url = env('WEB_URL') . '/update_booking_status';

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postData
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            return json_decode($response);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
