<?php

namespace App\Classes\User;

use App\Classes\General\GeneralCls;
use App\Classes\Validate;
use App\Repositories\Backend\Auth\UserRepository;
use http\Env\Request;

class UserCls {

    protected $accessTokenController;
    protected $client;
    protected $user;
    protected $validate;
    protected $feedback;

    public function __construct(UserRepository $user) {

        $this->user = $user;
        $this->validate = new Validate();
    }

    public function login($postData) {
        
        try {
            $userData = $this->user->login($postData);

            if (isset($userData['error']['message'])) {
                return GeneralCls::setResponse('other_error', $userData['error']['message']);
            } else {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $userData;
                return $data;
            }
        } catch (\Exception $e) {
            return GeneralCls::setResponse('other_error', $e->getMessage());
        }
    }

    // public function logout($request) {
    //     try {
    //         $userData = $this->user->logout($request);
    //         if (isset($userData['error'])) {
    //             return General::setResponse('other_error', $userData['error']);
    //         } else {
    //             $data = General::setResponse('success', $userData['message']);
    //             $data['response'] = $userData;
    //             return $data;
    //         }
    //     } catch (\Exception $e) {
    //         return General::setResponse('other_error', $e->getMessage());
    //     }
    // }

    // public function forgotPassword($postData) {
    //     $v = $this->validate->required($postData, array('email'));
    //     $v1 = $this->validate->email($postData, array('email'));


    //     if ($v->fails()) {
    //         return General::setResponse('validation_error', $v->errors()->first());
    //     }

    //     if ($v1->fails()) {
    //         return General::setResponse('validation_error', $v1->errors()->first());
    //     }

    //     try {
    //         $userData = $this->user->forgotPassword($postData);

    //         if (isset($userData['error'])) {
    //             return General::setResponse('other_error', $userData['error']);
    //         } else if (isset($userData['message'])) {
    //             $data = General::setResponse('success', $userData['message']);
    //             $data['response'] = $userData;
    //             return $data;
    //         } else {
    //             return $userData;
    //         }
    //     } catch (\Exception $e) {
    //         return General::setResponse('other_error', $e->getMessage());
    //     }
    // }

    // public function changePassword($postData) {
    //     $v = $this->validate->required($postData, array('oldPassword', 'newPassword'));
    //     if ($v->fails()) {
    //         return General::setResponse('validation_error', $v->errors()->first());
    //     }

    //     try {
    //         $userData = $this->user->changePassword($postData);
    //         if (isset($userData['error'])) {
    //             return General::setResponse('other_error', $userData['error']);
    //         } else {
    //             $data = General::setResponse('success');
    //             $data['response'] = $userData;
    //             return $data;
    //         }
    //     } catch (\Exception $e) {

    //         return General::setResponse('other_error', $e->getMessage());
    //     }
    // }

    // public function checkPassword($postData) {
    //     $v = $this->validate->required($postData, array('password'));
    //     if ($v->fails()) {
    //         return General::setResponse('validation_error', $v->errors()->first());
    //     }

    //     try {
    //         $userData = $this->user->checkPassword($postData);
    //         if (isset($userData['error'])) {
    //             return General::setResponse('other_error', $userData['error']);
    //         } else {
    //             $data = General::setResponse('success');
    //             $data['response'] = $userData;
    //             return $data;
    //         }
    //     } catch (\Exception $e) {

    //         return General::setResponse('other_error', $e->getMessage());
    //     }
    // }

    // public function resetPassword($postData) {
    //     $v = $this->validate->required($postData, array('newPassword', 'confirmPassword', 'token'));
    //     if ($v->fails()) {
    //         return General::setResponse('validation_error', $v->errors()->first());
    //     }

    //     $v1 = $this->validate->same($postData, 'newPassword', 'confirmPassword');
    //     if ($v1->fails()) {
    //         return General::setResponse('validation_error', $v1->errors()->first());
    //     }
    //     try {
    //         $userData = $this->user->resetPassword($postData);
    //         if (isset($userData['error'])) {
    //             if ($userData['error'] == 'already_reset') {
    //                 $userData['error'] = trans('auth.password.already_reset');
    //             } elseif ($userData['error'] == 'expire') {
    //                 $userData['error'] = trans('auth.password.token_expired');
    //             } else {
    //                 $userData['error'] = trans('auth.password.invalid_token');
    //             }
    //             return General::setResponse('other_error', $userData['error']);
    //         } else if (isset($userData['message'])) {
    //             $data = General::setResponse('success');
    //             $data['response'] = $userData;
    //             return $data;
    //         }
    //     } catch (\Exception $e) {
    //         return General::setResponse('other_error', $e->getMessage());
    //     }
    // }

    // public function checkPasswordToken($token) {

    //     $user = $this->user->findByPasswordResetTokenAPI($token);


    //     if ($user == 'already_reset') {
    //         $data = General::setResponse('unauthorized');
    //         $data['response'] = [
    //             'message' => trans('auth.password.already_reset'),
    //         ];
    //     } else if ($user == 'expire') {
    //         $data = General::setResponse('unauthorized');
    //         $data['response'] = [
    //             'message' => trans('auth.password.token_expired'),
    //         ];
    //     } else if ($user) {
    //         $this->user->activateUserAccount($user);
    //         $data = General::setResponse('success');
    //         $data['response'] = [
    //             'token' => $token,
    //         ];
    //     } else {
    //         $data = General::setResponse('unauthorized');
    //         $data['response'] = [
    //             'message' => trans('validation.invalid_token'),
    //         ];
    //     }
    //     return $data;
    // }
}
