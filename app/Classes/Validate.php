<?php

namespace App\Classes;

use Validator;
use App\Classes\General\General;

class Validate
{

    public function required($data, $field = array())
    {
        $messages = ['required' => trans('validation.required')];
        $fieldArr = array_fill_keys($field, 'required');
        $validator = Validator::make($data, $fieldArr, $messages);
        return $validator;
    }

    public function email($data, $field = array())
    {
        $messages = ['email' => trans('validation.invalid_format')];
        $fieldArr = array_fill_keys($field, 'email');
        $validator = Validator::make($data, $fieldArr, $messages);
        return $validator;
    }

    public function different($data, $field1, $field2)
    {
        $rules = array(
            $field2 => 'different:' . $field1,
        );

        $messages = ['different' => trans('validation.different')];
        $validator = Validator::make($data, $rules, $messages);
        return $validator;
    }

    public function same($data, $field1, $field2)
    {
        $rules = array(
            $field2 => 'same:' . $field1,
        );

        $messages = ['same' => trans('validation.same')];
        $validator = Validator::make($data, $rules, $messages);
        return $validator;
    }

    public function mobile($data, $field = array())
    {
        $messages = ['digits' => trans('validation.mobile')];
        $fieldArr = array_fill_keys($field, 'digits:9');
        $validator = Validator::make($data, $fieldArr, $messages);
        return $validator;
    }

    public function numeric($data, $field = array())
    {
        $messages = ['numeric' => trans('validation.numeric')];
        $fieldArr = array_fill_keys($field, 'numeric');
        $validator = Validator::make($data, $fieldArr, $messages);
        return $validator;
    }

    public function image($data)
    {
        $messages = ['image' => trans('validation.image')];
        $validator = Validator::make($data, [
            'companyLogo' => 'image|mimes:jpeg,png,jpg,svg',
        ],$messages);
        return $validator;
    }

    public function zip($data,$field=array())
    {
        $messages = ['mimes' => trans('validation.mimes')];
        $fieldArr = array_fill_keys($field, 'mimes:zip');
        $validator = Validator::make($data, $fieldArr,$messages);
        return $validator;
    }

}
