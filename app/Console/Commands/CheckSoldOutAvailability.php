<?php

namespace App\Console\Commands;

use App\Models\CheckAvailability;
use App\Models\CheckAvailabilityNotification;
use App\Models\SoldOutSearch;
use DB;
use Exception;
use Illuminate\Console\Command;
use Log;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use PDO;
use App\Classes\Sync\SyncCls;

class CheckSoldOutAvailability extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:sold_out_search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command check availabilty of Sold out serach on every 15 minutes and generate the logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //
        try {

            $currentTimeMinutesVal = date("Y-m-d H:i");
            logger('currentTimeMinutesVal:' . $currentTimeMinutesVal);
            $checkAvailabilty = CheckAvailabilityNotification::whereIn('status', ['Pending', 'InProgress'])
                            ->where(function ($query) {
                                $currentTimeMinutesVal = date("Y-m-d H:i");
                                $query->where('next_schedule_date', '=', null)
                                ->orWhere(DB::raw('DATE_FORMAT(next_schedule_date, "%Y-%m-%d %H:%i")'), '<=', $currentTimeMinutesVal);
                            })
                            ->orderBy('id', 'desc')->get();
//             print_r($checkAvailabilty->toArray());
//             print_r($currentTimeMinutesVal);
//             exit;
            foreach ($checkAvailabilty as $ca) {
                //$sos_id = '';
                $sos_camp_site = '';
                $sos_available = 'Not Available';
                $sos_create_booking = 'No';
                logger('================================');
                logger('CaId >> ' . $ca->id);
                $currentTimeMinutes = date("Y-m-d H:i");
                $currentTime = date("Y-m-d H:i:s");

//                $next15minTime = date('Y-m-d H:i:s', strtotime('+15 minute', strtotime($currentTime)));
                $next15minTime = date('Y-m-d H:i:s', strtotime('+' . (env('SOS_CRON_TIME')), strtotime($currentTime)));
                $checkAvalNoti = CheckAvailabilityNotification::find($ca->id);
                $sos_id = $checkAvalNoti->sold_out_search_id;

                $availabilityResult = $this->checkAvailablity($ca);
                $data = json_decode($availabilityResult);
                $result = json_encode($data->response);

                if (isset($data->response->availability) && $data->response->availability) {
                    $checkAvalNoti->positive_hit = $ca->positive_hit + 1;
                    $status = "Success";
                    logger('Ca Status >> ' . $ca->status . ' - process - Success');
                    if (is_null($checkAvalNoti->elapsed_time)) {
                        $time = strtotime(date($ca->created_at));
                        $curTime = time();
                        $ht = $this->humanTiming($curTime, $time);
                        $checkAvalNoti->elapsed_date = date('Y-m-d H:i:s', $curTime);
                        $checkAvalNoti->elapsed_time = $ht;
                        $sos_create_booking = 'Yes';
                    }
                    $sos_camp_site = (isset($data->response->details[0]) && isset($data->response->details[0]->title) && $data->response->details[0]->title != '') ? $data->response->details[0]->title : '';
                    $sos_available = 'Available';
                } else {
                    logger('Ca Status >> ' . $ca->status . ' - process - Failed');
                    $status = "Failed";
                    $sos_available = 'Not Available';
                }

                if ($ca->status == "Pending") {
                    $checkAvalNoti->first_search_availability = ($status == 'Success') ? 'Yes' : 'No';
                }

                $checkAvalNoti->result = $result;
                $checkAvalNoti->next_schedule_date = $next15minTime;

                $expiryDate = $duration = '';
                switch ($checkAvalNoti->duration) {
                    case '48_hour':
                        $duration = ' +48 hours';
                        break;
                    case '1_week':
                        $duration = ' +7 days';
                        break;
                    case '1_month':
                        $duration = ' +1 month';
                        break;
                }

                $expiryDate = date('Y-m-d H:i', strtotime($checkAvalNoti->created_at . $duration));
                $checkInDate = date('Y-m-d H:i', strtotime($checkAvalNoti->check_in_date));
                if ($expiryDate <= $currentTimeMinutesVal || $checkInDate <= $currentTimeMinutesVal) {
                    $checkAvalNoti->status = "Completed";
                    $sos_status = "Completed";
                } else {
                    $checkAvalNoti->status = "InProgress";
                    $sos_status = "InProgress";
                }
                $checkAvalNoti->save();

                $soldOutLog = new SoldOutSearch();
                $soldOutLog->check_availability_notification_id = $ca->id;
                $soldOutLog->status = $status;
                $soldOutLog->result = $result;
                $soldOutLog->log_time = $currentTime;
                $soldOutLog->save();

                if ($sos_id != '') {
                    $sosParams['sold_out_search_id'] = $sos_id;
                    $sosParams['available'] = $sos_available;
                    $sosParams['create_booking'] = $sos_create_booking;
                    $sosParams['camp_site'] = $sos_camp_site;
                    $sosParams['status'] = $sos_status;
                    $this->updateSOSStatus($sosParams);
                }
            }
        } catch (Exception $e) {
            logger($e->getMessage() . ",File:" . $e->getFile() . ",Line:" . $e->getLine());
        }
    }

    public function checkAvailablity($params) {
        $curl = curl_init();
        $postField = [
            'campground_id' => $params->campground_id,
            'check_in_date' => $params->check_in_date,
            'check_out_date' => $params->check_out_date,
            'details' => $params->details,
            'enquiry_from' => 'Soldout',
        ];
        curl_setopt_array($curl, array(
            CURLOPT_URL => route('check.availability'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postField,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "content-type: multipart/form-data;",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            logger('error occurs');
            logger($err);
            // print_r($err);
            return null;
        } else {
            // json_decode($dataVal);
            // $data= json_decode($response);
            // $a = $data->response;
            // print_r($a);
            logger('Success Response');
            logger($response);
            return $response;
        }
    }

    public function humanTiming($curTime, $time) {

        $time = $curTime - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
        }
    }

    public function updateSOSStatus($postData) {
        logger('Call Site API >> ' . json_encode($postData));
        $response = SyncCls::updateStatusSOS($postData);
        logger('Site API Response >> ' . json_encode($response));
        return $response;
    }

}
