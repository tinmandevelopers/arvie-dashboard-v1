<?php

namespace App\Console\Commands;

use Exception;
use App\Models\Campground;
use App\Models\BookingEnquiry;
use App\Models\Settings;
use Dacastro4\LaravelGmail\Services\Message\Mail;
use LaravelGmail;
use Illuminate\Console\Command;

class GmailUserConfirmationMailSend extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gmail:user_confirmation_mail_send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command send confirmation mail to user on successful booking on every minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $tokenDetail = $this->getAuthDetail();
            $request = \Request::capture();
            $request->code = $tokenDetail->code;
            $tokenData = LaravelGmail::makeToken($request);
            if (isset($tokenDetail) && ($tokenData['access_token'] != $tokenDetail->access_token)) {
                $this->saveAuthDetail($tokenData);
                $token = $tokenData['access_token'];
            } else {
                $token = $tokenData['access_token'];
            }
            if (isset($tokenDetail) && isset($token)) {
                $messages = LaravelGmail::message()
                        ->from(trim($tokenDetail->email))
                        ->subject('Confirmation Letter Email')
                        ->preload()
                        ->after($tokenDetail->last_mail_access_at)
                        ->all();
                
                foreach ($messages as $key => $message) {
                    $campGroundId = 0;
                    $body = $message->getHtmlBody();
                    $subject = $message->getSubject();

                    if ($key == 0) {
                        $this->saveLastMailAccess($message->getDate()->format('Y-m-d'));
                    }

                    //get email title
                    preg_match_all('/Your Reservation Number: <span .*?>(.*?)<\/span>/', $body, $matches);
                    $emailTitle = $matches[0][0];

                    if (str_contains($emailTitle, 'Cancelled')) {
                        continue;
                    } else {
                        //get campground name
                        preg_match_all('/<a .*?>(.*?)<\/a>/', $body, $matches);
                        $campGroundName = strip_tags($matches[0][0]);

                        //get Arrival Date
                        preg_match_all('/<b>Arrival Date:<\/b> <span .*?>(.*?)<\/span>/', $body, $matches);
                        $arrivalDate = $matches[1][0];
                        $arrivalDate = date('Y-m-d', strtotime($arrivalDate));

                        //get Customer Name
                        preg_match_all('/<b>Name: <\/b>([\w\W\s]*)<br>/', $body, $matches);
                        $customerName = explode("<br>", trim($matches[1][0]));
                        $customerName = trim($customerName[0]);

                        if ($campGroundName != "") {
                            $campGroundId = $this->getCampGroundId($campGroundName);
                        }

                        if ($campGroundId > 0 && $customerName != "" && $arrivalDate != "") {
                            $bookingEnquiry = $this->checkBookingDetails($campGroundId, $customerName, $arrivalDate);
                            if (isset($bookingEnquiry) && isset($bookingEnquiry->email) && $bookingEnquiry->email != '') {
                                if ($this->sendMail($tokenDetail->email, $token, $bookingEnquiry->email, $subject, $body)) {
                                    $this->updateBookingDetails($bookingEnquiry->id);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            dd($e->getMessage() . ",File:" . $e->getFile() . ",Line:" . $e->getLine());
            logger($e->getMessage() . ",File:" . $e->getFile() . ",Line:" . $e->getLine());
        }
    }

    public function getAuthDetail() {
        return Settings::first();
    }

    public function saveAuthDetail($authData) {
        return $this->saveSettings(['access_token' => $authData['access_token'], 'refresh_token' => $authData['refresh_token']]);
    }

    public function saveLastMailAccess($lastMailAccessDate) {
        return $this->saveSettings(['last_mail_access_at' => $lastMailAccessDate]);
    }

    public function saveSettings($data, $id = 1) {
        return Settings::where('id', $id)->update($data);
    }

    public function getCampGroundId($campGroundName) {
        $campGroundId = 0;
        if ($campGroundName != '') {
            $data = Campground::where('title', trim($campGroundName))->where('web_status', 'Listed')->first();
            if ($data) {
                $campGroundId = $data->id;
            }
        }
        return $campGroundId;
    }

    public function checkBookingDetails($campGroundId, $customerName, $arrivalDate) {
        return BookingEnquiry::where('campground_id', $campGroundId)->where('name', trim($customerName))->where('check_in_date', $arrivalDate)->where('index_status', "Completed")->where('confirmation_mail_sent', 0)->first();
    }

    public function updateBookingDetails($id) {
        return BookingEnquiry::where('id', $id)->update(['confirmation_mail_sent' => '1']);
    }

    public function sendMail($fromEmailId, $token, $toEmailId, $subject, $messageBody) {
        $mail = new Mail;
        $mail->using($token);
        $mail->to($toEmailId);
        $mail->from($fromEmailId);
        $mail->subject($subject);
        $mail->message($messageBody);

        if ($mail->send()) {
            return true;
        } else {
            return false;
        }
    }

}
