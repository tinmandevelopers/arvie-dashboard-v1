<?php

namespace App\Events\Backend\Auth\ProfileRequest;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated.
 */
class ProfileRequestCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $profileRequest;

    /**
     * @param $profileRequest
     */
    public function __construct($profileRequest)
    {
        $this->profileRequest = $profileRequest;
    }
}
