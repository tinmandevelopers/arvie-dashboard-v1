<?php

namespace App\Exports;

use App\Models\CheckAvailabilityNotification;
use App\Models\SoldOutSearch;
use App\Post;
use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class SoldDataExport implements FromCollection, WithHeadings {

    private $headers = [
        'id' => 'Id',
        'campground_id' => 'Campground',
        'location' => 'State',
        'source' => 'Data Source',
        'message' => 'Clean Inquiry',
        'first_search_availability' => 'Available Inventory on first attempt',
        'first_search_no_of_sites_available' => 'Number of Sites available when 1st attempt had availability',
        'day_of_week' => 'Check-in Day of Week',
        'no_of_days' => 'Number of nights requested',
        'check_in_date' => 'Check-in Date',
        'elapsed_time' => 'If SOS was inititated, Elapsed time until 1st bookable cancellation shows up',
        'first_success_SOS_no_of_sites_available' => 'Number of sites available on first SOS success',
        'length_of_time' => 'Length of time in hours and minutes from 1st SOS availabity hit until requested check in date',
        'available_length_of_time' => 'Length of time SOS options stayed available after first appearing',
//        'status' => 'Status',
//        'user_type' => 'User Type',
//        'elapsed_date' => 'Time Stamp of 1st availibility',
//        'no_of_status_changes' => 'Number of Status Changes',
//        'elapsed_time_till_status_change' => 'Elapse time till status change',
//        'search_interval' => 'Search Interval',
//        'positive_hit' => 'Positive Hit',
//        'created_at' => 'Requested At',
    ];

    public function collection() {
        $result = CheckAvailabilityNotification::with(['Campground'])->select('id', 'campground_id', 'location', DB::raw('"" as source'), DB::raw('"" as message'), 'first_search_availability', DB::raw('"0" as first_search_no_of_sites_available'), 'day_of_week', 'no_of_days', 'check_in_date', 'elapsed_time', DB::raw('"0" as first_success_SOS_no_of_sites_available'), DB::raw('if(elapsed_date is not null, SEC_TO_TIME(TIMESTAMPDIFF(SECOND,elapsed_date,check_in_date)), "") as length_of_time'), DB::raw('"0" as available_length_of_time'), 'result')->orderByDesc('id')->get();
        foreach ($result as $rkey => $r) {
            $r->campground_id = $r->campground->title;
            $r->source = $r->campground->getDataSource->title;
            $length_of_time = !empty($r->length_of_time) ? explode(':', $r->length_of_time) : [];
            if(count($length_of_time) == 3){
                $r->length_of_time = ($length_of_time[0] > 0) ? $length_of_time[0].' Hours '.$length_of_time[1]. ' Minutes' : $length_of_time[1]. ' Minutes' ;
            }
            
            $firstLog = $r->sosLog->first();
            if ($firstLog != '' && $firstLog != 'null' && $firstLog->result != 'null') {
                $responseData = (array) json_decode($firstLog->result);
                $r->first_search_no_of_sites_available = ($responseData['availability'] && isset($responseData['details'])) ? count($responseData['details']) : 0;
            }
            
            $firstAvailable = $r->firstAvailableSOSResult->first();
            if ($firstAvailable != '' && $firstAvailable != 'null' && isset($firstAvailable->result) && $firstAvailable->result != 'null') {
                $availableData = (array) json_decode($firstAvailable->result);
                $r->first_success_SOS_no_of_sites_available = ($availableData['availability'] && isset($availableData['details'])) ? count($availableData['details']) : 0;
                $notAvailableAfterfirstAvailable = $r->notAvailableAfterfirstAvailable($firstAvailable->id)->first();
                $r->available_length_of_time = str_replace('after','',$notAvailableAfterfirstAvailable->created_at->diffForHumans($firstAvailable->created_at));
            }
            
            if ($r->result != '' && $r->result != 'null') {
                $resultData = json_decode($r->result, true);
//                $r->status = ($resultData['availability']) ? 'Success' : 'Failed';
                $r->message = strstr($resultData['message'],'Error') ? 'No' : 'Yes';
//                $r->no_of_sites_available = ($resultData['availability']) ? count($resultData['details']) : 0;
                unset($r->result);
            }
        }
        return $result;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    // use Exportable;


    public function headings(): array {

        return $this->headers;
    }

}
