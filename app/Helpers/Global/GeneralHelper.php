<?php

use App\Models\Auth\User;
use App\Models\Campground;

if (!function_exists('app_name')) {

    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('gravatar')) {

    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (!function_exists('home_route')) {

    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {

            if (auth()->user()->can('view backend')) {
                $token = md5(rand(1, 10) . microtime());
                $user = User::find(auth()->user()->id);
                $user->token = $token;
                $user->save();
                return 'admin.dashboard';
            }

            return abort(403);
        }

        return 'frontend.index';
    }
}

if (!function_exists('get_user_picture')) {

    /**
     * Return the profile picture
     *
     * @return string
     */
    function get_user_picture($user, $size = false)
    {
        if ($user->avatar_location != '' && file_exists(public_path() . '/storage/' . $user->avatar_location)) {
            return url('storage/' . $user->avatar_location);
        } else {
            return gravatar()->get($user->email, ['size' => $size]);
        }
    }
}

if (!function_exists('campground_detail_from_json')) {

    /**
     * Return the campground detail
     *
     * @return string
     */
    function campground_detail_from_json($json_file_path, $return_html = false)
    {
        $json_data = @file_get_contents($json_file_path);
        if ($json_data) {
            if ($return_html) {
                $json_data = json_decode($json_data, true);
                return prepare_html($json_data);
            } else {
                return $json_data;
            }
        } else {
            return 'No detail found.';
        }
    }

    function prepare_html($data, $type = '', $li = false, $parent_key = '', &$level = 0, &$keyLevel = [])
    {
        $html = '';
        $ul_li_arr = ['amenities', 'unique_amenities', 'accomodation_types'];
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ((!is_array($value) && $value != '') || (is_array($value) && count($value) > 0)) {
                    if (!is_integer($key) && !in_array($parent_key, $keyLevel)) {
                        array_push($keyLevel, $key);
                        $parent_key = $key;
                        $level++;
                    } else {
                        $html .= "<hr>";
                    }
                    if (!is_integer($key)) {
                        $class = ($type == 'booking') ? "class='col-md-4 font-bold'" : "";
                        $html .= "<span $class><b>" . ucwords(str_replace('_', ' ', $key)) . ": </b></span>";
                    }
                    if (is_array($value)) {
                        if (!is_integer($key) && in_array($key, $ul_li_arr)) {
                            $html .= "<ul>";
                            $html .= prepare_html($value, $type, true, $parent_key);
                            $html .= "</ul>";
                        } else {
                            $html .= "<div style='margin-left:" . ($level * 20) . "px'>";
                            if (!$level)
                                $html .= "<div class='col-md-12 shadow p-20'>";
                            $html .= prepare_html($value, $type, false, $parent_key);
                            if (!$level)
                                $html .= "</div>";
                            $html .= "</div>";
                        }
                    } else {
                        if ($li) {
                            $html .= "<li>$value</li>";
                        } elseif (str_starts_with($value, "http") && (str_contains($value, '.jpg') || str_contains($value, '.jpeg') || str_contains($value, '.png') || str_contains($value, '.gif') || str_contains($value, '.axd'))) {
                            $html .= "<img src='$value' style='max-width:300px;'/>";
                        } elseif (str_starts_with($value, "http")) {
                            $html .= "<a href='$value' target='_blank'>$value</a>";
                        } else {
                            $html .= "<span>$value</span>";
                        }
                    }
                }
            }
        } else {
            $html = 'No detail found.';
        }
        return $html;
    }
}

if (!function_exists('set_booking_enquiry_data')) {

    /**
     * Helper to set Bookign Enquiry data .
     *
     * @return mixed
     */
    function set_booking_enquiry_data($details, $campgroundUrl)
    {
        $title = get_datasource_title(parse_url($campgroundUrl, PHP_URL_HOST));
        $data = json_decode($details);
        $path = storage_path() . "/booking/bookingFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $bookingParams = json_decode(file_get_contents($path), true);

        $data->reservation_type = isset($bookingParams[$title]['reservation_type'][strtolower($data->reservation_type)]) ? $bookingParams[$title]['reservation_type'][strtolower($data->reservation_type)] : '';
        $data->equipment_type = isset($bookingParams[$title]['equipment_type'][$data->equipment_type]) ? $bookingParams[$title]['equipment_type'][$data->equipment_type] : '';

        $data->length = (strtolower($data->length) == "no") ?  '0' : $data->length;

        if (strtolower($data->towing_vehicle) == 'yes') {
            if (strtolower($data->towing_length) == 'no') {
                $data->towing_length = "0";
            } else {
                $data->towing_length = $data->towing_length;
            }
        }

        // if (isset($data->card_detail->card_type)) {
        //     if (isset($data->card_detail->card_type)) {
        //         $str = $data->card_detail->card_type;
        //         $str = base64_decode(base64_decode(base64_decode($str)));
        //         $str = strtoupper($str);
        //         $str = base64_encode(base64_encode(base64_encode($str)));
        //         $data->card_detail->card_type = $str;
        //     }
        // }

        // if (isset($data->card_detail->card_exp_month)) {
        //     if (isset($data->card_detail->card_exp_month)) {
        //         $str = $data->card_detail->card_exp_month;
        //         $str = base64_decode(base64_decode(base64_decode($str)));
        //         if(strlen($str)==1){
        //             $str="0".$str;
        //         }
        //         $str = base64_encode(base64_encode(base64_encode($str)));
        //         $data->card_detail->card_exp_month = $str;
        //     }
        // }

        if ($title == 'koa') {
            $data->pet_friendly = (strtolower($data->pet_friendly) == 'no') ? "1" : "2";
            $data->slide_out = (strtolower($data->slide_out) == 'no') ? "1" : "2";
        } else if ($title == 'reserveamerica') {
            // if (isset($data->card_detail->card_type)) {
            //     $str = $data->card_detail->card_type;
            //     $str = base64_decode(base64_decode(base64_decode($str)));
            //     if ($str == 'MC') {
            //         $str = base64_encode(base64_encode(base64_encode('MAST')));
            //         $data->card_detail->card_type = $str;
            //     }
            // }
        } else if ($title == 'sun') {
            $data->pet_friendly = (strtolower($data->pet_friendly) == 'no') ? "0" : "1";
            switch ($data->rear) {
                case 'Driver':
                    $data->slide_out = "2";
                    break;
                case 'Passenger':
                    $data->slide_out = "3";
                    break;
                case 'Both':
                    $data->slide_out = "4";
                    break;
                default:
                    $data->slide_out = "1";
                    break;
            }
        } else {
            $data->slide_out = isset($bookingParams[$title]['rear'][$data->slide_out]) ? $bookingParams[$title]['rear'][$data->slide_out] : $data->slide_out;
        }


        return $data;
    }
}

if (!function_exists('verify_availablity_data')) {


    function verify_availablity_data($data, $from = '')
    {

        $response = [];
        if (isset($data['details']) && $data['details']) {

            $details = json_decode($data['details']);

            $campgroundFound = Campground::find($data['campground_id']);
            $title = get_datasource_title($campgroundFound->url);

            $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
            $params = json_decode(file_get_contents($path), true);
            $message = '';
            $error = false;
            if (isset($details->equipment_type) && $details->equipment_type) {
                $equipementData = isset($params[$title . '_equipement']) ? $params[$title . '_equipement'] : [];
                if (!array_key_exists($details->equipment_type, $equipementData)) {
                    $message .= " Campground is not supporting this equipment type";
                    $error = true;
                }
            } else {
                $message .= " Please provide equipement type ";
                $error = true;
            }

            if (isset($details->reservation_type) && $details->reservation_type) {
                $reservationData = isset($params['reservation_type']) ? $params['reservation_type'] : [];
                if (!array_key_exists(strtolower($details->reservation_type), $reservationData)) {
                    $message .= " Reservation type is invalid ";
                    $error = true;
                }
            } else {
                $message .= " Please provide reservation type ";
                $error = true;
            }

            if ($from != 'checkavailablity') {
                if (isset($details->card_detail) && $details->card_detail) {

                    if (!isset($details->card_detail->token) || !isset($details->card_detail->user) || !isset($details->card_detail->passphrase) || !isset($details->card_detail->reference)) {
                        $message .= "Please a update your payment detail";
                        $error = true;
                    }
//
//                    if (!isset($details->card_detail->user)) {
//                        $message .= " Please a provide card detail user ";
//                        $error = true;
//                    }
//
//                    if (!isset($details->card_detail->passphrase)) {
//                        $message .= " Please a provide card detail passphrase ";
//                        $error = true;
//                    }
//
//                    if (!isset($details->card_detail->reference)) {
//                        $message .= " Please a provide card detail reference ";
//                        $error = true;
//                    }

                    // if (isset($details->card_detail->card_type) && $details->card_detail->card_type) {
                    //     $cardData = isset($params['card_type']) ? $params['card_type'] : [];
                    //     $str = $details->card_detail->card_type;
                    //     $str = base64_decode(base64_decode(base64_decode($str)));
                    //     if (!array_key_exists(strtoupper($str), $cardData)) {
                    //         $message .= " Please a provide valid card type ";
                    //         $error = true;
                    //     }
                    // } else {
                    //     $message .= " Please a provide card type ";
                    //     $error = true;
                    // }

                    // if (!isset($details->card_detail->card_exp_month)) {
                    //     $message .= " Please a provide card expiration month ";
                    //     $error = true;
                    // }

                    // if (!isset($details->card_detail->card_exp_year)) {
                    //     $message .= " Please a provide card expiration year ";
                    //     $error = true;
                    // }

                    // if (!isset($details->card_detail->card_user_name)) {
                    //     $message .= " Please a provide card user name ";
                    //     $error = true;
                    // }

                    // if (!isset($details->card_detail->card_cvv)) {
                    //     $message .= " Please a provide card cvv ";
                    //     $error = true;
                    // }
                } else {
                    $message .= " Please provide card details ";
                    $error = true;
                }
            }
            if ($error) {
                $response['error'] = $error;
                $response['message'] = $message;
            } else {
                $response['error'] = $error;
                $response['message'] = $message;
            }
            return $response;
        } else {
            $response['error'] = true;
            $response['message'] = 'Please Provide details';
            return $response;
        }
    }
}

if (!function_exists('get_datasource_title')) {

    /**
     * Helper to set Datasource title for booking.
     *
     * @return mixed
     */
    function get_datasource_title($stringData)
    {
        $title = '';
        if (strpos($stringData, 'koa') !== false) {
            $title = 'koa';
        } else if (strpos($stringData, 'sunrvresorts') !== false) {
            $title = 'sun';
        } else if (strpos($stringData, 'recreation') !== false) {
            $title = 'recreation';
        } else if (strpos($stringData, 'bookyoursite') !== false) {
            $title = 'bookyoursite';
        } else if (strpos($stringData, 'reserveamerica') !== false) {
            $title = 'reserveamerica';
        }

        return $title;
    }
}
