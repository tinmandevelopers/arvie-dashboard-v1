<?php

namespace App\Http\Composers\Backend;

use App\Repositories\Backend\ProfileRequestRepository;
use Illuminate\View\View;

/**
 * Class SidebarComposer.
 */
class SidebarComposer
{
    /**
     * @var ProfileRequestRepository
     */
    protected $profileRequestRepository;

    /**
     * SidebarComposer constructor.
     *
     * @param ProfileRequestRepository $profileRequestRepository
     */
    public function __construct(ProfileRequestRepository $profileRequestRepository)
    {
        $this->profileRequestRepository = $profileRequestRepository;
    }

    /**
     * @param View $view
     *
     * @return bool|mixed
     */
    public function compose(View $view)
    {
        $view->with('pending_approval', $this->profileRequestRepository->getUnconfirmedCount()); 
    }
}
