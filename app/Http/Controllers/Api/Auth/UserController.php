<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\Api\Auth\ApiUserRequest;
use App\Models\Auth\User;
use App\Classes\General\GeneralCls;
use App\Classes\User\UserCls;

class UserController extends Controller {

    protected $user;

    public function __construct(UserCls $user)
    {
        $this->user = $user;
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(ApiUserRequest $request) {

        $postData = GeneralCls::stripRequest($request->all());
        $login = $this->user->login($postData);

        $response = $login['response'];
        $code = $login['code'];
        $error = isset($login['error']) ? $login['error'] : [];
        if (!empty($response)) {
            $response = $login['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);

    }       

}
