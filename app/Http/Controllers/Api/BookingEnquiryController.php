<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\BookingEnquiryRequest;
use App\Http\Requests\Api\UpdateBookingEnquiryRequest;
use App\Http\Requests\Api\DeleteBookingEnquiryRequest;
use App\Http\Requests\Api\InitiateBookingEnquiryRequest;
use App\Classes\BookingEnquiry\BookingEnquiryCls;
use App\Classes\General\GeneralCls;
use App\Http\Requests\Api\BookingAvailabilityCheckRequest;
use App\Http\Requests\Api\BookingEnquiryInitiateRequest;
use App\Http\Requests\Api\FileUploadInitiateBookingEnquiryRequest;
use App\Http\Requests\Api\UpdateInitiateBookingEnquiryRequest;
use File;
use Storage;

class BookingEnquiryController extends Controller {

    protected $bookingEnquiryCls;

    public function __construct(BookingEnquiryCls $bookingEnquiryCls) {
        $this->bookingEnquiryCls = $bookingEnquiryCls;
    }

    /**
     * Store a newly BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createBookingEnquiry(BookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $storebooking = $this->bookingEnquiryCls->storeBooking($postData);
        $response = $storebooking['response'];
        $code = $storebooking['code'];
        $error = isset($storebooking['error']) ? $storebooking['error'] : [];
        if (!empty($response)) {
            $response = $storebooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Update BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateBookingEnquiry(UpdateBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $updatebooking = $this->bookingEnquiryCls->updateBooking($postData);
        $response = $updatebooking['response'];
        $code = $updatebooking['code'];
        $error = isset($updatebooking['error']) ? $updatebooking['error'] : [];
        if (!empty($response)) {
            $response = $updatebooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Delete BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteBookingEnquiry(DeleteBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $deleteBooking = $this->bookingEnquiryCls->deleteBooking($postData);
        $response = $deleteBooking['response'];
        $code = $deleteBooking['code'];
        $error = isset($deleteBooking['error']) ? $deleteBooking['error'] : [];
        if (!empty($response)) {
            $response = $deleteBooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Initiate BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initiateBookingEnquiry(InitiateBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $initiateBooking = $this->bookingEnquiryCls->initiateBooking($postData);
        $response = $initiateBooking['response'];
        $code = $initiateBooking['code'];
        $error = isset($initiateBooking['error']) ? $initiateBooking['error'] : [];
        if (!empty($response)) {
            $response = $initiateBooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Initiate BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function detailBookingEnquiry(InitiateBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $detailBooking = $this->bookingEnquiryCls->detailBooking($postData);
        $response = $detailBooking['response'];
        $code = $detailBooking['code'];
        $error = isset($detailBooking['error']) ? $detailBooking['error'] : [];
        if (!empty($response)) {
            $response = $detailBooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Initiate BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initiateDetailBookingEnquiry(BookingEnquiryInitiateRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $checktoken = true;
        $detailBooking = $this->bookingEnquiryCls->detailBooking($postData, $checktoken);
        $response = $detailBooking['response'];
        $code = $detailBooking['code'];
        $error = isset($detailBooking['error']) ? $detailBooking['error'] : [];
        if (!empty($response)) {
            $response = $detailBooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Update BookingEnquiry resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initiateUpdateBookingEnquiry(UpdateInitiateBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $checktoken = true;

        $updatebooking = $this->bookingEnquiryCls->updateBooking($postData, $checktoken);
        $response = $updatebooking['response'];
        $code = $updatebooking['code'];
        $error = isset($updatebooking['error']) ? $updatebooking['error'] : [];
        if (!empty($response)) {
            $response = $updatebooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Uplaod BookingEnquiry File in s3 bucket.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initiateFileUploadBookingEnquiry(FileUploadInitiateBookingEnquiryRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $updatebooking = $this->bookingEnquiryCls->storeFileBooking($postData);
        $response = $updatebooking['response'];
        $code = $updatebooking['code'];
        $error = isset($updatebooking['error']) ? $updatebooking['error'] : [];
        if (!empty($response)) {
            $response = $updatebooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Check BookingAvailability.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkBookingAvailability(BookingAvailabilityCheckRequest $request) {
        $postData = GeneralCls::stripRequest($request->all());
        $fetchTitle = $this->bookingEnquiryCls->checkBookingAvailability($postData);

        if ($fetchTitle && isset($fetchTitle['code']) && $fetchTitle['code'] == 200) {
            $formData = set_booking_enquiry_data($postData['details'], $fetchTitle['url']);
            $postData['details'] = $formData;
            $filename = strtotime("now") . "_checkAvailability.json";
            $storeFile = file_put_contents('../scrapping/temp/' . $filename, json_encode($postData));
            $storLog=$postData;
            $storLog['date_of_api_called']=date('Y-m-d H:i:s');
            $myfile = file_put_contents('../scrapping/temp/checkAvailablityLogs.json', json_encode($storLog).PHP_EOL , FILE_APPEND | LOCK_EX);

            $cmd = 'node ../scrapping/availability/' . $fetchTitle['title'] . 'CheckAvailability.js --url=' . $fetchTitle['url'] . " --file_name=" . $filename;
            //        echo 'cmd is '.$cmd;
            while (@ob_end_flush()); // end all output buffers if any

            $proc = popen($cmd, 'r');
            $dataVal = '';
            while (!feof($proc)) {
                $dataVal .= fread($proc, 4096);
                // echo fread($proc, 4096);
                // echo '<br/>';
                @flush();
            }
            // echo $dataVal;
            @unlink('../scrapping/temp/' . $filename);
            // return response($data,200);
            $data = GeneralCls::setResponse('success');
            $data['response'] = (array) json_decode($dataVal);
            $data['response']['sites_available'] = isset($data['response']['details']) ? count($data['response']['details']) : 0;
            $data['response'] = (object) $data['response'];
            $postData['result'] = json_encode($data['response']);
            $postData['details'] = json_encode($postData['details']);
            if ($postData['enquiry_from'] != 'Soldout') {
                $storeLog = $this->bookingEnquiryCls->storeBookingAvailability($postData, $dataVal);
                if (isset($postData['debug']) && $postData['debug']) {
                    $data['store_log'] = $storeLog;
                }
            }
            return $data;
        } else {
            // print_r($fetchTitle);exit;
            return GeneralCls::setResponse('other_error', $fetchTitle);
        }
    }

}
