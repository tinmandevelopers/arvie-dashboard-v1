<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Classes\General\GeneralCls;
use App\Classes\CheckAvailabilityNotification\CheckAvailabilityNotificationCls;
use App\Http\Requests\Api\CheckAvailabilityNotificationRequest;
use App\Http\Requests\Api\UpdateCheckAvailabilityNotificationRequest;
class CheckAvailabilityNotificationController extends Controller
{

    protected $checkAvailabilityNotificationCls;

    public function __construct(CheckAvailabilityNotificationCls $checkAvailabilityNotificationCls)
    {
        $this->checkAvailabilityNotificationCls = $checkAvailabilityNotificationCls;
    }

    /**
     * Store a newly CheckAvailabilityNotification resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckAvailabilityNotificationRequest $request)
    {
      
        $postData = GeneralCls::stripRequest($request->all());
       
        $storebooking = $this->checkAvailabilityNotificationCls->store($postData);
        $response = $storebooking['response'];
        $code = $storebooking['code'];
        $error = isset($storebooking['error']) ? $storebooking['error'] : [];
        if (!empty($response)) {
            $response = $storebooking['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }

    /**
     * Update CheckAvailabilityNotification resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCheckAvailabilityNotificationRequest $request)
    {
        $postData = GeneralCls::stripRequest($request->all());
        $updateData = $this->checkAvailabilityNotificationCls->update($postData);
        $response = $updateData['response'];
        $code = $updateData['code'];
        $error = isset($updateData['error']) ? $updateData['error'] : [];
        if (!empty($response)) {
            $response = $updateData['response'];
        } else {
            $response = $error;
        }
        return response($response, $code);
    }
}
