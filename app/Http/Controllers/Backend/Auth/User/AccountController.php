<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\ProfileRequestRepository;

/**
 * Class AccountController.
 */
class AccountController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(RoleRepository $roleRepository, ProfileRequestRepository $profileRequestRepository) {
        $data = $profileRequestRepository->where('user_id', auth()->user()->id)->get();
        $user = auth()->user();
        if (count($data) > 0) {
            $user = $data[0];
            $user->id = $data[0]->user_id;
        }
        return view('backend.user.account', compact('user'))->withRoles($roleRepository->with('permissions')->get(['id', 'name']));
    }

}
