<?php

namespace App\Http\Controllers\Backend\Auth\User;


use App\Http\Controllers\Controller;


use App\Http\Requests\Backend\DataSource\UpdateDataSourceRequest;
use App\Http\Requests\Backend\DataSource\StoreDatasourceRequest;
use App\Http\Requests\Backend\DataSource\ManageDataSourceRequest;
use App\Http\Requests\Backend\DataSource\ViewDataSourceRequest;
use App\Models\DataSource;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\DataSourceRepository;
use DataTables;
use Illuminate\Support\Facades\File;


/**
 * Class DataSourceController.
 */
class DataSourceController extends Controller {

    /**
     * @var DataSourceRepository
     */
    protected $dataSourceRepository;

    /**
     * DataSourceController constructor.
     *
     * @param DataSourceRepository $dataSourceRepository
     */
    public function __construct(DataSourceRepository $dataSourceRepository) {
        $this->dataSourceRepository = $dataSourceRepository;
    }

    /**
     * @param ViewUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ViewDataSourceRequest $request) {
        return view('backend.data-source.index');
    }

    /**
     * @param ViewDataSourceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDataSourceList(ViewDataSourceRequest $request) {

        $postData = $request->all();
        return DataTables::of($this->dataSourceRepository->getForDataTable($postData))
            ->editColumn('created_at', function ($dataSource) {
                return '<div class="d-t-cell">' . $dataSource->created_at->format(config('access.date_fromat')) . '</span></div>';
            })
            ->editColumn('last_crawled', function ($dataSource) {
                return ($dataSource->last_crawled) ? '<div class="d-t-cell">' . date(config('access.date_fromat'),strtotime($dataSource->last_crawled)) . '</span></div>' : '';
            })
            ->editColumn('crawler_log', function ($dataSource) {
                if($dataSource->crawler_log){
                    $logarr = explode(":", $dataSource->crawler_log);
                    if(count($logarr)>1) {
                        return "<a href=" . env('S3_BUCKET_URL') . "/data_source/" . $dataSource->id . "/crawler/" . $logarr[1] . " target='_blank' >" . $logarr[0] . "</a>";
                    }else{
                        return "<a href=" . env('S3_BUCKET_URL') . "/data_source/" . $dataSource->id . "/crawler/" . $dataSource->crawler_log . " target='_blank' >" . $dataSource->crawler_log . "</a>";
                    }
                }else{
                    return $dataSource->crawler_log;
                }
            })
            ->addColumn('actions', function ($dataSource) {
                return view('backend.data-source.includes.actions', ['dataSource' => $dataSource]);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('backend.data-source.create');
    }

    /**
     * @param StoreDatasourceRequest $request
     *
     * @throws \Throwable
     * @return mixed
     */
    public function store(StoreDatasourceRequest $request) {
        $post = $request->only('title', 'url','crawler_name','crawler_time' );
        $data = [
            'title' => $post['title'],
            'url' => $post['url'],
            'crawler_name' => $post['crawler_name'],
            'crawler_time' => $post['crawler_time'],
        ];

        if ($request->has('crawler_file')) {
            $md5Name = md5_file($request->file('crawler_file')->getRealPath());
            $guessExtension = 'js';
            $data['crawler_file'] = $request->file('crawler_file')->storeAs('/datasource/crawler', $md5Name.'.'.$guessExtension  ,'public');

//            $data['crawler_file'] = $request->file('crawler_file')->store('/datasource/crawler', 'public');
        }
        if ($request->has('booking_file')) {
            $md5Name = md5_file($request->file('booking_file')->getRealPath());
            $guessExtension = 'js';
            $data['booking_file'] = $request->file('booking_file')->storeAs('/datasource/booking', $md5Name.'.'.$guessExtension  ,'public');

//            $data['booking_file'] = $request->file('booking_file')->store('/datasource/booking', 'public');
        }

        $this->dataSourceRepository->create($data);

        return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.created'));

    }

    /**
     * @param ManageDataSourceRequest    $request
     * @param DataSourceRepository       $dataSourceRepository
     *
     * @return mixed
     */
    public function edit(ManageDataSourceRequest $request) {

        $dataSourceRequest = $this->dataSourceRepository->getById($request->data_source)->toArray();
        if($dataSourceRequest['status']=='Running'){
            return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.crawler_running'));
        }else{
            return view('backend.data-source.edit', compact("dataSourceRequest"));
        }


    }

    /**
     * @param UpdateDataSourceRequest $request
     * @param DataSource              $dataSource
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(UpdateDataSourceRequest $request, DataSource $dataSource) {
        $post = $request->only('title', 'url','crawler_name','crawler_time' );
        $data = [
            'title' => $post['title'],
            'url' => $post['url'],
            'crawler_name' => $post['crawler_name'],
            'crawler_time' => $post['crawler_time'],
        ];

        if ($request->has('crawler_file')) {
            $md5Name = md5_file($request->file('crawler_file')->getRealPath());
            $guessExtension = 'js';
            $data['crawler_file'] = $request->file('crawler_file')->storeAs('/datasource/crawler', $md5Name.'.'.$guessExtension  ,'public');

//            $data['crawler_file'] = $request->file('crawler_file')->store('/datasource/crawler', 'public');
        }
        if ($request->has('booking_file')) {
            $md5Name = md5_file($request->file('booking_file')->getRealPath());
            $guessExtension = 'js';
            $data['booking_file'] = $request->file('booking_file')->storeAs('/datasource/booking', $md5Name.'.'.$guessExtension  ,'public');

//            $data['booking_file'] = $request->file('booking_file')->store('/datasource/booking', 'public');
        }

        $this->dataSourceRepository->update($dataSource, $data);

        return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.updated'));
    }
    /**
     * @param ManageDataSourceRequest $request
     * @param DataSource              $dataSource
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(ManageDataSourceRequest $request, DataSource $dataSource) {

        $this->dataSourceRepository->update($dataSource,  ['comment' => $request->comment]);

        $this->dataSourceRepository->delete($dataSource,$request);

        return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.deleted'));
    }

    /**
     * @param ManageDataSourceRequest $request
     * @param DataSource              $dataSource
     *
     * @return mixed
     */
    public function showLog(ManageDataSourceRequest $request) {
        $dataSourceRequest = $this->dataSourceRepository->with('crawlerLog')->getById($request->data_source)->toArray();

        return view('backend.data-source.log', compact("dataSourceRequest"));
    }

    /**
     * @param ManageDataSourceRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLogList(ManageDataSourceRequest $request) {

        $dataSourceRequest = $this->dataSourceRepository->with('crawlerLog')->getById($request->data_source)->toArray();
        return DataTables::of($dataSourceRequest['crawler_log'])
            ->editColumn('log_file_path', function ($dataSourceRequest) {
                if($dataSourceRequest['log_file_path']){
                    $logarr = explode(":", $dataSourceRequest['log_file_path']);

                    if(count($logarr)>1){
                        return "<a href=".env('S3_BUCKET_URL')."/data_source/".$dataSourceRequest['data_source_id']."/crawler/".$logarr[1]." target='_blank' >".$logarr[0]."</a>";
                    }else{
                        return "<a href=".env('S3_BUCKET_URL')."/data_source/".$dataSourceRequest['data_source_id']."/crawler/".$dataSourceRequest['log_file_path']." target='_blank' >".$dataSourceRequest['log_file_path']."</a>";
                    }
                }else{
                    return '';
                }
            })
            ->editColumn('created_at', function ($dataSourceRequest) {
                return '<div class="d-t-cell">' . date(config('access.date_fromat_time'),strtotime($dataSourceRequest['created_at'])) . '</span></div>';
            })
            ->editColumn('updated_at', function ($dataSourceRequest) {
                return '<div class="d-t-cell">' . date(config('access.date_fromat_time'),strtotime($dataSourceRequest['updated_at'])) . '</span></div>';
            })
            ->escapeColumns([])
            ->make(true);

    }


    /**
     * @param ManageDataSourceRequest $request
     *
     * @return mixed
     */
    public function startBooking(ManageDataSourceRequest $request, DataSource $dataSource) {

        $dataSourceRequest = $this->dataSourceRepository->getById($request->data_source)->toArray();

        $this->dataSourceRepository->update($dataSource,  ['status' => 'Running']);

//        $path='storage/'.$dataSourceRequest[0]['booking_file'];
//        exec('npx nightwatch -e chrome '.$path, $output, $result);
//        logger('output');
//        logger($output);
//        $file = time() . '_bookinglog.txt';
//        $path = public_path("/storage/datasource/log/booking/");
//        if(!File::isDirectory($path)){
//            File::makeDirectory($path, 0777, true, true);
//        }
//
//        $writelog=false;
//        File::append($path.$file,'Started At:'.date('Y-m-d H:i:s').PHP_EOL);
//        foreach ($output as $line){
//            if($line=='Final Result End'){
//                $writelog=false;
//            }
//            if($writelog==true){
//                File::append($path.$file,$line.PHP_EOL);
//            }
//            if($line=='Final Result'){
//                $writelog=true;
//            }
//        }
//        File::append($path.$file,'Started At:'.date('Y-m-d H:i:s'));
//
        $this->dataSourceRepository->update($dataSource,  ['status' => 'Complete']);
//
//        logger('result');
//        logger($result);
        return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.booking_start'));

    }

    /**
     * @param ManageDataSourceRequest $request
     *
     * @return mixed
     */
    public function startCrawler(ManageDataSourceRequest $request, DataSource $dataSource) {

        $dataSourceRequest = $this->dataSourceRepository->getById($request->data_source)->toArray();

        $this->dataSourceRepository->update($dataSource,  ['status' => 'Running']);

        $path='storage/'.$dataSourceRequest[0]['crawler_file'];

        $cmd = 'node '.$path.' --datasource_id='.$dataSourceRequest[0]['id'];

        while (@ ob_end_flush()); // end all output buffers if any

        $proc = popen($cmd, 'r');

        while (!feof($proc)) {
            echo '<p>' . fread($proc, 4096) . '</p>';
            echo '<br/>';
            @ flush();
        }

        $this->dataSourceRepository->update($dataSource,  ['status' => 'Complete','last_crawled' => date("Y-m-d H:i:s")]);
//        return redirect()->route('admin.auth.data-source.index')->withFlashSuccess(__('alerts.backend.data-source.crawler_start'));

    }

}
