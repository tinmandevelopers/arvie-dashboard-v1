<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\ViewUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateProfileRequest;
use App\Models\Auth\User;
use App\Repositories\Backend\ProfileRequestRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Models\ProfileRequest;
use DataTables;
use App\Notifications\Backend\Auth\ProfileVerification;


/**
 * Class UserController.
 */
class ProfileRequestController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var ProfileRequestRepository
     */
    protected $profileRequestRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     * @param ProfileRequestRepository $profileRequestRepository
     */
    public function __construct(UserRepository $userRepository, ProfileRequestRepository $profileRequestRepository) {
        $this->userRepository = $userRepository;
        $this->profileRequestRepository = $profileRequestRepository;
    }

    /**
     * @param ViewUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ViewUserRequest $request) {
        return view('backend.profile-request.index');
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfileRequestList(ViewUserRequest $request) {
        $postData = $request->all();
        return Datatables::of($this->profileRequestRepository->getForDataTable($postData))
                        ->editColumn('created_at', function ($user) {
                            return '<div class="d-t-cell">' . $user->created_at->format('M d, Y') . '</span></div>';
                        })
                        ->addColumn('actions', function ($user) {
                            return view('backend.profile-request.includes.actions', ['user' => $user]);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * @param ManageUserRequest    $request
     * @param UserRepository       $userRepository
     *
     * @return mixed
     */
    public function edit(ManageUserRequest $request, UserRepository $userRepository) {
        $profileRequest = $this->profileRequestRepository->getById($request->profile_request)->toArray();
        $user = $userRepository->getById($profileRequest['user_id'])->toArray();
        $notDisplay = ['id', 'user_id', 'reason', 'approved_by', 'status', 'created_at', 'updated_at', 'deleted_at', 'id_proof', 'id_number'];
        return view('backend.profile-request.edit', compact("profileRequest", "user", "notDisplay"));
    }

    /**
     * @param UpdateUserRequest $request
     * @param User              $user
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(UpdateProfileRequest $request, profileRequest $profileRequest) {
        $data = $request->only('user_id', 'status', 'reason');
        
         $user = $this->userRepository->getById($profileRequest->user_id);

        if ($data['status'] == 'Approved') {
            $user->first_name = $profileRequest->first_name;
            $user->last_name = $profileRequest->last_name;
            $user->screen_name = $profileRequest->screen_name;
            $user->alternative_email = $profileRequest->alternative_email;
            $user->contact_number = $profileRequest->contact_number;
            $user->address = $profileRequest->address;
            $user->kin_first_name = $profileRequest->kin_first_name;
            $user->kin_last_name = $profileRequest->kin_last_name;
            $user->kin_contact = $profileRequest->kin_contact;
            $user->kin_email = $profileRequest->kin_email;
            $user->medical_membership_number = $profileRequest->medical_membership_number;
            $user->medical_provider = $profileRequest->medical_provider;
            $user->medical_insurance_plan = $profileRequest->medical_insurance_plan;
            $user->about_me = $profileRequest->about_me;
            $user->avatar_location = $profileRequest->avatar_location;
            $user->medical_membership_card = $profileRequest->medical_membership_card;
            $user->income_tax_number = $profileRequest->income_tax_number;
            $user->specialized_in = $profileRequest->specialized_in;
            $user->save();
        }
        
        $user->notify(new ProfileVerification($data));
        $this->profileRequestRepository->forceDelete($profileRequest);

        $redirect = 'admin.auth.profile-request.index';
        return redirect()->route($redirect)->withFlashSuccess(__('alerts.backend.profile-request.updated'));
    }

}
