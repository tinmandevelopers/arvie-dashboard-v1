<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Events\Backend\Auth\User\UserDeleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Http\Requests\Backend\Auth\User\ViewUserRequest;
use App\Http\Requests\Backend\Auth\User\StoreUserRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserRequest;
use App\Models\Auth\User;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\ProfileRequestRepository;
use App\Models\ProfileRequest;
use DataTables;

/**
 * Class UserController.
 */
class UserController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository, ProfileRequestRepository $profileRequestRepository) {
        $this->userRepository = $userRepository;
        $this->profileRequestRepository = $profileRequestRepository;
    }

    /**
     * @param ViewUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ViewUserRequest $request) {
        return view('backend.auth.user.index');
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsersList(ViewUserRequest $request) {
        $postData = $request->all();
        return Datatables::of($this->userRepository->getForDataTable($postData))
                        ->editColumn('first_name', function ($user) {
                            $name = $user->first_name . ($user->screen_name != '' ? " (" . $user->screen_name . ")" : "");
                            return '<div class="user-picture"><img src="' . get_user_picture($user) . '" alt="' . $name . '"/></div><div class="d-t-cell">' . $name . '</div>';
                        })
                        ->editColumn('created_at', function ($user) {
                            return '<div class="d-t-cell">' . $user->created_at->format(config('access.date_fromat')) . '<br/><span class="updated_at">' . __('labels.backend.access.users.table.updated') . ' ' . $user->updated_at->diffForHumans() . '</span></div>';
                        })
                        ->editColumn('active', function ($user) {
                            return view('backend.auth.user.includes.status', ['user' => $user]);
                        })
                        ->addColumn('actions', function ($user) {
                            return view('backend.auth.user.includes.actions', ['user' => $user]);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request, RoleRepository $roleRepository) {
        return view('backend.auth.user.create')->withRoles($roleRepository->with('permissions')->get(['id', 'name']));
    }

    /**
     * @param StoreUserRequest $request
     *
     * @throws \Throwable
     * @return mixed
     */
    public function store(StoreUserRequest $request) {
        $post = $request->only('first_name', 'last_name', 'screen_name', 'email', 'alternative_email', 'contact_number', 'address', 'dob', 'id_number', 'id_proof', 'income_tax_number', 'kin_first_name', 'kin_last_name', 'kin_contact', 'kin_email', 'medical_membership_number', 'medical_provider', 'medical_insurance_plan', 'medical_membership_card', 'avatar_location', 'about_me', 'roles', 'specialized_in', 'active');
        $data = [
            'first_name' => $post['first_name'],
            'last_name' => $post['last_name'],
            'screen_name' => $post['screen_name'],
            'email' => $post['email'],
            'alternative_email' => $post['alternative_email'],
            'contact_number' => $post['contact_number'],
            'address' => $post['address'],
            'dob' => $post['dob'],
            'id_number' => $post['id_number'],
            'income_tax_number' => $post['income_tax_number'],
            'kin_first_name' => $post['kin_first_name'],
            'kin_last_name' => $post['kin_last_name'],
            'kin_contact' => $post['kin_contact'],
            'kin_email' => $post['kin_email'],
            'medical_membership_number' => $post['medical_membership_number'],
            'medical_provider' => $post['medical_provider'],
            'medical_insurance_plan' => $post['medical_insurance_plan'],
            'about_me' => $post['about_me'],
            'specialized_in' => $post['specialized_in'],
            'active' => (isset($post['active'])) ? $post['active'] : 0,
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed' => '1',
        ];
        if ($request->has('id_proof')) {
            $data['id_proof'] = $request->file('id_proof')->store('/users', 'public');
        }
        if ($request->has('avatar_location')) {
            $data['avatar_location'] = $request->file('avatar_location')->store('/avatars', 'public');
        }
        if ($request->has('medical_membership_card')) {
            $data['medical_membership_card'] = $request->file('medical_membership_card')->store('/users', 'public');
        }
        $this->userRepository->create($data);

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.created'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function show(ViewUserRequest $request, User $user) {
        return view('backend.auth.user.show')
                        ->withUser($user);
    }

    /**
     * @param ManageUserRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param User                 $user
     *
     * @return mixed
     */
    public function edit(ManageUserRequest $request, RoleRepository $roleRepository, User $user) {
        return view('backend.auth.user.edit')
                        ->withUser($user)
                        ->withRoles($roleRepository->get())
                        ->withUserRoles($user->roles->pluck('name')->all());
    }

    /**
     * @param UpdateUserRequest $request
     * @param User              $user
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(UpdateUserRequest $request, User $user, profileRequest $profileRequest) {
        $post = $request->only('first_name', 'last_name', 'screen_name', 'email', 'alternative_email', 'contact_number', 'address', 'dob', 'id_number', 'id_proof', 'income_tax_number', 'kin_first_name', 'kin_last_name', 'kin_contact', 'kin_email', 'medical_membership_number', 'medical_provider', 'medical_insurance_plan', 'medical_membership_card', 'avatar_location', 'about_me', 'roles', 'specialized_in', 'active');
        $data = [
            'first_name' => $post['first_name'],
            'last_name' => $post['last_name'],
            'screen_name' => $post['screen_name'],
            'alternative_email' => $post['alternative_email'],
            'contact_number' => $post['contact_number'],
            'address' => $post['address'],
            'kin_first_name' => $post['kin_first_name'],
            'kin_last_name' => $post['kin_last_name'],
            'kin_contact' => $post['kin_contact'],
            'kin_email' => $post['kin_email'],
            'medical_membership_number' => $post['medical_membership_number'],
            'medical_provider' => $post['medical_provider'],
            'medical_insurance_plan' => $post['medical_insurance_plan'],
            'about_me' => $post['about_me'],
        ];
        if ($request->has('form_name')) {
            $data['user_id'] = $request->user_id;
            $data['email'] = $user->email;
            $data['dob'] = $user->dob;
            $data['id_number'] = $user->id_number;
            $data['income_tax_number'] = $user->income_tax_number;
            $data['specialized_in'] = $user->specialized_in;
            $data['id_proof'] = $user->id_proof;
            if ($request->has('avatar_location')) {
                $data['avatar_location'] = $request->file('avatar_location')->store('/avatars', 'public');
            }else{
                $data['avatar_location'] = $user->avatar_location;
            }
            if ($request->has('medical_membership_card')) {
                $data['medical_membership_card'] = $request->file('medical_membership_card')->store('/users', 'public');
            }else{
                $data['medical_membership_card'] = $user->medical_membership_card;
            }
        } else {
            if ($request->has('active')) {
                $data['active'] = $request->active;
            } else {
                $data['active'] = 0;
            }

            $data['email'] = $request->email;
            $data['dob'] = $request->dob;
            $data['id_number'] = $request->id_number;
            $data['income_tax_number'] = $request->income_tax_number;
            $data['specialized_in'] = $request->specialized_in;

            $roles = [$post['roles']];
        }
//
        if ($request->has('id_proof')) {
            $user->id_proof = $request->file('id_proof')->store('/users', 'public');
        }
        if ($request->has('avatar_location')) {
            $user->avatar_location = $request->file('avatar_location')->store('/avatars', 'public');
        }
        if ($request->has('medical_membership_card')) {
            $user->medical_membership_card = $request->file('medical_membership_card')->store('/users', 'public');
        }
        if (auth()->user()->isAdmin()) {
            $this->userRepository->update($user, $data, ($request->has('form_name') ? [] : $roles));
        } else {
            $this->profileRequestRepository->create($profileRequest, $data, $user);
        }
        if ($request->has('form_name')) {
            $redirect = 'admin.auth.user.account';
        } else {
            $redirect = 'admin.auth.user.index';
        }
        return redirect()->route($redirect)->withFlashSuccess(__('alerts.backend.users.updated'));
    }

    /**
     * @param ManageUserRequest $request
     * @param User              $user
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(ManageUserRequest $request, User $user) {
        $this->userRepository->mark($user, '0');

        event(new UserDeleted($user));

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }

}
