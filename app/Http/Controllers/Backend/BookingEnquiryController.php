<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\BookingEnquiry\UpdateBookingEnquiryRequest;
use App\Http\Requests\Backend\BookingEnquiry\ManageBookingEnquiryRequest;
use App\Http\Requests\Backend\BookingEnquiry\ViewBookingEnquiryRequest;
use App\Models\Auth\User;
use App\Models\Campground;
use App\Models\BookingEnquiry;
use App\Repositories\Backend\CampgroundRepository;
use App\Repositories\Backend\DataSourceRepository;
use App\Repositories\Backend\BookingEnquiryRepository;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\Http;
use Session;
use Storage;

class BookingEnquiryController extends Controller {

    /**
     * @var BookingEnquiryRepository
     */
    protected $bookingEnquiryRepository;

    /**
     * @var DataSourceRepository
     */
    protected $dataSourceRepository;

    /**
     * @var CampgroundRepository
     */
    protected $campgroundRepository;

    public function __construct(BookingEnquiryRepository $bookingEnquiryRepository, DataSourceRepository $dataSourceRepository, CampgroundRepository $campgroundRepository) {
        $this->bookingEnquiryRepository = $bookingEnquiryRepository;
        $this->dataSourceRepository = $dataSourceRepository;
        $this->campgroundRepository = $campgroundRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ViewBookingEnquiryRequest $request) {
        return view('backend.booking-enquiry.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBookingEnquiryList(ViewBookingEnquiryRequest $request) {
        $postData = $request->all();
        return DataTables::of($this->bookingEnquiryRepository->getForDataTable($postData))
                        ->editColumn('campground_id', function ($enquiry) {
                            return $enquiry->campground->title;
                        })
                        ->editColumn('check_in_date', function ($enquiry) {
                            return date(config('access.date_fromat'), strtotime($enquiry->check_in_date));
                        })
                        ->editColumn('assign_id', function ($enquiry) {
                            return ($enquiry->user) ? $enquiry->user->first_name . ' ' . $enquiry->user->last_name : '';
                        })
                        ->editColumn('check_out_date', function ($enquiry) {
                            return date(config('access.date_fromat'), strtotime($enquiry->check_out_date));
                        })
                        ->editColumn('updated_at', function ($enquiry) {
                            return $enquiry->updated_at->format(config('access.date_fromat_time'));
                        })
                        ->addColumn('actions', function ($enquiry) {
                            return view('backend.booking-enquiry.includes.actions', ['enquiry' => $enquiry]);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *  Show BookingEnquiry data.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageBookingEnquiryRequest $request) {
        $enquiry = $this->bookingEnquiryRepository->getBookingEnquiryDetails($request);
        return view('backend.booking-enquiry.show', compact("enquiry"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function booking(ManageBookingEnquiryRequest $request, BookingEnquiry $bookingEnquiry) {
        $enquiryVal = $this->bookingEnquiryRepository->setBookingToken($bookingEnquiry, $request->enquiry);
        $enquiry = $this->bookingEnquiryRepository->getById($request->enquiry);

        return redirect(env('LOCAL_APP_URL') . '/' . $enquiry->campground_id . '/' . $enquiry->booking_id . '/' . $enquiryVal);

        //         $campground = $this->campgroundRepository->getById($enquiry->campground_id);
        //         $datasource = $this->dataSourceRepository->getById($campground['data_source_id']);
        //         // $cmd = 'node ../scrapping/koaBookingReserve.js --campground_id='.$campground['id'];
        //         $path = 'storage/' . $datasource['booking_file'];
        //         $cmd = 'node '.$path.' --campground_id='.$campground['id'].' --booking_id='.$enquiry->id;
        // //        echo 'cmd is '.$cmd;
        //         while (@ ob_end_flush()); // end all output buffers if any
        //         $proc = popen($cmd, 'r');
        //         while (!feof($proc)) {
        //             echo '<p>' . fread($proc, 4096) . '</p>';
        //             echo '<br/>';
        //             @ flush();
        //         }
        // return redirect()->route('admin.campground.index');
    }

    /**
     * Display a Check Availablity Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkAvailabilityCreate(Request $request) {
        $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $params = json_decode(file_get_contents($path), true);
        return view('backend.check-availability.checkAvailability', compact("params"));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkAvailabilityLogIndex(Request $request) {
        return view('backend.check-availability.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getcheckAvailabilityLogList(Request $request) {

        $postData = $request->all();
        return DataTables::of($this->bookingEnquiryRepository->getForDataTableAvailability($postData))
                        ->editColumn('campground_id', function ($enquiry) {
                            return $enquiry->campground->title;
                        })
                        ->editColumn('check_in_date', function ($enquiry) {
                            return date(config('access.date_fromat'), strtotime($enquiry->check_in_date));
                        })
                        ->editColumn('check_out_date', function ($enquiry) {
                            return date(config('access.date_fromat'), strtotime($enquiry->check_out_date));
                        })
                        ->editColumn('created_at', function ($enquiry) {
                            return '<div class="d-t-cell">' . $enquiry->created_at->format(config('access.date_fromat')) . '</span></div>';
                        })
                        ->editColumn('result', function ($enquiry) {
                            if ($enquiry->result != 'null') {
                                $result = json_decode($enquiry->result, true);
                                $enquiry->result = isset($result['availability']) && $result['availability'] ? 'Yes' : 'No';
                                return '<div class="d-t-cell">' . $enquiry->result . '</span></div>';
                            } else {
                                return '<div class="d-t-cell">No</span></div>';
                            }
                        })
                        ->addColumn('actions', function ($enquiry) {
                            return view('backend.check-availability.includes.actions', ['log' => $enquiry]);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * Display the specified resource.
     *  Show BookingEnquiry data.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showCheckAvailabilityLog(Request $request) {
        $log = $this->bookingEnquiryRepository->getCheckAvailablityLogDetails($request);
        $log['result'] = json_decode($log['result'], true);
        return view('backend.check-availability.show', compact("log"));
    }

}
