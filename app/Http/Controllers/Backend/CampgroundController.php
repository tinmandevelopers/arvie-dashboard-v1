<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Campground\UpdateCampgroundRequest;
use App\Http\Requests\Backend\Campground\ManageCampgroundRequest;
use App\Http\Requests\Backend\Campground\ViewCampgroundRequest;
use App\Models\Campground;
use App\Repositories\Backend\CampgroundRepository;
use App\Repositories\Backend\DataSourceRepository;
use DataTables;
use App\Classes\Sync\SyncCls;
use Illuminate\Http\Request;

/**
 * Class CampgroundController.
 */
class CampgroundController extends Controller {

    /**
     * @var CampgroundRepository
     */
    protected $campgroundRepository;

    /**
     * @var DataSourceRepository
     */
    protected $dataSourceRepository;

    /**
     * @var Sync Class
     */
    protected $syncCls;

    /**
     * CampgroundController constructor.
     *
     * @param CampgroundRepository $campgroundRepository
     */
    public function __construct(CampgroundRepository $campgroundRepository, DataSourceRepository $dataSourceRepository, SyncCls $syncCls) {
        $this->campgroundRepository = $campgroundRepository;
        $this->dataSourceRepository = $dataSourceRepository;
        $this->syncCls = $syncCls;
    }

    /**
     * @param ViewUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ViewCampgroundRequest $request) {
        return view('backend.campground.index')->withDataSource($this->dataSourceRepository->getDataSources());
    }

    /**
     * @param ViewCampgroundRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCampgroundList(ViewCampgroundRequest $request) {
        $postData = $request->all();
        return DataTables::of($this->campgroundRepository->getForDataTable($postData))
                        ->editColumn('data_source_id', function ($campground) {
                            return $campground->getDataSource->crawler_name;
                        })
                        ->editColumn('last_indexed_time', function ($campground) {
                            return $campground->last_indexed_time->format(config('access.date_fromat_time'));
                        })
                        ->addColumn('actions', function ($campground) {
                            return view('backend.campground.includes.actions', ['campground' => $campground]);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * @param ManageCampgroundRequest    $request
     * @param CampgroundRepository       $campgroundRepository
     *
     * @return mixed
     */
    public function show(ManageCampgroundRequest $request) {

        $campground = $this->campgroundRepository->getById($request->campground);
        if ($campground['status'] == 'Running') {
            return redirect()->route('admin.campground.index')->withFlashSuccess(__('alerts.backend.campground.crawler_running'));
        } else {
            return view('backend.campground.show', compact("campground"));
        }
    }

    /**
     * @param UpdateCampgroundRequest $request
     * @param Campground              $campground
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(UpdateCampgroundRequest $request, Campground $campground) {
        $data = $request->only('status');
        switch ($data['status']) {
            case 'Active':
                $webStatus = 'Listed';
                break;
            case 'Inactive':
                $webStatus = 'Unlisted';
                break;
            default:
                $webStatus = 'Pending';
                break;
        }

        $update = $this->campgroundRepository->update($campground, $data);

        if ($update) {
            $postData['campground_id'] = $update->id;
            $postData['url'] = env('S3_BUCKET_URL') . '/data_source/' . $update->data_source_id . '/campgrounds/' . $update->id . '/' . $update->data_file_path;
            $postData['status'] = $webStatus;
            $response = $this->syncCls->pushCampground($postData);
            if ($response) {
                if (!$response->status_code) {
                    return redirect()->route('admin.campground.index')->withFlashDanger($response->success_message);
                } else {
                    $data['web_status'] = $webStatus;
                    $update = $this->campgroundRepository->update($campground, $data);
                }
            }
        }

        return redirect()->route('admin.campground.index')->withFlashSuccess(__('alerts.backend.campground.updated'));
    }

    /**
     * @param ManageCampgroundRequest $request
     * @param Campground              $campground
     *
     * @throws \Exception
     * @return mixed
     */
    public function destroy(ManageCampgroundRequest $request, Campground $campground) {

        $this->campgroundRepository->delete($campground, $request);

        return redirect()->route('admin.campground.index')->withFlashDanger(__('alerts.backend.campground.deleted'));
    }

    public function booking(ManageCampgroundRequest $request) {

        $campground = $this->campgroundRepository->getById($request->campground);
        $datasource = $this->dataSourceRepository->getById($campground['data_source_id']);
        // $cmd = 'node ../scrapping/koaBookingReserve.js --campground_id='.$campground['id'];

        $path = 'storage/' . $datasource['booking_file'];

        $cmd = 'node ' . $path . ' --campground_id=' . $campground['id'];

        //        echo 'cmd is '.$cmd;
        while (@ob_end_flush()); // end all output buffers if any

        $proc = popen($cmd, 'r');

        while (!feof($proc)) {
            echo '<p>' . fread($proc, 4096) . '</p>';
            echo '<br/>';
            @flush();
        }

        // return redirect()->route('admin.campground.index');
    }

    /**
     * @return mixed
     */
    public function compare() {
        $campgrounds = $this->campgroundRepository->getCampgroundForCompare();
        $dataSources = $this->dataSourceRepository->all();
        return view('backend.campground.compare', compact("campgrounds", "dataSources"));
    }

    /**
     * @return array
     */
    public function actcamp($id) {
        $msg = [];
        $failed = $success = 0;
        $allCampgrounds = $this->campgroundRepository->where('data_source_id', $id)->where('web_status', 'Listed')->whereIn('status', ['Pending', 'Active'])->get();
        $msg[] = "Total > " . $allCampgrounds->count();
        foreach ($allCampgrounds as $key => $campground) {
            $data['status'] = 'Active';
            switch ($data['status']) {
                case 'Active':
                    $webStatus = 'Listed';
                    break;
                case 'Inactive':
                    $webStatus = 'Unlisted';
                    break;
                default:
                    $webStatus = 'Pending';
                    break;
            }

            $title = get_datasource_title($campground->url);
            $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
            $params = json_decode(file_get_contents($path), true);
            $equipement = isset($params[$title . '_equipement']) ? $params[$title . '_equipement'] : [];

            $postData['campground_id'] = $campground->id;
            $postData['url'] = env('S3_BUCKET_URL') . '/data_source/' . $campground->data_source_id . '/campgrounds/' . $campground->id . '/' . $campground->data_file_path;
            $postData['status'] = $webStatus;
            $postData['equipement_type'] = json_encode($equipement);
            $response = $this->syncCls->pushCampground($postData);
            if ($response) {
                if (!$response->status_code) {
                    $failed++;
                    continue;
                } else {
                    $data['web_status'] = $webStatus;
                    $update = $this->campgroundRepository->update($campground, $data);
                    $success++;
                }
            }
        }
        $msg[] = "Failed > " . $failed;
        $msg[] = "Success > " . $success;
        dd($msg);
    }

    /**
     * @return array
     */
    public function deactcamp() {
        $msg = [];
        $failed = $success = 0;
        $allCampgrounds = Campground::onlyTrashed()->get();
        $msg[] = "Total > " . count($allCampgrounds);
        foreach ($allCampgrounds as $key => $campground) {
            $data['status'] = 'Inactive';
            $webStatus = 'Unlisted';

            $title = get_datasource_title($campground->url);
            $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
            $params = json_decode(file_get_contents($path), true);
            $equipement = isset($params[$title . '_equipement']) ? $params[$title . '_equipement'] : [];

            $postData['campground_id'] = $campground->id;
            $postData['url'] = env('S3_BUCKET_URL') . '/data_source/' . $campground->data_source_id . '/campgrounds/' . $campground->id . '/' . $campground->data_file_path;
            $postData['status'] = $webStatus;
            $postData['equipement_type'] = json_encode($equipement);
            $response = $this->syncCls->pushCampground($postData);
            if ($response) {
                if (!$response->status_code) {
                    $failed++;
                    continue;
                } else {
                    $data['web_status'] = $webStatus;
                    $update = $this->campgroundRepository->update($campground, $data);
                    $success++;
                }
            }
        }
        $msg[] = "Failed > " . $failed;
        $msg[] = "Success > " . $success;
        dd($msg);
    }

    /**
     * Display a Check Availablity Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $allCampgrounds = $this->campgroundRepository->search($request);
        return $allCampgrounds;
    }

    /**
     * Display a Check Availablity Form Equipement based on cmpground.
     *
     * @return \Illuminate\Http\Response
     */
    public function equipementList(Request $request) {
        $allEquipement = $this->campgroundRepository->searchEquipement($request);
        return $allEquipement;
    }

}
