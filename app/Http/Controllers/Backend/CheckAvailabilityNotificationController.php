<?php

namespace App\Http\Controllers\Backend;

use App\Exports\SoldDataExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CheckAvailabilityNotification\ManageCheckAvailabilityNotificationRequest;
use App\Http\Requests\Backend\CheckAvailabilityNotification\StoreCheckAvailabilityNotificationRequest;
use App\Http\Requests\Backend\CheckAvailabilityNotification\ViewCheckAvailabilityNotificationRequest;
use App\Models\CheckAvailabilityNotification;
use App\Models\SoldOutSearch;
use App\Repositories\Backend\CheckAvailabilityNotificationRepository;
// use Request;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use SebastianBergmann\Environment\Console;

use Illuminate\Http\Request;

/**
 * Class CheckAvailabilityNotificationController.
 */
class CheckAvailabilityNotificationController extends Controller
{

    /**
     * @var CheckAvailabilityNotificationRepository
     */
    protected $checkAvailabilityNotificationRepository;

    public function __construct(CheckAvailabilityNotificationRepository $checkAvailabilityNotificationRepository)
    {
        $this->checkAvailabilityNotificationRepository = $checkAvailabilityNotificationRepository;
    }

    /**
     * @param ViewCheckAvailabilityNotificationRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ViewCheckAvailabilityNotificationRequest $request)
    {
        return view('backend.check-availability-notification.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getcheckAvailabilityNotificationList(ViewCheckAvailabilityNotificationRequest $request)
    {
        $postData = $request->all();

        return DataTables::of($this->checkAvailabilityNotificationRepository->getForDataTableAvailability($postData))
            ->editColumn('campground_id', function ($enquiry) {
                return $enquiry->campground->title;
            })
            ->editColumn('datasource_id', function ($enquiry) {
                return $enquiry->campground->getDataSource->title;
            })
            ->editColumn('status', function ($enquiry) {
                return str_replace('Expire', 'Terminated', $enquiry->status);
            })
            ->addColumn('actions', function ($enquiry) {
                return view('backend.check-availability-notification.includes.actions', ['log' => $enquiry]);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Display a Sold Out Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $params = json_decode(file_get_contents($path), true);
        return view('backend.check-availability-notification.create', compact("params"));
    }

    /**
     * Display the specified resource.
     *  Show BookingEnquiry data.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ManageCheckAvailabilityNotificationRequest $request)
    {
        $log = $this->checkAvailabilityNotificationRepository->getAvailabilityNotificationDetails($request);
        $log->details = json_decode($log->details);
        // print_r($log->details);exit;
        return view('backend.check-availability-notification.show', compact("log"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLog(ViewCheckAvailabilityNotificationRequest $request)
    {
        $postData = $request->all();

        return DataTables::of($this->checkAvailabilityNotificationRepository->getForDataTableAvailabilityLog($postData))
            ->editColumn('log_time', function ($enquiry) {
                return ($enquiry->log_time != '') ? date(config('access.date_fromat_time'), strtotime($enquiry->log_time)) : '';
            })->editColumn('result', function ($enquiry) {
                if ($enquiry->result != 'null') {
                    $result = json_decode($enquiry->result, true);
                    $result = isset($result['availability']) && $result['availability'] ? 'Yes' : 'No';
                    return '<div class="d-t-cell">' . $result . '</span></div>';
                } else {
                    return '<div class="d-t-cell">No</span></div>';
                }
            })
            ->editColumn('message', function ($enquiry) {
                if ($enquiry->result != 'null') {
                    $result = json_decode($enquiry->result, true);
                    return '<div class="d-t-cell">' . (isset($result['availability']) ? $result['message'] : 'Not Available') . '</span></div>';
                } else {
                    return '<div class="d-t-cell"></span></div>';
                }
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * @param object    $data
     *
     * @return mixed
     */
    public function export(Request $request)
    {
        return Excel::download(new SoldDataExport(), 'SoldOutData.xlsx');
    }


    /**
     * Display the specified resource.
     *  Show BookingEnquiry data.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageCheckAvailabilityNotificationRequest $request)
    {
        $log = $this->checkAvailabilityNotificationRepository->delete($request);
        return view('backend.check-availability-notification.index')->withFlashDanger(__('alerts.backend.sold-out.deleted'));
    }
}
