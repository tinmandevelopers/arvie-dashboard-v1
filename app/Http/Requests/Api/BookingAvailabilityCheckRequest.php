<?php

namespace App\Http\Requests\Api;

use App\Models\Campground;
use Illuminate\Foundation\Http\FormRequest;

class BookingAvailabilityCheckRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $todayDate = date('Y/m/d');

        return [
            'campground_id' => 'required',
            'check_in_date' => 'required',
            'check_out_date' => 'required',
//            'check_in_date' => 'after_or_equal:'.$todayDate,
//            'check_out_date' => 'after:check_in_date',
//            'check_in_date' => 'date_format:Y/m/d|after_or_equal:'.$todayDate,
//            'check_out_date' => 'date_format:Y/m/d|after:check_in_date',
            'details' => 'required',
            'enquiry_from' => 'required',
        ];
        // 'campsite_id'=>'required',
        // 'campsite_name'=>'required',
    }

}
