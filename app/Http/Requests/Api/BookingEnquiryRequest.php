<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class BookingEnquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $todayDate = date('Y/m/d');
        return [
            'booking_id' => 'required',
            'campground_id' => 'required',
            'check_in_date' => 'date_format:Y/m/d|after_or_equal:'.$todayDate,
            'check_out_date' => 'date_format:Y/m/d|after:check_in_date',
            'name' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'country_code' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'state' => 'required',
            'post_code' => 'required',
            'email' => 'required|email',
        ];
    }
}
