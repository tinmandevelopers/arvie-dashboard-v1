<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CheckAvailabilityNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'campground_id' => 'required',
            'check_in_date' => 'required',
            'check_out_date' => 'required',
            'details'=>'required',
            'enquiry_from'=>'required',
            'duration'=>'required',
        ];
    }
}
