<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingEnquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booking_id'=>'required',
            // 'id' => 'required',
            // 'campground_id' => 'required',
            // 'check_in_date' => 'required',
            // 'check_out_date' => 'required',
            // 'name' => 'required',
            // 'address1' => 'required',
            // 'address2' => 'required',
            // 'country_code' => 'required',
            // 'phone' => 'required|min:8|max:15',
            // 'city' => 'required',
            // 'state' => 'required',
            // 'post_code' => 'required',
            // 'email' => 'required|email',
        ];
    }
}
