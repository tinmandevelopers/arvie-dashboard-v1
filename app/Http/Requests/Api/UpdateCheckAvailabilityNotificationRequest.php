<?php

namespace App\Http\Requests\Api;

use App\Models\CheckAvailabilityNotification;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCheckAvailabilityNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sold_out_search_id' => 'required',
            'status' => 'required',
        ];
    }
}
