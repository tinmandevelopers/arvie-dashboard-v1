<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreUserRequest.
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required', 'email', Rule::unique('users')],
            'alternative_email' => ['required'],
            'contact_number' => ['required'],
            'address' => ['required'],
            'dob' => ['required'],
            'id_number' => ['required'],
            'id_proof' => ['required'],
            'income_tax_number' => ['required'],
//            'roles' => ['required', 'array'],
        ];
    }
}
