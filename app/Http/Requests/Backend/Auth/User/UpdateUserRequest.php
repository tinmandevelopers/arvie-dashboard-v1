<?php

namespace App\Http\Requests\Backend\Auth\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest.
 */
class UpdateUserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
//    public function authorize() {
//        return $this->user()->isAdmin();
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        return [
            'email' =>  (isset($this->post()['email']))  ? ['required', 'email'] : [],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'alternative_email' => ['required'],
            'contact_number' => ['required'],
            'address' => ['required'],
            'dob' => (isset($this->post()['dob']))  ? ['required'] : [] ,
            'id_number' => (isset($this->post()['id_number']))  ? ['required'] : [],
            'income_tax_number' =>  (isset($this->post()['income_tax_number']))  ? ['required']: [],
            'kin_first_name' => ['required'],
            'kin_last_name' => ['required'],
            'kin_contact' => ['required'],
            'kin_email' => ['required'],
            'roles' => (isset($this->post()['form_name']) && $this->post()['form_name'] == 'update_profile_form' ? [] : ['required']),
        ];
    }

}
