<?php

namespace App\Http\Requests\Backend\DataSource;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ViewUserRequest.
 */
class ViewDataSourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
