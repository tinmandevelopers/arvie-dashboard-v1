<?php

namespace App\Http\Requests\Backend\User;

use App\Helpers\Auth\SocialiteHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateProfileRequest.
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'alternative_email' => ['required'],
            'contact_number' => ['required'],
            'address' => ['required'],
            'dob' => ['required'],
            'id_number' => ['required'],
            'income_tax_number' => ['required'],
            'kin_first_name' => ['required'],
            'kin_last_name' => ['required'],
            'kin_contact' => ['required'],
            'kin_email' => ['required']
        ];
    }
}
