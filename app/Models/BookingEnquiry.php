<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Campground;
use App\Models\Auth\User;

class BookingEnquiry extends Model
{
	use SoftDeletes;

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_enquiry';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'campground_id',
    	'user_id',
    	'check_in_date',
    	'check_out_date',
    	'name',
    	'address1',
    	'address2',
    	'country_code',
    	'phone',
    	'city',
    	'state',
    	'post_code',
    	'email',
    	'details',
    	'assign_id',
    	'process_by',
    	'details',
    	'status',
    	'index_status',
        'comment',
        'log_file_path',
		'booking_id',
		'token',
    ];

    public function Campground() {
        return $this->hasOne(Campground::class, 'id', 'campground_id');
    }

    public function User() {
        return $this->hasOne(User::class, 'id', 'assign_id');
    }
}
