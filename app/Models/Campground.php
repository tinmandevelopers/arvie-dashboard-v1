<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\DataSource;

/**
 * Class Campground.
 */
class Campground extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campgrounds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['data_source_id', 'campground_group_id', 'title', 'url', 'web_status', 'status', 'index_status', 'last_indexed_time','location'];

    /**
     * @var array
     */
    protected $dates = [
        'last_indexed_time',
    ];

    /**
     * @return mixed
     */
    public function getDataSource() {
        return $this->hasOne(DataSource::class, 'id', 'data_source_id');
    }

     /**
     * @return mixed
     */
    public function getCampGroundGroup() {
        return $this->hasOne(CampgroundGroup::class, 'id', 'campground_group_id');
    }
}
