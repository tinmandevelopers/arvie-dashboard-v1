<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CheckAvailability extends Model
{
    use SoftDeletes;

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'check_availability';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'campground_id',
    	'check_in_date',
    	'check_out_date',
    	'details',
        'result',
        'enquiry_from',
    ];

    public function Campground() {
        return $this->hasOne(Campground::class, 'id', 'campground_id');
    }

}
