<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SoldOutSearch;

class CheckAvailabilityNotification extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'check_availability_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campground_id',
        'location',
        'check_in_date',
        'check_out_date',
        'no_of_days',
        'details',
        'result',
        'enquiry_from',
        'booking_type',
        'check_till',
        'next_schedule_date',
        'day_of_week',
        'first_search_availability',
        'elapsed_date',
        'elapsed_time',
        'positive_hit',
        'user_type',
        'status',
        'duration',
        'sold_out_search_id'
    ];

    public function Campground() {
        return $this->hasOne(Campground::class, 'id', 'campground_id')->with(['getDataSource']);
    }

    public function sosLog() {
        return $this->hasMany(SoldOutSearch::class, 'check_availability_notification_id', 'id');
    }

    public function firstAvailableSOSResult() {
        return $this->sosLog()->where('status', 'Success')->orderBy('id')->limit(1);
    }

    public function notAvailableAfterfirstAvailable($id) {
        return $this->sosLog()->where('status', 'failed')->where('id', '>', $id)->orderBy('id')->limit(1);
    }

}
