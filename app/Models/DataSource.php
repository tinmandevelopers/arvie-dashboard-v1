<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProfileRequest.
 */
class DataSource extends Model {
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_sources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','url','crawler_name','crawler_file','booking_file','crawler_time','last_crawled','crawler_log','status','comment'];

    /**
     * @return mixed
     */
    public function crawlerLog()
    {
        return $this->hasMany(DataSourceLog::class,'data_source_id','id');
    }
}
