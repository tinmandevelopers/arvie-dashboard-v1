<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class DataSourceLog.
 */
class DataSourceLog extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_source_crawler_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['data_source_id','log_file_path','status'];

}
