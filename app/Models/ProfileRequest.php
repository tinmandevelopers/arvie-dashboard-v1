<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfileRequest.
 */
class ProfileRequest extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'first_name', 'last_name', 'screen_name', 'email', 'alternative_email', 'contact_number',
        'address', 'dob', 'id_number', 'income_tax_number', 'kin_first_name', 'kin_last_name',
        'kin_contact', 'kin_email', 'medical_membership_number', 'medical_provider', 'medical_insurance_plan',
        'about_me', 'approved_by', 'reason', 'status', 'specialized_in', 'avatar_location', 'medical_membership_card'];

}
