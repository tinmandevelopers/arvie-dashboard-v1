<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoldOutSearch extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sold_out_search_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'check_availability_notification_id',
        'status',
        'log_time',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'log_time'
    ];

    public function CheckOutAvailabilityNotification()
    {
        return $this->hasOne(CheckAvailabilityNotification::class, 'id', 'check_availability_notification_id')->with(['Campground']);
    }
    
}
