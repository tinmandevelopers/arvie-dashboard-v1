<?php

namespace App\Notifications\Backend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class NewProfileRequest.
 */
class NewProfileRequest extends Notification
{

    use Queueable;

    /**
     * Create a notification instance.
     *
     * @param  string  $data
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->code = 'new_profile_request';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {        
        $subject = __('email.' . $this->code . '.subject');

        $body = __('email.' . $this->code . '.body', ['user_name' => $this->data['user_name']]);

        $url = route('admin.auth.profile-request.edit', ['profile_request' => $this->data['profile_request_id']]);

        $data = ['actionUrl' => $url,
            'displayableActionUrl' => $url
        ];
        $emailData = array_merge($data, $body);

        return (new MailMessage)
                        ->subject($subject)
                        ->markdown('notifications::email', $emailData);
    }

}
