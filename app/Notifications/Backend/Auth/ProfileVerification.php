<?php

namespace App\Notifications\Backend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class ProfileVerification.
 */
class ProfileVerification extends Notification {

    use Queueable;

    /**
     * Create a notification instance.
     *
     * @param  string  $data
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
        $this->code = 'profile_verification';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        $reason = '';

        if (isset($this->data['reason']) && $this->data['reason'] != "") {
            $reason = 'Reason : ' . $this->data['reason'];
        }

        $status = strtolower($this->data['status']);

        $bodyArr = ['status' => $status, 'reason' => $reason];

        $subject = __('email.' . $this->code . '.subject', ['status' => $status]);

        $body = __('email.' . $this->code . '.body', $bodyArr);

        $url = route('frontend.auth.login');

        $data = ['actionUrl' => $url,
            'displayableActionUrl' => $url
        ];
        $emailData = array_merge($data, $body);

        return (new MailMessage)
                        ->subject($subject)
                        ->markdown('notifications::email', $emailData);
    }

}
