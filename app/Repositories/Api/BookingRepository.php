<?php

namespace App\Repositories\Api;

use App\Models\BookingEnquiry;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Classes\General\GeneralCls;
use App\Models\Campground;
use App\Models\CheckAvailability;
use File;
use SebastianBergmann\Environment\Console;
use Storage;

/**
 * Class BookingRepository.
 */
class BookingRepository extends BaseRepository
{

    protected $userReposiotry;

    /**
     * BookingRepository constructor.
     *
     * @param  Campground  $model
     */
    public function __construct(BookingEnquiry $model, UserRepository $userReposiotry)
    {
        $this->model = $model;
        $this->userReposiotry = $userReposiotry;
    }

    /**
     * @param array    $postData
     *  Store new booking Data
     * @return mixed
     */
    public function storeBooking($postData)
    {
        try {
            $postData['assign_id'] = $this->userReposiotry->fetchRandomAgent();
            $create = BookingEnquiry::updateOrCreate(['booking_id' => $postData['booking_id']], $postData);
            // $create = BookingEnquiry::create($postData);
            if ($create) {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $create;
                return $data;
            } else {
                $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

    /**
     * @param array    $postData
     *  Update Booking Data base on id
     * @return mixed
     */
    public function updateBooking($postData, $checktoken)
    {
        try {
            $updateFlag = true;
            if ($checktoken) {
                $bookingData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
                if ($bookingData) {
                    if ($postData['token'] != $bookingData->token) {
                        $updateFlag = false;
                    }
                }
            }
            if ($updateFlag) {
                $update = BookingEnquiry::where('booking_id', $postData['booking_id'])->update($postData);
                if ($update) {
                    $updateData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = $updateData;

                    return $data;
                } else {
                    $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Token Mismatched');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

    /**
     * @param array    $postData
     *  Delete Booking Data base on id
     * @return mixed
     */
    public function deleteBooking($postData)
    {
        try {
            $deleteData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
            if ($deleteData) {
                $result = $deleteData->delete();
                if ($result) {
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = 'Booking Enquiry Delete Successfully';
                    return $data;
                } else {
                    $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'We Cant Found This Booking Enquiry');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }



    /**
     * @param array    $postData
     * Initiate Booking Data base on id
     * @return mixed
     */
    public function initiateBooking($postData)
    {
        try {

            $data = BookingEnquiry::where('booking_id', $postData['booking_id'])->with('Campground')->first();

            $campground = Campground::where('id', $data['campground']['id'])->with('getDataSource')->first();

            $response = [];
            if ($campground) {

                $responseData = [];
                $title = get_datasource_title($campground->getDataSource->url);
                if ($title == 'reserveamerica') {
                    $inDate = date_create($data->check_in_date);
                    $outDate = date_create($data->check_out_date);
                    $diff = date_diff($inDate, $outDate);
                    $days = $diff->format("%a");

                    $daysStr = '/campsites?arrivalDate=' . $data->check_in_date . '&lengthOfStay=' . $days . '&availStartDate=' . $data->check_in_date;
                    $campTxt = strtolower(str_replace(" ", "-", $campground->title));
                    $campname = str_replace("jaxrs-json/facilities", 'explore/' . $campTxt, $campground->url);
                    $campUrl = str_replace('?temp_facility_details_only=true', $daysStr, $campname);
                    $responseData['url'] = $campUrl;
                } else {
                    $responseData['url'] = $campground->url;
                }
                $responseData['title'] = $title;
                $response = GeneralCls::setResponse('success');
                $response['response'] = $responseData;
                return $response;
            } else {
                $response = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $response;
            }
        } catch (\Exception $e) {
            $response = GeneralCls::setResponse('other_error', $e->getMessage());
            return $response;
        }
    }


    /**
     * @param array    $postData
     *  Details of Booking Data base on id
     * @return mixed
     */
    public function detailBooking($postData, $checktoken)
    {
        try {
            $updateFlag = true;
            if ($checktoken) {
                $bookingData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
                if ($bookingData) {
                    if ($postData['token'] != $bookingData->token) {
                        $updateFlag = false;
                    }
                }
            }
            if ($updateFlag) {
                $bookingData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
                $campground = Campground::find($bookingData->campground_id);
                if ($bookingData) {
                    $title = get_datasource_title($campground->getDataSource->url);
                    if ($title == 'recreation' || $title == 'koa' || $title == 'reserveamerica') {
                        $stateCode = DB::table('state')->where('state_name', $bookingData->state)->first();
                        if ($stateCode) {
                            $bookingData->state = $stateCode->state_code;
                        }
                        if ($title == 'recreation' || $title == 'reserveamerica') {
                            $countryCode = DB::table('countries')->where('iso', $bookingData->country_code)->first();
                            if ($countryCode) {
                                $bookingData->country_code = $countryCode->iso3;
                            }
                        }
                    }
                    if ($title == 'sun' ) 
                    {
                        $countryCode = DB::table('countries')->where('iso', $bookingData->country_code)->first();
                        if ($countryCode) {
                            $bookingData->country_code = $countryCode->nicename;
                        }
                    }
                    $formData = set_booking_enquiry_data($bookingData->details, $campground->url);
                    $bookingData->details = $formData;
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = $bookingData;

                    return $data;
                } else {
                    $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Token Mismatched');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

    /**
     * @param array    $postData
     *  Details of Booking Data base on id
     * @return mixed
     */
    public function storeFileBooking($postData)
    {
        try {
            $updateFlag = true;

            $bookingData = BookingEnquiry::where('booking_id', $postData['booking_id'])->first();
            if ($bookingData) {
                if ($postData['token'] != $bookingData->token) {
                    $updateFlag = false;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $data;
            }

            if ($updateFlag) {

                $data = $postData['file_content'];

                $destinationPath = storage_path("booking");
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $fileName =  time() . rand() . '_file.json';
                $file = File::put($destinationPath . $fileName, $data);
                $storeFile = Storage::disk('s3')->put($postData['file_path'], fopen($destinationPath . $fileName, 'r+'), 'public');
                // $storeFile = Storage::disk('s3')->put($postData['file_path'], file_get_contents($destinationPath . $fileName));
                if ($storeFile) {
                    unlink($destinationPath . $fileName);
                    $data = GeneralCls::setResponse('success');
                    $data['response'] = env('AWS_URL') . $postData['file_path'];
                    return $data;
                } else {
                    $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                    return $data;
                }
            } else {
                $data = GeneralCls::setResponse('other_error', 'Token Mismatched');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }


    /**
     * @param array    $postData
     *  Store new booking Availability Data
     * @return mixed
     */
    public function storeBookingAvailability($postData)
    {
        try {
            $create = CheckAvailability::create($postData);
            if ($create) {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $create;
                return $data;
            } else {
                $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }
}
