<?php

namespace App\Repositories\Api;

use App\Repositories\BaseRepository;
use App\Classes\General\GeneralCls;
use App\Models\CheckAvailabilityNotification;
use File;
use Storage;

/**
 * Class CheckAvailabilityNotificationRepository.
 */
class CheckAvailabilityNotificationRepository extends BaseRepository
{


    /**
     * CheckAvailabilityNotificationRepository constructor.
     *
     * @param  Campground  $model
     */
    public function __construct(CheckAvailabilityNotification $model)
    {
        $this->model = $model;
    }

    /**
     * @param array    $postData
     *  Store new CheckAvailabilityNotification Data
     * @return mixed
     */
    public function store($postData)
    {
        try {

            $create = CheckAvailabilityNotification::create($postData);

            if ($create) {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $create;
                return $data;
            } else {
                $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

    /**
     * @param array    $postData
     *  Update CheckAvailabilityNotification Data base on id
     * @return mixed
     */
    public function update($postData)
    {
        try {
//            $update = CheckAvailabilityNotification::where('id', $postData['id'])->update($postData);
            $update = CheckAvailabilityNotification::where('sold_out_search_id', $postData['sold_out_search_id'])->update($postData);
            if ($update) {
                $data = GeneralCls::setResponse('success');
                $data['response'] = $update;
                return $data;
            } else {
                $data = GeneralCls::setResponse('other_error', 'Something Went Wrong');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }
}
