<?php

namespace App\Repositories\Backend\Auth;

use App\Events\Backend\Auth\User\UserConfirmed;
use App\Events\Backend\Auth\User\UserCreated;
use App\Events\Backend\Auth\User\UserDeactivated;
use App\Events\Backend\Auth\User\UserPasswordChanged;
use App\Events\Backend\Auth\User\UserPermanentlyDeleted;
use App\Events\Backend\Auth\User\UserReactivated;
use App\Events\Backend\Auth\User\UserRestored;
use App\Events\Backend\Auth\User\UserUnconfirmed;
use App\Events\Backend\Auth\User\UserUpdated;
use App\Exceptions\GeneralException;
use App\Models\Auth\User;
use App\Notifications\Backend\Auth\UserAccountActive;
use App\Notifications\Frontend\Auth\UserNeedsPasswordReset;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Auth;
use App\Classes\General\GeneralCls;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository {

    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(User $model) {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount(): int {
        return $this->model
                        ->where('confirmed', false)
                        ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getUsersPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator {
        return $this->model
                        ->with('roles', 'permissions', 'providers')
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator {
        return $this->model
                        ->with('roles', 'permissions', 'providers')
                        ->active(false)
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator {
        return $this->model
                        ->with('roles', 'permissions', 'providers')
                        ->onlyTrashed()
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(array $data, $image = false): User {
        return DB::transaction(function () use ($data, $image) {
                    if (!isset($data['roles'])) {
                        $data['roles'] = [2];
                    }
                    $user = $this->model::create($data);

                    if ($user) {
                        // User must have at least one role
                        if (!count($data['roles'])) {
                            throw new GeneralException(__('exceptions.backend.access.users.role_needed_create'));
                        }

                        // Add selected roles
                        $user->syncRoles($data['roles']);
                        $user->givePermissionTo('view backend');

                        //Send confirmation email if requested and account approval is off
                        Password::broker()->sendResetLink(['email' => $data['email']]);
//                            $user->notify(new UserNeedsPasswordReset($user->confirmation_code));

                        event(new UserCreated($user));

                        return $user;
                    }

                    throw new GeneralException(__('exceptions.backend.access.users.create_error'));
                });
    }

    /**
     * @param User  $user
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function update(User $user, array $data, array $roles): User {

        return DB::transaction(function () use ($user, $data, $roles) {
                    if ($user->update($data)) {

                        if (count($roles) > 0) {
                            // Add selected roles/permissions
                            $user->syncRoles($roles);
                            $user->givePermissionTo('view backend');
//                        $user->syncPermissions($data['permissions']);
                        }

                        event(new UserUpdated($user));

                        return $user;
                    }

                    throw new GeneralException(__('exceptions.backend.access.users.update_error'));
                });
    }

    /**
     * @param User $user
     * @param      $input
     *
     * @throws GeneralException
     * @return User
     */
    public function updatePassword(User $user, $input): User {
        if ($user->update(['password' => $input['password']])) {
            event(new UserPasswordChanged($user));

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.update_password_error'));
    }

    /**
     * @param User $user
     * @param      $status
     *
     * @throws GeneralException
     * @return User
     */
    public function mark(User $user, $status): User {
        if ($status === 0 && auth()->id() === $user->id) {
            throw new GeneralException(__('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $user->active = $status;

        switch ($status) {
            case 0:
                event(new UserDeactivated($user));
                break;
            case 1:
                event(new UserReactivated($user));
                break;
        }

        if ($user->save()) {
            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param User $user
     *
     * @throws GeneralException
     * @return User
     */
    public function confirm(User $user): User {
        if ($user->confirmed) {
            throw new GeneralException(__('exceptions.backend.access.users.already_confirmed'));
        }

        $user->confirmed = true;
        $confirmed = $user->save();

        if ($confirmed) {
            event(new UserConfirmed($user));

            // Let user know their account was approved
            if (config('access.users.requires_approval')) {
                $user->notify(new UserAccountActive);
            }

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.cant_confirm'));
    }

    /**
     * @param User $user
     *
     * @throws GeneralException
     * @return User
     */
    public function unconfirm(User $user): User {
        if (!$user->confirmed) {
            throw new GeneralException(__('exceptions.backend.access.users.not_confirmed'));
        }

        if ($user->id === 1) {
            // Cant un-confirm admin
            throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm_admin'));
        }

        if ($user->id === auth()->id()) {
            // Cant un-confirm self
            throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm_self'));
        }

        $user->confirmed = false;
        $unconfirmed = $user->save();

        if ($unconfirmed) {
            event(new UserUnconfirmed($user));

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm'));
    }

    /**
     * @param User $user
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function forceDelete(User $user): User {
        if ($user->deleted_at === null) {
            throw new GeneralException(__('exceptions.backend.access.users.delete_first'));
        }

        return DB::transaction(function () use ($user) {
                    // Delete associated relationships
                    $user->passwordHistories()->delete();
                    $user->providers()->delete();

                    if ($user->forceDelete()) {
                        event(new UserPermanentlyDeleted($user));

                        return $user;
                    }

                    throw new GeneralException(__('exceptions.backend.access.users.delete_error'));
                });
    }

    /**
     * @param User $user
     *
     * @throws GeneralException
     * @return User
     */
    public function restore(User $user): User {
        if ($user->deleted_at === null) {
            throw new GeneralException(__('exceptions.backend.access.users.cant_restore'));
        }

        if ($user->restore()) {
            event(new UserRestored($user));

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param User $user
     * @param      $email
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail(User $user, $email) {
        // Figure out if email is not the same and check to see if email exists
        if ($user->email !== $email && $this->model->where('email', '=', $email)->first()) {
            throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
        }
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTable($data) {
        if (auth()->user()->isAdmin()) {
            return $this->model->with('roles', 'permissions', 'providers');
        } else {
            return $this->model->where('active', 1)->whereHas(
                            'roles', function($q) {
                        $q->where('name', '!=', config('access.users.admin_role'));
                    })->with('roles', 'permissions', 'providers');
        }
    }

    /**
     * @param array    $postData 
     * this function is used for login api 
     *
     * @return mixed
     */
    public function login($postData) {
        try {
            $condition = Auth::attempt(['email' => $postData['email'], 'password' => $postData['password']]);
            $user = [];
            if ($condition) {
                $user = Auth::user();
                $user->token = $user->createToken('authToken')->accessToken;
                $data = GeneralCls::setResponse('success');
                $data['response'] = $user;
                return $data;
            } else {
                $data = GeneralCls::setResponse('other_error', 'Wrong Credential');
                return $data;
            }
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

    /**
     * this function is used for fetch agent Id
     *
     * @return mixed
     */
    public function fetchRandomAgent() {
        try {
            $blueAgent = $this->model->whereHas(
                            'roles', function($q) {
                        $q->where('name', config('access.users.blue_agent_role'));
                    })->inRandomOrder()->limit(1)->get('id');
            if (count($blueAgent) > 0 && isset($blueAgent[0])) {
                return $blueAgent[0]->id;
            }
            
            $primeAgent = $this->model->whereHas(
                            'roles', function($q) {
                        $q->where('name', config('access.users.prime_agent_role'));
                    })->inRandomOrder()->limit(1)->get('id');
            if (count($primeAgent) > 0 && isset($primeAgent[0])) {
                return $primeAgent[0]->id;
            }
            
            $agent = $this->model->whereHas(
                            'roles', function($q) {
                        $q->where('name', config('access.users.agent_role'));
                    })->inRandomOrder()->limit(1)->get('id');
            return (count($agent) > 0 && isset($agent[0])) ? $agent[0]->id : '';
        } catch (\Exception $e) {
            $data = GeneralCls::setResponse('other_error', $e->getMessage());
            return $data;
        }
    }

}
