<?php

namespace App\Repositories\Backend;

use App\Models\BookingEnquiry;
use App\Models\Campground;
use App\Models\Auth\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Models\CheckAvailability;

/**
 * Class BookingEnquiryRepository.
 */
class BookingEnquiryRepository extends BaseRepository
{

    /**
     * BookingEnquiry Model constructor.
     *
     * @param  BookingEnquiry  $model
     */
    public function __construct(BookingEnquiry $model)
    {
        $this->model = $model;
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTable($data)
    {
        $booking = $this->model->with(['Campground', 'User']);
        if (isset($data['fromPage']) && $data['fromPage'] == "dashboard") {
            $booking->whereIn('index_status', ['Pending', 'InProgress']);
        }
        if (auth()->user()->roles()->get()[0]->name == config('access.users.blue_agent_role')) {
            $booking->where('assign_id', auth()->user()->id);
        }
        $booking->orderBy('updated_at');
        return $booking->get();
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTableAvailability($data)
    {
        $log = CheckAvailability::with(['Campground']);
       
        return $log->get();
    }
    
    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getBookingEnquiryDetails($data)
    {
        return $this->model->with(['Campground'])->find($data->enquiry);
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function setBookingToken(BookingEnquiry $bookingEnquiry, $data)
    {

        $user = User::find(auth()->user()->id);
        $token = $user->token;
        $enquiry = $this->model::where('id', $data)->update(['token' => $user->token, 'index_status' => 'InProgress','process_by'=>$user->id]);

        if ($enquiry) {
            return $token;
        }
        throw new GeneralException(__('exceptions.backend.access.booking-enquiry.update_token_error'));
    }
    
     /**
     * @param object    $data
     *
     * @return mixed
     */
    public function getCheckAvailablityLogDetails($data)
    {
        return CheckAvailability::with(['Campground'])->find($data->log);
    }
    
}
