<?php

namespace App\Repositories\Backend;

use App\Models\Campground;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;

/**
 * Class CampgroundRepository.
 */
class CampgroundRepository extends BaseRepository
{

    /**
     * CampgroundRepository constructor.
     *
     * @param  Campground  $model
     */
    public function __construct(Campground $model)
    {
        $this->model = $model;
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTable($data)
    {
        if (isset($data['dataSource'])) {
            return $this->model->where('data_source_id', $data['dataSource'])->get();
        } else {
            return $this->model->get();
        }
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(array $data, $image = false): Campground
    {
        return DB::transaction(function () use ($data, $image) {
            $campground = $this->model::create($data);

            if ($campground) {

                return $campground;
            }

            throw new GeneralException(__('exceptions.backend.access.data-source.create_error'));
        });
    }

    /**
     * @param Campground  $campground
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return Campground
     */
    public function update(Campground $campground, array $data): Campground
    {

        return DB::transaction(function () use ($campground, $data) {
            if ($campground->update($data)) {

                return $campground;
            }

            throw new GeneralException(__('exceptions.backend.access.data-source.update_error'));
        });
    }

    /**
     * @param Campground $campground
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return Campground
     */
    public function delete(Campground $campground): Campground
    {

        return DB::transaction(function () use ($campground) {
            $id = $campground->delete();
            if ($id) {
                return $campground;
            }
            throw new GeneralException(__('exceptions.backend.access.data-source.delete_error'));
        });
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getCampgroundForCompare()
    {
        return $this->model->select(
            '*',
            'title',
            DB::raw('group_concat(concat(id,"||",status,"||",data_source_id)) as data_sources'),
            DB::raw('count(distinct data_source_id) as cnt')
        )
            ->groupBy('title')->having('cnt', '>', 1)->orderBy('cnt', 'desc')->get();
    }

    /**
     * @param object    $data
     *
     * @return mixed
     */
    public function search($request)
    {
        $search = $request->q;
        return $this->model->where("title", "LIKE", "%{$search}%")->where('web_status','Listed')->where('status','Active')->with(['getCampGroundGroup'])->get();
    }

     /**
     * @param object    $data
     *
     * @return mixed
     */
    public function searchEquipement($request)
    {
        $title = get_datasource_title($request->camp_url);
        $path = storage_path() . "/booking/bookingCheckAvailabilityFormData.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $params = json_decode(file_get_contents($path), true);
        $equipement=($params[$title.'_equipement']);
        return $equipement;
    }
    
}
