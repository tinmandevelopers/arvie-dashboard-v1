<?php

namespace App\Repositories\Backend;

use App\Models\Auth\User;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Models\CheckAvailabilityNotification;
use App\Models\SoldOutSearch;

/**
 * Class CheckAvailabilityNotificationRepository.
 */
class CheckAvailabilityNotificationRepository extends BaseRepository
{

    /**
     * CheckAvailabilityNotificationRepository constructor.
     *
     * @param  DataSource  $model
     */
    public function __construct(CheckAvailabilityNotification $model)
    {
        $this->model = $model;
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTableAvailability($data)
    {
        $log = CheckAvailabilityNotification::with(['Campground'])->orderBy('id', 'desc');

        return $log->get();
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(array $data): CheckAvailabilityNotification
    {
        return DB::transaction(function () use ($data) {
            $result = $this->model::create($data);

            if ($result) {
                return $result;
            }

            throw new GeneralException(__('exceptions.backend.access.data-source.create_error'));
        });
    }

    /**
     * @param object    $data
     *
     * @return mixed
     */
    public function getAvailabilityNotificationDetails($data)
    {

        return CheckAvailabilityNotification::with(['Campground'])->find($data->log);
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTableAvailabilityLog($data)
    {

        $logList = SoldOutSearch::where('check_availability_notification_id', $data['dataSource']);

        return $logList->get();
    }



    /**
     * @param object    $data
     *
     * @return mixed
     */
    public function delete($data)
    {

        $id =  SoldOutSearch::where('check_availability_notification_id', $data->log)->delete();
        if ($id) {
            $result = CheckAvailabilityNotification::where('id', $data->log)->delete();
            if ($result) {
                return $result;
            } else {
                throw new GeneralException(__('exceptions.backend.access.data-source.delete_error'));
            }
        } else {
            throw new GeneralException(__('exceptions.backend.access.data-source.delete_error'));
        }
    }
}
