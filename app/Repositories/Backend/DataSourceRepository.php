<?php

namespace App\Repositories\Backend;

use App\Models\Auth\User;
use App\Models\DataSource;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;

/**
 * Class DataSourceRepository.
 */
class DataSourceRepository extends BaseRepository {

    /**
     * DataSourceRepository constructor.
     *
     * @param  DataSource  $model
     */
    public function __construct(DataSource $model) {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getDataSources() {
        return $this->model->pluck('title', 'id');
    }

    /**
     * @param array    $data
     *
     * @return mixed
     */
    public function getForDataTable($data) {
        return $this->model->get();
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(array $data, $image = false): DataSource {
        return DB::transaction(function () use ($data, $image) {
                    $dataSource = $this->model::create($data);

                    if ($dataSource) {

                        return $dataSource;
                    }

                    throw new GeneralException(__('exceptions.backend.access.data-source.create_error'));
                });
    }

    /**
     * @param DataSource  $dataSource
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return DataSource
     */
    public function update(DataSource $dataSource, array $data): DataSource {

        return DB::transaction(function () use ($dataSource, $data) {
                    if ($dataSource->update($data)) {

                        return $dataSource;
                    }

                    throw new GeneralException(__('exceptions.backend.access.data-source.update_error'));
                });
    }

    /**
     * @param DataSource $dataSource
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return DataSource
     */
    public function delete(DataSource $dataSource): DataSource {

        return DB::transaction(function () use ($dataSource) {
                    $id = $dataSource->delete();
                    if ($id) {
                        return $dataSource;
                    }
                    throw new GeneralException(__('exceptions.backend.access.data-source.delete_error'));
                });
    }

}
