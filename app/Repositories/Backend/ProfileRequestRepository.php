<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Auth\ProfileRequest\ProfileRequestCreated;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\ProfileRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Auth\User;
use App\Notifications\Backend\Auth\NewProfileRequest;

/**
 * Class ProfileRequestRepository.
 */
class ProfileRequestRepository extends BaseRepository
{

    /**
     * ProfileRequestRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(profileRequest $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount(): int
    {
        return $this->model->where('status', 'Pending')->count();
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     * @throws \Throwable
     * @return User
     */
    public function create(ProfileRequest $profileRequest, array $data, $user)
    {
        return DB::transaction(function () use ($data, $user) {
                    $profileRequest = $this->model::updateOrCreate(['user_id' => $data['user_id']], $data);

                    if ($profileRequest) {

                        //Send new request email to all admin users
                        //$profileRequest->notify(new UserNeedsPasswordReset($profileRequest->confirmation_code));

                        event(new ProfileRequestCreated($profileRequest));


                        $admins = User::whereHas(
                                        'roles', function($q) {
                                    $q->where('name', config('access.users.admin_role'));
                                }
                                )->get();
                      
                        $notificationData['profile_request_id'] = $profileRequest->id;
                        $notificationData['user_name'] = $user->name;
                        foreach ($admins as $admin) {
                            $admin->notify(new NewProfileRequest($notificationData));
                        }

                        return $profileRequest;
                    }

                    throw new GeneralException(__('exceptions.backend.access.users.create_error'));
                });
    }

    /**
     * @param ProfileRequest $profileRequest
     *
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     * @return ProfileRequest 
     */
    public function forceDelete(ProfileRequest $profileRequest)
    {
        return DB::transaction(function () use ($profileRequest) {
                    if ($profileRequest->forceDelete()) {
                        return $profileRequest;
                    }

                    throw new GeneralException(__('exceptions.backend.access.users.delete_error'));
                });
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($data)
    {
        return $this->model->where('status', 'Pending');
    }

}
