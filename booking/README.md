### Setup Node for scrapping

## For Windows
# Install nodeJs and NPM
https://nodejs.org/en/download/
# Node version should v15.*

cd {path}\booking\

npm install

# Create temp folder under booking/scrapping/
mkdir {path}\booking\scrapping\temp

npm install forever -g

# Run below command for run script
forever start app.js 


## For Ubuntu
sudo apt-get install npm

sudo apt-get -y install libnspr4 libnss3 libnss3-tools libatk1.0-0 libatk-bridge2.0-0 libxkbcommon-x11-0 libgtk-3-0 libgbm-dev

## If required chromium browser then install
sudo apt-get install chromium-browser

cd {path}\booking\

npm install

# Create temp folder under booking/scrapping/
mkdir {path}\booking\scrapping\temp

npm install forever -g

# Run below command for run script
forever start app.js 
