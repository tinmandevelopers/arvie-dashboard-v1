var express = require('express');
var app = express();

var common = require('./utils/common')
var appUrl = common.appUrl;
var bookingApi = require('./utils/bookingapi')

app.get('/:campground_id/:booking_id/:token', async function (req, res) {

    var bookingId = req.params.booking_id;
    var campgroundId = req.params.campground_id;
    var token = req.params.token;
// console.log('in coming')
    //Calling Api for fetching booking title and url
    var initiate = await bookingApi.bookingInitiate(appUrl + '/api/booking/enquiry/initiate', bookingId);
    // console.log(initiate);

    if (initiate && initiate.code == 200) {

        var title = initiate.response.title;
        var siteUrl = initiate.response.url.replace(/&/g,'@@');
        
        var cmd = 'node '+title+'BookingReserve.js --campground_id=' + campgroundId + ' --booking_id=' + bookingId + ' --token=' + token + ' --site_url=' + siteUrl;
 
        const exec = require('child_process').exec;
        const myShellScript = exec(cmd);
        myShellScript.stdout.on('data', (data) => {
//            res.write(`</br> ${data} </br>`);
            console.log(data)
            //  res.send(data)   
        });
        myShellScript.stderr.on('data', (data) => {
            console.error(data);
        });

        res.send('ok');
        // const exec = require('child_process').exec;
        // const myShellScript = exec(cmd);

        // myShellScript.stdout.on('data', (data) => {
        //     // res.send(data)
        //     console.log(data);
        //     // do whatever you want here with data
        // });
        // myShellScript.stderr.on('data', (data) => {

        //     console.error(data);
        // });

    } else {
        var message = (initiate.message) ? initiate.message : 'Something Went Wrong';
        res.send(message);
    }

});

app.listen(3000);
