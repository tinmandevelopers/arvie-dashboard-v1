var common = require('./utils/common')

var configPath = common.configPath;
var bookingApi = require(configPath + '/utils/bookingapi')

const puppeteer = common.puppeteer;
var express = common.express;
var fs = common.fs;
const args = common.args;
const path = common.path;

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var siteUrl = args.site_url.replace(/@@/g, '&');
var token = args.token;

var comment = common.comment;
var debugLog = common.debugLog;
var browseClose = true;
var appUrl = common.appUrl;

var fetchBookingFormDataUrl = common.fetchBookingFormDataUrl;
var updateBookingUrl = common.updateBookingUrl;
var bookingStatus = true;

async function getEnquiryDataID() {
    console.log(fetchBookingFormDataUrl)
    var formData = await bookingApi.fetchBookingFormData(fetchBookingFormDataUrl, bookingId, token)
    // console.log(formData);
    if (formData && formData.code == 200) {
        var data = formData.response;

        common.form_siteCategory = data['details']['reservation_type'];
        // console.log(common.form_siteCategory);
        common.form_checkIn = data['check_in_date'].replace(/-/g, '/.');
        common.form_checkOut = data['check_out_date'].replace(/-/g, '/');
        common.form_adults = data['details']['adults'];
        common.form_petGroup = data['details']['pet_friendly'];
        common.form_equipmentType = data['details']['equipment_type'];
        common.form_equipmentLength = data['details']['length'];
        common.form_slideOutsGroup = data['details']['slide_out'];
        var name = data['name'].split(' ');
        common.form_firstName = name[0];
        common.form_lastName = name[1];
        common.form_address1 = data['address1'];
        common.form_address2 = data['address2'];
        common.form_countryCode = data['country_code'];
        common.form_phone = data['phone'];
        common.form_city = data['city'];
        common.form_state = data['state'];
        common.form_postcode = data['post_code'];
        common.form_email = data['email'];
        common.form_confirmEmail = data['email'];
        common.form_campsite = data['details']['campsite'];
        var cardData = await bookingApi.fetchCardDetail(data['details']['card_detail']['token'], data['details']['card_detail']['user'], data['details']['card_detail']['passphrase'],data['details']['card_detail']['reference']);
        // console.log(cardData);

    } else {
        var message = (formData.message) ? formData.message : 'Something Went Wrong';
        console.log(message)
        comment = message;
        bookingStatus = false;
        debugLog.push(await bookingApi.dateTime() + ":" + message);
    }

}

(async () => {
    try {
        // console.log('Time is '+await bookingApi.dateTime())
        await getEnquiryDataID();

        if (bookingStatus) {
            // let rawdata = fs.readFileSync('scrapping/koa_camp_link.json');
            // campgrounds = JSON.parse(rawdata);
            // for (var p = 0; p < campgrounds.length; p++) {

            var browser = await puppeteer.launch({
                headless: false,
                args: [
                    '--start-maximized' // you can also use '--start-fullscreen'
                ]
                // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
            });
            console.log('*************************');
            console.log(await browser.version());
            debugLog.push(await bookingApi.dateTime() + ":" + await browser.version());
            // open a new page
            var page = await browser.newPage();

            try {
                // console.log('Nevigating : ' + campgrounds[p].title)
                console.log('Nevigating : ' + siteUrl)
                debugLog.push(await bookingApi.dateTime() + ":" + 'Nevigating : ' + siteUrl);
                await page.setViewport({ width: 1366, height: 768 });

                await page.setDefaultNavigationTimeout(0);

                var browseUrl = siteUrl + 'reserve';
                // var browseUrl = campgrounds[p].link + 'reserve';
                debugLog.push(await bookingApi.dateTime() + ":" + 'Url is : ' + browseUrl);

                await page.goto(browseUrl, { waitUntil: 'networkidle2' });
                if (await page.$('#reserveForm') !== null) {

                    debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling page");
                    await page.evaluate(() => {
                        document.querySelector('.nav-steps').scrollIntoView()
                    })

                    // await page.click("#Reservation_CheckInDate");
                    // // await bookingApi.sleep(1000)
                    // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(2) > td:nth-child(4)");
                    // await bookingApi.sleep(1000)

                    // await page.click("#Reservation_CheckOutDate");
                    // // await bookingApi.sleep(1000)
                    // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(4) > td:nth-child(4) > a");
                    // await bookingApi.sleep(1000)
                    if (await page.$('#Reservation_SiteCategory') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Reservation SiteCategory");
                        await page.select('#Reservation_SiteCategory', common.form_siteCategory)
                        await bookingApi.sleep(1000)
                    }

                    if (await page.$('#Reservation_CheckInDate') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Storing Check In Date");
                        await page.click("#Reservation_CheckInDate");
                        await page.type("#Reservation_CheckInDate", common.form_checkIn);
                        await bookingApi.sleep(1000)
                    }


                    if (await page.$('#Reservation_CheckOutDate') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Storing Check Out Date");
                        await page.click("#Reservation_CheckOutDate");
                        await page.type("#Reservation_CheckOutDate", common.form_checkOut);
                        await page.click(".reserve-top-text");
                        await bookingApi.sleep(1000);
                    }


                    if (await page.$('#Reservation_Adults') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Storing Adult");
                        await page.click("#Reservation_Adults");
                        // console.log('adult is '+form_adults)
                        await page.type("#Reservation_Adults", common.form_adults);
                        // await bookingApi.sleep(1000)
                    }


                    if (await page.$('#Reservation_Kids') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Storing Kids");
                        await page.click("#Reservation_Kids");
                        await page.type('#Reservation_Kids', common.form_kids);
                        // await bookingApi.sleep(1000)
                    }


                    if (await page.$('#Reservation_Free') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Storing Free");
                        await page.click("#Reservation_Free");
                        await page.type('#Reservation_Free', common.form_free);
                        // await bookingApi.sleep(1000)
                    }

                    if (await page.$('#Reservation_Pets_Group') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Pet Group");
                        await page.click('#Reservation_Pets_Group > label:nth-child(' + common.form_petGroup + ')');
                        // await bookingApi.sleep(1000)
                    }

                    if (await page.$('#Reservation_EquipmentType') !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Equipment Type");
                        await page.select('#Reservation_EquipmentType', common.form_equipmentType)
                        await bookingApi.sleep(1000)
                    }

                    if (await page.$('#Reservation_EquipmentLength') !== null) {
                        var length = await isLocatorReady('#reserve-equipment-length', page);
                        if (length) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Storing Equipment Length");
                            await page.click("#Reservation_EquipmentLength");
                            await page.type('#Reservation_EquipmentLength', common.form_equipmentLength)
                        }
                        // await bookingApi.sleep(1000)
                    }

                    if (await page.$('#Reservation_SlideOuts_Group') !== null) {
                        var sildeout = await isLocatorReady('#reserve-equipment-slideouts', page);
                        if (sildeout) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Slideouts Group");
                            await page.click('#Reservation_SlideOuts_Group > label:nth-child(' + common.form_slideOutsGroup + ')');

                        }
                        // await bookingApi.sleep(1000)
                    }

                    await page.evaluate(() => {
                        document.querySelector('#reserve-step-vkr').scrollIntoView()
                    })


                    // const VkrNumber = await page.waitForSelector("#Reservation_VkrNumber");
                    // await page.click("#Reservation_VkrNumber");
                    // await VkrNumber.type("12345");
                    // await bookingApi.sleep(1000)

                    // if (await page.$('#Reservation_VkrPostalCode') !== null) {
                    //     debugLog.push(await bookingApi.dateTime() + ":" + "Storing Vkr Postal code");
                    //     await page.click("#Reservation_VkrPostalCode");
                    //     await page.type('#Reservation_VkrPostalCode', common.form_VkrPostalCode);
                    //     // await bookingApi.sleep(1000)
                    // }

                    // if (await page.$('#Reservation_PurchaseVkr_Group') !== null) {
                    //     debugLog.push(await bookingApi.dateTime() + ":" + "Storing Vkr Purchase Group");
                    //     await page.click('#Reservation_PurchaseVkr_Group > label:nth-child(' + common.form_purchaseVkr_Group + ')');
                    //     await bookingApi.sleep(1000)
                    // }

                    debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on next Button");
                    await page.click('#nextButton');
                    await bookingApi.sleep(5000)

                    if (await page.$(".validation-summary-errors") !== null) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Fetching Validation errors ");

                        let element = await page.$('.validation-summary-errors');
                        let errorMsg = await page.evaluate(el => el.textContent, element)
                        comment = errorMsg;
                        bookingStatus = false;
                        console.log(errorMsg);
                    } else {

                        debugLog.push(await bookingApi.dateTime() + ":" + "Waiting For next Page ");
                        await bookingApi.sleep(10000)



                        if (await page.$("#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button") !== null) {
                            var campsiteflag = false;
                            let elements = await page.$$("#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button");
                            var count = 2;
                            for (const el of elements) {
                                count++;

                                if (await page.$('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div') !== null) {

                                    var element = await page.$('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div');
                                    // console.log('element')
                                    // console.log('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div')
                                    if (await element.$('div:nth-child(2) > div > h4') !== null) {
                                        var title = await element.$('div:nth-child(2) > div > h4');
                                        var titleInfo = await page.evaluate(el => el.textContent, title);
                                        // console.log(titleInfo.trim() + "==" + common.form_campsite.trim())
                                        if (titleInfo.trim() == common.form_campsite.trim()) {
                                            console.log('Campsite matched:' + titleInfo)
                                            campsiteflag = true;
                                            await el.click();
                                            break;
                                        }
                                    }

                                }
                            }
                            if (campsiteflag) {

                            } else {
                                if (common.form_campsite == '') {
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on reserve Button ");
                               
                                    console.log('campsite is not provided so selecting first available site');
                                    campsiteflag = true;

                                    await page.click('#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button');
                                    
                                }
                            }
                            /* start 

                            debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling Page ");
                            await page.evaluate(() => {
                                document.querySelector('#site-types > div.row.no-padding > div > div').scrollIntoView()
                            })
                            await bookingApi.sleep(2000)

                            debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on reserve Button ");
                            await page.click('#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button');
                            
                            */
                            if (campsiteflag) {
                                await bookingApi.sleep(5000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Waiting For Next Page")

                                await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                                await page.evaluate(() => {
                                    document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                                })

                                debugLog.push(await bookingApi.dateTime() + ":" + "In Step 3")
                                await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing First Name")
                                const firstname = await page.waitForSelector("#Reservation_FirstName");
                                await page.click("#Reservation_FirstName");
                                await firstname.type(common.form_firstName)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Last Name")
                                const lastname = await page.waitForSelector("#Reservation_LastName");
                                await page.click("#Reservation_LastName");
                                await lastname.type(common.form_lastName)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Address 1")
                                const address1 = await page.waitForSelector("#Reservation_Address1");
                                await page.click("#Reservation_Address1");
                                await address1.type(common.form_address1)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Address 2")
                                const address2 = await page.waitForSelector("#Reservation_Address2");
                                await page.click("#Reservation_Address2");
                                await address2.type(common.form_address2)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Coontry Code")
                                await page.select('#Reservation_CountryCode', common.form_countryCode)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Phone Number")
                                const phone = await page.waitForSelector("#Reservation_PhoneNumber");
                                await page.click("#Reservation_PhoneNumber");
                                await phone.type(common.form_phone)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing City")
                                const city = await page.waitForSelector("#Reservation_City");
                                await page.click("#Reservation_City");
                                await city.type(common.form_city)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling Page")
                                await page.waitForSelector("#reserve-step3-form");
                                await page.evaluate(() => {
                                    document.querySelector('#reserve-step3-form').scrollIntoView()
                                })

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing State")
                                const state = await page.waitForSelector("#Reservation_StateProvinceCode");
                                await page.click("#Reservation_StateProvinceCode");
                                await state.type(common.form_state)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Postal Code")
                                const postcode = await page.waitForSelector("#Reservation_PostalCode");
                                await page.click("#Reservation_PostalCode");
                                await postcode.type(common.form_postcode)
                                // await bookingApi.sleep(1000)


                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Reservastion Email")
                                const resemail = await page.waitForSelector("#Reservation_EmailAddress");
                                await page.click("#Reservation_EmailAddress");
                                await resemail.type(common.form_email)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storing Reservastion Confirm Email");
                                const resconfemail = await page.waitForSelector("#Reservation_ConfirmEmailAddress");
                                await page.click("#Reservation_ConfirmEmailAddress");
                                await resconfemail.type(common.form_confirmEmail)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling Page");
                                await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                                await page.evaluate(() => {
                                    document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                                })

                                debugLog.push(await bookingApi.dateTime() + ":" + "Storign Credit Card Number");
                                const creditcard = await page.waitForSelector("#Reservation_CreditCardNumber");
                                await page.click("#Reservation_CreditCardNumber");
                                await creditcard.type(String(common.form_creditCard))

                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Card Type");
                                await page.select('#Reservation_CreditCardType', common.form_creditCardType)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Card Expiration Month");
                                await page.select('#Reservation_CreditCardExpMonth', common.form_creditCardExpMonth)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Card Expiration Year");
                                await page.select('#Reservation_CreditCardExpYear', common.form_creditCardExpYear)
                                // await bookingApi.sleep(1000)


                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Card Security Code");
                                const cardcode = await page.waitForSelector("#Reservation_CreditCardSecurityCode");
                                await page.click("#Reservation_CreditCardSecurityCode");
                                await cardcode.type(common.form_creditCardCode)
                                // await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling the page");
                                await page.waitForSelector("#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox");
                                await page.evaluate(() => {
                                    document.querySelector('#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox').scrollIntoView()
                                })

                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Tearms and Condition");
                                await page.click("#Reservation_TermsAgree");
                                await bookingApi.sleep(1000)

                                debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on Continue buttom");
                                await page.click('#continueButton')
                                await bookingApi.sleep(3000)

                                if (await page.$(".validation-summary-errors") !== null) {
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Fetching Validation errors ");

                                    let element = await page.$('.validation-summary-errors');
                                    let errorMsg = await page.evaluate(el => el.textContent, element)
                                    comment = errorMsg;
                                    bookingStatus = false;
                                    console.log(errorMsg);
                                } else {
                                    await bookingApi.sleep(3000)
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Scrolling Page");

                                    await page.waitForSelector("#bookButton");
                                    await page.evaluate(() => {
                                        document.querySelector('#bookButton').scrollIntoView()
                                    })

                                    await bookingApi.sleep(1000)
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on Booking Button");
                                    await page.click('#bookButton')
                                    await bookingApi.sleep(1000)
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Booking is done for this campground");

                                    console.log('Booking is Done for this campground')
                                    comment = 'Booking is done for this campground';
                                    bookingStatus = true;
                                    browseClose = false;
                                }
                            } else {
                                console.log('No Campsite matched with you provided are available');
                                comment = "No Campsite Match with you provides";
                                bookingStatus = false;
                                flag = true;
                            }

                        } else {
                            var firstBlockSelector = '';

                            debugLog.push(await bookingApi.dateTime() + ":" + "checking Site availability");
                            if (await page.$(".reserve-sitetype-unavailable") !== null) {
                                firstBlockSelector = '.reserve-sitetype-unavailable';
                            } else {
                                firstBlockSelector = '.reserve-sitetype-special';
                            }
                            debugLog.push(await bookingApi.dateTime() + ":" + "scrolling the Page");
                            await page.waitForSelector(firstBlockSelector);
                            await page.evaluate((firstBlockSelector) => {
                                document.querySelector(firstBlockSelector + '> div:nth-child(2)').scrollIntoView()
                            }, firstBlockSelector)
                            await bookingApi.sleep(1000)

                            debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on Avaiblity button");

                            await page.click(firstBlockSelector + ' > div:nth-child(2) > div >div:nth-child(1) > div >div:nth-child(2) > a')
                            await page.waitForSelector("#modal-window-iframe-lg-title");

                            const elementHandle = await page.$('#modal-window-iframe-lg > div > div > iframe');
                            const frame = await elementHandle.contentFrame();
                            // console.log('frame')
                            // console.log(frame)

                            debugLog.push(await bookingApi.dateTime() + ":" + "Popup open with dates");
                            await frame.waitForSelector('#availability-form-wrapper'),
                                await bookingApi.sleep(5000)

                            var firstavailableDate = '#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability';
                            var flag = false;
                            if (await frame.$(".validation-summary-errors") !== null) {

                                debugLog.push(await bookingApi.dateTime() + ":" + "Fetching Validation Errors");

                                let element = await frame.$('.validation-summary-errors');
                                let errorMsgPopup = await frame.evaluate(el => el.textContent, element)
                                console.log(errorMsgPopup);
                                comment = errorMsgPopup;
                                bookingStatus = false;
                            }
                            while (flag == false) {
                                if (await frame.$(firstavailableDate) != null) {
                                    flag = true;
                                    await frame.waitForSelector(firstavailableDate)
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Fetching Date Where campground Available from")
                                    const dateValue = await frame.evaluate('document.querySelector("#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability").getAttribute("data-date")')
                                    console.log("This CampGroud is now available from " + dateValue);
                                    comment = "This CampGroud is now available from " + dateValue;
                                    bookingStatus = false;
                                } else {


                                    if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button') != null) {
                                        if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button.fc-state-disabled') != null) {
                                            debugLog.push(await bookingApi.dateTime() + ":" + "No Campgrounds are Availabal for this year")
                                            console.log('No campgrounds are available for this Year');
                                            flag = true;
                                            bookingStatus = false;
                                            comment = 'No campgrounds are available for this Year';
                                        } else {
                                            debugLog.push(await bookingApi.dateTime() + ":" + "No Campgrounds are Available for this then clicking on next month button")
                                            await frame.waitForSelector('#calendar > div.fc-toolbar > div.fc-right > button');
                                            await frame.click('#calendar > div.fc-toolbar > div.fc-right > button');
                                        }
                                        await bookingApi.sleep(5000);
                                    } else {
                                        debugLog.push(await bookingApi.dateTime() + ":" + "No Campgrounds are Available")

                                        console.log('No campgrounds are available');
                                        comment = "No campgrounds are available";
                                        bookingStatus = false;
                                        flag = true;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    debugLog.push(await bookingApi.dateTime() + ":" + "Fetching Error Message")

                    // let element = await page.$('#mainContent > div > div > div:nth-child(2) > div > div');
                    // let element = await page.$('.maintenance-container');

                    // let errorMsgCamp = await page.evaluate(el => el.textContent, element)
                    let errorMsgCamp = 'Site is Under Maintenance'
                    console.log(errorMsgCamp);
                    bookingStatus = false;
                    comment = errorMsgCamp;
                }
                // await bookingApi.sleep(5000)
                // await browser.close();
            } catch (err) {

                debugLog.push(await bookingApi.dateTime() + ":" + "Sorry, an error has ocurred ")
                comment = 'Sorry, an error has ocurred';
                bookingStatus = false;
                console.log(err)
            }
            if (browseClose == true) {
                // await browser.close();
            }

        }
    } catch (err) {
        console.log(err.message);
        bookingStatus = false;
        debugLog.push(await bookingApi.dateTime() + ":" + "Sometghing Went Wrong")
        comment = 'Sometghing Went Wrong';
        // await browser.close();
        console.log("Browser Closed");
    }

    await bookingApi.storeBookingCommentAndLog(updateBookingUrl, bookingId, campgroundId, bookingStatus, token, comment);
    // await browser.close();

})();

async function isLocatorReady(element, page) {
    const isVisibleHandle = await page.evaluateHandle((e) => {
        const style = window.getComputedStyle(document.querySelector(e))
        //   const style = window.getComputedStyle(e);
        return (style && style.display !== 'none' &&
            style.visibility !== 'hidden' && style.opacity !== '0');
    }, element);
    var visible = await isVisibleHandle.jsonValue();

    if (visible) {
        return true;
    }
    return false;
}

