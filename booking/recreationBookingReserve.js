var common = require('./utils/common')

var configPath = common.configPath;
var bookingApi = require(configPath + '/utils/bookingapi')

const puppeteer = common.puppeteer;
var express = common.express;
var fs = common.fs;
const args = common.args;
const path = common.path;

//BlackWood

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var siteUrl = args.site_url.replace(/@@/g, '&');
siteUrl = siteUrl.replace(/api[/]camps/g, "camping");
var token = args.token;
var closeBrowser = true;
var bookingStatus = false;

var logInEmail = common.recreationLogin.user_name;
var logInPassword = common.recreationLogin.password;

var comment = common.comment;
var debugLog = common.debugLog;

var fetchBookingFormDataUrl = common.fetchBookingFormDataUrl;
var updateBookingUrl = common.updateBookingUrl;

async function getDateValues(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var monthName = checkDate.toLocaleString("default", { month: "long" })
    var dayNumber = checkDate.getDate();
    var year = checkDate.getFullYear();

    var dateArr = new Array();

    dateArr['month'] = monthName;
    dateArr['year'] = year;
    dateArr['day'] = dayNumber;
    dateArr['month_year'] = monthName + " " + year;
    dateArr['original_date'] = dateVal;
    // var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateArr;
}

async function getEnquiryDataID() {
    debugLog.push(await bookingApi.dateTime() + ":" + 'Fetching booking enquiry detail by id');
    var formData = await bookingApi.fetchBookingFormData(fetchBookingFormDataUrl, bookingId, token)
    // console.log(formData);
    if (formData && formData.code == 200) {
        var data = formData.response;
        // console.log(data);
        common.form_siteCategory = data['details']['reservation_type'];
        // console.log('commonform_siteCategory:' + common.form_siteCategory)
        common.form_checkIn = await getDateValues(data['check_in_date']);
        common.form_checkOut = await getDateValues(data['check_out_date']);
        common.form_postcode = data['post_code'];
        common.form_countryCode = data['country_code'];
        common.form_address1 = data['address1'];
        common.form_address2 = data['address2'];
        common.form_city = data['city'];
        common.form_state = data['state'];
        common.form_vehicles = data['details']['towing_length'];
        common.form_group = data['details']['adults'];
        common.form_equipmentType = data['details']['equipment_type'];
        common.form_equipmentLength = data['details']['length'];
        common.form_campsite = data['details']['campsite'];
        var cardData = await bookingApi.fetchCardDetail(data['details']['card_detail']['token'], data['details']['card_detail']['user'], data['details']['card_detail']['passphrase'],data['details']['card_detail']['reference']);       
        var name = data['name'].split(' ');
        common.form_firstName = name[0];
        common.form_lastName = name[1];
        common.form_phone = data['phone'];
        common.form_email = data['email'];
        
    } else {
        var message = (formData.message) ? formData.message : 'Something Went Wrong';
        console.log(message)
        comment = message;
        bookignStatus = false;
        debugLog.push(await bookingApi.dateTime() + ":" + message);
    }
}

async function checkDate(indate, outdate) {

    var checkInDate = new Date(indate);
    var checkOutDate = new Date(outdate);
    var response = [];
    // Get today's date
    var todaysDate = new Date();

    // call setHours to take the time out of the comparison
    if (checkInDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
        response['message'] = "Check In Date is Less then current date";
        response['status'] = false;
    } else {
        if (checkOutDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
            response['message'] = "Check Out Date is Less then current date";
            response['status'] = false;
        } else {
            if (checkOutDate.setHours(0, 0, 0, 0) <= checkInDate.setHours(0, 0, 0, 0)) {
                response['message'] = "Check Out Date needs to be greter then check in date";
                response['status'] = false;
            } else {
                response['message'] = "Both date are Valid";
                response['status'] = true;
            }
        }
    }
    return response;
}

async function selectDatePicker(inMonthyear, inDayValue, inYearValue) {
    return new Promise(async function (resolve, reject) {
        // debugger;
        var calenderMonth = document.querySelectorAll('.CalendarMonth');
        var checkInFlag = false;
        for (var i = 0; i < calenderMonth.length; i++) {
            var datavisible = await calenderMonth[i].getAttribute("data-visible");
            if (datavisible) {

                var title = await calenderMonth[i].querySelector('div > strong').textContent;

                var titleYear = parseInt(new Date(title).getFullYear());
                var inputYear = parseInt(inYearValue);

                if (inputYear === titleYear) {
                    if (title == inMonthyear) {
                        var availDays = await calenderMonth[i].querySelectorAll('table > tbody > tr > td > div.rec-calendar-day > div >strong');
                        for (var d = 0; d < availDays.length; d++) {
                            var dayVal = await availDays[d].parentNode.parentNode.querySelector('div').textContent;
                            if (dayVal == inDayValue) {
                                await availDays[d].parentNode.parentNode.querySelector('div').click();
                                checkInFlag = true;
                                // console.log(dayVal+"=="+inDayValue)
                                resolve("true")
                            }
                        }
                    }
                } else {
                    resolve("false");
                }
            }
        }

        if (checkInFlag == false) {
            await document.querySelector('.sarsa-day-picker-range-controller-month-navigation-button.right').click();

            setTimeout(async function () {
                await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(function (result) {
                    if (result == "true") {
                        resolve("true")
                    } else {
                        resolve("false");
                    }
                });
            }, 5000)
        } else {
            resolve("true")
        }
    });
}

(async () => {
    try {


        await getEnquiryDataID();

        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });
        console.log('*************************');
        console.log(await browser.version());
        debugLog.push(await bookingApi.dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        try {
            // console.log('Nevigating : ' + campgrounds[p].title)
            console.log('Nevigating : ' + siteUrl)
            debugLog.push(await bookingApi.dateTime() + ":" + 'Nevigating : ' + siteUrl);
            await page.setViewport({ width: 1300, height: 768 });

            await page.setDefaultNavigationTimeout(0);

            var browseUrl = siteUrl;
            // var browseUrl = campgrounds[p].link + 'reserve';
            debugLog.push(await bookingApi.dateTime() + ":" + 'Url is : ' + browseUrl);

            await page.goto(browseUrl, { waitUntil: 'networkidle2' });
            // console.log('browseUrl:' + browseUrl)
            await bookingApi.sleep(3000)

            // page.on('console', msg => {
            //     for (let i = 0; i < msg._args.length; ++i)
            //         console.log(`${msg._args[i]}`)
            // });

            // if (await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(3) > button")) {
                // debugLog.push(await bookingApi.dateTime() + ":" + "Clicking Add More Filters");
                // page.click("#campsite-filter-container > div > div > div:nth-child(3) > button");

                // await bookingApi.sleep(1000);
                // var form_siteCategory = common.form_siteCategory;
                // console.log('form_siteCategory:' + form_siteCategory);
                // var siteCatAvailable = await page.evaluate(async (form_siteCategory) => {
                //     var allcampSite = document.querySelectorAll("[id^='campsite-type']:not([disabled]");
                //     var found = false;
                //     for (var a = 0; a < allcampSite.length; a++) {
                //         var str = allcampSite[a].getAttribute('value').split(" ");
                //         console.log(str[0] + "==" + form_siteCategory);
                //         if (str[0] == form_siteCategory) {
                //             allcampSite[a].parentNode.click()
                //             found = true;
                //         }
                //     }
                //     document.querySelector('#shared-filter-options-modal > div > div > div > div > div > button').click();
                //     return found;
                // }, form_siteCategory)
                // debugLog.push(await bookingApi.dateTime() + ":" + "Checking if site available is found or not");
                // if (siteCatAvailable) {

                    debugLog.push(await bookingApi.dateTime() + ":" + "site available is found now selecting dates");

                    var selectDates = await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(1) > button")

                    let title = await page.evaluate(el => el.textContent, selectDates)
                    console.log(title.trim());
                    if (title == 'Select Dates ...') {
                        await page.click('#campsite-filter-container > div > div > div:nth-child(1) > button');
                    }

                    debugLog.push(await bookingApi.dateTime() + ":" + "Verifying CheckIn/Out Date");

                    var verifyCheckInDate = await checkDate(common.form_checkIn['original_date'], common.form_checkOut['original_date']);
                    // Testing
                    // page.on('console', msg => {
                    //     for (let i = 0; i < msg._args.length; ++i)
                    //         console.log(`${msg._args[i]}`);
                    // });

                    // console.log(verifyCheckInDate);

                    if (verifyCheckInDate['status'] == true) {
                        debugLog.push(await bookingApi.dateTime() + ":" + "If date is valid then selecting date");

                        var inMonthyear = common.form_checkIn['month_year'];
                        var inDayValue = common.form_checkIn['day'];
                        var inYearValue = common.form_checkIn['year'];
                        var bothDateSelected = false;

                        await page.addScriptTag({ content: `${selectDatePicker}` });
                        var inDateSelect = await page.evaluate(async (inMonthyear, inDayValue, inYearValue) => {

                            // return new Promise(function (resolve, reject) {
                            var resultVal = false;
                            await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(async function (result) {
                                resultVal = result
                            });
                            return resultVal;

                        }, inMonthyear, inDayValue, inYearValue)

                        if (inDateSelect == "true") {
                            debugLog.push(await bookingApi.dateTime() + ":" + "If in date found then selcting out date");
                            await bookingApi.sleep(1500)

                            var outMonthyear = common.form_checkOut['month_year'];
                            var outDayValue = common.form_checkOut['day'];
                            var outYearValue = common.form_checkOut['year'];


                            var outDateSelect = await page.evaluate(async (outMonthyear, outDayValue, outYearValue) => {

                                // return new Promise(function (resolve, reject) {
                                var resultVal = false;
                                await selectDatePicker(outMonthyear, outDayValue, outYearValue).then(async function (result) {
                                    resultVal = result
                                });
                                return resultVal;

                            }, outMonthyear, outDayValue, outYearValue)

                            // console.log("outDateSelect:"+outDateSelect)
                            if (outDateSelect == "false") {
                                debugLog.push(await bookingApi.dateTime() + ":" + "Check Out Date is Not Available");
                                comment = 'Check Out Date is Not Available';
                                console.log('Check Out Date is Not Available')
                                bookingStatus = false;
                            }
                            if (inDateSelect == "true" && outDateSelect == "true") {
                                bothDateSelected = true;
                                debugLog.push(await bookingApi.dateTime() + ":" + "Both date are available");

                            }

                        } else {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Check in Date is Not Available");
                            comment = 'Check in Date is Not Available';
                            console.log('Check in Date is Not Available');
                            bookingStatus = false;
                        }

                        await page.click('#shared-filter-options-modal > div > div > div > div > div > button');
                        await bookingApi.sleep(3000)
                        var campsiteflag = false;
                        if (bothDateSelected) {

                            if (await page.$(".campsite-search-card-column:nth-child(1) .rec-flex-card-price-button-wrap:nth-child(1) > button:not([disabled]") !== null) {
                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting First campsite");
                                
                                if (await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(2) > button")) {
                                    await page.click("#campsite-filter-container > div > div > div:nth-child(2) > button");
                
                                   var form_equipmentType=common.form_equipmentType;
                                    var equipAvailable = await page.evaluate(async (form_equipmentType) => {
                                        var allEquip = document.querySelectorAll(".search-filter-check-item input:not([disabled])");
                                        var found = false;
                                        for (var a = 0; a < allEquip.length; a++) {
                                            var str = allEquip[a].getAttribute('value');
                                            // console.log(str + "==" + form_equipmentType);
                                            if (str == form_equipmentType) {
                                                allEquip[a].parentNode.click()
                                                found = true;
                                            }
                                        }
                                        document.querySelector('#shared-filter-options-modal > div > div > div > div > div > button').click();
                                        return found;
                                    }, form_equipmentType)
                                    // console.log(equipAvailable);
                                }
                                await bookingApi.sleep(2000)
                                // .campsite-search-card-column >div>div.rec-flex-card-content-wrap>div.rec-flex-card-content-bottom-wrap button
                                //   .campsite-search-card-column >div>div.rec-flex-card-content-wrap>
                                // let elements = await page.$$(".campsite-search-card-column >div>div.rec-flex-card-content-wrap");

                                // for (const el of elements) {

                                //     if (await el.$('div.rec-flex-card-content-bottom-wrap button') !== null) {
                                //         var btnName = await el.$('div.rec-flex-card-content-bottom-wrap button');
                                //         var btnInfo = await page.evaluate(el => el.textContent, btnName);
                                    
                                //         if (btnInfo.trim() == 'Add to Cart') {
                                //             var titleName = await el.$('div>div>div.rec-flex-card-top-left-content-wrap');
                                //             var titleInfo = await page.evaluate(el => el.textContent, titleName);
                                //             if(common.form_campsite!="" && common.form_campsite!=undefined){
                                //                 var campsitename = common.form_campsite.trim();
                                //                 if(!campsitename.startsWith("Site:")){
                                //                     campsitename='Site: '+campsitename; 
                                //                 }
                                //             }
                                //             if (titleInfo.trim() == campsitename) {
                                //                 console.log('Campsite matched:' + titleInfo)
                                //                 campsiteflag = true;
                                //                 await btnName.click();
                                //             }
                                //         }
                                //     }
                                // }
                                var form_campsite=common.form_campsite;
                                if (form_campsite) {
                                    // console.log("form_campsite:"+form_campsite)
                                    var siteList = await page.evaluate(async (form_campsite) => {
        
                                        var siteTotal = await document.querySelectorAll('.campsite-search-card-column');
                                        var found =false;
                                        for (var t = 0; t < siteTotal.length; t++) {
                                            if(found ==false){
                                                var btnName = siteTotal[t].querySelector('.sarsa-button-inner-wrapper > span').textContent.trim();
                                                // console.log("title Name:"+btnName);
                                                
                                                if (btnName == "Add to Cart") {
                                                    siteName = siteTotal[t].querySelector('.rec-flex-card-top-left-content-wrap').textContent.trim();
                                                    if(form_campsite!="" && form_campsite!=undefined){
                                                        var campsitename = form_campsite.trim();
                                                        if(!campsitename.startsWith("Site:")){
                                                            campsitename='Site: '+campsitename; 
                                                        }
                                                    }
                                                    // console.log('siteName:' + siteName + ' == Campsite Name:' + campsitename)
                                                    if (siteName == campsitename) {
                                                        // console.log('Campsite matched:' + siteName)
                                                        await new Promise(function(resolve) {setTimeout(resolve, 1000)});
                                                        found = true;
                                                        await siteTotal[t].querySelector('.rec-flex-card-price-button-wrap > button').click();
                                                    }
                
                                                }
                                            }
                                        }
                                        if(found == false){
                                            await new Promise(function(resolve) {setTimeout(resolve, 1000)});
                                            if (await document.querySelector('.search-pagination-wrap .rec-pagination button >svg.rec-icon-arrow-forward')) {
                                                do {
                                                    
                                                        await new Promise(function(resolve) {setTimeout(resolve, 1000)});
                                                        await document.querySelector('.search-pagination-wrap .rec-pagination button >svg.rec-icon-arrow-forward').parentElement.click();
                                                        var siteTotal = await document.querySelectorAll('.campsite-search-card-column');
                                                        for (var t = 0; t < siteTotal.length; t++) {
                                                            var btnName = siteTotal[t].querySelector('.sarsa-button-inner-wrapper > span').textContent.trim();
                                                            // console.log("title:"+btnName);
                                    
                                                            if (btnName == "Add to Cart") {
                                                                siteName = siteTotal[t].querySelector('.rec-flex-card-top-left-content-wrap').textContent.trim();
                                                                if(form_campsite!="" && form_campsite!=undefined){
                                                                    var campsitename = form_campsite.trim();
                                                                    if(!campsitename.startsWith("Site:")){
                                                                        campsitename='Site: '+campsitename; 
                                                                    }
                                                                }
                                                                if (siteName == campsitename) {
                                                                    console.log('Campsite matched:' + siteName)
                                                                    await new Promise(function(resolve) {setTimeout(resolve, 1000)});
                                                                    found = true;
                                                                    await siteTotal[t].querySelector('.rec-flex-card-price-button-wrap > button').click();
                                                                }
                                    
                                                            }
                                                        }
                                                    
                                
                                                }while(!await document.querySelector('.search-pagination-wrap .rec-pagination button.rec-disabled-button >svg.rec-icon-arrow-forward') && found == false)
                                            }
                                        }
                                        return found;
                                    },form_campsite);

                                    campsiteflag=siteList;

                                }
                                if (campsiteflag) {

                                } else {
                                    if (common.form_campsite == '' || common.form_campsite==undefined) {
                                        await bookingApi.sleep(2000)
                                        await page.goto(siteUrl+'?start=0', { waitUntil: 'networkidle2' });
                                        await bookingApi.sleep(1000);
                                       
                                        if(await page.$(".campsite-list > div:nth-child(1) .sarsa-button-inner-wrapper > span")){
                                            var element = await page.$('.campsite-list > div:nth-child(1) .sarsa-button-inner-wrapper > span');
                                            var btntext = await page.evaluate(el => el.textContent, element);
                                            if(btntext=="Add to Cart"){
                                                await page.click('.campsite-search-card-column:nth-child(1) .rec-flex-card-price-button-wrap:nth-child(1) > button');
                                                console.log('campsite is not provided so selecting first available site');
                                                campsiteflag = true;    
                                            }else{
                                                campsiteflag = false;
                                            }
                                        }else{
                                            campsiteflag = false;
                                        }
                                        
                                    }
                                }
                                if (campsiteflag) {
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Filing Login Details");
                                    if (await page.$("#email") !== null) {
                                        const loginemail = await page.waitForSelector("#email");
                                        await page.click("#email");
                                        await loginemail.type(logInEmail)
                                    }

                                    if (await page.$("#rec-acct-sign-in-password") !== null) {
                                        const loginpass = await page.waitForSelector("#rec-acct-sign-in-password");
                                        await page.click("#rec-acct-sign-in-password");
                                        await loginpass.type(logInPassword)
                                    }

                                    if (await page.$(".flex-grid.justify-content-center > div > div > div > form > button") !== null) {
                                        await page.click('.flex-grid.justify-content-center > div > div > div > form > button');
                                    }
                                    await bookingApi.sleep(2000)
                                    console.log('checking Login is success or not')
                                    if (await page.$(".sarsa-alert.error .alert-body") !== null) {

                                        let element = await page.$('.sarsa-alert.error .alert-body');
                                        let errorMsg = await page.evaluate(el => el.textContent, element)
                                        console.log(errorMsg)
                                        debugLog.push(await bookingApi.dateTime() + ":" + errorMsg);
                                        comment = errorMsg;
                                        bookingStatus = false;
                                        if (await page.$(".rec-acct-signin-modal .rec-close-modal") !== null) {
                                            await page.click(".rec-acct-signin-modal .rec-close-modal");
                                        }
                                    } else if(await page.$(".booking-notification-block") !== null){
                                        let element = await page.$('.booking-notification-block > p');
                                        let errorMsg = await page.evaluate(el => el.textContent, element)
                                        console.log(errorMsg)
                                        debugLog.push(await bookingApi.dateTime() + ":" + errorMsg);
                                        comment = errorMsg;
                                        bookingStatus = false;
                                    
                                    }else {
                                        debugLog.push(await bookingApi.dateTime() + ":" + "Login is successful");
                                        console.log(new Date());
                                        await bookingApi.sleep(3000)
                                        console.log(new Date())
                                        console.log('checking Login is success');
                                        debugLog.push(await bookingApi.dateTime() + ":" + "Login is successful so now filling form details");
                                        // await page.$("id^=other_occupant'").click();
                                        if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div>div>div>label input[value='other_occupant']")){
                                            await bookingApi.sleep(2000)
                                            const current_element = await page.$(".camp-group-leader-details-form > div.flex-grid  >div>div>div>label input[value='other_occupant']");
                                            const parent_node = await current_element.getProperty('parentNode');
                                            await parent_node.click();

                                            await bookingApi.sleep(3000);

                                            if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='text']") !== null) {
                                                const fristName = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='text']");
                                                await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='text']");
                                                await fristName.type(common.form_firstName)
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Adding First name");
                                            }

                                            if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='text']") !== null) {
                                                const lastName = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='text']");
                                                await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='text']");
                                                await lastName.type(common.form_lastName)
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Adding Last name");
                                            }

                                            if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='email']") !== null) {
                                                const email = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='email']");
                                                await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(1) input[type='email']");
                                                await email.type(common.form_email)
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Adding Email");
                                            }
                                            
                                            if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='tel']") !== null) {
                                                const phone = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='tel']");
                                                await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(2) input[type='tel']");
                                                await phone.type(common.form_phone)
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Adding Phone");
                                            }
                                            
                                        }
                                        
                                        if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']") !== null) {
                                            const postalcode = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']");
                                            await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']");
                                            await postalcode.type(common.form_postcode)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding Post Code");
                                        }

                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(2) > div> div > div> div > select") !== null) {
                                            await page.select('.camp-group-leader-address-form > div:nth-child(2) > div> div > div> div > select', common.form_countryCode)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "selecting Country");
                                        }

                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input") !== null) {
                                            const address = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input");
                                            await page.click(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input");
                                            await address.type(common.form_address1)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding Address1");
                                        }

                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input") !== null) {
                                            const address2 = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input");
                                            await page.click(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input");
                                            await address2.type(common.form_address2)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding Address2");
                                        }

                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input") !== null) {
                                            const city = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input");
                                            await page.click(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input");
                                            await city.type(common.form_city)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding city");

                                        }

                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div >select") !== null) {
                                            await page.select('.camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div >select', common.form_state)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Selecting State");
                                        }else if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div input") !== null) {
                                            await page.type('.camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div input', common.form_state)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Selecting State");
                                        }


                                        if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input") !== null) {
                                            const postcode = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input");
                                            await page.click(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input");
                                            await postcode.type(common.form_postcode)
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding Post Code");
                                        }

                                        if (await page.$(".camp-group-size-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)") !== null) {

                                            for (var i = 0; i < parseInt(common.form_group); i++) {
                                                await page.click(".camp-group-size-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)");
                                            }
                                            debugLog.push(await bookingApi.dateTime() + ":" + "Adding campgroup");

                                        }
                                        // page.on('console', msg => {
                                        //     for (let i = 0; i < msg._args.length; ++i)
                                        //         console.log(`${msg._args[i]}`)
                                        // });
                                        // if (common.form_equipmentType == 'Tent') {
                                            if (await page.$(".camp-camping-equipment-form > div > div > div >div> label") !== null) {

                                                form_equipmentType = common.form_equipmentType;
                                                form_equipmentLength = common.form_equipmentLength;

                                                var selectEquipment = await page.evaluate(async (form_equipmentType, form_equipmentLength) => {
                                                    var totalLbl = document.querySelectorAll(".camp-camping-equipment-form > div > div > div >div");
                                                    console.log(totalLbl.length)
                                                    for (var l = 0; l < totalLbl.length; l++) {
                                                        var equipmentName = totalLbl[l].querySelector('label > span:nth-child(3)').firstChild.nodeValue;
                                                        // console.log(equipmentName.trim()+"=="+form_equipmentType.trim());
                                                        if((form_equipmentType.trim()=="RV" || form_equipmentType.trim()=="RV/Motorhome") && (equipmentName.trim()=="RV" || equipmentName.trim()=="RV/Motorhome")){
                                                            totalLbl[l].querySelector('label > span:nth-child(3)').click();
                                                            return totalLbl[l].querySelector('label > input:checked').value;
                                                        }else{
                                                            if (equipmentName.trim() == form_equipmentType.trim()) {
                                                                totalLbl[l].querySelector('label > span:nth-child(3)').click();
                                                                return totalLbl[l].querySelector('label > input:checked').value;
                                                            }    
                                                        }
                                                    }

                                                }, form_equipmentType, form_equipmentLength);
                                                // console.log(selectEquipment);
                                                if(selectEquipment && selectEquipment != 'tent'){
                                                    await page.type('input[id^=equip_' + selectEquipment + '_length]',form_equipmentLength)
                                                    debugLog.push(await bookingApi.dateTime() + ":" + "Adding equipment");    
                                                }

                                            }
                                            await bookingApi.sleep(1000);

                                            if (await page.$(".camp-vehicle-info-form  input") !== null) {
                                                for (var i = 0; i < parseInt(common.form_vehicles); i++) {
                                                    await page.click(".camp-vehicle-info-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)");
                                                }
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Adding Vehicle Information");

                                            }
                                            await bookingApi.sleep(1000);

                                            if (await page.$(".rec-order-detail-need-to-know.sarsa-need-to-know > div > label >span") !== null) {
                                                await page.click(".rec-order-detail-need-to-know.sarsa-need-to-know > div > label >span");
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Terms and conditions");
                                            }
                                            await bookingApi.sleep(2000);

                                            console.log('Proceeed to cart')
                                            if (await page.$("#action-bar-submit") !== null) {
                                                await page.click("#action-bar-submit");
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Click on proceed to cart");
                                            }

                                            console.log('Proceeed to Payment')
                                            await bookingApi.sleep(8000);

                                            if (await page.$(".cart-order-summary-actions > button.rec-button-primary-large") !== null) {
                                                await page.click(".cart-order-summary-actions > button.rec-button-primary-large");
                                                debugLog.push(await bookingApi.dateTime() + ":" + "Click on proceed to Payment");
                                                console.log("click on card detail button")

                                                if (await page.$(".sarsa-form-validation-alert .alert-body") !== null) {
                                                    let element = await page.$('.sarsa-form-validation-alert .alert-body');
                                                    let errorMsg = await page.evaluate(el => el.textContent, element)
                                                    console.log(errorMsg)
                                                    debugLog.push(await bookingApi.dateTime() + ":" + errorMsg);
                                                    bookingStatus = false;

                                                } else {

                                                    await bookingApi.sleep(5000);
                                                    if (await page.$(".rec-cart-form-field input[name='name']") !== null) {
                                                        const nameOnCard = await page.waitForSelector(".rec-cart-form-field input[name='name']");
                                                        await page.click(".rec-cart-form-field input[name='name']");
                                                        console.log("credit card name:" + common.form_name_on_credit_card);
                                                        await nameOnCard.type(common.form_name_on_credit_card)
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Adding Card Name");
                                                        closeBrowser = false;
                                                    }
                                                    await bookingApi.sleep(1000)
                                                    if (await page.$(".rec-cart-form-field input[name='number']") !== null) {
                                                        const cardNumber = await page.waitForSelector(".rec-cart-form-field input[name='number']");
                                                        await page.click(".rec-cart-form-field input[name='number']");
                                                        await cardNumber.type(String(common.form_creditCard))
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Adding Card Number");
                                                        closeBrowser = false;
                                                    }
                                                    await bookingApi.sleep(1000)
                                                    if (await page.$(".rec-cart-form-field select[name='month']") !== null) {
                                                        await page.click(".rec-cart-form-field select[name='month']");
                                                        await page.select(".rec-cart-form-field select[name='month']", common.form_creditCardExpMonth)
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Adding Card Month");
                                                        closeBrowser = false;
                                                    }

                                                    if (await page.$(".rec-cart-form-field select[name='year']") !== null) {
                                                        await page.click(".rec-cart-form-field select[name='year']");
                                                        await page.select(".rec-cart-form-field select[name='year']", common.form_creditCardExpYear.substring(2))
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Adding Card Year");
                                                        closeBrowser = false;
                                                    }

                                                    if (await page.$(".rec-cart-form-field input[name='cvc']") !== null) {
                                                        const cardNo = await page.waitForSelector(".rec-cart-form-field input[name='cvc']");
                                                        await page.click(".rec-cart-form-field input[name='cvc']");
                                                        await cardNo.type(common.form_creditCardCode)
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Adding Card Code");
                                                        await bookingApi.sleep(1000);
                                                        await page.click(".rec-cart-form-field input[name='cvc']");
                                                    }
                                                    await bookingApi.sleep(1000)

                                                    if (await page.$("#page-body > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > button:not([disabled])") !== null) {
                                                        await page.click("#page-body > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > button")
                                                        debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on next");
                                                        if (await page.$("#page-body > div > div > div > div > div.flex-col-md-8 > div.ml-1.mr-2 > div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md") !== null) {
                                                            await page.click("#page-body > div > div > div > div > div.flex-col-md-8 > div.ml-1.mr-2 > div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md")
                                                            debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on Confirm Button");
                                                            await bookingApi.sleep(5000)

                                                            if (await page.$(".cart-procesing-modal-text") !== null) {
                                                                let element = await page.$('.cart-procesing-modal-text');
                                                                let message = await page.evaluate(el => el.textContent, element)
                                                                console.log(message);
                                                                comment = message;
                                                                debugLog.push(await bookingApi.dateTime() + ":" + message);
                                                                bookingStatus = true;
                                                                closeBrowser = false;
                                                            }
                                                        }
                                                    } else {
                                                        await bookingApi.sleep(2000);
                                                        if(await page.$(".flex-col-xl-7 .sarsa-button-primary")){
                                                            debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on next");
                                                            console.log('Clicking on next')
                                                            await page.click(".flex-col-xl-7 button");
                                                            await bookingApi.sleep(2000);
                                                            console.log('Booking is done for this campground.')
                                                            if(await page.$("div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md")){ 
                                                                await page.click("div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md");
                                                            }
                                                            bookingStatus = true;
                                                            closeBrowser = false;    
                                                        }else{
                                                            debugLog.push(await bookingApi.dateTime() + ":" + "Invalid Payment Details");
                                                            console.log('Continue with your payment details')
                                                            comment = 'Invalid Payment Details';
                                                            bookingStatus = true;
                                                            closeBrowser = false;    
                                                        }
                                                    }

                                                }
                                            } else {
                                                console.log("Payment button not found Continue from here...");
                                                comment = 'Payment process failed.';
                                                debugLog.push(await bookingApi.dateTime() + ":" + 'Payment button not found Continue from here');
                                                bookingStatus = true;
                                                closeBrowser = false;
                                            }


                                        // } else {
                                        //     console.log("Booking Done For this campground.");
                                        //     comment = 'Booking Done For this campground.';
                                        //     debugLog.push(await bookingApi.dateTime() + ":" + comment);
                                        //     bookingStatus = true;
                                        //     closeBrowser = false;
                                        // }
                                    }
                                }else{
                                    debugLog.push(await bookingApi.dateTime() + ":" + "Campsite Not Available");
                                    console.log('Campsite Not Available')
                                    comment = 'Campsite Not Available';
                                    bookingStatus = false;
                                }


                            } else {
                                console.log('if campsite not found the fetching message')
                                if (await page.$(".rec-notification-body") !== null) {
                                    let element = await page.$('.rec-notification-body');
                                    let errorMsg = await page.evaluate(el => el.textContent, element)
                                    console.log(errorMsg);
                                    comment = errorMsg;
                                    debugLog.push(await bookingApi.dateTime() + ":" + errorMsg);
                                    bookingStatus = false;
                                } else {
                                    debugLog.push(await bookingApi.dateTime() + ":" + "There are no campsites are availble");
                                    console.log("There are no campsites are availble");
                                    comment = 'There are no campsites are availble';
                                    bookingStatus = false;
                                }
                            }
                        }
                    } else {
                        debugLog.push(await bookingApi.dateTime() + ":" + "Date Error :" + verifyCheckInDate['message']);
                        console.log("Date Error :" + verifyCheckInDate['message']);
                        comment = verifyCheckInDate['message'];
                        bookingStatus = false;
                    }
                // }
                // else {
                //     debugLog.push(await bookingApi.dateTime() + ":" + "No Site Category available");
                //     console.log('No Site Category available')
                //     comment = 'No Site Category available';
                //     bookingStatus = false;
                // }
            // }

            // console.log(calenderMonth);
        } catch (err) {
            await browser.close();
            debugLog.push(await bookingApi.dateTime() + ":" + "Sorry, an error has ocurred ")
            comment = 'Sorry, an error has ocurred';
            bookingStatus = false;
            console.log(err)
        }
        if (closeBrowser == true) {
            await browser.close();
        }
    } catch (err) {
        console.log(err);
        bookingStatus = false;
        await browser.close();
        debugLog.push(await bookingApi.dateTime() + ":" + "Something Went Wrong")
        comment = 'Something Went Wrong';
        console.log("Browser Closed");
    }

    await bookingApi.storeBookingCommentAndLog(updateBookingUrl, bookingId, campgroundId, bookingStatus, token, comment);

})();
