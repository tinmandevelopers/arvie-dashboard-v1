var common = require('./utils/common')

var configPath = common.configPath;
var bookingApi = require(configPath + '/utils/bookingapi')

const puppeteer = common.puppeteer;
var express = common.express;
var fs = common.fs;
const args = common.args;
const path = common.path;

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var siteUrl = args.site_url.replace(/@@/g, '&');
var token = args.token;

var bookingStatus = false;
var browseClose = true;

var dataFound = false;
var selectEquipment = false;
var setadult = false;
var setcard = false;

var comment = common.comment;
var debugLog = common.debugLog

var fetchBookingFormDataUrl = common.fetchBookingFormDataUrl;
var updateBookingUrl = common.updateBookingUrl;


async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        log(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getEnquiryDataID() {

    var formData = await bookingApi.fetchBookingFormData(fetchBookingFormDataUrl, bookingId, token)
    // console.log(formData);
    if (formData && formData.code == 200) {
        var data = formData.response;
        common.form_siteCategory = data['details']['reservation_type'];
        common.form_equipmentType = data['details']['equipment_type'];
        common.form_equipmentLength = data['details']['length'];
        common.form_vehicle = data['details']['towing_length'];
        common.form_adults = data['details']['adults'];
        common.form_campsite = data['details']['campsite'];
        var cardData = await bookingApi.fetchCardDetail(data['details']['card_detail']['token'], data['details']['card_detail']['user'], data['details']['card_detail']['passphrase'],data['details']['card_detail']['reference']);       
        common.form_address1 = data['address1'];

        var name = data['name'].split(' ');
        common.form_firstName = name[0];
        common.form_lastName = name[1];
        common.form_phone = data['phone'];
        common.form_email = data['email'];
        common.form_city = data['city'];
        common.form_countryCode = data['country_code'];
        common.form_postcode = data['post_code'];
        common.form_state=data['state'];
        dataFound = true;
    } else {
        var message = (formData.message) ? formData.message : 'Something Went Wrong';
        console.log(message)
        comment = message;
        bookingStatus = false;
        dataFound=false;
        log(await dateTime() + ":" + message);
    }

}

(async () => {

    await getEnquiryDataID();
    if (dataFound) {
        try {
            var browser = await puppeteer.launch({
                headless: false,
                args: [
                    '--start-maximized' // you can also use '--start-fullscreen'
                ]
            });
            log(await browser.version());
            log(await dateTime() + ":" + await browser.version());
            // open a new page
            var page = await browser.newPage();

            try {

                await page.setViewport({ width: 1366, height: 768 });

                await page.setDefaultNavigationTimeout(0);

                let browseUrl = siteUrl;
                log(await dateTime() + ":" + 'Nevigating : ' + browseUrl);
                await page.goto(browseUrl, { waitUntil: 'networkidle2' });
                await sleep(1000);
                if (await page.$('#sysError')) {
                    let element = await page.$('.pageerror');
                    let errorMsg = await page.evaluate(el => el.textContent, element)
                    comment = errorMsg;
                    bookingStatus = false;
                    log(await dateTime() + ":" + errorMsg);

                } else {

                    if (await page.$('#lookingfor')) {
                        var form_siteCategory = common.form_siteCategory;
                        var val = await getSelctBoxOptionValue('#lookingfor', form_siteCategory, page);
                        await page.select('#lookingfor', val);
                        if (val != '') {
                            await sleep(1000);
                            if (await page.$('.actionButton  button')) {
                                await page.click('.actionButton  button')
                                await sleep(1000);
                            }

                            await sleep(3000);
                            log(await dateTime() + ":" + "Scrolling page");
                            if (await page.$('#facility_map')) {
                                await page.evaluate(() => {
                                    document.querySelector('#facility_map').scrollIntoView()
                                });
                            }
                            const bookTableTd = await page.evaluate(() => {
                                const rows = document.querySelectorAll('.table-responsive table tbody tr');
                                return Array.from(rows, row => {
                                    const columns = row.querySelectorAll('td');
                                    return Array.from(columns, column => column.innerText);
                                });
                            });
                            let campsiteflag = false;
                            let getTdChild = 0;
                            if (bookTableTd && bookTableTd.length) {
                                for (let a = 0; a < bookTableTd.length; a++) {
                                    let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');

                                    if (getBookTr.length) {
                                        var titleInfo = bookTableTd[a][0] + " Loop: " + bookTableTd[a][1];
                                        // console.log(titleInfo.trim() + '==' + common.form_campsite.trim());
                                        if (titleInfo.trim() == common.form_campsite.trim()) {
                                            // console.log('Campsite matched:' + titleInfo)
                                            campsiteflag = true;
                                            getTdChild = a + 1;
                                            break;
                                            // await bookTableTd[a].filter(book => book == 'Book Now').click();
                                        }
                                    }
                                }
                                if (campsiteflag) {

                                } else {
                                    if (common.form_campsite == '') {
                                        getTdChild = 0
                                        for (let a = 0; a < bookTableTd.length; a++) {
                                            let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');

                                            if (getBookTr.length) {
                                                getTdChild = a + 1;
                                                campsiteflag = true;
                                                console.log("Campsite is not Provided so booking first available campsite")
                                                break;
                                                // await bookTableTd[a].filter(book => book == 'Book Now').click();
                                            }
                                        }
                                    }

                                }
                                if (campsiteflag) {
                                    if (await page.$(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`)) {
                                        log(await dateTime() + ":" + "Click on book button");
                                        await page.click(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`);
                                        await sleep(3000);

                                        log(await dateTime() + ":" + "Scrolling page");
                                        await page.evaluate(() => {
                                            document.querySelector('.quickbook__datepager').scrollIntoView()
                                        });
                                        await sleep(1000);

                                        await page.click('.quickbook button');
                                        await sleep(6000);

                                        if (await page.$('#emailGroup input.TextBoxRenderer')) {
                                            log(await dateTime() + ":" + "Waiting for login page ");
                                            log(await dateTime() + ":" + "Storing email")
                                            const email = await page.waitForSelector("#emailGroup input.TextBoxRenderer");
                                            await page.click("#emailGroup input.TextBoxRenderer");
                                            await email.type(common.reserveamericaLogin.user_name);
                                        }

                                        if (await page.$('.PasswordBoxRenderer')) {
                                            log(await dateTime() + ":" + "Storing Password")
                                            const password = await page.waitForSelector(".PasswordBoxRenderer");
                                            await page.click(".PasswordBoxRenderer");
                                            await password.type(common.reserveamericaLogin.password);
                                            await sleep(3000)
                                        }

                                        if (await page.$('#signinbutton button')) {
                                            await page.click('#signinbutton button')

                                        }
                                        await sleep(3000)
                                        // if (await page.$(".col-md-9 div:nth-child(2)")) {

                                        // log(await dateTime() + ":" + "Scrolling EQUIPMENT section");
                                        // await page.evaluate(() => {
                                        //     document.querySelector('.col-md-9 div:nth-child(2)').scrollIntoView()
                                        // });
                                        await sleep(1000);
                                        // page.on('console', msg => {
                                        //     for (let i = 0; i < msg._args.length; ++i)
                                        //         console.log(`${msg._args[i]}`)
                                        // });

                                        await sleep(1000);
                                        if (await page.$('#numOfOccup1')) {
                                            log(await dateTime() + ":" + "Storing Free");
                                            const numOfOccup1 = await page.waitForSelector("#numOfOccup1");
                                            await page.click("#numOfOccup1");
                                            await numOfOccup1.type(common.form_adults);
                                            setadult = true;
                                        }else{
                                            if (await page.$('#numOfOccupants')) {
                                                const numOfOccup1 = await page.waitForSelector("#numOfOccupants");
                                                await page.click("#numOfOccupants");
                                                await numOfOccup1.type(common.form_adults);
                                                setadult = true;
                                            }
                                        }
                                        await sleep(1000);
                                        if (await page.$('[data-ref="primaryOccupantType-1"]')) {
                                            console.log("clicking on other Occupant")
                                            await page.click('[data-ref="primaryOccupantType-1"]');
                                        }
                                        
                                        await sleep(1000);
                                        
                                        if (await page.$('#firstName')) {
                                            log(await dateTime() + ":" + "Adding First name");
                                            const firstName = await page.waitForSelector("#firstName");
                                            await page.click("#firstName");
                                            await firstName.type(common.form_firstName);
                                        }
                                        
                                        if (await page.$('#lastName')) {
                                            log(await dateTime() + ":" + "Adding Last name");
                                            const firstName = await page.waitForSelector("#lastName");
                                            await page.click("#firstName");
                                            await firstName.type(common.form_firstName);
                                        }

                                        if (await page.$('#address')) {
                                            log(await dateTime() + ":" + "Adding Address");
                                            const address = await page.waitForSelector("#address");
                                            await page.click("#address");
                                            await address.type(common.form_address1);
                                        }

                                        if (await page.$('#city')) {
                                            log(await dateTime() + ":" + "Adding City");
                                            const city = await page.waitForSelector("#city");
                                            await page.click("#city");
                                            await city.type(common.form_city);
                                        }

                                        if (await page.$('#postalCode')) {
                                            log(await dateTime() + ":" + "Adding postalCode");
                                            const postalCode = await page.waitForSelector("#postalCode");
                                            await page.click("#postalCode");
                                            await postalCode.type(common.form_postcode);
                                        }

                                        if (await page.$('#country') !== null) {
                                            log(await dateTime() + ":" + "Adding Contry");
                                            await page.select('#country', common.form_countryCode)
                                        }

                                        if (await page.$('#state'+common.form_countryCode) !== null) {
                                            log(await dateTime() + ":" + "Adding State");
                                            await page.select('#state'+common.form_countryCode, common.form_state)
                                        }
                                        
                                        if (await page.$('#phoneNumber')) {
                                            log(await dateTime() + ":" + "Adding phoneNumber");
                                            const phoneNumber = await page.waitForSelector("#phoneNumber");
                                            await page.click("#phoneNumber");
                                            await phoneNumber.type(common.form_phone);
                                        }
                                        
                                        if (await page.$('#emailAddress')) {
                                            log(await dateTime() + ":" + "Adding emailAddress");
                                            const emailAddress = await page.waitForSelector("#emailAddress");
                                            await page.click("#emailAddress");
                                            await emailAddress.type(common.form_email);
                                        }
                                        
                                        if (await page.$("#container  div.highlight__child-container div.input-holder-main select")) {
                                            var form_equipmentType = common.form_equipmentType;
                                            var val = await getSelctBoxOptionValue("#container  div.highlight__child-container div.input-holder-main select",common.form_equipmentType, page);
                                            await page.select("#container  div.highlight__child-container div.input-holder-main select", val);

                                        }

                                        if (await page.$("#container  div.highlight__child-container div.input-holder-main input[id^=equipLength_primEquipTemplate]")) {
                                            const equipLength = await page.waitForSelector("#container  div.highlight__child-container div.input-holder-main input[id^=equipLength_primEquipTemplate]");
                                            await equipLength.type(common.form_equipmentLength);
                                        }                                 
                                        
                                        if (await page.$('select#equipmentType')) {
                                            var form_equipmentType = common.form_equipmentType;
                                            var val = await getSelctBoxOptionValue('#equipmentType', form_equipmentType, page);
                                            await page.select('#equipmentType', val);
                                            await sleep(1000);
                                            if (await page.$('#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input')) {
                                                log(await dateTime() + ":" + "Selecting Equipment Length");
                                                const equipLength = await page.waitForSelector("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                                await page.click("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                                await equipLength.type(common.form_equipmentLength);
                                            }
                                            await sleep(1000);
                                        }else if (await page.$('select#equipmentType_primEquipTemplate_0') && !selectEquipment ) {
                                            selectEquipment = true;
                                            var form_equipmentType = common.form_equipmentType;
                                            var val = await getSelctBoxOptionValue('#equipmentType_primEquipTemplate_0', form_equipmentType, page);
                                            await page.select('#equipmentType_primEquipTemplate_0', val);
                                            await sleep(1000);
                                            if (await page.$('input#equipLength_primEquipTemplate_0')) {
                                                log(await dateTime() + ":" + "Selecting Equipment Length");
//                                                const equipLength = await page.waitForSelector("input#equipLength_primEquipTemplate_0");
//                                                await page.click("input#equipLength_primEquipTemplate_0");
                                                await page.$('input#equipLength_primEquipTemplate_0',common.form_equipmentLength);
                                            }
                                            await sleep(1000);
                                        }
                                        
                            
                                        // await page.evaluate(() => {
                                        //     document.querySelector('.col-md-9 div:nth-child(2) > div:nth-child(4)').scrollIntoView()
                                        // });
                                        await sleep(1000);

                                        
                                        if (await page.$('#creditcardcollection_cardType')) {
                                            log(await dateTime() + ":" + "Selecting Card Type");
                                            if(common.form_creditCardType == 'MC'){
                                                common.form_creditCardType="MAST";
                                            }
                                            await page.select('#creditcardcollection_cardType', common.form_creditCardType)
                                            setcard = true;
                                        }

                                        if (await page.$('#creditcardcollection_cardNumber')) {
                                            log(await dateTime() + ":" + "Storing card number ");
                                            const cardNumber = await page.waitForSelector("#creditcardcollection_cardNumber");
                                            await page.click("#creditcardcollection_cardNumber");
                                            await cardNumber.type(String(common.form_creditCard))
                                        }
                                        
                                        if (await page.$('#creditcardcollection_cardExpMonth')) {
                                            log(await dateTime() + ":" + "Selecting month");
                                            const cardNumber = await page.waitForSelector("#creditcardcollection_cardExpMonth");
                                            await page.click("#creditcardcollection_cardExpMonth");
                                            await cardNumber.type(String(common.form_creditCardExpMonth))
                                        }
                                        
                                        if (await page.$('#creditcardcollection_cardExpYear')) {
                                            log(await dateTime() + ":" + "Selecting Year");
                                            const cardNumber = await page.waitForSelector("#creditcardcollection_cardExpYear");
                                            await page.click("#creditcardcollection_cardExpYear");
                                            await cardNumber.type(String(common.form_creditCardExpYear))
                                                                                    }
                                        var name = common.form_name_on_credit_card.split(' ');
                                        
                                        if (await page.$('#creditcardcollection_cardFirstName')) {
                                            log(await dateTime() + ":" + "Storing First Name");
                                            const firstname = await page.waitForSelector("#creditcardcollection_cardFirstName");
                                            await page.click("#creditcardcollection_cardFirstName");
                                            await firstname.type(name[0])
                                        }

                                        if (await page.$('#creditcardcollection_cardLastName')) {
                                            log(await dateTime() + ":" + "Storing Second Name");
                                            const lastname = await page.waitForSelector("#creditcardcollection_cardLastName");
                                            await page.click("#creditcardcollection_cardLastName");
                                            if(name[1]){
                                                await lastname.type(name[1])
                                            }
                                        }

                                        
                                        if (await page.$('#numOfVehicles')) {
                                            log(await dateTime() + ":" + "Selecting vehicles");
//                                            const numOfVehicle = await page.waitForSelector("#numOfVehicles");
//                                            await page.click("#numOfVehicles");
//                                            await numOfVehicle.type(common.form_vehicle);
                                                await page.$('input#numOfVehicles',common.form_vehicle);
                                        }
                                        await sleep(1000)
                                        if (await page.$('#numOfOccup1') && !setadult) {
                                            log(await dateTime() + ":" + "Storing Free");
                                            const numOfOccup1 = await page.waitForSelector("#numOfOccup1");
                                            await page.click("#numOfOccup1");
                                            await numOfOccup1.type(common.form_adults)
                                        }else{
                                            if (await page.$('#numOfOccupants') && !setadult) {
                                                const numOfOccup1 = await page.waitForSelector("#numOfOccupants");
                                                await page.click("#numOfOccupants");
                                                await numOfOccup1.type(common.form_adults);
                                            }
                                        }

                                        await sleep(1000)

                                        if (await page.$('#agreementCheck')) {
                                            log(await dateTime() + ":" + "Selecting Tearms and Condition");
                                            await page.click("#agreementCheck");
                                            await sleep(1000)
                                        }

                                        await sleep(2000);
                                        log(await dateTime() + ":" + "Clicking on Continue to Cart button");
                                        await page.click('.btn-success')
                                        await sleep(3000);
                                        // if (await page.$('.alert-danger')) {
                                        //     let element = await page.$('.alert-danger');
                                        //     let errorMsg = await page.evaluate(el => el.textContent, element)
                                        //     comment = errorMsg;
                                        //     bookingStatus = false;
                                        //     log(await dateTime() + ":" + errorMsg);
                                        // } else {
                                            // await page.evaluate(() => {
                                            //     document.querySelector('.btn-success').scrollIntoView()
                                            // });

                                            // log(await dateTime() + ":" + "Waiting for review section");
                                            // await page.click('.btn-success')
                                            // await sleep(3000);

                                            // log(await dateTime() + ":" + "Waiting for Checkout section");
                                            // if(await page.$(".encapsulate")){
                                            //     await page.evaluate(() => {
                                            //         document.querySelector('.encapsulate').scrollIntoView()
                                            //     });
                                            // }

                                            // var name = common.form_name_on_credit_card.split(' ');
                                              
                                            // if (await page.$('#firstNamePRCG')) {
                                            //     log(await dateTime() + ":" + "Storing First Name");
                                            //     const firstname = await page.waitForSelector("#firstNamePRCG");
                                            //     await page.click("#firstNamePRCG");
                                            //     await firstname.type(name[0])
                                            // }

                                            // if (await page.$('#lastNamePRCG')) {
                                            //     log(await dateTime() + ":" + "Storing Second Name");
                                            //     const lastname = await page.waitForSelector("#lastNamePRCG");
                                            //     await page.click("#lastNamePRCG");
                                            //     if(name[1]){
                                            //         await lastname.type(name[1])
                                            //     }
                                            // }

                                            // if (await page.$('#cardTypePRCG')) {
                                            //     log(await dateTime() + ":" + "Selecting Card Type");
                                            //     if(common.form_creditCardType == 'MC'){
                                            //         common.form_creditCardType="MAST";
                                            //     }
                                            //     await page.select('#cardTypePRCG', common.form_creditCardType)
                                            // }

                                            // if (await page.$('#cardNumberPRCG')) {
                                            //     log(await dateTime() + ":" + "Storing card number ");
                                            //     const cardNumber = await page.waitForSelector("#cardNumberPRCG");
                                            //     await page.click("#cardNumberPRCG");
                                            //     await cardNumber.type(String(common.form_creditCard))
                                            // }

                                            // if (await page.$('#expMonthPRCG')) {
                                            //     log(await dateTime() + ":" + "Selecting month");
                                            //     await page.select('#expMonthPRCG', common.form_creditCardExpMonth);
                                            // }

                                            // if (await page.$('#expYearPRCG')) {
                                            //     log(await dateTime() + ":" + "Selecting Year");
                                            //     await page.select('#expYearPRCG', common.form_creditCardExpYear);
                                            // }
                                            
                                            
                                            log(await dateTime() + ":" + "Clicking on Continue to Make Payment");
                                            await page.click('div.form-group .btn-success')

                                            log(await dateTime() + ":" + "Scrolling to acknowledgement checkbox");
                                            await page.waitForSelector(".checkbox input");
                                            await page.evaluate(() => {
                                                document.querySelector('.checkbox input').scrollIntoView()
                                            });

                                            if(!setcard){
                                                await sleep(1000)
                                                var name = common.form_name_on_credit_card.split(' ');
                                              
                                                if (await page.$('#firstNamePRCG')) {
                                                    log(await dateTime() + ":" + "Storing First Name");
                                                    const firstname = await page.waitForSelector("#firstNamePRCG");
                                                    await page.click("#firstNamePRCG");
                                                    await firstname.type(name[0])
                                                }

                                                if (await page.$('#lastNamePRCG')) {
                                                    log(await dateTime() + ":" + "Storing Second Name");
                                                    const lastname = await page.waitForSelector("#lastNamePRCG");
                                                    await page.click("#lastNamePRCG");
                                                    if(name[1]){
                                                        await lastname.type(name[1])
                                                    }
                                                }

                                                if (await page.$('#cardTypePRCG')) {
                                                    log(await dateTime() + ":" + "Selecting Card Type");
                                                    if(common.form_creditCardType == 'MC'){
                                                        common.form_creditCardType="MAST";
                                                    }
                                                    await page.select('#cardTypePRCG', common.form_creditCardType)
                                                }

                                                if (await page.$('#cardNumberPRCG')) {
                                                    log(await dateTime() + ":" + "Storing card number ");
                                                    const cardNumber = await page.waitForSelector("#cardNumberPRCG");
                                                    await page.click("#cardNumberPRCG");
                                                    await cardNumber.type(String(common.form_creditCard))
                                                }

                                                if (await page.$('#expMonthPRCG')) {
                                                    log(await dateTime() + ":" + "Selecting month");
                                                    await page.select('#expMonthPRCG', common.form_creditCardExpMonth);
                                                }

                                                if (await page.$('#expYearPRCG')) {
                                                    log(await dateTime() + ":" + "Selecting Year");
                                                    await page.select('#expYearPRCG', common.form_creditCardExpYear);
                                                    setcard=true;
                                                }
                                            }
                                            // console.log("setcard:"+setcard)
                                            if(!setcard){
                                                await sleep(1000)
                                                var name = common.form_name_on_credit_card.split(' ');
                                                // console.log("in coming setcard")
                                                
                                                var url = browseUrl.split("/");
                                                // console.log(url[5]);
                                                var idVal=url[5];
                                                if (await page.$("#firstName"+idVal)) {
                                                    log(await dateTime() + ":" + "Storing First Name");
                                                    const firstname = await page.waitForSelector("#firstName"+idVal);
                                                    await page.click("#firstName"+idVal);
                                                    await firstname.type(name[0])
                                                }

                                                if (await page.$('#lastName'+idVal)) {
                                                    log(await dateTime() + ":" + "Storing Second Name");
                                                    const lastname = await page.waitForSelector("#lastName"+idVal);
                                                    await page.click("#lastName"+idVal);
                                                    if(name[1]){
                                                        await lastname.type(name[1])
                                                    }
                                                }

                                                if (await page.$('#cardType'+idVal)) {
                                                    log(await dateTime() + ":" + "Selecting Card Type");
                                                    if(common.form_creditCardType == 'MC'){
                                                        common.form_creditCardType="MAST";
                                                    }
                                                    await page.select('#cardType'+idVal, common.form_creditCardType)
                                                }

                                                if (await page.$('#cardNumber'+idVal)) {
                                                    log(await dateTime() + ":" + "Storing card number ");
                                                    const cardNumber = await page.waitForSelector("#cardNumber"+idVal);
                                                    await page.click("#cardNumber"+idVal);
                                                    await cardNumber.type(String(common.form_creditCard))
                                                }

                                                if (await page.$('#securityCode')) {
                                                    log(await dateTime() + ":" + "Storing card number ");
                                                    const cardNumber = await page.waitForSelector("#securityCode");
                                                    await page.click("#securityCode");
                                                    await cardNumber.type(String(common.form_creditCardCode))
                                                }

                                                if (await page.$('#expMonth'+idVal)) {
                                                    log(await dateTime() + ":" + "Selecting month");
                                                    await page.select('#expMonth'+idVal, common.form_creditCardExpMonth);
                                                }

                                                if (await page.$('#expYear'+idVal)) {
                                                    log(await dateTime() + ":" + "Selecting Year");
                                                    await page.select('#expYear'+idVal, common.form_creditCardExpYear);
                                                }
                                            }
                                            await sleep(1000)
                                            
                                            if (await page.$('#acknowledgement')) {
                                                log(await dateTime() + ":" + "Selecting acknowledgement");
                                                await page.click("#acknowledgement");
                                                await sleep(1000)
                                            }

                                            log(await dateTime() + ":" + "Clicking on Continue to Complete Purchase");
                                            await page.click('.btn-success')
                                            
                                            log(await dateTime() + ":" + "log Set card" + setcard);
                                            
                                            await sleep(1000)

                                            if (await page.$(".alert-danger")) {
                                                log(await dateTime() + ":" + "Selecting acknowledgement");
                                                let element = await page.$('.alert-danger');
                                                let errorMsg = await page.evaluate(el => el.textContent, element)
                                                comment = errorMsg;
                                                //comment = 'Booking done for this campground';
                                                bookingStatus = true;
                                                log(await dateTime() + ":" + comment);
                                                log(await dateTime() + ":" + errorMsg);
                                                browseClose = false;
                                            } else {
                                                bookingStatus = true;
                                                comment = "Booking done for this campground";
                                                log(await dateTime() + ":" + comment);
                                                log(await dateTime() + ":" + "Order Confirm");
                                                browseClose = false;
                                            }
                                        // }
                                        // }else{
                                        //   console.log("in else not found")  
                                        // }
                                    } else {
                                        bookingStatus = false;
                                        comment = "Sometghing went wrong when click on book button";
                                        log(await dateTime() + ":" + "Sometghing went wrong when click on book button");
                                    }
                                } else {
                                    bookingStatus = false;
                                    comment = "Campsite Not Available";
                                    log(await dateTime() + ":" + "Campsite Not Available For this Campground");
                                }
                            } else {
                                bookingStatus = false;
                                comment = "Booking Not Found For this Campground";
                                log(await dateTime() + ":" + "Booking Not Found For this Campground");
                            }

                        } else {
                            comment = "No Site Category Found";
                            bookingStatus = false;
                            log(await dateTime() + ":" + comment);
                        }
                    }


                }
            } catch (err) {
                log(await dateTime() + ":" + "Sorry, an error has ocurred")
                comment = 'Sorry, an error has ocurred';
                bookingStatus = false;
                console.log(err)
            }

        } catch (err) {
            console.log(err);
            bookingStatus = false;
            log(await dateTime() + ":" + "Sometghing Went Wrong")
            comment = 'Sometghing Went Wrong';
        }
        if (browseClose) {
            log(await dateTime() + ":" + "Browser Closed")
            await browser.close();
        }
    }
    await bookingApi.storeBookingCommentAndLog(updateBookingUrl, bookingId, campgroundId, bookingStatus, token, comment);
})();


async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function log(txt) {
    console.log(txt);
    // console.log('*************************');
    debugLog.push(txt);
}
async function getSelctBoxOptionValue(selector, title, page) {
    // console.log("in select box function come")
    var value = await page.evaluate(async (title, selector) => {
        const example = document.querySelector(selector);
        const example_options = example.querySelectorAll('option');

        for (var i = 0; i < example_options.length; i++) {
            console.log(example_options[i].text + '==' + title)
            if (example_options[i].text == title) {
                console.log(example_options[i].value)
                return example_options[i].value;
            }
        }
        return '';
    }, title, selector);
    return value;
}