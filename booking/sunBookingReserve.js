const e = require('express');
var common = require('./utils/common')

var configPath = common.configPath;
var bookingApi = require(configPath + '/utils/bookingapi')

const puppeteer = common.puppeteer;
var express = common.express;
var fs = common.fs;
const args = common.args;
const path = common.path;


var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var siteUrl = args.site_url.replace(/@@/g, '&');
var token = args.token;

var closeBrowser = true;
var bookingStatus = true;

var comment = common.comment;
var debugLog = common.debugLog;

var fetchBookingFormDataUrl = common.fetchBookingFormDataUrl;
var updateBookingUrl = common.updateBookingUrl;

async function changeDateFormat(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var dayName = checkDate.toLocaleString("default", { weekday: "short" })
    var monthName = checkDate.toLocaleString("default", { month: "short" })
    var dayNumber = checkDate.getDate();

    var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateString;
}

async function getEnquiryDataID() {
    debugLog.push(await bookingApi.dateTime() + ":" + 'Fetching booking enquiry detail by id');
    var formData = await bookingApi.fetchBookingFormData(fetchBookingFormDataUrl, bookingId, token)
    console.log(formData);
    if (formData && formData.code == 200) {
        var data = formData.response;
        common.form_siteCategory = data['details']['reservation_type'];;
        common.form_checkIn = await changeDateFormat(data['check_in_date']);
        common.form_checkOut = await changeDateFormat(data['check_out_date']);
        common.form_adults = data['details']['adults'];
        common.form_petGroup = data['details']['pet_friendly'];
        common.form_equipmentType = data['details']['equipment_type'];
        common.form_equipmentLength = data['details']['length'];
        common.form_slideOutsGroup = data['details']['slide_out'];
        var name = data['name'].split(' ');
        common.form_firstName = name[0];
        common.form_lastName = name[1];
        common.form_address1 = data['address1'];
        common.form_address2 = data['address2'];
        common.form_countryCode = data['country_code'];
        common.form_phone = data['phone'];
        common.form_postcode = data['post_code'];
        common.form_state = data['state'];
        common.form_email = data['email'];
        common.form_campsite = data['details']['campsite'];
       
        var cardData = await bookingApi.fetchCardDetail(data['details']['card_detail']['token'], data['details']['card_detail']['user'], data['details']['card_detail']['passphrase'],data['details']['card_detail']['reference']);       
        common.form_city = data['city'];
    } else {
        var message = (formData.message) ? formData.message : 'Something Went Wrong';
        console.log(message)
        comment = message;
        bookignStatus = false;
        debugLog.push(await bookingApi.dateTime() + ":" + message);
    }
}

(async () => {
    try {

        await getEnquiryDataID();

        await campspot();

    } catch (err) {
        console.log(err);
        bookingStatus = false;
        debugLog.push(await bookingApi.dateTime() + ":" + "Sometghing Went Wrong")
        comment = 'Sometghing Went Wrong';
        console.log("Browser Closed");
    }

    await bookingApi.storeBookingCommentAndLog(updateBookingUrl, bookingId, campgroundId, bookingStatus, token, comment);

})();

async function campspot() {
    var browser = await puppeteer.launch({
        headless: false,
        args: [
            '--start-maximized' // you can also use '--start-fullscreen'
        ]
        // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
    });
    console.log('*************************');
    console.log(await browser.version());
    debugLog.push(await bookingApi.dateTime() + ":" + await browser.version());
    // open a new page
    var page = await browser.newPage();

    try {
        // console.log('Nevigating : ' + campgrounds[p].title)
        console.log('Nevigating : ' + siteUrl)
        debugLog.push(await bookingApi.dateTime() + ":" + 'Nevigating : ' + siteUrl);
        await page.setViewport({ width: 1366, height: 768 });

        await page.setDefaultNavigationTimeout(0);

        var browseUrl = siteUrl;
        // var browseUrl = campgrounds[p].link + 'reserve';
        debugLog.push(await bookingApi.dateTime() + ":" + 'Url is : ' + browseUrl);

        await page.goto(browseUrl, { waitUntil: 'networkidle2' });

        await bookingApi.sleep(3000)

        debugLog.push(await bookingApi.dateTime() + ":" + 'Click on search availability link');

        const link = await page.$('#nav > div.nav-drop-classic-holder > div.search-block > div > a');             // declare object

        const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));    // declare promise
        await link.click();                             // click, a new tab opens

        debugLog.push(await bookingApi.dateTime() + ":" + 'New tab open');

        const newPage = await newPagePromise;           // open new tab /window, 

        await bookingApi.sleep(25000);

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input') !== null) {
            debugLog.push(await bookingApi.dateTime() + ":" + 'Fill check in date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input", common.form_checkIn);
        }

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input') !== null) {
            debugLog.push(await bookingApi.dateTime() + ":" + 'Fill check out date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input", common.form_checkOut);
        }

        await newPage.click('#search-form-label-dates');

        await newPage.evaluate(() => {
            document.querySelector('.home-search').scrollIntoView()
        })

        debugLog.push(await bookingApi.dateTime() + ":" + 'Fill guest details');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div') !== null) {
            await bookingApi.sleep(1000);
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
        }

        // for (var i = 0; i < common.form_inFants; i++) {
        //     await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(1) > td:nth-child(2) > div > button.app-increase-guest-count-button-0.guests-picker-menu-category-controls-stepper.mod-increase');
        // }
        // await bookingApi.sleep(1000);
        // for (var k = 0; k < common.form_kids; k++) {
        //     await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(2) > td:nth-child(2) > div > button.app-increase-guest-count-button-1.guests-picker-menu-category-controls-stepper.mod-increase');
        // }
        await bookingApi.sleep(1000);
        if (common.form_adults > 2) {
            common.form_adults = common.form_adults - 2;
            for (var a = 0; a < common.form_adults; a++) {
                await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-increase-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-increase');
            }
        } else {
            if (common.form_adults == 0) {
                common.form_adults = 2;
                for (var a = 0; a < common.form_adults; a++) {
                    await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                }
            }
            if (common.form_adults == 1) {
                for (var a = 0; a < common.form_adults; a++) {
                    await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                }
            }
        }
        await bookingApi.sleep(1000);
        for (var p = 0; p < common.form_petGroup; p++) {
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.guests-picker-menu-category-controls-stepper.mod-increase.app-increase-pet-count-button');
        }
        await bookingApi.sleep(1000);


        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
        await bookingApi.sleep(1000);
        debugLog.push(await bookingApi.dateTime() + ":" + 'Click on search button');
        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-submit > button');
        await bookingApi.sleep(1000);

        debugLog.push(await bookingApi.dateTime() + ":" + 'Checking error found or not');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > div.search-form-error-message') != null) {
            debugLog.push(await bookingApi.dateTime() + ":" + 'Error found in search page');

            let element = await newPage.$('.search-form-error-message');
            let elementText = await newPage.evaluate(el => el.textContent, element)
            console.log(elementText);
            bookingStatus = false;
            comment = elementText;
        } else {
            await bookingApi.sleep(5000);
            debugLog.push(await bookingApi.dateTime() + ":" + 'Check campsite is found or not');
            if (await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div') !== null) {
                debugLog.push(await bookingApi.dateTime() + ":" + 'No campsite found');

                let element = await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div');
                let elementText = await newPage.evaluate(el => el.textContent, element)
                console.log(elementText)
                comment = elementText;
                bookingStatus = false;
            } else {
                debugLog.push(await bookingApi.dateTime() + ":" + 'Campsite found');
                var sitefound = false;
                var equipfound = false;
                if (await newPage.$('.app-search-results-list') !== null) {
                    await bookingApi.sleep(1000);
                    debugLog.push(await bookingApi.dateTime() + ":" + 'Search campsite as par reservation type');
                    var form_siteCategory = common.form_siteCategory;
                    if (await newPage.$('.search-main  > div > div > div > div') != null) {

                        var test = await newPage.evaluate(async (form_siteCategory) => {
                            var sitetypeTotal = await document.querySelectorAll('.search-main  > div > div > div > div');
                            var found = false;
                            for (var t = 0; t < sitetypeTotal.length; t++) {
                                var title = sitetypeTotal[t].querySelector('button >h3:nth-child(1)').textContent.trim();

                                if (title == form_siteCategory) {

                                    sitetypeTotal[t].querySelector('button >h3:nth-child(1)').click();
                                    found = true;
                                }
                            }
                            return found;
                        }, form_siteCategory)
                        await bookingApi.sleep(2000)
                        if (!test) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'No site Category found');

                            console.log('No site Category found...')
                            comment = 'No site Category found...';
                            bookingStatus = false;
                        } else {
                            console.log("Site Category Found");
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Campsite found as per reservation type');

                            sitefound = true;
                            await bookingApi.sleep(2000)
                                newPage.on('console', msg => {
                                            for (let i = 0; i < msg._args.length; ++i)
                                                console.log(`${msg._args[i]}`)
                                        });
                            var campsite=common.form_campsite;
                            var selectEquipment = await newPage.evaluate(async (campsite) => {
                                var totalLbl = document.querySelectorAll(".search-results-list-item");
                                // console.log(totalLbl.length)
                               var eflag=false;
                                for (var l = 0; l < totalLbl.length; l++) {
                                    var equipmentName = totalLbl[l].querySelector('div>div>h1').textContent.trim();
                                    var equipmentVal = equipmentName.replace(/\d+/g, '').trim();
                                    if(eflag==false){
                                        if(campsite.indexOf(equipmentVal) > -1){
                                            eflag=true;
                                            totalLbl[l].querySelector("a").click();

                                            console.log("Contain name:"+equipmentName +" : "+campsite);
                                        
                                        }else{
                                            eflag=false;
                                            console.log("Not Contain name:"+equipmentName+" : "+campsite);
                                        }    
                                    }
                                }
                                return eflag;
                            }, campsite);
                            equipfound=selectEquipment;
                                        

                            // await newPage.click('body > app-root > div > main > search-results > div > section.search-main > div > list-view > search-results-list > div > ul:nth-child(1) > li > a');
                        }

                    }
                    await bookingApi.sleep(10000)
                }
                if (sitefound) {
                    if (equipfound) {
                    await bookingApi.sleep(1000);
                    debugLog.push(await bookingApi.dateTime() + ":" + 'Click on campsite to book');
                    // var a=await newPage.$$(".app-campsite-type-name");
                    
                    
                    // console.log('in site list page')
                    await bookingApi.sleep(1000);
                    await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button');
                    await bookingApi.sleep(3000);

                    //Checking if Location Field is Presernt or not
                    debugLog.push(await bookingApi.dateTime() + ":" + 'Checking if Location Field is Present or not');
                    // console.log('Checking if Location Field is Present or not')
                    // if (await newPage.$('.site-booking-form-location-selection') !== null) {
                    let element = await newPage.$('.site-booking-form-location-selection');
                    let elementText = await newPage.evaluate(el => el.textContent, element)
                    // console.log(elementText)
                    debugLog.push(await bookingApi.dateTime() + ":" + 'Checking if Location Field Has value or not');
                    // console.log('Checking if Location Field Has value or not')
                    var campsiteFlag = false;

                    if (elementText.trim() != '') {
                        // console.log('If Location Already inserted then clicking on add to cart button')
                        await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal > div > form > div.mobile-modal-add-to-cart > input');
                        campsiteFlag=true;
                    } else {
                        debugLog.push(await bookingApi.dateTime() + ":" + 'If Location Field is Blanke the Clicking on location and selecting First Location');

                        // console.log('If Location Field is Blanke the Clicking on location and selecting First Location')
                        await newPage.click('.site-booking-form-location-button');
                        await bookingApi.sleep(2000)

                        if (await newPage.$('.site-locations-table>tr') !== null) {

                            let elements = await newPage.$$('.site-locations-table>tr.site-locations-table-site');
                            var titleInfo = "";

                            for (const el of elements) {
                                if (await el.$('td.site-locations-table-site-number') !== null) {
                                    var title = await el.$('td.site-locations-table-site-number');
                                    var titleInfo = await newPage.evaluate(el => el.textContent, title);
                                    titleInfo = titleInfo.replace(/\s+/g, ' ').trim();

                                    if (titleInfo == common.form_campsite) {
                                        console.log("Matched Campsite:" + common.form_campsite)
                                        campsiteFlag = true;
                                        el.click();
                                        await bookingApi.sleep(1000)
                                        await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button')
                                    }
                                }


                            }
                        }

                        // await newPage.click('#locations > table > tr:nth-child(2) > td.site-locations-table-site-select > button');

                    }
                    if (campsiteFlag) {

                    } else {
                        if (common.form_campsite == '') {
                            await bookingApi.sleep(2000)
                            debugLog.push(await bookingApi.dateTime() + ":" + "Clicking on reserve Button ");
                            await newPage.click('#locations > table > tr:nth-child(2) > td.site-locations-table-site-select > button');
                            console.log('campsite is not provided so selecting first available site');
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button');
                            campsiteFlag = true;
                        }
                    }
                    // //Clicking on Review Cart
                    // await newPage.click('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a');
                    // } else {
                    // console.log('Checking if Location Field is not present then Filling other values Like Equipement and related fields')

                    if (campsiteFlag) {
                        debugLog.push(await bookingApi.dateTime() + ":" + 'Checking if Location Field is not present then Filling other values Like Equipement and related fields');

                        if (await newPage.$('#rv-type-select') !== null) {
                            // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.select('#rv-type-select', common.form_equipmentType)
                            await bookingApi.sleep(1000)
                        }
                        await bookingApi.sleep(1000)
                        if (await newPage.$('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.site-booking-form-section.site-booking-form-rv.app-booking-form-rv > div:nth-child(2) > label.site-booking-form-rv-length.app-rv-length-filter > input') !== null) {

                            await newPage.type('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.site-booking-form-section.site-booking-form-rv.app-booking-form-rv > div:nth-child(2) > label.site-booking-form-rv-length.app-rv-length-filter > input', common.form_equipmentLength)

                        }

                        if (await newPage.$('#rv-slide-out-select') !== null) {
                            // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.select('#rv-slide-out-select', common.form_slideOutsGroup)
                            await bookingApi.sleep(1000)
                        }
                        await bookingApi.sleep(1000);

                        if (await newPage.$('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input') !== null) {
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input');
                        }
                        await bookingApi.sleep(1000);
                        // After Filing Those detail Popup is open so decline it
                        debugLog.push(await bookingApi.dateTime() + ":" + 'After Filing Those detail Popup is open so decline it');

                        if (await newPage.$('body > app-root > div > main > site > lock-site-modal > div > div > div.site-lock-modal-actions > button.site-lock-modal-actions-button.mod-decline') !== null) {
                            // debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.click('body > app-root > div > main > site > lock-site-modal > div > div > div.site-lock-modal-actions > button.site-lock-modal-actions-button.mod-decline')
                            await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a') !== null) {
                            // debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.click('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a')
                            await bookingApi.sleep(1000)
                        }
                        await bookingApi.sleep(1000)
                        debugLog.push(await bookingApi.dateTime() + ":" + 'clicking on Checkout Button');

                        console.log('clicking on Checkout Button')
                        if (await newPage.$('body > app-root > div > main > shopping-cart > div > section.cart-summary > div > a') !== null) {
                            // console.log('in button');
                            // debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.click('body > app-root > div > main > shopping-cart > div > section.cart-summary > div > a')
                            await bookingApi.sleep(1000)
                        }
                        await bookingApi.sleep(10000)

                        console.log('Filing Guest Information');
                        debugLog.push(await bookingApi.dateTime() + ":" + 'Filing Guest Information');

                        if (await newPage.$('#first-name') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filing first name');

                            const firstname = await newPage.waitForSelector("#first-name");
                            await newPage.click("#first-name");
                            await firstname.type(common.form_firstName)
                        }
                        if (await newPage.$('#guest-full-name-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filing guest full name');

                            const firstname = await newPage.waitForSelector("#guest-full-name-input");
                            await newPage.click("#guest-full-name-input");
                            await firstname.type(common.form_firstName + ' ' + common.form_lastName)
                        }
                        // await bookingApi.sleep(1000)

                        // debugLog.push(await bookingApi.dateTime() + ":" + "Storing Last Name")
                        if (await newPage.$('#last-name') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filing last name');
                            const lastname = await newPage.waitForSelector("#last-name");
                            await newPage.click("#last-name");
                            await lastname.type(common.form_lastName)
                            // await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-country') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Selecting guest country');
                            await newPage.select('#guest-country', common.form_countryCode)
                            await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-city-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filing City');
                            const city = await newPage.waitForSelector("#guest-city-input");
                            await newPage.click("#guest-city-input");
                            await city.type(common.form_city)
                            // await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-region-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filing Region');
                            const region = await newPage.waitForSelector("#guest-region-input");
                            await newPage.click("#guest-region-input");
                            await region.type(common.form_state)
                            // await bookingApi.sleep(1000)
                        }

                        // if (await newPage.$('#country-select') !== null) {
                        //     debugLog.push(await bookingApi.dateTime() + ":" + 'Selecting country');

                        //     // debugLog.push(await bookingApi.dateTime() + ":" + "Selecting Card Type");
                        //     await newPage.select('#country-select', common.form_countryCode)
                        //     await bookingApi.sleep(1000)
                        // }

                        if (await newPage.$('#guest-address-line-1') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filling guest address 1');
                            // debugLog.push(await bookingApi.dateTime() + ":" + "Storing Address 1")
                            const address1 = await newPage.waitForSelector("#guest-address-line-1");
                            await newPage.click("#guest-address-line-1");
                            await address1.type(common.form_address1)
                            // await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('#address-line-1') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filling address 1');

                            // debugLog.push(await bookingApi.dateTime() + ":" + "Storing Address 1")
                            const address1 = await newPage.waitForSelector("#address-line-1");
                            await newPage.click("#address-line-1");
                            await address1.type(common.form_address1)
                            // await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#address-line-2') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filling address 2');

                            // debugLog.push(await bookingApi.dateTime() + ":" + "Storing Address 2")
                            const address2 = await newPage.waitForSelector("#address-line-2");
                            await newPage.click("#address-line-2");
                            await address2.type(common.form_address2)
                            // await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-postal-code-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filling guest postal code');

                            const postCode = await newPage.waitForSelector("#guest-postal-code-input");

                            await newPage.click("#guest-postal-code-input");
                            await postCode.type(common.form_postcode)
                            // await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('#postal-code') !== null) {

                            debugLog.push(await bookingApi.dateTime() + ":" + 'Filling post code');
                            const postCode = await newPage.waitForSelector("#postal-code");
                            await newPage.click("#postal-code");
                            await postCode.type(common.form_postcode)
                            // await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('#guest-province-select') !== null) {
                            console.log('in sellcting state:' + common.form_state);
                            debugLog.push(await bookingApi.dateTime() + ":" + 'Selecting guest staye');
                            await newPage.select('#guest-province-select', common.form_state)
                            await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-phone-number-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Storing Guest Phone Number")
                            const phoneNo = await newPage.waitForSelector("#guest-phone-number-input");
                            await newPage.click("#guest-phone-number-input");
                            await phoneNo.type(common.form_phone)
                            // await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('#phone-number') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Storing Phone Number")
                            const phoneNo = await newPage.waitForSelector("#phone-number");
                            await newPage.click("#phone-number");
                            await phoneNo.type(common.form_phone)
                            // await bookingApi.sleep(1000)
                        }

                        if (await newPage.$('#guest-email-input') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Storing guest email")
                            const email = await newPage.waitForSelector("#guest-email-input");
                            await newPage.click("#guest-email-input");
                            await email.type(common.form_email)
                            // await bookingApi.sleep(1000)
                        }
                        if (await newPage.$('#email-address') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Storing email")
                            const email = await newPage.waitForSelector("#email-address");
                            await newPage.click("#email-address");
                            await email.type(common.form_email)
                            // await bookingApi.sleep(1000)
                        }
                        // if (await newPage.$('#referral-source') !== null) {
                        //     debugLog.push(await bookingApi.dateTime() + ":" + "Selecting referral source");
                        //     await newPage.select('#referral-source', common.form_referal_resource)
                        //     await bookingApi.sleep(1000)
                        // }
                        // if (await newPage.$('#reason-for-visit') !== null) {
                        //     debugLog.push(await bookingApi.dateTime() + ":" + "Selecting reason for visit");
                        //     await newPage.select('#reason-for-visit', common.form_reason_visit)
                        //     await bookingApi.sleep(1000)
                        // }
                        await bookingApi.sleep(5000)
                        console.log('click on Submit Button')
                        debugLog.push(await bookingApi.dateTime() + ":" + "click on Submit Button");
                        if (await newPage.$('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(2) > div > div.checkout-form-submit > button') !== null) {
                            await newPage.click('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(2) > div > div.checkout-form-submit > button');
                        }

                        await bookingApi.sleep(2000)
                        debugLog.push(await bookingApi.dateTime() + ":" + "Checking error in form");
                        if (await newPage.$('.checkout-form-error-summary-content') != null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Error in form filling");

                            let element = await newPage.$('.checkout-form-error-summary-content');
                            let elementText = await newPage.evaluate(el => el.textContent, element)
                            console.log(elementText)
                            comment = elementText;
                            bookingStatus = false;
                        }

                        if (await newPage.$('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail.app-guest-information > form > button') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Click on submit ");
                            await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail.app-guest-information > form > button');
                            await bookingApi.sleep(30000)
                        }
                        
                        if (await newPage.$('.checkout-form-submit-button') !== null) {
                            debugLog.push(await bookingApi.dateTime() + ":" + "Click on Continue to payment method. ");
                            await newPage.click('.checkout-form-submit-button');
                        }
                      
                        await bookingApi.sleep(3000)
                        
                        let selector = 'input[name="payment-method-credit-card"]';
                        await newPage.evaluate((selector) => document.querySelector(selector).click(), selector); 

                        await bookingApi.sleep(1500)
                        
                        var cardNo=common.form_creditCard;
                       
                        await newPage.evaluate((cardNo) => {
                            let dom = document.querySelector('#payment-card-number .checkout-form-field-label');
                            dom.parentNode.append(cardNo);
                        },cardNo);
                        // const frameHandle = await newPage.$("#tokenFrame");
                        // const frame = await frameHandle.contentFrame();
                        // // const elementHandle = await newPage.$('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(4) > div > div > form >label>iframe > #document');
                        // // const frame = await elementHandle.contentFrame();
                        // console.log('frame is')
                        // console.log(frame)
                        // if (frame != null) {

                        //     if (await frame.$('#ccnumfield') !== null) {
                        //         console.log('ccnumber filling')
                        //         const cardNumber = await frame.waitForSelector("#ccnumfield");
                        //         await frame.click("#ccnumfield");
                        //         await cardNumber.type(common.form_creditCard)
                        //     }


                        // } else {
                        //     console.log('in else')
                        //     if (await newPage.$('#ccnumfield') !== null) {
                        //         console.log('ccnumber filling')

                        //         const cardNumber = await newPage.waitForSelector("#ccnumfield");
                        //         await newPage.click("#ccnumfield");
                        //         await cardNumber.type(common.form_creditCard)
                        //     }


                        // }
                        if(await newPage.waitForSelector("#month")){
                            const expMonth = await newPage.waitForSelector("#month");
                            await newPage.click("#month");
                            await expMonth.type(common.form_creditCardExpMonth)    
                            closeBrowser = false;
                        }

                        if(await newPage.waitForSelector("#year")){
                            const expYear = await newPage.waitForSelector("#year");
                            await newPage.click("#year");
                            await expYear.type(common.form_creditCardExpYear.substr(-2))
                            closeBrowser = false;
                        }
                        
                        if(await newPage.waitForSelector("#payment-security-code-input")){ 
                            const cardCode = await newPage.waitForSelector("#payment-security-code-input");
                            await newPage.click("#payment-security-code-input");
                            await cardCode.type(common.form_creditCardCode.trim())
                            closeBrowser = false;
                            console.log("Booking Successfully Done.")
                            debugLog.push(await bookingApi.dateTime() + ":" + "Booking Successfully Done.");

                            comment = "Booking Successfully Done."
                            bookingStatus = true;
                        }

                        
                        // await bookingApi.sleep(3000)
                        // console.log('click on continue order review')
                        // await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > button');

                        // await bookingApi.sleep(3000)
                        // await newPage.click('#agree-to-terms-and-conditions');
                        // await bookingApi.sleep(1000)

                        // await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > button');
                    } else {
                        console.log('No site Category found...')
                        comment = 'Campsite name not matched which you provided.';
                        bookingStatus = false;
                    }
                } else {
                    debugLog.push(await bookingApi.dateTime() + ":" + 'No site matched');

                    console.log('No site matched...')
                    comment = 'No site matched';
                    bookingStatus = false;
                }   
                } else {
                    debugLog.push(await bookingApi.dateTime() + ":" + 'No site Category found');

                    console.log('No site Category found...')
                    comment = 'No site Category found...';
                    bookingStatus = false;
                }
            }
            await bookingApi.sleep(1000)
        }
        if (closeBrowser) {
            console.log(comment)
            debugLog.push(await bookingApi.dateTime() + ":" + "Browser Closed ")
            await newPage.close();
            await browser.close();
        }
    } catch (err) {
        console.log(err.message)
        // await newPage.close();
        await browser.close();
        debugLog.push(await bookingApi.dateTime() + ":" + "Sorry, an error has ocurred ")
        comment = 'Sorry, an error has ocurred';
        bookingStatus = false;
        console.log(comment)
    }
}