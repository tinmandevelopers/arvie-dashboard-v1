
const axios = require('axios');
const { api_token } = require('../utils/common');

var common = require('../utils/common')
var configPath = '../';
var filePathStore = '';

const FormData = require('form-data');

var self = module.exports = {
    bookingInitiate: async (url, bookingId) => {
        return new Promise(async function (resolve, reject) {
            await axios.post(url, {
                booking_id: bookingId,
            })
                .then(async response => {
                    var data = response.data;
                    return resolve(data);
                })
                .catch(async error => {
                    return resolve(error.response.data);
                })
        })
    },
    fetchBookingFormData: async (url, bookingId, token) => {
        // console.log("token:" + token)
        return new Promise(async function (resolve, reject) {
            await axios.post(url, {
                booking_id: bookingId,
                token: token,
            })
                .then(async response => {
                    var data = response.data;
                    return resolve(data);
                })
                .catch(async error => {
                    return resolve(error.response.data);
                })
        })
    },
    fetchCardDetail: async (tokenval, user, passphrase, reference) => {
        // console.log("token:" + token)
        var url = common.cardUrl;
        var env = common.env;
        return new Promise(async function (resolve, reject) {
            await axios.get(url,{ params: { token: tokenval,user:user,passphrase:passphrase,reference:reference } })
                .then(async response => {
                    var data = response.data;
                    // var my= response.data.card_expiry.split("-");
                    // console.log(env);
                    if(env=="local"){
                        common.form_creditCard="4242424242424242";
                        common.form_creditCardType="VISA";
                        common.form_creditCardExpMonth="03";
                        common.form_creditCardExpYear="2025";
                        common.form_name_on_credit_card="Test user";
                        common.form_creditCardCode="123";
                    }else{
                        common.form_creditCard=data.card_number;
                        common.form_creditCardType=data.card_type;
                        //common.form_creditCardExpMonth=data.card_exp_month;
                        common.form_creditCardExpMonth=(data.card_exp_month.length == 1 && data.card_exp_month < 10) ? "0"+data.card_exp_month : data.card_exp_month;
                        common.form_creditCardExpYear=data.card_exp_year;
                        common.form_name_on_credit_card=data.card_user_name;
                        common.form_creditCardCode=data.card_cvv;
                    
                    }
                    return resolve(data);
                })
                .catch(async error => {
                    return resolve(error);
                })
        })
    },
    updateBooking: async (url, data) => {
        return new Promise(async function (resolve, reject) {
            await axios.post(url, data)
                .then(async response => {
                    var data = response.data;
                    return resolve(data);
                })
                .catch(async error => {
                    return resolve(error.response.data);
                })
        })
    },
    uploadBookingFile: async (bookingId, token, filepath, s3path) => {

        return new Promise(async function (resolve, reject) {
            // var data=;

            var url = common.uploadUrl;
            await axios.post(url, {
                booking_id: bookingId,
                token: token,
                file_content: JSON.stringify(common.debugLog),
                file_path: s3path
            })
                .then(async response => {
                    var data = response.data;
                    return resolve(data);
                })
                .catch(async error => {
                    return resolve(error);
                })

        })
    },
    storeBookingCommentAndLog: async (updateBookingUrl, bookingId, campgroundId, bookingStatus, token, comment) => {
        return new Promise(async function (resolve, reject) {
            // console.log("Addeding Store Booking Comment And Log");
            // common.fs.writeFileSync(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json", JSON.stringify(common.debugLog));
            // const filepath = common.path.resolve(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json");

            // var s3result;
            // var crawlerLogfilename = new Date().getTime() + '_booking_log.json';
            var crawlerLogfilename = "";
            // var s3path = 'booking/' + campgroundId + '/' + bookingId + '/' + crawlerLogfilename;
            // s3result = await self.uploadBookingFile(bookingId, token, filepath, s3path);
            // // console.log(s3result)
            // if (s3result && s3result.code == 200) {
            //     console.log("successfully uploaded Booking Crawler log file on server");
            //     common.fs.unlinkSync(filepath)
            // } else {
            //     crawlerLogfilename = '';
            //     console.log("Failed to upload Booking Crawler log file on server");
            // }

            console.log(await self.dateTime() + ":" + "Storing Comment And Update the Booking Enquiry Status")

            var enquiryUpdate = {
                booking_id: bookingId,
                index_status: 'Completed',
                comment: comment.replace(/(\r\n|\n|\r)/g, ''),
                log_file_path: crawlerLogfilename,
                // updated_at: Date.now(),
                status: (bookingStatus == true) ? 'Accepted' : 'Decline',
                token: token,
            };

            await self.updateBooking(updateBookingUrl, enquiryUpdate);
            // await self.uploadBookingStatusOnServer(bookingId, bookingStatus);
            resolve(true);
        })
    },
    uploadBookingStatusOnServer: async (bookingId, status) => {
        return new Promise(async function (resolve, reject) {

            console.log('Booking status update on server');

            var url = common.webUrl + '/update_booking_status';
            var data = {
                booking_id: bookingId,
                status: (status == true) ? 'Accepted' : 'Decline',
            };
            await axios.post(url, data)
                .then(async response => {
                    var data = response.data;
                    console.log(data)
                    return resolve(data);
                })
                .catch(async error => {
                    console.log(data)
                    return resolve(error.response.data);
                })
        })
    },
    dateTime: async () => {
        return new Promise(async function (resolve, reject) {
            var today = new Date();
            var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var val = date + ' ' + time;
            return resolve(val);
        })
    },
    sleep: async function sleep(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms)
        })
    },
    decodeCardDetail: async (data) => {
        return new Promise(async function (resolve, reject) {
            let buff1 = new Buffer(data, 'base64');
            let res1=buff1.toString('ascii');
            let buff2 = new Buffer(res1, 'base64');
            let res2=buff2.toString('ascii');
            let buff3 = new Buffer(res2, 'base64');
            let result=buff3.toString('ascii');
            return resolve(result);

        })
    }
}