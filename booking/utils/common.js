
const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

var configPath = '.';
var filePathStore = '';
var credit= require('../config/credentials.config');

// var appUrl= 'http://localhost:8000';
var webUrl= 'https://beta.arvie.com/';
// var webUrl= 'https://web.tmdev.co.za/';
var appUrl= 'http://dashboard.tmdev.co.za';
var cardUrl = 'https://arvie:Philip800403@api.pcivault.io/v1/vault';

var fetchBookingFormDataUrl = appUrl + '/api/booking/enquiry/initiate/detail';
var updateBookingUrl = appUrl + '/api/booking/enquiry/initiate/update';
var uploadUrl = appUrl + '/api/booking/enquiry/initiate/file/upload';
// var env="local";
var env="production";

module.exports = {
    env:env,
    puppeteer: puppeteer,
    express: express,
    fs: fs,
    args: args,
    path: path,
    configPath:configPath,
    filePathStore:filePathStore,
    appUrl: appUrl,
    webUrl: webUrl,
    cardUrl: cardUrl,
    fetchBookingFormDataUrl:fetchBookingFormDataUrl,
    updateBookingUrl:updateBookingUrl,
    uploadUrl:uploadUrl,
    api_token:'',
    form_siteCategory: "",
    form_checkIn: "",
    form_checkOut: "",
    form_adults: "",
    form_kids: "",
    form_free: "",
    form_petGroup: "",
    form_equipmentType: "",
    form_equipmentLength: "",
    form_slideOutsGroup: "",
    form_VkrPostalCode: "",
    form_purchaseVkr_Group: "",
    form_firstName: "",
    form_lastName: "",
    form_address1: "",
    form_address2: "",
    form_countryCode: "",
    form_phone: "",
    form_city: "",
    form_state: "",
    form_postcode: "",
    form_email: "",
    form_confirmEmail: "",
    form_vehicle:"",
    form_campsite:"",
    form_creditCard: credit.creditCardNumber,
    form_creditCardType: credit.creditCardType,
    form_creditCardExpMonth: credit.creditCardExpMonth,
    form_creditCardExpYear: credit.creditCardExpYear,
    form_name_on_credit_card: credit.creditCardName,
    form_creditCardCode:credit.creditCardCode,
    debugLog: [],
    comment: '',
    recreationLogin:{user_name:"qrnlgmdytsouvpicre@twzhhq.online",password:"Hello@007"},
    reserveamericaLogin:{user_name:"dummayaccount@mailinator.com",password:"Dummayaccount@123"}
};