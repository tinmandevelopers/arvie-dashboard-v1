<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('screen_name')->nullable();
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('avatar_type')->default('storage');
            $table->string('avatar_location')->nullable();
            $table->text('address')->nullable();
            $table->date('dob')->nullable();
            $table->string('id_number', 15)->nullable();
            $table->string('id_proof')->nullable();
            $table->string('income_tax_number', 50)->nullable();
            $table->string('kin_first_name')->nullable();
            $table->string('kin_last_name')->nullable();
            $table->string('kin_contact')->nullable();
            $table->string('kin_email')->nullable();
            $table->string('medical_membership_number')->nullable();
            $table->string('medical_provider')->nullable();
            $table->string('medical_insurance_plan')->nullable();
            $table->string('medical_membership_card')->nullable();
            $table->string('alternative_email')->nullable();
            $table->text('about_me')->nullable();
            $table->text('specialized_in')->nullable();
            $table->timestamp('password_changed_at')->nullable();
            $table->unsignedTinyInteger('active')->default(1);
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(config('access.users.confirm_email') ? false : true);
            $table->string('timezone')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->boolean('to_be_logged_out')->default(false);
            $table->string('token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
