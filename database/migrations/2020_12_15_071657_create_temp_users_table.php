<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('screen_name')->nullable();
            $table->string('email')->unique();
            $table->string('contact_number')->nullable();
            $table->string('avatar_location')->nullable();
            $table->text('address')->nullable();
            $table->date('dob')->nullable();
            $table->string('id_number', 15)->nullable();
            $table->string('id_proof')->nullable();
            $table->string('income_tax_number', 50)->nullable();
            $table->string('kin_first_name')->nullable();
            $table->string('kin_last_name')->nullable();
            $table->string('kin_contact')->nullable();
            $table->string('kin_email')->nullable();
            $table->string('medical_membership_number')->nullable();
            $table->string('medical_provider')->nullable();
            $table->string('medical_insurance_plan')->nullable();
            $table->string('medical_membership_card')->nullable();
            $table->string('alternative_email')->nullable();
            $table->text('about_me')->nullable();
            $table->text('specialized_in')->nullable();
            $table->text('reason')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->foreign('approved_by')->references('id')->on('users');
            $table->enum('status',['Pending','Approved','Rejected'])->default('Pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_users');
    }
}
