<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_sources', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->string('crawler_name')->nullable();
            $table->string('crawler_file')->nullable();
            $table->string('booking_file')->nullable();
            $table->string('crawler_time')->nullable();
            $table->dateTime('last_crawled')->nullable();
            $table->string('crawler_log')->nullable();
            $table->enum('status',['Running','Complete','Scheduled'])->default('Scheduled');
            $table->text('comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_sources');
    }
}
