<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSourceCrawlerLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_source_crawler_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('data_source_id')->nullable();
            $table->foreign('data_source_id')->references('id')->on('data_sources');
            $table->string('log_file_path')->nullable();
            $table->enum('status',['Success','Failed','InProgress'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_source_crawler_log');
    }
}
