<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampgroundsGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campgrounds_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('data_source_id')->nullable();
            $table->foreign('data_source_id')->references('id')->on('data_sources');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campgrounds_group');
    }
}
