<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campgrounds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('data_source_id')->nullable();
            $table->foreign('data_source_id')->references('id')->on('data_sources');
            $table->unsignedBigInteger('campground_group_id')->nullable();
            $table->foreign('campground_group_id')->references('id')->on('campgrounds_group');
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->json('location')->nullable();
            $table->string('data_file_path')->nullable();
            $table->enum('web_status',['Pending','Listed','Unlisted','Resubmit','Inactive'])->nullable();
            $table->enum('status',['Pending','Active','Inactive'])->nullable();
            $table->enum('index_status',['Pending','InProgress','Completed'])->nullable();
            $table->enum('crawler_status',['Pending','Success','Failed'])->nullable();
            $table->dateTime('last_indexed_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campgrounds');
    }
}
