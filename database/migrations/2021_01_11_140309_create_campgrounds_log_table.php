<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampgroundsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campgrounds_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('crawler_log_id')->nullable();
            $table->foreign('crawler_log_id')->references('id')->on('data_source_crawler_log');
            $table->unsignedBigInteger('campground_id')->nullable();
            $table->foreign('campground_id')->references('id')->on('campgrounds');
            $table->enum('status',['Success','Failed','Pending'])->nullable();
            $table->string('log_file_path')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campgrounds_log');
    }
}
