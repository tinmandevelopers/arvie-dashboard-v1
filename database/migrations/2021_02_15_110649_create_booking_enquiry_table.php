<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_enquiry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('booking_id')->nullable();
            $table->unsignedBigInteger('campground_id')->nullable();
            $table->foreign('campground_id')->references('id')->on('campgrounds');
            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();
            $table->string('name')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('country_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('email')->nullable();
            $table->json('details')->nullable();
            $table->text('comment')->nullable();
            $table->string('log_file_path')->nullable();
            $table->unsignedInteger('assign_id')->nullable();
            $table->unsignedInteger('process_by')->nullable();
            $table->enum('status',['Pending','Accepted','Decline','Cancelled'])->default('Pending');
            $table->enum('index_status',['Pending','InProgress','Completed'])->default('Pending');
            $table->string('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_enquiry');
    }
}
