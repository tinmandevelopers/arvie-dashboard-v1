<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_availability', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('campground_id')->nullable();
            $table->foreign('campground_id')->references('id')->on('campgrounds');
            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();
            $table->json('details')->nullable();
            $table->json('result')->nullable();
            $table->enum('enquiry_from',['Frontend','Backend'])->default('Frontend');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_availability');
    }
}
