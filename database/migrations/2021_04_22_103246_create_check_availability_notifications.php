<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckAvailabilityNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_availability_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sold_out_search_id')->nullable();
            $table->unsignedBigInteger('campground_id')->nullable();
            $table->foreign('campground_id')->references('id')->on('campgrounds');
            $table->string('location')->nullable();
            $table->date('check_in_date')->nullable();
            $table->date('check_out_date')->nullable();
            $table->integer('no_of_days')->nullable();
            $table->json('details')->nullable();
            $table->json('result')->nullable();
            $table->integer('check_till')->nullable();
            $table->dateTime('next_schedule_date')->nullable();
            $table->string('day_of_week')->nullable();
            $table->string('elapsed_time')->nullable();
            $table->integer('positive_hit')->default(0)->nullable();
            $table->string('user_type')->nullable();
            $table->string('duration')->nullable();
            $table->tinyInteger('booking_type')->default(1)->nullable();
            $table->enum('status',['Pending','InProgress','Completed',"Expire"])->default('Pending');
            $table->enum('enquiry_from',['Frontend','Backend'])->default('Frontend');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_availability_notifications');
    }
}
