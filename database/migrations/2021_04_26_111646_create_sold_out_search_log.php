<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldOutSearchLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_out_search_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('check_availability_notification_id')->nullable();
            $table->foreign('check_availability_notification_id')->references('id')->on('check_availability_notifications');
            $table->enum('status',['Success','Failed','Pending'])->default('Pending');
            $table->json('result')->nullable();
            $table->dateTime('log_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_out_search_log');
    }
}
