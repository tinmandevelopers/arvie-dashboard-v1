<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConfirmationSentToBookingEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_enquiry', function (Blueprint $table) {
            $table->tinyInteger('confirmation_mail_sent')->default('0')->after('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_enquiry', function (Blueprint $table) {
            $table->dropColumn('confirmation_mail_sent');
        });
    }
}
