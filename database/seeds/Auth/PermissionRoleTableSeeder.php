<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $agent = Role::create(['name' => config('access.users.agent_role')]);
        $prime_agent = Role::create(['name' => config('access.users.prime_agent_role')]);
        $blue_agent = Role::create(['name' => config('access.users.blue_agent_role')]);

        // Create Permissions
        Permission::create(['name' => 'view backend']);

        // Assign Permissions to other Roles
        // Note: Admin (User 1) Has all permissions via a gate in the AuthServiceProvider
        $agent->givePermissionTo('view backend');
        $prime_agent->givePermissionTo('view backend');
        $blue_agent->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
