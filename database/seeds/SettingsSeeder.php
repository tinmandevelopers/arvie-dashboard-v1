<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            ['email' => 'reserveamerica@reserveamerica.com', 'code' => '4/0AX4XfWgXd9XxndFANls_jo0bRPSBeSUvNChPUFcXAepP1hU2jXEbK-8lVd8nJR8ndEqbRw']
        ]);
    }
}
