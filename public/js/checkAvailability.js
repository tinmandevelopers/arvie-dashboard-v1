
$(document).ready(function () {

  $('input[type=radio][name=slide_out]').change(function () {
    if (this.value == 'Yes') {
      $("#rearblock").show();
    }
    else if (this.value == 'No') {
      $("#rearblock").hide();
    }
  });

  $('input[type=radio][name=towing_vehicle]').change(function () {
    if (this.value == 'Yes') {
      $("#towingblock").show();
    }
    else if (this.value == 'No') {
      $("#towingblock").hide();
    }
  });


  var today = new Date().toISOString().split('T')[0];
  $("#check_in_date").attr('min', today);
  $("#check_out_date").attr('min', today);

  $("#check_in_date").blur(function () {
    var start = $("#check_in_date").val();
    $("#check_out_date").attr('min', start);
  });

  $("#check_out_date").blur(function () {
    var end = $("#check_out_date").val();
    $("#check_in_date").attr('max', end);
  });

  $('#check_availability_form').validate({
    rules: {
      campground_id: {
        required: true
      },
      check_in: {
        required: true,
      },
      check_out: {
        required: true,
      },
      reservation_type: {
        required: true
      },
      adults: {
        required: true,
        min: 1,
      },
      // equipment_type: {
      //   required: true,
      // },
      length: {
        required: true,
        min: 0
      },
      towing_length: {
        required: true,
        min: 0
      },
      rear: {
        required: true,
      },

    },
    submitHandler: function (form) {
      var details = {
        "reservation_type": "",
        "adults": "",
        "equipment_type": "",
        "length": "",
        "towing_vehicle": "",
        "towing_length": "",
        "slide_out": "",
        "rear": "",
        "pet_friendly": "",
        "pull_through": "",
      }
      var formData = new FormData($("form#check_availability_form")[0]);

      var x = $('form').serializeArray();

      $.each(x, function (i, field) {

        if (field.name == 'check_in_date' || field.name == 'check_out_date') {
          var dateAr = field.value.split('-');
          var newDateVal = dateAr[0] + '/' + dateAr[1] + '/' + dateAr[2];
          formData.set(field.name,newDateVal);
        }
        if (field.name == 'reservation_type') {
          details.reservation_type = field.value;
        }
        if (field.name == 'adults') {
          details.adults = field.value;
        }
        if (field.name == 'equipment_type') {
          details.equipment_type = field.value;
        }
        if (field.name == 'length') {
          details.length = field.value;
        }
        if (field.name == 'towing_vehicle') {
          details.towing_vehicle = field.value;
        }
        if (field.name == 'towing_length') {
          details.towing_length = field.value;
        }
        if (field.name == 'slide_out') {
          details.slide_out = field.value;
        }
        if (field.name == 'rear') {
          details.rear = field.value;
        }
        if (field.name == 'pet_friendly') {
          details.pet_friendly = field.value;
        }
        if (field.name == 'pull_through') {
          details.pull_through = field.value;
        }
      });

      formData.append("details", JSON.stringify(details));
      formData.append("enquiry_from", "Backend");
      formData.append("enquiry_by", userID);

      
      formData.delete('reservation_type');
      formData.delete('pull_through');
      formData.delete('adults');
      formData.delete('equipment_type');
      formData.delete('length');
      formData.delete('towing_vehicle');
      formData.delete('towing_length');
      formData.delete('slide_out');
      formData.delete('rear');
      formData.delete('pet_friendly');

      $.ajax({
        url: baseURL + "/api/booking/availability/check",
        type: "POST",
        dataType: 'json',
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function () {
          $('#overlay').fadeIn();

          // $('#loader').addClass("loader"); 
        },
        success: function (data) {
          $(window).scrollTop(0);
          $('#overlay').fadeOut();
          if (data.code == 200) {
            var result = (data.response.availability) ? 'Yes' : 'No';
            var message = (data.response.message) ? data.response.message : '';
            var details = (data.response.details) ? data.response.details : '';
            var classVal = (data.response.availability) ? 'alert-success' : 'alert-danger';
            $('#enquiry_result').html('<div class="alert ' + classVal + ' alert-dismissible fade show" role="alert">'
              + '<b>Availability: ' + result + '</b> <br> <b>Message: ' + message + '</b>'
              + '</div>');
          } else {
            $('#enquiry_result').html('<div class="alert alert-danger alert-dismissible fade show" role="alert">'
              + '<b>Message:' + data.response.message + '</b>'
              + '</div>');
          }
          $('#detail').html("");
          if (details.length > 0) {
            $('#detail').append('<div class=" " id="camp_detail" role="alert"> <p class="font-weight-bold">CampSites </p></div>')
          }
          
          for (var d = 0; d < details.length; d++) {
            $('#camp_detail').append('<hr>');
            iterate(details[d], '')
            // $('#detail').append('</ul>');
          }
          $('#availablity_result').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
          $(window).scrollTop(0);
          $('#overlay').fadeOut();
          alert('AJAX ERROR ! Check the console !');
          console.error(errorThrown);
          // submitBtn.button('reset');
        }
      });


      return false;
    }


  });

  var path = baseURL + "/admin/campground-search";

  $('#campground_search').select2({
    placeholder: 'Select an campground',
    minimumInputLength: 3,
    ajax: {
      url: path,
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        // console.log(data)
        return {
          results: $.map(data, function (item) {
            return {
              text: item.title,
              id: item.id,
              location: item.get_camp_ground_group.name,
              camp_url: item.url,
            }
          })
        };
      },
      cache: true
    }
  });

  function iterate(obj, stack) {
    for (var property in obj) {
      if (obj.hasOwnProperty(property)) {
        if (typeof obj[property] == "object") {
          iterate(obj[property], stack + '.' + property);
        } else {
          // console.log(property + "   " + obj[property]);
          $('#camp_detail').append($("<li></li>").html("<b class='text-capitalize'>" + property + "</b>" + ' : ' + obj[property]))
        }
      }
    }
  }

  $('#equipment_type').select2();

  $('#equipment_type').on('select2:select', function (e) {

    var equipement = $('#equipment_type').val();
    equipementArr = ["Automobile", "Bike/motorcycle", "Tent", "Other"];
    if (jQuery.inArray(equipement, equipementArr) !== -1) {
      $('#equipment_length').attr('disabled', 'disabled');
    } else {
      $('#equipment_length').removeAttr('disabled');
    }
  });

  function getTitle(url) {

    var title = '';

    if (url.indexOf("koa") > -1) {
      title = 'koa';
    } else if (url.indexOf("sunrvresorts") > -1) {
      title = 'sun';
    } else if (url.indexOf("recreation") > -1) {
      title = 'recreation';
    } else if (url.indexOf("bookyoursite") > -1) {
      title = 'bookyoursite';
    } else if (url.indexOf("reserveamerica") > -1) {
      title = 'reserveamerica';
    }

    return title;
  }

  var campurl = '';

  $('#campground_search').on('select2:select', function (e) {
    campurl = e.params.data.camp_url;
    // console.log(e.params.data)
    var location = e.params.data.location.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    });
    $('#location').html('<label>' + location + '</label>')
    changeEquipment();
  });

  $("#reservation_type").change(function () {
    changeEquipment();
  });


  function changeEquipment() {
    if (campurl) {
      var title = getTitle(campurl);
      var resType = $("#reservation_type").val();
      var equipements;
      $(".equipment_type").find('option').remove().end();
      if (title == 'koa' || title == 'sun') {
        equipements = formDataJson[title + '_' + resType + '_equipement'];
      } else {
        equipements = formDataJson[title + '_equipement'];
      }
      if (equipements.length == 0) {
        $("#equipment_type").attr('disabled', 'disabled');
      } else {
        $('#equipment_type').removeAttr('disabled');
      }
      $.each(equipements, function (key, val) {
        $(".equipment_type").append('<option value=' + val + '>' + key + '</option>');
      });
    }
  }
});
