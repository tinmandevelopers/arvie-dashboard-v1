
$(document).ready(function () {

  $("#custom_search").change(function () {
    if (this.checked) {
      $("#custom_search_form").show();
    } else {
      $("#custom_search_form").hide();
    }
  });

  $("#user_1").change(function () {
    if (this.checked) {
      $("#user_1_form").show();
    } else {
      $("#user_1_form").hide();
    }
  });

  $("#user_2").change(function () {
    if (this.checked) {
      $("#user_2_form").show();
    } else {
      $("#user_2_form").hide();
    }
  });

  $("#user_3").change(function () {
    if (this.checked) {
      $("#user_3_form").show();
    } else {
      $("#user_3_form").hide();
    }
  });


  $("input[type=radio][name*='details[custom][towing_vehicle]']").change(function () {
    if (this.value == 'Yes') {
      $("#towingblock_custom").show();
    }
    else if (this.value == 'No') {
      $("#towingblock_custom").hide();
    }
  });

  $("input[type=radio][name*='details[custom][slide_out]']").change(function () {
    if (this.value == 'Yes') {
      $("#rearblock_custom").show();
    }
    else if (this.value == 'No') {
      $("#rearblock_custom").hide();
    }
  });

  $("input[type=radio][name*='details[user_1][towing_vehicle]']").change(function () {
    if (this.value == 'Yes') {
      $("#towingblock_user1").show();
    }
    else if (this.value == 'No') {
      $("#towingblock_user1").hide();
    }
  });

  $("input[type=radio][name*='details[user_1][slide_out]']").change(function () {
    if (this.value == 'Yes') {
      $("#rearblock_user1").show();
    }
    else if (this.value == 'No') {
      $("#rearblock_user1").hide();
    }
  });

  $("input[type=radio][name*='details[user_2][towing_vehicle]']").change(function () {
    if (this.value == 'Yes') {
      $("#towingblock_user2").show();
    }
    else if (this.value == 'No') {
      $("#towingblock_user2").hide();
    }
  });

  $("input[type=radio][name*='details[user_2][slide_out]']").change(function () {
    if (this.value == 'Yes') {
      $("#rearblock_user2").show();
    }
    else if (this.value == 'No') {
      $("#rearblock_user2").hide();
    }
  });

  $("input[type=radio][name*='details[user_3][towing_vehicle]']").change(function () {
    if (this.value == 'Yes') {
      $("#towingblock_user3").show();
    }
    else if (this.value == 'No') {
      $("#towingblock_user3").hide();
    }
  });

  $("input[type=radio][name*='details[user_3][slide_out]']").change(function () {
    if (this.value == 'Yes') {
      $("#rearblock_user3").show();
    }
    else if (this.value == 'No') {
      $("#rearblock_user3").hide();
    }
  });

  var today = new Date().toISOString().split('T')[0];
  $("#check_in_date").attr('min', today);
  $("#check_out_date").attr('min', today);

  $("#check_in_date").blur(function () {
    var start = $("#check_in_date").val();
    $("#check_out_date").attr('min', start);
  });

  $("#check_out_date").blur(function () {
    var end = $("#check_out_date").val();
    $("#check_in_date").attr('max', end);
  });

  $('#check_availability_notification_form').validate({
    rules: {
      campground_id: {
        required: true
      },
      check_in: {
        required: true,
      },
      check_out: {
        required: true,
      },
      reservation_type: {
        required: true
      },
      adults: {
        required: true,
        min: 1,
      },
      equipment_type: {
        required: true,
      },
      length: {
        required: true,
        min: 0
      },
      towing_length: {
        required: true,
        min: 0
      },
      rear: {
        required: true,
      },
      'search_type[]': {
        required: true
      },
      'details[custom][length]': {
        required: true,
      },
      'details[user_1][length]': {
        required: true,
      },
      'details[user_2][length]': {
        required: true,
      },
      'details[user_3][length]': {
        required: true,
      },
      'details[custom][towing_length]': {
        required: true,
      },
      'details[user_1][towing_length]': {
        required: true,
      },
      'details[user_2][towing_length]': {
        required: true,
      },
      'details[user_3][towing_length]': {
        required: true,
      }

    },

    submitHandler: function (form) {    
      // var formData = $('form').serializeArray();
      var formData = new FormData($("form#check_availability_notification_form")[0]);
      formData.append("enquiry_from", "Backend");

      // console.log(formData);
      $.ajax({
        url: baseURL + "/api/check-availability-notification/store",
        type: "POST",
        dataType: 'json',
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function () {
          $('#overlay').fadeIn();

          // $('#loader').addClass("loader"); 
        },
        success: function (data) {
          $(window).scrollTop(0);
          $('#overlay').fadeOut();

          if (data.code == 200) {
            window.location.href = listURL
          }

        },
        error: function (xhr, textStatus, errorThrown) {
          $(window).scrollTop(0);
          $('#overlay').fadeOut();
          alert('AJAX ERROR ! Check the console !');
          console.error(errorThrown);
          submitBtn.button('reset');
        }
      });
      // form.submit();
    }
  });

  var path = baseURL + "/admin/campground-search";

  $('#campground_search').select2({
    placeholder: 'Select an campground',
    minimumInputLength: 3,
    ajax: {
      url: campgroundSearch,
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results: $.map(data, function (item) {
            return {
              text: item.title,
              id: item.id,
              location: item.get_camp_ground_group.name,
              camp_url: item.url,
            }
          })
        };
      },
      cache: true
    }
  });

  $('.equipment_type').select2();

  $('.equipment_type').on('select2:select', function (e) {

    var equipement = $('.equipment_type').val();
    // console.log(equipement);
    var equipementArr = ["Automobile", "Bike/motorcycle", "Tent", "Other"];
    if (jQuery.inArray(equipement, equipementArr) !== -1) {
      $('#equipment_length').attr('disabled', 'disabled');
    } else {
      $('#equipment_length').removeAttr('disabled');
    }
  });
  function getTitle(url) {

    var title = '';

    if (url.indexOf("koa") > -1) {
      title = 'koa';
    } else if (url.indexOf("sunrvresorts") > -1) {
      title = 'sun';
    } else if (url.indexOf("recreation") > -1) {
      title = 'recreation';
    } else if (url.indexOf("bookyoursite") > -1) {
      title = 'bookyoursite';
    } else if (url.indexOf("reserveamerica") > -1) {
      title = 'reserveamerica';
    }

    return title;
  }

  var campurl = '';

  $('#campground_search').on('select2:select', function (e) {
    campurl = e.params.data.camp_url;
    var data = e.params.data;
    // console.log(e.params.data.camp_url);
    var location = data.location.toLowerCase().replace(/\b[a-z]/g, function (letter) {
      return letter.toUpperCase();
    });
    $('#location').val(location)
    changeEquipment();
    // $.ajax({
    //   url: baseURL + "/admin/equipement-list",
    //   type: "get", //send it through get method
    //   data: {
    //     camp_url: e.params.data.camp_url,
    //   },
    //   beforeSend: function () {
    //     $('#overlay').fadeIn();
    //   },
    //   success: function (response) {
    //     //Do Something
    //     $('#overlay').fadeOut();
    //     console.log(response)
    //     $(".equipment_type").find('option').remove().end()

    //     $.each(response, function (key, val) {
    //       $(".equipment_type").append('<option value=' + val + '>' + key + '</option>');
    //     });
    //   },
    //   error: function (xhr) {
    //     //Do Something to handle error
    //     $(window).scrollTop(0);
    //     $('#overlay').fadeOut();
    //     alert('AJAX ERROR ! Check the console !');
    //     console.error(errorThrown);
    //   }
    // });

  });

  $("#reservation_type").change(function () {
    changeEquipment();
  });

  function changeEquipment() {
    if (campurl) {
      var title = getTitle(campurl);
      var resType = $("#reservation_type").val();
      var equipements;
      $(".equipment_type").find('option').remove().end();
      if (title == 'koa' || title == 'sun') {
        equipements = formDataJson[title + '_' + resType + '_equipement'];
      } else {
        equipements = formDataJson[title + '_equipement'];
      }
      if (equipements.length == 0) {
        $(".equipment_type").attr('disabled', 'disabled');
      } else {
        $('.equipment_type').removeAttr('disabled');
      }
      $.each(equipements, function (key, val) {
        $(".equipment_type").append('<option value=' + val + '>' + key + '</option>');
      });
    }
  }

});
