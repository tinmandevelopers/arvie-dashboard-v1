$(function () {
    filterCustomData = (typeof (filterCustomData) == 'undefined') ? '' : filterCustomData;
    fromPage = (typeof (fromPage) == 'undefined') ? '' : fromPage;
    var dataTable = $('#' + list_id).DataTable({
        pagingType: 'simple_numbers',
        processing: true,
        serverSide: true,
        ajax: {
            url: list_url,
            type: 'post',
            data: function (data) {
                if (filterCustomData) {
                    data.dataSource = $('#dataSource').val()
                }
                if (fromPage) {
                    data.fromPage = fromPage;
                }
            },
            beforeSend: function () {
                if (dataTable && dataTable.hasOwnProperty('settings')) {
                    dataTable.settings()[0].jqXHR.abort();
                }
            }
        },
        columns: columns,
        order: order,
        searchDelay: 500,
        "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false
            }],
        initComplete: function () {
            addDeleteForms();
        },
        "dom": '<"top"f>rt<"bottom"lip><"clear">',
        language: {
            paginate: {
                next: '<i class="fas fa-angle-right">',
                previous: '<i class="fas fa-angle-left">'
            },
            info: "_START_-_END_ of _TOTAL_",
            lengthMenu: "Rows per page: _MENU_"
        }
    });

    $('#dataSource').change(function () {
        filterCustomData = {'dataSource': $('#dataSource').val()};
        dataTable.draw();
    });
});
