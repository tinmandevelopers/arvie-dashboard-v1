$(document).ready(function () {
    $('#data_source_form').validate({
        rules: {
            title: {
                required: true
            },
            url: {
                required: true,
                url:true
            },
            crawler_name: {
                required: true
            },
            crawler_file: {
                required: true
            },
            booking_file: {
                required: true
            },
            crawler_time: {
                required: true,
                crawlerTimeFormat : true
            },
        }
    });

    var cronstrue = window.cronstrue;

    jQuery.validator.addMethod("crawlerTimeFormat", function(value, element) {
        var cronregex = new RegExp(/^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/);
        if(cronregex.test(value)==true){
            $('#crawler_readable_time').text("( "+cronstrue.toString(value)+" )");
        }else{
            $('#crawler_readable_time').text('');
        }
        return cronregex.test(value);

    }, "Crawler time must be in minute hour day-of-month month day-of-week format");

    var cronval=$("#crawler_time").val();
    if(cronval!=''){
        $('#crawler_readable_time').text("( "+cronstrue.toString(cronval)+" )");
    }


    $('input, select, textarea').on('keyup blur', function () { // fires on every keyup & blur
        if ($(this).parents('form').valid()) {                   // checks form for validity
            $('button.btn').prop('disabled', false);        // enables button
        } else {
            $('button.btn').prop('disabled', 'disabled');   // disables button
        }
    });

});
