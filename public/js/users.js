$(document).ready(function () {
    $('#user_change_password').validate({
        rules: {
            old_password: {
                required: true
            },
            password: {
                required: true,
                minlength: 8
            },
            password_confirmation: {
                required: true
            }
        }
    });
    
    $('#change_password').validate({
        rules: {
            password: {
                required: true,
                minlength: 8
            },
            password_confirmation: {
                required: true
            }
        }
    });

    $('#user_form').validate({
        rules: {
            first_name: {
                required: true
            },
            arvie_email: {
                required: true,
                email: true
            },
            alternative_email: {
                required: true,
                email: true
            },
            contact_number: {
                required: true,
                number: true,
                maxlength: 11
            },
            address: {
                required: true
            },
            dob: {
                required: true
            },
            id_number: {
                required: true,
                number: true,
                minlength: 13,
                maxlength: 13
            },
            id_proof: {
                required: true
            },
            income_tax_number: {
                required: true
            },
            kin_first_name: {
                required: true
            },
            kin_last_name: {
                required: true
            },
            kin_email: {
                required: true,
                email: true
            },
            kin_contact: {
                required: true,
                number: true,
                maxlength: 11
            }
        }
    });

    $('input, select, textarea').on('keyup blur', function () { // fires on every keyup & blur
        if ($(this).parents('form').valid()) {                   // checks form for validity
            $('button.btn').prop('disabled', false);        // enables button
        } else {
            $('button.btn').prop('disabled', 'disabled');   // disables button
        }
    });

    $('#first_name,#last_name').on('keyup', function (e) {
        $('#screen_name').val($('#first_name').val().toLowerCase().trim().replaceAll(' ', '-') + ($('#last_name').val() != '' ? '.' + $('#last_name').val().charAt(0) : ''));
    });

});


sales@commonsensewebmarketing.com
From Dr. Derek Sheavly to Everyone:  09:16 PM
GMB login: healingderek
