<?php
error_reporting(1);
global $database;
global $connection;
include('../core/database.php');
$database = new Database;
$connection = $database::connect();
class Data
{
	public function GlobalAvailabilityStatsData()
	{
		global $database;
		global $connection;

		$start = @$_POST['start'];
		$end = @$_POST['end'];
		
		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		$numberOfAvailChecks = $num;

		if($num > 0){
			$availableArray = array();
			$unAvailableArray = array();
			$availableCampgroundIDs = array();
			$unavailableCampgroundIDs = array();
			$dayNameArray = array();

			while($row = $database::data_array($query)){
				$availDetail = json_decode($row['details']);
				$availResult = $row['result'];
				$availResult = json_decode($availResult);
				$campgroundID = $row['campground_id'];
				$check_in_date = $row['check_in_date'];
				$available = $availResult->message;
				$sitesAvailable = $availResult->sites_available;

				switch($available){
						case 'Available':
						$availableArray[] = 1;
						$availableCampgroundIDs[] = $campgroundID;
						$check_in_date_success = $check_in_date;
						$dayofweek = date('l', strtotime($check_in_date_success));
						$dayNameArray[] = $dayofweek;

						if($sitesAvailable){
						$sitesAvailableArray[] = $sitesAvailable;

						}



						break;

						case 'Not Available':
						$unAvailableArray[] = 1;
						$unavailableCampgroundIDs[] = $campgroundID;
				}
			}

			$availablitySuccess = count($availableArray);
			$availabilityUnsuccess = count($unAvailableArray);
			$numberOfActualChecks = $availablitySuccess + $availabilityUnsuccess;
			$percentageSuccess = round($availablitySuccess / $numberOfActualChecks * 100,1);



			// Availability Checks
			echo '<div class="data-row">';
				 echo '<div class="data-row-label"><div class="padding-small">Total Availability Checks</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$numberOfActualChecks.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';


			// Availability Success
			echo '<div class="data-row gray">';
				 echo '<div class="data-row-label"><div class="padding-small">Successful Availability Checks</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$availablitySuccess.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			// Average Sites Availalable on Success
			echo '<div class="data-row ">';
				 echo '<div class="data-row-label"><div class="padding-small">Average Sites found on Availability</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.self::getAverageSitesAvailable($sitesAvailableArray).'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			// UnSuccessfull
			echo '<div class="data-row gray">';
				 echo '<div class="data-row-label"><div class="padding-small">Availability Unsuccesful</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$availabilityUnsuccess.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			// Success Percentage
			echo '<div class="data-row">';
				 echo '<div class="data-row-label"><div class="padding-small">Availability Unsuccesful</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$percentageSuccess.'%</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

		}

		

	}

	public function getAverageSitesAvailable($sitesArray='')
	{
		$result = 0;
		

		$array_count = count($sitesArray);
		$array_sum = array_sum($sitesArray);

		$averageSitesAvailable = round($array_sum / $array_count,1);
		return $averageSitesAvailable;
	}

	public function globalAvailabilityDataSourcesData()
	{
		global $database;
		global $connection;

		$start = @$_POST['start'];
		$end = @$_POST['end'];
		
		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		$numberOfAvailChecks = $num;

		if($num > 0){
			$availableArray = array();
			$unAvailableArray = array();
			$availableCampgroundIDs = array();
			$unavailableCampgroundIDs = array();
			$dayNameArray = array();

			while($row = $database::data_array($query)){
				$availDetail = json_decode($row['details']);
				$availResult = $row['result'];
				$availResult = json_decode($availResult);
				$campgroundID = $row['campground_id'];
				$check_in_date = $row['check_in_date'];
				$available = $availResult->message;
				$sitesAvailable = $availResult->sites_available;

				switch($available){
						case 'Available':
						$availableArray[] = 1;
						$availableCampgroundIDs[] = $campgroundID;
						$check_in_date_success = $check_in_date;
						$dayofweek = date('l', strtotime($check_in_date_success));
						$dayNameArray[] = $dayofweek;

						if($sitesAvailable){
						$sitesAvailableArray[] = $sitesAvailable;

						}



						break;

						case 'Not Available':
						$unAvailableArray[] = 1;
						$unavailableCampgroundIDs[] = $campgroundID;
						break;
				}
			}
			$availablitySuccess = count($availableArray);
			$campgroundSourceData = self::campgroundSourceReport($availableCampgroundIDs);
			echo '<div class="data-source">';
			foreach($campgroundSourceData as $source=>$availablityPerSource){
						 $percentageOfChecks = round($availablityPerSource / $availablitySuccess * 100,1);

						 echo '<div class="small-box" style="float: left; width: 20%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$source.'</div><div class="percentage">'.$percentageOfChecks.'%</div>';
						 echo '</div></div>';

						
			}
			echo '<div class="clearfix"></div>';
			echo '<div class="data-source">';

	    }

    }

	public function campgroundSourceReport($campgroundIDs='')
	{
		global $connection;
		global $database;

		if(is_array($campgroundIDs)){
				$dataSourceIds = array();

				foreach($campgroundIDs as $key=>$campground_id)
				{
						$query = "SELECT * FROM campgrounds where id='$campground_id'";
						$query = $database::query($connection, $query);
						$num = $query->num_rows;
						if($num == 1){
							$array = $database::data_array($query);
							$dataSourceIds[] = $array['data_source_id'];
						}
				}

					$vals = array_count_values($dataSourceIds);
					$koa = $vals[1];
					$sun = $vals[2];
					$recreation = $vals[3];
					$reserve = $vals[5];

					$sourceArray = array(
						'KOA'=>$koa,
						'Sun RV'=>$sun,
						'Recreation'=>$recreation,
						'Reserve America'=>$reserve
					);


					return $sourceArray;


		}

	}


	public function campgroundAvailablityBySeason($startDate='', $endDate='')
	{
		global $connection;
		global $database;



		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$startDate' and '$endDate'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$availableArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];

						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$availableArray[] = 1;
								break;
								
						}



					 

			}

		

			return count($availableArray);
		}
	}

	public function globalAvailabilityRaw($start='', $end='')
	{
		global $database;
		global $connection;


		
		
		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		$numberOfAvailChecks = $num;

		if($num > 0){
			$availableArray = array();
			$unAvailableArray = array();
			$availableCampgroundIDs = array();
			$unavailableCampgroundIDs = array();
			$dayNameArray = array();

			while($row = $database::data_array($query)){
				$availDetail = json_decode($row['details']);
				$availResult = $row['result'];
				$availResult = json_decode($availResult);
				$campgroundID = $row['campground_id'];
				$check_in_date = $row['check_in_date'];
				$available = $availResult->message;
				$sitesAvailable = $availResult->sites_available;

				switch($available){
						case 'Available':
						$availableArray[] = 1;
						$availableCampgroundIDs[] = $campgroundID;
						$check_in_date_success = $check_in_date;
						$dayofweek = date('l', strtotime($check_in_date_success));
						$dayNameArray[] = $dayofweek;

						if($sitesAvailable){
						$sitesAvailableArray[] = $sitesAvailable;

						}



						break;

						case 'Not Available':
						$unAvailableArray[] = 1;
						$unavailableCampgroundIDs[] = $campgroundID;
						break;
				}
			}
			$availablitySuccess = count($availableArray);
			return array('total'=>$availablitySuccess,'availableIDs'=>$availableCampgroundIDs);
		}
	}


	public function AvailabilityByStateData()
	{

			$start = @$_POST['start'];
			$end = @$_POST['end'];
			$rawData = self::globalAvailabilityRaw($start, $end);
			$numberOfChecks = $rawData['total'];
			$availableCampgroundIDs = $rawData['availableIDs'];
			$campgroundStateData = self::campgroundStateReport($availableCampgroundIDs);


			foreach($campgroundStateData as $state=>$availablityPerState){
						 
						 	$percentage = round($availablityPerState/$numberOfChecks*100,1);

						 


							echo '<div class="small-box" style="float: left; width: 15%"><div class="padding-small">';
								echo '<div class="title">'.$state.'</div><div class="percentage">
								'.$percentage.'%</div>';
							echo '</div></div>';
			}

			echo '<div class="clearfix"></div>';

	}


	public function globalAvailabilitySeasonsData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$rawData = self::globalAvailabilityRaw($start, $end);
			$numberOfChecks = $rawData['total'];

			$springAvailablity = self::campgroundAvailablityBySeason('2021/03/01','2021/05/31');
			$summerAvailablity = self::campgroundAvailablityBySeason('2021/06/01','2021/10/31');
			$winterAvailablity = self::campgroundAvailablityBySeason('2021/12/01','2022/02/28');
			$fallAvailablity = self::campgroundAvailablityBySeason('2021/09/01','2022/11/30');
			$seasonArray = array('Spring'=>$springAvailablity,'Summer'=>$summerAvailablity,'Winter'=>$winterAvailablity,'Fall'=>$fallAvailablity);

			foreach($seasonArray as $season=>$availability)
			{

						$percentage = round($availability/$numberOfChecks*100,1);

						 echo '<div class="small-box" style="float: left; width: 20%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$season.'</div><div class="percentage">'.$percentage.'%</div>';
						 echo '</div></div>';
			}


			echo '
			<div class="clearfix"></div>
			<small>Spring: March 1 to May 31;
			Summer: June 1 to August 31;
			Fall: September 1 to November 30;
			Winter: December 1 to February 28 
			</small>
			';

			
	}

	public function campgroundStateReport($campgroundIDs='')
	{
		global $connection;
		global $database;

		if(is_array($campgroundIDs)){
				$dataSourceIds = array();
				$campgroundGroups = array();

				foreach($campgroundIDs as $key=>$campground_id)
				{
						$query = "SELECT * FROM campgrounds where id='$campground_id'";
						$query = $database::query($connection, $query);
						$num = $query->num_rows;
						if($num == 1){
							$array = $database::data_array($query);
							$group_id = $array['campground_group_id'];
							$group_name = self::getGroupName($group_id);
							$campgroundGroups[] = $group_name;
						}
				}
				$vals = array_count_values($campgroundGroups);
				return $vals;
		}
	}

	public function getGroupName($group_id)
	{
			global $connection;
			global $database;
			if($group_id){
					$query = "SELECT * FROM campgrounds_group WHERE id='$group_id'";
					$query = $database::query($connection, $query);
					$num   = $query->num_rows;

				

					if($num == 1){
						$array = $database::data_array($query);
						$name = strtoupper($array['name']);
						return $name;
					}
			}
	}

	public function AvailabilityByCheckinData()
	{
		$start = @$_POST['start'];
		$end = @$_POST['end'];
		

		$rawData = self::globalAvailabilityRaw($start, $end);
		$numberOfChecks = $rawData['total'];

		$checkinDayAvailablities = self::checkAvailablityByDateCheckin($start,$end);

		foreach($checkinDayAvailablities as $day=>$availablityPerCheckin){
						$percentage = round($availablityPerCheckin / $numberOfChecks * 100,2);


						 echo '<div class="small-box" style="float: left; width: 20%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$day.'</div><div class="percentage">'.$percentage.'%</div>';
						 echo '</div></div>';
						
		}

		echo '<div class="clearfix"></div>';
	}

	public function checkAvailablityByDateCheckin($start='', $end='')
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$checkInDateArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$checkInDate = $row['check_in_date'];
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$checkInDateArray[] = date('D',strtotime($checkInDate));
								break;
								
						}



					 

			}
			$vals = array_count_values($checkInDateArray);
			return $vals;
			
		}
	}


	public function AvailabilityByRequestDayData()
	{
		$start = @$_POST['start'];
		$end = @$_POST['end'];
		

		$rawData = self::globalAvailabilityRaw($start, $end);
		$numberOfChecks = $rawData['total'];

		$dayAvailablitiesRequested = self::checkAvailablityRequestDay($start, $end);

		

		foreach($dayAvailablitiesRequested as $day=>$dayCheck){
						$percentage = round($dayCheck / $numberOfChecks * 100,2);


						 echo '<div class="small-box" style="float: left; width: 20%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$day.'</div><div class="percentage">'.$percentage.'%</div>';
						 echo '</div></div>';
						
		}

		echo '<div class="clearfix"></div>';
	}

	public function checkAvailablityRequestDay($start='', $end='')
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$checkInDateArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$created_at = $row['created_at'];
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$checkInDateArray[] = date('D',strtotime($created_at));
								break;
								
						}



					 

			}
			$vals = array_count_values($checkInDateArray);
			return $vals;
			
		}
	}

	public function LenghtOfStaysData()
	{
		$start = @$_POST['start'];
		$end = @$_POST['end'];
		

		$rawData = self::globalAvailabilityRaw($start, $end);
		$numberOfChecks = $rawData['total'];
		$stayLength = self::lengthOfStayData($start, $end);
		foreach($stayLength as $lKey=>$lTotals){
					
						$percentage = round($lTotals / $numberOfChecks * 100,1);


						 echo '<div class="small-box" style="float: left; width: 10%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$lKey.'</div><div class="percentage">'.$percentage.'%</div>';
						 echo '</div></div>';
		}

		echo '<div class="clearfix"></div>';
		
	}

	public function lengthOfStayData($start='', $end='')
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$start' and '$end'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$stays = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$checkin_date = strtotime($row['check_in_date']);
						$checkout_date = strtotime($row['check_out_date']);
						$day_diff = $checkout_date - $checkin_date;
						$day_diff = floor($day_diff/(60*60*24));
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								if($day_diff != 0){
								$stays[] = ''.$day_diff.'';
								}
								break;
								
						}



					 

			}

			$vals = array_count_values($stays);
			return $vals;
		
			
		}
	}

	public function GlobalSOSData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);


			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];

			echo '<div class="data-row">';
				 echo '<div class="data-row-label"><div class="padding-small">Total Number of Sold Out Searches</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$sosTotal.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			echo '<div class="data-row gray">';
				 echo '<div class="data-row-label"><div class="padding-small">Initiated from website</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$totalFrontend.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			echo '<div class="data-row">';
				 echo '<div class="data-row-label"><div class="padding-small">Initiated by admin</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$totalBackend.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			echo '<div class="data-row gray">';
				 echo '<div class="data-row-label"><div class="padding-small">Total number of successful Sold Out Searches where Availablity was found</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.$sosSuccess.'</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			echo '<div class="data-row">';
				 echo '<div class="data-row-label"><div class="padding-small">Success Rate</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.round($sosSuccessRate,1).'%</div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';

			echo '<div class="data-row gray">';
				 echo '<div class="data-row-label"><div class="padding-small">Average time to get successful SOS</div></div>';
				 echo '<div class="data-row-value"><div class="padding-small">'.round($averageTimeSuccess,1).' hours </div></div>';
				 echo '<div class="clearfix"></div>';
			echo '</div>';





	}

	public function GlobalSOSElapsedData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);


			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];
 			$sosDataMore = self::sosDataMore($sosRaw);

 			 if(is_array($sosDataMore['elapsed'])) {
			 	foreach($sosDataMore['elapsed'] as $title=>$value){
			 			$percentageElapsed = round($value/$sosSuccess * 100,2);
			 			

			 			echo '<div class="data-row">';
						 echo '<div class="data-row-label"><div class="padding-small">'.$title.'</div></div>';
						 echo '<div class="data-row-value"><div class="padding-small">'.$value.'<small> ('.$percentageElapsed.'%)</small></div></div>';
						 echo '<div class="clearfix"></div>';
						echo '</div>';
			 	}
		     }
	}

	public function SOSDayFoundData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);
			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];
 			$sosDataMore = self::sosDataMore($sosRaw);

 			 $daySosFound = array();
				 
		 foreach($sosRaw as $key=>$data){
		 		$elapsed_date = $data['elapsed_date'];

		 		$day = date('D', strtotime($elapsed_date));
		 		$daySosFound[] = $day;		
		 }
			
			$daysSosFoundCount = count($daySosFound);
			$daySosFoundClean = array_count_values($daySosFound);
			
			foreach($daySosFoundClean as $day=>$dayCount){
				$percentageSos = round($dayCount / $daysSosFoundCount * 100,2);

				echo '<div class="data-row">';
						 echo '<div class="data-row-label"><div class="padding-small">'.$day.'</div></div>';
						 echo '<div class="data-row-value"><div class="padding-small">'.$dayCount.'<small> ('.$percentageSos.'%)</small></div></div>';
						 echo '<div class="clearfix"></div>';
						echo '</div>';

		
			}
	}

	public function SOSAvailabilityByDayData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);
			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];
 			$sosDataMore = self::sosDataMore($sosRaw);

 			 if(is_array($sosDataMore['dayofweek'])) {
				 	foreach($sosDataMore['dayofweek'] as $title=>$value){
				 			$percentageElapsed = round($value/$sosSuccess * 100,2);
				 			
				 				echo '<div class="data-row">';
						 echo '<div class="data-row-label"><div class="padding-small">'.$title.'</div></div>';
						 echo '<div class="data-row-value"><div class="padding-small">'.$value.'<small> ('.$percentageElapsed.'%)</small></div></div>';
						 echo '<div class="clearfix"></div>';
						echo '</div>';

				 		
				 	}
				 }
	}

	public function TimeBeforeCheckinData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);
			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];
 			$sosDataMore = self::sosDataMore($sosRaw);
 			$timeBeforeCheckin = self::TimeBeforeCheckin($sosRaw);

 				echo '<div class="data-row">';
						 echo '<div class="data-row-label gray"><div class="padding-small">Average days before checkin</div></div>';
						 echo '<div class="data-row-value"><div class="padding-small">'.$timeBeforeCheckin['avg'].'</div></div>';
						 echo '<div class="clearfix"></div>';
						echo '</div>';

				foreach($timeBeforeCheckin['data'] as $key=>$value){


				 				echo '<div class="data-row">';
						 echo '<div class="data-row-label"><div class="padding-small">Days before checkin</div></div>';
						 echo '<div class="data-row-value"><div class="padding-small">'.$value.'</div></div>';
						 echo '<div class="clearfix"></div>';
						echo '</div>';

					
				}

	}

	public function SOSByStateData()
	{
			$start = @$_POST['start'];
			$end = @$_POST['end'];
		

			$sosData =  self::SoldOutSearchData($start,$end);
			$sosSuccessData = self::SOSSuccess($start,$end);
			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];
 			$sosDataMore = self::sosDataMore($sosRaw);

 			foreach($sosStateData as $state=>$sosPerState){
						$percentageOfChecks = round($sosPerState / $sosTotal * 100,1);


						 echo '<div class="small-box" style="float: left; width: 15%;"><div class="padding-small">';
						 	 echo '<div class="title">'.$state.'</div><div class="percentage">'.$percentageOfChecks.'%</div>';
						 echo '</div></div>';
						

			}

			echo '<div class="clearfix"></div>';
 			
	}

	public function TimeBeforeCheckin($data='')
	{
		if(is_array($data)){
			$timeLeft = array();

			
			foreach($data as $key=>$attr){
				$check_in_date = $attr['check_in_date'];
				$elapsed_date = $attr['elapsed_date'];

				$t1 = strtotime($check_in_date);
				$t2 = strtotime($elapsed_date);
				$diff = $t1 - $t2;
				$hours = $diff / ( 60 * 60 );
				if($hours>0){
					 $days = $hours / 24;
					 $daysRounded = round($days);
					 $timeLeft[] = $daysRounded;
				}


			}

			

			$dupsRemoved = array_unique($timeLeft);
			$count = count($dupsRemoved);
			$countDays = array_sum($dupsRemoved);

			$countAverage = round($countDays / $count);

			return array('data'=>$dupsRemoved,'totalCount'=>$countDays, 'total'=>$count,'avg'=>$countAverage);


		}
	}

	public function SoldOutSearchData($start='',$end='')
	{
		global $connection;
		global $database;

		
		$query = "SELECT * FROM check_availability_notifications WHERE check_in_date BETWEEN '$start' and '$end'";
		

		$query = $database::query($connection, $query);
		$num = $query->num_rows;

		if($num > 0){
					$numberSOS = $num;
					$numberBackend = array();
					$numberFrontend = array();
					$sosLocation = array();
					$availFound = array();
					$sosDataRaw = array();

					while($row =  $database::data_array($query)){
							$sos_id = $row['sold_out_search_id'];
							$sos_result = $row['result'];
							$location = $row['location'];
							$sosLocation[] = $location;
							$json_result = json_decode($sos_result);
							$availability = $json_result->availability;
							if($availability){
								$availFound[] = 1;
							}

							$sosDataRaw[] = $row;
							

							if($sos_id){
								$numberFrontend[] = 1;
								

							
							}else{
								$numberBackend[] = 1;

							}

					}

					$numberBackendCount = count($numberBackend);
					$numberFrontendCount	 = count($numberFrontend);
					$availFoundCount = count($availFound);

					$sosLocationsClean = array_count_values($sosLocation);
			

					
		}

		return array(
			'total'=>$numberSOS,
			'totalBackend'=>$numberBackendCount,
			'totalFrontend'=>$numberFrontendCount,
			'sosLocations'=>$sosLocation,
			'sosLocationsClean'=>$sosLocationsClean,
			'sosNumberofAvailablityFound'=>$availFoundCount,
			'sosDataRaw'=>$sosDataRaw,
		);

	}

	public function SOSSuccess($start='',$end='')
	{
		global $connection;
		global $database;

		
		$query = "SELECT * FROM check_availability_notifications WHERE check_in_date BETWEEN '$start' and '$end' and not isnull(elapsed_time)";
		
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$hour = array();
				$raw  = array();

				while($row =  $database::data_array($query)){
						$elapsed_time = $row['elapsed_time'];
						$raw[] = $row;

						//echo $elapsed_time.'<br/>';
						$x = explode(' ', $elapsed_time);
						//echo $x[1].'<br/>';

						switch($x[1]){
							case 'week':
							$hour[] = 168;
							break;

							case 'weeks':
							$time_in_week = 168;
							$hour[] = $x[0] * $time_in_week;
							break;

							case 'days':
							$time_in_day = 24;
							$hour[] = $x[0] * $time_in_day;
							break;

							case 'hours':
							$hour[] = $x[0];
							break;

							case 'minutes':
							$hour[] = '0.'.$x[0];
							break;
						}

						
				}

				$totalHours = array_sum($hour);
				$averageTime = $totalHours / $num;

		}

		return array('total'=>$num,'averageTime'=>$averageTime, 'raw'=>$raw);
	}

	public function sosDataMore($sosData= '')
	{
		if(is_array($sosData)){
			
			
			$first_availability = array();
			$elapsed_time = array();
			$dayOfWeek = array();

			foreach($sosData as $key=>$data){

					$first_search_availability[] = $data['first_search_availability'];


					$created_at = $data['created_at'];
					$elapsed_date = $data['elapsed_date'];
					$check_in_date = $data['check_in_date'];
					$elapsed_time[] = $data['elapsed_time'];
					$dayOfWeek[] = $data['day_of_week'];


			}
 

			$first_search_availabilityClean = array_count_values($first_search_availability);
			$elapsed_timeClean = array_count_values($elapsed_time);
			$dayOfWeekClean = array_count_values($dayOfWeek);

			
			return array('elapsed'=>$elapsed_timeClean, 'dayofweek'=>$dayOfWeekClean);


		}
	}





}


$data = new Data;
$request = @$_GET['request'];
if($request){
	$data::$request();
}

$database::disconnect($connection);
?>