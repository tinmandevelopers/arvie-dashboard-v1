<?php session_start();
class Load
{
	public function validate()
	{
		$sid = $_SESSION['sid'];
		if(!$sid){
		    self::authenticate();
		}else{
			self::dataCentre();
		}
	}

	public function authenticate()
	{
		echo '<div class="box box-login"><form action="#" method="post" onsubmit="return false">';
			echo '<h1>Login to Arvie</h1>';
			echo '<div id="loginMessage" style="display:none;"><div class="errorMessage"></div></div>';

			echo '<div class="form-item"><input type="username" id="username" name="username" value="" placeholder="Username"/></div>';
			echo '<div class="form-item"><input type="password" id="password" name="password" value="" placeholder="Password"/></div>';
			echo '<div class="form-item"><button class="btn" onclick="ValidateLogin()">Login</button>';
		echo '</form></div>';
	}

	public function login()
	{
		$username = @$_POST['user'];
		$password = @$_POST['pass'];
		$response = array('error'=>'Unknown error - could not login');

		if(!$username and !$password){
			$response = array('error'=>'Please enter your credentials');
		}else{
			$actual_username = 'rvadmin';
			$actual_password = 'Password!@';

			if($username != $actual_username){
				$response = array('error'=>'Username invalid');
			}else{
				if($password != $actual_password){
					$response = array('error'=>'Username and password combination did not match');
				}else{
					session_start();
					$_SESSION['sid']='1';
					$response = array('success'=>'true');
					$_POST['user']='';
					$_POST['pass']='';
				}
			}
		}

		echo json_encode($response);
	}

	function logout()
	{
		session_destroy();
		echo 'logout';
	}

	public function dataCentre()
	{
		
		echo '<header>';
		echo '<div class="logo"><img src="arvie-logo-white.png"/></div>';
		echo '<div class="filter">';
			 echo self::filter();
		echo '</div>';
		echo '<div class="navigation">Welcome! <a href="#" onclick="refresh();">Refresh</a> <a href="#" onclick="ValidateLogout();">Logout</a>';
		echo '</div>';
		echo '<div class="clearfix"></div>';
		echo '</header>';
		echo '<div id="data">';

		echo '</div>';
		echo '<script>loadData()</script>';

	}

	public function filter()
	{

		$result = '<form action="#">';
		    $result .= '<div class="filter-item">';
		       $result .= '<div class="filter-heading">Data date range</div>';
		       $result .= '<input type="date" name="fromDate" id="fromDate"/> - ';
		       $result .= '<input type="date" name="toDate" id="toDate"/>';
		    $result .= '</div>';

		    $result .= '<div class="clearfix"></div>';
		$result .= '</form>
		<script>
		jQuery("#fromDate").change(function(){
				var fromDate = jQuery("#fromDate").val();
				jQuery("#dateFromPlain").val(fromDate);
				refresh();
		});
		jQuery("#toDate").change(function(){
				var toDate = jQuery("#toDate").val();
				jQuery("#dateToPlain").val(toDate);
				refresh();
		});

		</script>
		';
		return $result;
	}

	public function data()
	{
		echo '<div class="column"><div class="padding">';
			echo self::GlobalAvailabilityStats();
		echo '</div>';
		echo '</div>';

		echo '<div class="column"><div class="padding">';
			echo self::GlobalAvailabilityDataSources();
		echo '</div>';
		echo '</div>';


		echo '<div class="column"><div class="padding">';
			 echo self::SeasonalAvailabilityChecks();
		echo '</div></div>';
		echo '<div class="clearfix"></div>';

		echo '<div class="column-large">';
		echo '<div class="padding">';
			 echo self::AvailabilityByState();
		echo '</div>';
		echo '<div class="clearfix"></div>';
		echo '</div>';

		echo '<div class="column-2">';
			echo '<div class="padding">';
				echo self::AvailabilityByCheckinDay();
			echo '</div>';
		echo '</div>';
		echo '<div class="column-2">';
			echo '<div class="padding">';
				echo self::AvailabilityByRequestDay();
			echo '</div>';
		echo '</div>';
		echo '<div class="clearfix"></div>';

		echo '<div class="column-large">';
		echo '<div class="padding">';
			 echo self::LenghtOfStays();
		echo '</div>';
		echo '<div class="clearfix"></div>';
		echo '</div>';

		// sos data
		echo '<div class="column"><div class="padding">';
			echo self::GlobalSOS();
			echo '<br/>';
			echo self::SOSDayFound();
		echo '</div>';
		echo '</div>';

		echo '<div class="column"><div class="padding">';
			echo self::GlobalSOSElapsed();
			echo '<br/>';
			echo self::SOSAvailabilityByDay();
		echo '</div>';
		echo '</div>';


		echo '<div class="column"><div class="padding">';
			echo self::TimeBeforeCheckin();
		echo '</div></div>';
		echo '<div class="clearfix"></div>';

		echo '<div class="column-large">';
		echo '<div class="padding">';
			 echo self::SOSByState();
		echo '</div>';
		echo '<div class="clearfix"></div>';
		echo '</div>';
	}

	public function SOSByState()
	{
		$result .= '<div class="box">';
			$result .= '<h2>Sold Out Searches in States</h2>';
			$result .= '<div id="SOSByState"></div>';
			$result .= '<script>SOSByState()</script>';
			$result .= '</div>';
			return $result;
	}

	public function TimeBeforeCheckin()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Time before Check-In Date</h2>';
			$result .= '<div id="TimeBeforeCheckin"></div>';
			$result .= '<script>TimeBeforeCheckin()</script>';
			$result .= '</div>';
			return $result;
	}

	public function SOSAvailabilityByDay()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Sold Out Search Availability by Day</h2>';
			$result .= '<div id="SOSAvailabilityByDay"></div>';
			$result .= '<script>SOSAvailabilityByDay()</script>';
			$result .= '</div>';
			return $result;
	}

	public function SOSDayFound()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Sold Out Search Day found</h2>';
			$result .= '<div id="SOSDayFound"></div>';
			$result .= '<script>SOSDayFound()</script>';
			$result .= '</div>';
			return $result;
	}

	public function GlobalSOSElapsed()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Sold Out Search Elapsed Time</h2>';
			$result .= '<div id="GlobalSOSElapsed"></div>';
			$result .= '<script>GlobalSOSElapsed()</script>';
			$result .= '</div>';
			return $result;
	}

	public function GlobalSOS()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Global SOS Data</h2>';
			$result .= '<div id="GlobalSOS"></div>';
			$result .= '<script>GlobalSOS()</script>';
			$result .= '</div>';
			return $result;
	}

	public function LenghtOfStays()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Length of Stays</h2>';
			$result .= '<div id="LenghtOfStays"></div>';
			$result .= '<script>LenghtOfStays()</script>';
			$result .= '</div>';
			return $result;
	}

	public function AvailabilityByRequestDay()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Availability by Request Day</h2>';
			$result .= '<div id="AvailabilityByRequestDay"></div>';
			$result .= '<script>AvailabilityByRequestDay()</script>';
		$result .= '</div>';
		return $result;
	}

	public function AvailabilityByCheckinDay()
	{
			$result .= '<div class="box">';
			$result .= '<h2>Availability by Check-In Day</h2>';
			$result .= '<div id="AvailabilityByCheckinDay"></div>';
			$result .= '<script>AvailabilityByCheckinDay()</script>';
		$result .= '</div>';
		return $result;
	}


	public function AvailabilityByState()
	{
		$result .= '<div class="box">';
			$result .= '<h2>Availability by State</h2>';
			$result .= '<div id="AvailabilityByState"></div>';
			$result .= '<script>AvailabilityByState()</script>';
		$result .= '</div>';
		return $result;
	}


	public function GlobalAvailabilityStats()
	{
		$result = '<div class="box">';
			$result .= '<h2>Global Availability Stats</h2>';
			$result .= '<div id="globalAvailabilityStats"></div>';
			$result .= '<script>globalAvailabilityStats()</script>';
		$result .= '</div>';

		return $result;
	}

	public function GlobalAvailabilityDataSources()
	{
		$result = '<div class="box">';
			$result .= '<h2>Data Sources</h2>';
			$result .= '<div id="globalAvailabilityDataSources"></div>';
			$result .= '<script>globalAvailabilityDataSources()</script>';
		$result .= '</div>';

		return $result;
	}

	public function SeasonalAvailabilityChecks()
	{
		$result = '<div class="box">';
			$result .= '<h2>Availability per Season</h2>';
			$result .= '<div id="globalAvailabilitySeasons"></div>';
			$result .= '<script>globalAvailabilitySeasons()</script>';
		$result .= '</div>';

		return $result;
	}


}


error_reporting(1);
$load = new Load;
$request = @$_REQUEST['request'];
if($request){
	$load::$request();
}


?>