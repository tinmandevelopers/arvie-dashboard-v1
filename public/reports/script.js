function initApplication()
{	
	
	jQuery.get('api/load.php?request=validate', function(data){
			jQuery('#application').html(data);
	});
}

function ValidateLogin()
{
	var username = jQuery("#username").val();
	var password = jQuery("#password").val();
	jQuery.post('api/load.php?request=login', {user:username,pass:password}, function(data){
			var error = data.error;
			var success = data.success;
			if(error){
				jQuery('#loginMessage .errorMessage').html(error);
				jQuery("#loginMessage").slideDown("fast");
			}else{
				if(success){

					window.location.href="?welcome";
				
				}
			}
	},'json');
}

function ValidateLogout()
{
	jQuery.get('api/load.php?request=logout', function(data){
		  if(data){
			window.location.href="?logout";
		 }
	});
}

function loadData()
{
	
	jQuery.post('api/load.php?request=data', function(data){
		if(data){
			jQuery("#data").html(data);
		}

	});
}

function refresh()
{
	loadData();
}


function globalAvailabilityStats()
{	
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();

	jQuery.post('api/data.php?request=GlobalAvailabilityStatsData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#globalAvailabilityStats").html(data);
		}

	});
}

function globalAvailabilityDataSources()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();

	jQuery.post('api/data.php?request=globalAvailabilityDataSourcesData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#globalAvailabilityDataSources").html(data);
		}

	});
}

function globalAvailabilitySeasons()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();

	jQuery.post('api/data.php?request=globalAvailabilitySeasonsData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#globalAvailabilitySeasons").html(data);
		}

	});
}

function AvailabilityByState()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=AvailabilityByStateData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#AvailabilityByState").html(data);
		}

	});

	
}

function AvailabilityByCheckinDay()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=AvailabilityByCheckinData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#AvailabilityByCheckinDay").html(data);
		}

	});
}

function AvailabilityByRequestDay()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=AvailabilityByRequestDayData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#AvailabilityByRequestDay").html(data);
		}

	});
}

function LenghtOfStays()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=LenghtOfStaysData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#LenghtOfStays").html(data);
		}

	});
}

function GlobalSOS()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=GlobalSOSData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#GlobalSOS").html(data);
		}

	});
}

function GlobalSOSElapsed()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=GlobalSOSElapsedData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#GlobalSOSElapsed").html(data);
		}

	});
}

function SOSDayFound()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=SOSDayFoundData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#SOSDayFound").html(data);
		}

	});
}

function SOSAvailabilityByDay()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=SOSAvailabilityByDayData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#SOSAvailabilityByDay").html(data);
		}

	});
}

function TimeBeforeCheckin()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=TimeBeforeCheckinData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#TimeBeforeCheckin").html(data);
		}

	});
}

function SOSByState()
{
	var startDate = jQuery('#dateFromPlain').val();
	var endDate = jQuery('#dateToPlain').val();
	jQuery.post('api/data.php?request=SOSByStateData', {start:startDate,end:endDate}, function(data){
		if(data){
			jQuery("#SOSByState").html(data);
		}

	});
}