<?php
class Core
{
	public function init()
	{
		global $database;
		global $connection;

		echo self::checkAvailabilityData();
	}

	public function checkAvailabilityData()
	{
		global $database;
		global $connection;

		$result = self::check_availabilityGlobal();
		return $result;

	}

	public function check_availabilityGlobal()
	{
		global $database;
		global $connection;
		$date = true;

		$query = "SELECT * FROM check_availability ";
		if(@$_GET['date1'] and @$_GET['date2']){
			$date1 = $_GET['date1'];
			$date2 = $_GET['date2'];
			$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$date1' and '$date2'";
		}

		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		$numberOfAvailChecks = $num;

		

		if($num > 0){
			$availableArray = array();
			$unAvailableArray = array();
			$availableCampgroundIDs = array();
			$unavailableCampgroundIDs = array();
			$dayNameArray = array();



			while($row =  $database::data_array($query)){
				$availDetail = json_decode($row['details']);
				$availResult = $row['result'];
				$availResult = json_decode($availResult);
				$campgroundID = $row['campground_id'];
				$check_in_date = $row['check_in_date'];
	
			
				
				
				
				$available = $availResult->message;
				$sitesAvailable = $availResult->sites_available;
				switch($available){
						case 'Available':
						$availableArray[] = 1;
						$availableCampgroundIDs[] = $campgroundID;
						$check_in_date_success = $check_in_date;
						$dayofweek = date('l', strtotime($check_in_date_success));
						$dayNameArray[] = $dayofweek;

						if($sitesAvailable){
						$sitesAvailableArray[] = $sitesAvailable;

						}



						break;

						case 'Not Available':
						$unAvailableArray[] = 1;
						$unavailableCampgroundIDs[] = $campgroundID;
				}



			 

			}


			

			$availablitySuccess = count($availableArray);
			$availabilityUnsuccess = count($unAvailableArray);
			$numberOfActualChecks = $availablitySuccess + $availabilityUnsuccess;

			$percentageSuccess = $availablitySuccess / $numberOfActualChecks * 100;

			// Data Source
			$campgroundSourceData = self::campgroundSourceReport($availableCampgroundIDs);
			$count = count($campgroundSourceData);
			$percentageWidth = 100 / $count -5;

			// State
			$campgroundStateData = self::campgroundStateReport($availableCampgroundIDs);

			// Season
			$springAvailablity = self::campgroundAvailablityBySeason('2021/03/01','2021/05/31');
			$summerAvailablity = self::campgroundAvailablityBySeason('2021/06/01','2021/10/31');
			$winterAvailablity = self::campgroundAvailablityBySeason('2021/12/01','2022/02/28');
			$fallAvailablity = self::campgroundAvailablityBySeason('2021/09/01','2022/11/30');
			$seasonArray = array('Spring'=>$springAvailablity,'Summer'=>$summerAvailablity,'Winter'=>$winterAvailablity,'Fall'=>$fallAvailablity);
			$result .= '<style>h2 { margin: 0; padding:0;}</style><div style="font-family: verdana;">';
			
			/*$result .= '<form action="" method="get">';
			if(@$_GET['date1']){
				$date1=$_GET['date1'];
			}

			if(@$_GET['date2']){
				$date2=$_GET['date2'];
			}


			$result .= 'Date Range: <input type="date" name="date1" value="'.$date1.'"/> - <input type="date" name="date2" value="'.$date2.'"/>';
			$result .= '<input type="submit" value="Refresh">';

			$result .= '</form>';	
			*/

			// Global Availablity Stats
			$result .= '<h1>Availablity Check Data</h1>';
			$result .= '<div style="float: left; width: 33%;">';

			$result .= '<table style=" width: 100%" cellspacing="5">';	
				$result .= '<tr style="background: #f1f1f1;"><td><h2>Global Availability</h2></td><td></td></tr>';
				$result .= '<tr><td colspan="2"><br/>Availablity Check data from<br/><strong> '.$date1.' - ';
				$result .= $date2;

				$result .= '</strong><br/><br/> <small>Includes general check availability and<br/>sold out search availability data</small><b/r><br/><br/></td></tr>';
			$result .= '<tr><td style="text-align: right;">Availability Checks&nbsp;&nbsp;</td><td>'.$numberOfActualChecks.'</td></tr>';
			$result .= '<td style="text-align: right;">Availability Success&nbsp;&nbsp;</td><td>'.$availablitySuccess.'</td></tr>';
			$result .= '<tr><td style="text-align: right;">Avg. Sites Available&nbsp;&nbsp;</td><td>'.self::getAverageSitesAvailable($sitesAvailableArray).'</td></tr>';
			$result .= '<td style="text-align: right;">Availability Unsuccesful&nbsp;&nbsp;</td><td>'.$availabilityUnsuccess.'</td></tr>';
			$result .= '<td style="text-align: right;">Percentage Success Rate&nbsp;&nbsp;</td><td>';
			 $result .= round($percentageSuccess,2).'%';
			$result .= '</td></tr><tr><td></td><td>&nbsp;</td></tr>';
			$result .= '</table>';

			$result .= '</div><div style="float: left; width: 33%;">';

			// Data source
			$result .= '<table width="100%">';
			$result .= '<tr style="background: #f1f1f1;"><th colspan="2"><h2>Data Sources</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of Availablity found per data source</strong><br/><br/></td></tr>';
			$result .= '<tr><td colspan="2">';
				
				
				foreach($campgroundSourceData as $source=>$availablityPerSource){
						 $percentageOfChecks = round($availablityPerSource / $availablitySuccess * 100,2);

						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 5px; padding: 5px; width: '.$percentageWidth.'%; height: 75px;">';
							$result .= '<div style="text-align: center; font-weight: bold;">'.$source.'</div>';
							
							$result .= '<div style="font-size: 20px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}
				$result .= '<div style="clear: both;"></div>';
			$result .= '</td></tr>';	

			$result .= '</table></div><div style="float: right; width: 33%">';


			// Per Season
				$result .= '<table width="100%">';
			$result .= '<tr style="background: #f1f1f1;"><th colspan="2"><h2>Availability  per Season</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of Availablity found per Season</strong><br/><br/><br/>
			<small>Spring: March 1 to May 31;<br/>
			Summer: June 1 to August 31<br/>
			Fall: September 1 to November 30<br/>
			Winter: December 1 to February 28 
			</small><br/><br/>
			</td></tr>';
			$result .= '<tr><td colspan="2">';
				foreach($seasonArray as $season=>$availablityPerSeason){
						$percentageOfChecks = round($availablityPerSeason / $availablitySuccess * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 5px; padding: 5px; width: 20%">';
							$result .= '<div style="text-align: center; font-weight: bold;">'.$season.'</div>';
							
							$result .= '<div style="font-size: 20px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}
			$result .= '</td></tr>';
			$result .= '</table>';



			$result .= '</div><div style="clear:both;"></div>';
		


			// Per state
			$result .= '<table><tr style="background: #f1f1f1;"><th colspan="2"><h2>Availability per State</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of Availablity found per State</strong><br/><br/></td></tr>';
			$result .= '<tr><td colspan="2">';
				foreach($campgroundStateData as $state=>$availablityPerState){
						$percentageOfChecks = round($availablityPerState / $availablitySuccess * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 10px; padding: 10px; width: 7%; height: 60px">';
							$result .= '<div style="text-align: center; font-weight: bold; font-size: 12px;">'.$state.'<br/><br/></div>';
							
							$result .= '<div style="font-size: 20px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}

			$result .= '</td></tr>';


			$result .= '</table>';

			$result .= '<div style="float: left; width: 50%;">';

			// check availablity days
			// Checkiin Date Days
		
		 	$checkinDayAvailablities = self::checkAvailablityByDateCheckin();
		
			// Per state
			$result .= '<table><tr style="background: #f1f1f1;"><th colspan="2"><h2>Availability for  Check-in Day</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of availability found by day of the week (check-in day) </strong><br/><br/></td></tr>';
			
			$result .= '<tr><td colspan="2">';
				foreach($checkinDayAvailablities as $day=>$availablityPerCheckin){
						$percentageOfChecks = round($availablityPerCheckin / $availablitySuccess * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 10px; padding: 10px; width: 7.5%; height: 60px">';
							$result .= '<div style="text-align: center; font-weight: bold;">'.$day.'<br/><br/></div>';
							
							$result .= '<div style="font-size: 14px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}

			$result .= '</td></tr>';
			$result .= '</table>';

			$result .= '</div><div style="float: left; width: 50%;">';

			// stats availablity was check on day
			$dayAvailablitiesRequested = self::checkAvailablityRequestDay();
		
			// Per state
			$result .= '<table><tr style="background: #f1f1f1;"><th colspan="2"><h2>Day Availability  Requested</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of availability found by availability request initiation (day availablity was requested on) </strong><br/><br/></td></tr>';
			$result .= '<tr><td colspan="2">';
				foreach($dayAvailablitiesRequested as $day=>$dayCheck){
						$percentageOfChecks = round($dayCheck / $availablitySuccess * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 10px; padding: 10px; width: 7.5%; height: 60px">';
							$result .= '<div style="text-align: center; font-weight: bold;">'.$day.'</div>';
							
							$result .= '<div style="font-size: 14px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}

			$result .= '</td></tr>';
			$result .= '</table>';

			$result .= '</div><div style="clear:both;"></div>';

		

			// lengthOfStayData
			$stayLength = self::lengthOfStayData();
			$result .= '<table><tr style="background: #f1f1f1;"><th colspan="2"><h2>Length of Stay Stats</h2></th></tr>';
			$result .= '<tr><td colspan="2">';
				foreach($stayLength as $lKey=>$lTotals){
						$percentageOfChecks = round($lTotals / $availablitySuccess * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 10px; padding: 10px; width: 7%; height: 60px">';
							$result .= '<div style="text-align: center; font-weight: bold;">'.$lKey.'</div>';
							
							$result .= '<div style="font-size: 20px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}

			$result .= '</td></tr>';
			$result .= '</table>';

			// sold out search
			$sosData =  self::SoldOutSearchData();
			$sosSuccessData = self::SOSSuccess();



			$sosSuccess = $sosSuccessData['total'];
			$averageTimeSuccess = $sosSuccessData['averageTime'];
			$sosTotal = $sosData['total'];
			$totalFrontend = $sosData['totalFrontend'];
			$totalBackend = $sosData['totalBackend'];
			$sosStateData = $sosData['sosLocationsClean'];
			$sosNumberofAvailablityFound = $sosData['sosNumberofAvailablityFound'];
			$sosSuccessRate = $sosSuccess/$sosTotal*100;
			$sosRaw = $sosSuccessData['raw'];



			$result .= '<h1>Sold Out Search Data</h1>';
			$result .= '<div style="float: left; width: 33%">';
				$result .= '<table style=" width: 100%" cellspacing="5">';	
				$result .= '<tr style="background: #f1f1f1;"><td><h2>Global SOS Data</h2></td><td></td></tr>';
					$result .= '<tr><td colspan="2"><br/>Sold Out Search Data from <br/><strong>'.$date1.' - ';
						$result .= $date2;
					$result .= '</strong></td></tr>';
					$result .= '<tr><td>Total Number of Sold Out Searches</td><td>'.$sosTotal.'</td></tr>';
					$result .= '<tr><td>Initiated from website</td><td>'.$totalFrontend.'</td></tr>';
					$result .= '<tr><td>Initiated by admin</td><td>'.$totalBackend.'</td></tr>';
					$result .= '<tr><td>Total number of successful Sold Out Searches where Availablity was found</td><td>'.$sosSuccess.'</td></tr>';
					$result .= '<tr><td>Success Rate</td><td>'.round($sosSuccessRate,2).'%</td></tr>';
					$result .= '<tr><td>Average time to get successful SOS</td><td>'.round($averageTimeSuccess,2).' hours </td></tr>';
				$result .= '</table>';


			$result .= '</div>';
			$result .= '<div style="float: left; width: 33%">';
				$result .= '<table style=" width: 100%" cellspacing="5">';	
				$result .= '<tr style="background: #f1f1f1;"><td colspan="2"><h2>Sold Out Search Elapsed Time</h2></td><td></td></tr>';
					$result .= '<Tr><td colspan="2">Time it took to get availablity on successful Sold Out Search</td></tr>';
				 $sosDataMore = self::sosDataMore($sosRaw);
				



				 if(is_array($sosDataMore['elapsed'])) {
				 	foreach($sosDataMore['elapsed'] as $title=>$value){
				 			$percentageElapsed = round($value/$sosSuccess * 100,2);
				 			$result .= '<tr><td width="50%">'.$title.'</td><td width="50%">'.$value.'<small> ('.$percentageElapsed.'%)</small></td></tr>';
				 	}
				 }



				$result .= '</table><br/>';
				$result .= '<table>';
				 $result .= '<tr style="background: #f1f1f1;"><td colspan="2"><h2>Sold Out Search Day found</h2></td><td></td></tr>';
				 $result .= '<Tr><td colspan="2">Day when Sold Out Search was found<br/><br/></td></tr>';

				 $daySosFound = array();
				 
				 foreach($sosRaw as $key=>$data){
				 		$elapsed_date = $data['elapsed_date'];

				 		$day = date('D', strtotime($elapsed_date));
				 		$daySosFound[] = $day;		
				 }
					
					$daysSosFoundCount = count($daySosFound);
					$daySosFoundClean = array_count_values($daySosFound);
					
					foreach($daySosFoundClean as $day=>$dayCount){
						$percentageSos = round($dayCount / $daysSosFoundCount * 100,2);

						$result .= '<tr><td>'.$day.'</td><td>'.$dayCount.' <small>('.$percentageSos.'%)</small></td></tr>';
					}

				$result .= '</table>';

			$result .= '<br/></div>';
			$result .= '<div style="float: left; width: 33%">';
				$result .= '<table style=" width: 100%" cellspacing="5">';
				$result .= '<tr style="background: #f1f1f1;"><td colspan="2"><h2>Sold Out Search Availability by Day</h2></td><td></td></tr>';
					$result .= '<Tr><td colspan="2">Check in day on successful Sold Out Search</td></tr>';

					 if(is_array($sosDataMore['dayofweek'])) {
				 	foreach($sosDataMore['dayofweek'] as $title=>$value){
				 			$percentageElapsed = round($value/$sosSuccess * 100,2);
				 			$result .= '<tr><td width="50%">'.$title.'</td><td width="50%">'.$value.'<small> ('.$percentageElapsed.'%)</small></td></tr>';
				 	}
				 }


					$result .= '</table>';
					$result .= '<table style=" width: 100%" cellspacing="5">';
				$result .= '<tr style="background: #f1f1f1;"><td colspan="2"><h2>Time before Check-In Date</h2></td><td></td></tr>';
					$result .= '<Tr><td colspan="2">Time before checkin date when Sold Out Search found</td></tr>';

					$timeBeforeCheckin = self::TimeBeforeCheckin($sosRaw);
					

					foreach($timeBeforeCheckin['data'] as $key=>$value){

						$result .= '<tr><td>Days before checkin</td><td>'.$value.'</td></tr>';
					}

					$result .= '<tr><td>Average days before checkin</td><td>'.$timeBeforeCheckin['avg'].'</td></tr>';

					$result .= '</table>';


			$result .= '<br/></div>';
			$result .= '<div style="clear:both;"></div>';

			// sos state data
				// Per state
			$result .= '<table><tr style="background: #f1f1f1;"><th colspan="2"><h2>Sold Out Searches in States</h2></th></tr>';
			$result .= '<tr><td colspan="2"><br/>Distribution of Sold Out Searches started in State</strong><br/><br/></td></tr>';
			$result .= '<tr><td colspan="2">';
				foreach($sosStateData as $state=>$sosPerState){
						$percentageOfChecks = round($sosPerState / $sosTotal * 100,2);
						$result .= '<div style="float: left; border: 1px solid #ddd; margin: 10px; padding: 10px; width: 7%; height: 60px">';
							$result .= '<div style="text-align: center; font-weight: bold; font-size: 12px;">'.$state.'<br/><br/></div>';
							
							$result .= '<div style="font-size: 20px; text-align: center;">'.$percentageOfChecks.'%</div>';
						$result .= '</div>';
				}

			$result .= '</td></tr>';


			$result .= '</table>';




			// booking stats
			//$result .= '<h1>Booking Stats</h1>';


			

		}
		


		$result .= '</div></div>';
		return $result;
	}
	//$result .= self::TimeBeforeCheckin($sosDataMore);

	public function TimeBeforeCheckin($data='')
	{
		if(is_array($data)){
			$timeLeft = array();

			
			foreach($data as $key=>$attr){
				$check_in_date = $attr['check_in_date'];
				$elapsed_date = $attr['elapsed_date'];

				$t1 = strtotime($check_in_date);
				$t2 = strtotime($elapsed_date);
				$diff = $t1 - $t2;
				$hours = $diff / ( 60 * 60 );
				if($hours>0){
					 $days = $hours / 24;
					 $daysRounded = round($days);
					 $timeLeft[] = $daysRounded;
				}


			}

			

			$dupsRemoved = array_unique($timeLeft);
			$count = count($dupsRemoved);
			$countDays = array_sum($dupsRemoved);

			$countAverage = round($countDays / $count);

			return array('data'=>$dupsRemoved,'totalCount'=>$countDays, 'total'=>$count,'avg'=>$countAverage);


		}
	}

	public function sosDataMore($sosData= '')
	{
		if(is_array($sosData)){
			
			
			$first_availability = array();
			$elapsed_time = array();
			$dayOfWeek = array();

			foreach($sosData as $key=>$data){

					$first_search_availability[] = $data['first_search_availability'];


					$created_at = $data['created_at'];
					$elapsed_date = $data['elapsed_date'];
					$check_in_date = $data['check_in_date'];
					$elapsed_time[] = $data['elapsed_time'];
					$dayOfWeek[] = $data['day_of_week'];


			}
 

			$first_search_availabilityClean = array_count_values($first_search_availability);
			$elapsed_timeClean = array_count_values($elapsed_time);
			$dayOfWeekClean = array_count_values($dayOfWeek);

			
			return array('elapsed'=>$elapsed_timeClean, 'dayofweek'=>$dayOfWeekClean);


		}
	}

	public function checkinByDay($availDays = '', $totalAvails='')
	{

		$days = array_count_values($availDays);
		foreach($days as $day=>$number){
				$percentageAvailDays = round($number/$totalAvails * 100,2);

				$result .= '<td width="14%">';
				$result .= '<div style="border: 1px solid #ddd; margin: 10px; padding: 10px; width: 100%; height: 60px"><strong>'.$day.'</strong><br/><br/>';
				$result .= $percentageAvailDays.'%';

			$result .= '</div></td>';
		}

		return $result;
		
	}

	public function SOSSuccess()
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability_notifications where not isnull(elapsed_time)";
		if(@$_GET['date1'] and @$_GET['date2']){
			$date1 = $_GET['date1'];
			$date2 = $_GET['date2'];
			$query = "SELECT * FROM check_availability_notifications WHERE check_in_date BETWEEN '$date1' and '$date2' and not isnull(elapsed_time)";
		}
	
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$hour = array();
				$raw  = array();

				while($row =  $database::data_array($query)){
						$elapsed_time = $row['elapsed_time'];
						$raw[] = $row;

						//echo $elapsed_time.'<br/>';
						$x = explode(' ', $elapsed_time);
						//echo $x[1].'<br/>';

						switch($x[1]){
							case 'week':
							$hour[] = 168;
							break;

							case 'weeks':
							$time_in_week = 168;
							$hour[] = $x[0] * $time_in_week;
							break;

							case 'days':
							$time_in_day = 24;
							$hour[] = $x[0] * $time_in_day;
							break;

							case 'hours':
							$hour[] = $x[0];
							break;

							case 'minutes':
							$hour[] = '0.'.$x[0];
							break;
						}

						
				}

				$totalHours = array_sum($hour);
				$averageTime = $totalHours / $num;

		}

		return array('total'=>$num,'averageTime'=>$averageTime, 'raw'=>$raw);
	}

	public function SoldOutSearchData()
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability_notifications";
		if(@$_GET['date1'] and @$_GET['date2']){
			$date1 = $_GET['date1'];
			$date2 = $_GET['date2'];
			$query = "SELECT * FROM check_availability_notifications WHERE check_in_date BETWEEN '$date1' and '$date2'";
		}

		$query = $database::query($connection, $query);
		$num = $query->num_rows;

		if($num > 0){
					$numberSOS = $num;
					$numberBackend = array();
					$numberFrontend = array();
					$sosLocation = array();
					$availFound = array();
					$sosDataRaw = array();

					while($row =  $database::data_array($query)){
							$sos_id = $row['sold_out_search_id'];
							$sos_result = $row['result'];
							$location = $row['location'];
							$sosLocation[] = $location;
							$json_result = json_decode($sos_result);
							$availability = $json_result->availability;
							if($availability){
								$availFound[] = 1;
							}

							$sosDataRaw[] = $row;
							

							if($sos_id){
								$numberFrontend[] = 1;
								

							
							}else{
								$numberBackend[] = 1;

							}

					}

					$numberBackendCount = count($numberBackend);
					$numberFrontendCount	 = count($numberFrontend);
					$availFoundCount = count($availFound);

					$sosLocationsClean = array_count_values($sosLocation);
			

					
		}

		return array(
			'total'=>$numberSOS,
			'totalBackend'=>$numberBackendCount,
			'totalFrontend'=>$numberFrontendCount,
			'sosLocations'=>$sosLocation,
			'sosLocationsClean'=>$sosLocationsClean,
			'sosNumberofAvailablityFound'=>$availFoundCount,
			'sosDataRaw'=>$sosDataRaw,
		);

	}

	public function lengthOfStayData()
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$stays = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$checkin_date = strtotime($row['check_in_date']);
						$checkout_date = strtotime($row['check_out_date']);
						$day_diff = $checkout_date - $checkin_date;
						$day_diff = floor($day_diff/(60*60*24));
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								if($day_diff != 0){
								$stays[] = ''.$day_diff.'';
								}
								break;
								
						}



					 

			}

			$vals = array_count_values($stays);
			return $vals;
		
			
		}
	}

	public function checkAvailablityRequestDay()
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$checkInDateArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$created_at = $row['created_at'];
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$checkInDateArray[] = date('D',strtotime($created_at));
								break;
								
						}



					 

			}
			$vals = array_count_values($checkInDateArray);
			return $vals;
			
		}
	}

	public function checkAvailablityByDateCheckin()
	{
		global $connection;
		global $database;

		$query = "SELECT * FROM check_availability";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$checkInDateArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];
						$checkInDate = $row['check_in_date'];
						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$checkInDateArray[] = date('D',strtotime($checkInDate));
								break;
								
						}



					 

			}
			$vals = array_count_values($checkInDateArray);
			return $vals;
			
		}
	}

	public function campgroundAvailablityBySeason($startDate='', $endDate='')
	{
		global $connection;
		global $database;



		$query = "SELECT * FROM check_availability WHERE check_in_date BETWEEN '$startDate' and '$endDate'";
		$query = $database::query($connection, $query);
		$num = $query->num_rows;
		if($num > 0){
				$availableArray = array();
			


					while($row =  $database::data_array($query)){
						$availDetail = json_decode($row['details']);
						$availResult = $row['result'];
						$availResult = json_decode($availResult);
						$campgroundID = $row['campground_id'];

						
						
						
						$available = $availResult->message;
						$sitesAvailable = $availResult->sites_available;
						switch($available){
								case 'Available':
								$availableArray[] = 1;
								break;
								
						}



					 

			}

		

			return count($availableArray);
		}
	}

	public function campgroundStateReport($campgroundIDs='')
	{
		global $connection;
		global $database;

		if(is_array($campgroundIDs)){
				$dataSourceIds = array();
				$campgroundGroups = array();

				foreach($campgroundIDs as $key=>$campground_id)
				{
						$query = "SELECT * FROM campgrounds where id='$campground_id'";
						$query = $database::query($connection, $query);
						$num = $query->num_rows;
						if($num == 1){
							$array = $database::data_array($query);
							$group_id = $array['campground_group_id'];
							$group_name = self::getGroupName($group_id);
							$campgroundGroups[] = $group_name;
						}
				}
				$vals = array_count_values($campgroundGroups);
				return $vals;
		}
	}

	public function getGroupName($group_id)
	{
			global $connection;
			global $database;
			if($group_id){
					$query = "SELECT * FROM campgrounds_group WHERE id='$group_id'";
					$query = $database::query($connection, $query);
					$num   = $query->num_rows;

				

					if($num == 1){
						$array = $database::data_array($query);
						$name = strtoupper($array['name']);
						return $name;
					}
			}
	}


	public function campgroundSourceReport($campgroundIDs='')
	{
		global $connection;
		global $database;

		if(is_array($campgroundIDs)){
				$dataSourceIds = array();

				foreach($campgroundIDs as $key=>$campground_id)
				{
						$query = "SELECT * FROM campgrounds where id='$campground_id'";
						$query = $database::query($connection, $query);
						$num = $query->num_rows;
						if($num == 1){
							$array = $database::data_array($query);
							$dataSourceIds[] = $array['data_source_id'];
						}
				}

					$vals = array_count_values($dataSourceIds);
					$koa = $vals[1];
					$sun = $vals[2];
					$recreation = $vals[3];
					$reserve = $vals[5];

					$sourceArray = array(
						'KOA'=>$koa,
						'Sun RV'=>$sun,
						'Recreation'=>$recreation,
						'Reserve America'=>$reserve
					);


					return $sourceArray;


		}

	}


	public function getAverageSitesAvailable($sitesArray='')
	{
		$result = 0;
		

		$array_count = count($sitesArray);
		$array_sum = array_sum($sitesArray);

		$averageSitesAvailable = round($array_sum / $array_count,1);
		return $averageSitesAvailable;
	}


	

	
	public function DataSource($dataSourceId='')
	{
		global $database;
		global $connection;


		// Campground

		$query = "SELECT * FROM data_sources WHERE id='$dataSourceId'";
		$query = $database::query($connection, $query);
		$num   = $query->num_rows;

		if($num > 0){
			$array = $database::data_array($query);
			return $array;


			

		}
		
	}

}