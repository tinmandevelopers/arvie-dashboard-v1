<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate' => 'Activate',
                'change_password' => 'Change Password',
                'clear_session' => 'Clear Session',
                'confirm' => 'Confirm',
                'approve' => 'Approve',
                'reject' => 'Reject',
                'deactivate' => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'login_as' => 'Login As :user',
                'resend_email' => 'Resend Confirmation E-mail',
                'restore_user' => 'Restore User',
                'unconfirm' => 'Un-confirm',
                'unlink' => 'Unlink',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirm Account',
            'reset_password' => 'Reset Password',
        ],
    ],

    'general' => [
        'cancel' => 'Cancel',
        'continue' => 'Continue',
        'change' => 'Change',
        'approve' => 'Approve',
        'reject' => 'Reject',

        'crud' => [
            'next' => 'Next',
            'back' => 'Back',
            'submit' => 'Submit',
            'create' => 'Create',
            'delete' => 'Delete',
            'edit' => 'Edit',
            'update' => 'Update',
            'view' => 'View',
            'download' => 'Download',
            'export' => 'Export',
            'start_crawler' => 'Start Crawler',
            'running_crawler' => 'Crawler Running',
            'start_booking' => 'Start Booking',
            'running_booking' => 'Booking Running',
        ],
        'log' => 'View Log',
        'save' => 'Save',
        'view' => 'View',
    ],
];
