<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Email Template Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'new_profile_request' => [
        'subject' => app_name().' - New Profile Verification Request',
        'body' => [
            'introLines' => ['You have new profile verification request from - :user_name'],
            'actionText' => 'Click here to view'
        ],
    ],
    'profile_verification' => [
        'subject' => app_name().' - Profile Verification :status',
        'body' => [
            'introLines' => ['Your profile verification request is - :status', ':reason'],
            'actionText' => 'Click here to login'
        ],
    ]
];
