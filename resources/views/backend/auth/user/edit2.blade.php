@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.create'))

@section('breadcrumb-links')
@endsection

@section('content')

{{ html()->modelForm($user, 'PATCH', route('admin.auth.user.update', $user->id))->id('user_step2')->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }}
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.users.edit')
                </h4>
                <br>
                <h5 class="card-sub-title">
                    @lang('labels.backend.access.users.personal_details_next_kin')
                </h5>
            </div><!--col-->
            <div class="steps">
                <ul class="list-unstyled reverse">
                    <li><span>1</span></li>
                    <li class="current"><span>2</span></li>
                </ul>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body user-form">
        <div class="row plr-50">
            {{ html()->hidden('step',2)}}
            <div class="col-md-6">
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.first_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_first_name') }}

                    <div class="col-md-12">
                        {{ html()->text('kin_first_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.last_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_last_name') }}

                    <div class="col-md-12">
                        {{ html()->text('kin_last_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.contact_number')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_contact') }}

                    <div class="col-md-12">
                        {{ html()->text('kin_contact')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.contact_number'))
                                    ->attribute('maxlength', 10)
                                    ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.email')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_email') }}

                    <div class="col-md-12">
                        {{ html()->email('kin_email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.membership_number'))->class('col-md-12 form-control-label')->for('medical_membership_number') }}

                    <div class="col-md-12">
                        {{ html()->text('medical_membership_number')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.membership_number'))
                                    ->attribute('maxlength', 50)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.provider'))->class('col-md-12 form-control-label')->for('medical_provider') }}

                    <div class="col-md-12">
                        {{ html()->text('medical_provider')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.provider'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.insurance_plan'))->class('col-md-12 form-control-label')->for('medical_insurance_plan') }}

                    <div class="col-md-12">
                        {{ html()->text('medical_insurance_plan')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.insurance_plan'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.membership_card'))->class('col-md-12 form-control-label')->for('medical_membership_card') }}

                    <div class="col-md-12">
                        @php($hide = '')
                        @if ($user->medical_membership_card != '' && file_exists(public_path() . '/storage/' . $user->medical_membership_card))
                        <div class="image-preview">
                            @php($hide = 'hidden')
                            {{ html()->img(url('/storage/' . $user->medical_membership_card))->class("picture") }}
                            {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                        </div>
                        @endif
                        <div class="custom-file {{$hide}}">
                            {{ html()->file('medical_membership_card')->class('form-control-file custom-file-input') }}
                            {{ html()->label(__('validation.attributes.backend.access.users.choose_file'))->class('col-md-12 custom-file-label')->for('medical_membership_card') }}
                            {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->

            <div class="col-md-6">

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.avatar_location'))->class('col-md-12 form-control-label')->for('avatar_location') }}

                    <div class="col-md-12">
                        @php($hide = '')
                        @if ($user->avatar_location != '' && file_exists(public_path() . '/storage/' . $user->avatar_location))
                        <div class="image-preview">
                            @php($hide = 'hidden')
                            {{ html()->img(url('/storage/' . $user->avatar_location))->class("picture") }}
                            {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                        </div>
                        @endif
                        <div class="custom-file {{$hide}}">
                            {{ html()->file('avatar_location')->class('form-control-file custom-file-input')}}
                            {{ html()->label(__('validation.attributes.backend.access.users.choose_file'))->class('col-md-12 custom-file-label')->for('avatar_location') }}
                            {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.about_me'))->class('col-md-12 form-control-label')->for('about_me') }}

                    <div class="col-md-12">
                        {{ html()->textarea('about_me')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.about_me'))
                                    ->attribute('rows', 5) }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.role_of_user'))->class('col-md-12 form-control-label')->for('roles') }}
                    <div class="col-md-12">
                        {{html()->select('roles', $roles->pluck('name','id'), $user->roles[0]->id)->class('form-control')}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.users.specialized_in'))->class('col-md-12 form-control-label')->for('specialized_in') }}

                    <div class="col-md-12">
                        {{ html()->text('specialized_in')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.specialized_in'))
                                    ->attribute('maxlength', 13)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    <div class="col-md-12">
                        {{ html()->checkbox('active', $user->active)}}
                        {{ html()->label(__('validation.attributes.backend.access.users.active'))->class('form-control-label checkbox-label')->for('active') }}
                        
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group hidden">
                    {{ html()->label(__('validation.attributes.backend.access.users.confirmed'))->class('col-md-12 form-control-label')->for('confirmed') }}

                    <div class="col-md-12">
                        <label class="switch switch-label switch-pill switch-primary">
                            {{ html()->checkbox('confirmed', false)->class('switch-input') }}
                            <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                        </label>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.auth.user.edit',$user), __('buttons.general.crud.back')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.submit')) }}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->form()->close() }}
@endsection

@push('after-scripts')
{!! script('js/users.js') !!}
@endpush