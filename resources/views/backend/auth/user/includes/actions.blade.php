@if ($user->trashed())
<div class="btn-group" role="group" aria-label="@lang('labels.backend.access.users.user_actions')">
    <a href="{{ route('admin.auth.user.restore', $user) }}" name="confirm_item" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="@lang('buttons.backend.access.users.restore_user')">
        <i class="fas fa-sync"></i>
    </a>

    <a href="{{ route('admin.auth.user.delete-permanently', $user) }}" name="confirm_item" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="@lang('buttons.backend.access.users.delete_permanently')">
        <i class="fas fa-trash"></i>
    </a>
</div>
@else
<ul class="navbar-nav ml-auto mr-sm-3">
    <li class="dropdown">
        <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" class="text-center">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right mt-sm-2 mr-n3">
            @if ($logged_in_user->isAdmin())
            <a href="{{ route('admin.auth.user.change-password', $user) }}" class="dropdown-item">
                <i class="fa fa-sync"></i> @lang('buttons.backend.access.users.change_password')
            </a>
            @endif
            
            <a href="{{ route('admin.auth.user.show', $user) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="dropdown-item">
                <i class="fas fa-eye"></i> @lang('buttons.general.crud.view')
            </a>
            @if ($logged_in_user->isAdmin())
            <a href="{{ route('admin.auth.user.edit', $user) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.edit')" class="dropdown-item">
                <i class="fas fa-edit"></i> @lang('buttons.general.crud.edit')
            </a>

            @if ($user->id !== 1 && $user->id !== auth()->id() && $user->active)
            <a href="{{ route('admin.auth.user.destroy', $user) }}"
               data-method="delete"
               data-trans-button-cancel="@lang('buttons.general.cancel')"
               data-trans-button-confirm="@lang('buttons.general.crud.delete')"
               data-trans-title="@lang('strings.backend.general.are_you_sure')"
               class="dropdown-item">
                <i class="fas fa-trash"></i>  @lang('buttons.general.crud.delete')
            </a>
            @endif
            @endif
        </div>
    </li>
</ul>
@endif
