@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute">
                    {{ __('labels.backend.access.users.all') }}
                    @if ($logged_in_user->isAdmin())
                    <a href="{{ route('admin.auth.user.create') }}" class="btn ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')">@lang('labels.general.create_new')</a>
                    @endif
                </h4>
            
                <div class="table-responsive">
                    <table class="table data-table" id="users_list" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.users.table.agent_id')</th>
                                <th>@lang('labels.backend.access.users.table.first_name')</th>
                                <th>@lang('labels.backend.access.users.table.last_name')</th>
                                <th>@lang('labels.backend.access.users.table.date')</th>
                                <th>@lang('labels.backend.access.users.table.status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'users_list';
    var list_url = '{{ route("admin.auth.users.list") }}';
    var order = [[1, 'asc']];
    var columns = [
        {data: 'id'},
        {data: 'first_name'},
        {data: 'last_name'},
        {data: 'created_at'},
        {data: 'active'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];
</script>
{!! script('js/custom_datatable.js') !!}
@endpush