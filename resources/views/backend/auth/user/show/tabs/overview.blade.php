<div class="row">
    <div class="col-md-4 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-column align-items-center text-center">
                    <img src="{{ $user->picture }}" alt="{{ $user->name }}" class="rounded-circle" width="150">
                    <div class="mt-3">
                        <h4>{{ $user->name }}</h4>
                        <p class="text-muted mb-1">{{ $user->screen_name }}</p>
                        <p class="text-muted font-size-sm">{{ $user->email }}</p>
                        <p class="text-muted font-size-sm">{{ $user->specialized_in }}</p>
                    </div>
                </div>
            </div>
        </div>
        @if(auth()->user()->isAdmin() || auth()->user()->id == $user->id)
        <div class="card mt-3">
            <div class="card-header">
                <h4>@lang('labels.backend.access.users.tabs.content.overview.next_to_kin')</h4>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.name')
                    </h6>
                    <span class="text-muted">{{ $user->kin_first_name.' '.$user->kin_last_name }}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.contact_number')
                    </h6>
                    <span class="text-muted">{{ $user->kin_contact }}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.email')
                    </h6>
                    <span class="text-muted">{{ $user->kin_email }}</span>
                </li>
            </ul>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                <h4>@lang('labels.backend.access.users.tabs.content.overview.medical_info')</h4>
            </div>
            <ul class="list-group list-group-flush">
                @if ($user->medical_membership_card != '' && file_exists(public_path() . '/storage/' . $user->medical_membership_card))
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <div class="card-preview text-center">
                        {{ html()->img(url('/storage/' . $user->medical_membership_card))->class("picture") }}
                    </div>
                </li>
                @endif
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.membership_number')
                    </h6>
                    <span class="text-muted">{{ $user->medical_membership_number}}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.provider')
                    </h6>
                    <span class="text-muted">{{ $user->medical_provider }}</span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0">
                        @lang('labels.backend.access.users.tabs.content.overview.insurance_plan')
                    </h6>
                    <span class="text-muted">{{ $user->medical_insurance_plan }}</span>
                </li>
            </ul>
        </div>
        @endif
    </div>
    <div class="col-md-8">
        @if(auth()->user()->isAdmin() || auth()->user()->id == $user->id)
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.dob')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> {{ \Carbon\Carbon::parse($user->dob)->format('M d, Y') }}</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.address')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> {{ $user->address }}</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.contact_number')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> {{ $user->contact_number }}</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.alternative_email')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> {{ $user->alternative_email }}</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.id_number')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> 
                        @if ($user->id_proof != '' && file_exists(public_path() . '/storage/' . $user->id_proof))
                        <div class="card-preview">
                            {{ html()->img(url('/storage/' . $user->id_proof))->class("picture") }}
                        </div>
                        @endif
                        {{ $user->id_number }}
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-sm-3">
                        <h6 class="mb-0">@lang('labels.backend.access.users.tabs.content.overview.income_tax_number')</h6>
                    </div>
                    <div class="col-sm-9 text-muted"> {{ $user->income_tax_number }}</div>
                </div>
            </div>
        </div>
        @endif
        <div class="card mb-3">   
            <div class="card-header">
                <h4>@lang('labels.backend.access.users.tabs.content.overview.about_me')</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">{{ $user->about_me }}</div>
                </div>
            </div>
        </div>
    </div>
</div>