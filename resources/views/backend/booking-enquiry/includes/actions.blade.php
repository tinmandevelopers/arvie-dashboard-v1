<ul class="navbar-nav ml-auto mr-sm-3">
    <li class="dropdown">
        <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" class="text-center">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        
        <div class="dropdown-menu dropdown-menu-right mt-sm-2 mr-n3">
            @if($enquiry->index_status == 'Pending')
            <a href="{{ route('admin.booking.enquiry.booking',$enquiry) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.start_booking')" target="_blank" class="dropdown-item">
                <i class="fas fa-ticket-alt"></i> @lang('buttons.general.crud.start_booking')
            </a>
            {{-- <a href="{{ env('LOCAL_APP_URL') }}/{{$enquiry->campground_id}}/{{$enquiry->id}}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.start_booking')" target="_blank" class="dropdown-item">
                <i class="fas fa-ticket-alt"></i> @lang('buttons.general.crud.start_booking')
            </a> --}}
            @endif
            <a href="{{ route('admin.booking.enquiry.view', $enquiry) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="dropdown-item">
                <i class="fas fa-edit"></i> @lang('buttons.general.crud.view')
            </a>
        </div>
    </li>
</ul>
