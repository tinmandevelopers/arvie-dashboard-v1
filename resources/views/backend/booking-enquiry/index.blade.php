@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.bookingenquiry.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute w-70">
                    {{ __('labels.backend.access.bookingenquiry.all') }}
                </h4>
                <div class="table-responsive">
                    <table class="table data-table" id="bookingenquiry_list" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.bookingenquiry.table.title')</th>
                                <th>@lang('labels.backend.access.bookingenquiry.table.user_name')</th>
                                <th>@lang('labels.backend.access.bookingenquiry.table.check_in_date')</th>
                                <th>@lang('labels.backend.access.bookingenquiry.table.check_out_date')</th>
                                <th>@lang('labels.backend.access.bookingenquiry.table.booking_date')</th>
                                @if (auth()->user()->roles()->get()[0]->name != config('access.users.blue_agent_role'))
                                    <th>@lang('labels.backend.access.bookingenquiry.table.agent_name')</th>
                                @endif
                                <th>@lang('labels.backend.access.bookingenquiry.table.status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'bookingenquiry_list';
    var list_url = '{{ route("admin.booking.enquiry.list") }}';
    var order = [[4, 'desc']];
    var columns = [
        {data: 'campground_id'},
        {data: 'name'},
        {data: 'check_in_date'},
        {data: 'check_out_date'},
        {data: 'created_at'},
        @if (auth()->user()->roles()->get()[0]->name != config('access.users.blue_agent_role'))
        {data: 'assign_id'},
        @endif
        {data: 'index_status'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];
    
    var filterCustomData = true;
</script>
{!! script('js/custom_datatable.js') !!}
@endpush
