@extends('backend.layouts.app')

@section('title', __('labels.backend.access.bookingenquiry.management') . ' | ' . __('labels.backend.access.bookingenquiry.view'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.bookingenquiry.view')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body enquiry-form">
        <div class="row plr-50">
            <div class="col-md-6 ">
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.campground_title').': ')->class('col-md-4 font-bold')->for('campground_title') }}</b> 
                    {{ $enquiry->campground->title }}
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.check_in_date').': ')->class('col-md-4 font-bold')->for('check_in_date') }} </b>
                    {{date(Config('access.date_fromat'),strtotime( $enquiry->check_in_date)) }} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.check_out_date').': ')->class('col-md-4 font-bold')->for('check_out_date') }} </b>
                    {{date(Config('access.date_fromat'),strtotime($enquiry->check_out_date))}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.name').': ')->class('col-md-4 font-bold')->for('name') }} </b>
                    {{ $enquiry->name }}
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.email').': ')->class('col-md-4 font-bold')->for('email') }} </b>
                    {{$enquiry->email}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.phone').': ')->class('col-md-4 font-bold')->for('phone') }} </b>
                    {{$enquiry->phone}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.address1').': ')->class('col-md-4 font-bold')->for('address1') }} </b>
                    {{$enquiry->address1}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.address2').': ')->class('col-md-4 font-bold')->for('address2') }} </b>
                    {{$enquiry->address2}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.country_code').': ')->class('col-md-4 font-bold')->for('country_code') }} </b>
                    {{$enquiry->country_code}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.city').': ')->class('col-md-4 font-bold')->for('city') }} </b>
                    {{$enquiry->city}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.state').': ')->class('col-md-4 font-bold')->for('state') }} </b>
                    {{$enquiry->state}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.post_code').': ')->class('col-md-4 font-bold')->for('post_code') }} </b>
                    {{$enquiry->post_code}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.log_file_path').': ')->class('col-md-4 font-bold')->for('index_status') }}</b> 
                    @if (isset($enquiry->log_file_path) && $enquiry->log_file_path!='')
                    {{ html()->a( env('S3_BUCKET_URL') .'/booking/'.$enquiry->campground_id.'/'.$enquiry->booking_id.'/'.$enquiry->log_file_path,__('buttons.general.crud.download') )->class('btn btn-primary btn-sm')->attribute('download') }}
                    @else     
                    {{$enquiry->log_file_path}}
                    @endif
                </div> <!--form-group-->
                <hr>
            </div><!--col-->
            <div class="col-md-6">
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.booking_id').': ')->class('col-md-4 font-bold')->for('booking_id') }}</b> 
                    {{$enquiry->booking_id}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.index_status').': ')->class('col-md-4 font-bold')->for('index_status') }}</b> 
                    {{$enquiry->index_status}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.status').': ')->class('col-md-4 font-bold')->for('status') }} </b>
                    {{$enquiry->status}} 
                </div> <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.comment').': ')->class('col-md-4 font-bold')->for('index_status') }}</b> 
                    {{$enquiry->comment}} 
                </div> <!--form-group-->
                <hr>
                @php echo prepare_html(json_decode($enquiry->details, true), 'booking'); @endphp
                <hr>
            </div>
        </div><!--row-->
    </div>

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.booking.enquiry'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                <!-- {{ form_submit(__('buttons.general.crud.submit')) }} -->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->closeModelForm() }}
@endsection