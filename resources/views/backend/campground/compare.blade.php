@extends('backend.layouts.app')

@section('title', __('labels.backend.access.campground.compare'))

@section('breadcrumb-links')
@endsection
@php 
$i=0;
@endphp
@section('content')
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.campground.compare')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body campground-form">
        <div class="row plr-50">
            <div class="col-md-12">
                <table class="table data-table" id="profile_request_list" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="3%">#</th>
                            <th width="17%">Campground</th>
                            @foreach($dataSources as $key => $value)
                            <th style="text-align: center" width="{{80/count($dataSources)}}%">{{$value->title}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($campgrounds as $key => $data)
                        @php
                        $i++;
                        $dataSourceArr = $campgroundIdArr = $campgroundStatusArr = [];
                        $dataSourcesArr = explode(',',$data->data_sources);
                        foreach($dataSourcesArr as $dskey => $dsval){
                            $dataSourceValArr = explode('||', $dsval);
                            array_push($campgroundIdArr,$dataSourceValArr[0]);
                            array_push($campgroundStatusArr,$dataSourceValArr[1]);
                            array_push($dataSourceArr,$dataSourceValArr[2]);
                        }
                        @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{ html()->label($data->title)->class('col-md-12 form-control-label') }}</td>
                            @foreach($dataSources as $key => $value)
                            @php
                            $dataSourceKey = array_search($value->id, $dataSourceArr);
                            $campgroundId = isset($campgroundIdArr[$dataSourceKey]) ? $campgroundIdArr[$dataSourceKey] : '';
                            $campgroundStatus = isset($campgroundStatusArr[$dataSourceKey]) ? $campgroundStatusArr[$dataSourceKey] : '';
                            @endphp
                            <td style="text-align: center">{{in_array($value->id, $dataSourceArr) ? html()->a(route('admin.campground.view', $campgroundId),$value->title.'-'.$campgroundStatus)->attributes(['target'=>'_blank','class'=>($campgroundStatus == 'Active' ? 'text-success' : ($campgroundStatus == 'Inactive' ? 'text-danger' : 'text-info'))]) : false}}</td>
                            @endforeach
                        </tr>
                        @empty
                    <td colspan="3">No campgrounds</td>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div><!--row-->
    </div>
</div><!--card-->
@endsection