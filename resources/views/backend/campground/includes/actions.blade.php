<ul class="navbar-nav ml-auto mr-sm-3">
    <li class="dropdown">
        <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" class="text-center">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right mt-sm-2 mr-n3">
            <a href="{{ route('admin.campground.view', $campground) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="dropdown-item">
                <i class="fas fa-edit"></i> @lang('buttons.general.crud.view')
            </a>
            <a href="{{ route('admin.campground.destroy', $campground) }}"
               data-method="delete"
               class="dropdown-item"
               data-trans-button-cancel="@lang('buttons.general.cancel')"
               data-trans-button-confirm="@lang('buttons.general.crud.delete')"
               data-trans-title="@lang('strings.backend.general.are_you_sure')"
               >
                <i class="fas fa-trash"></i>  @lang('buttons.general.crud.delete')
            </a>

        </div>

    </li>
</ul>
