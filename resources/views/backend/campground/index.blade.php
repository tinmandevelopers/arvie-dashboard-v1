@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.campground.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute w-70">
                    {{ __('labels.backend.access.campground.all') }}
                    {{html()->select('dataSource', $dataSource)->class('btn float-right filter-custom')}}
                </h4>

                <div class="table-responsive">
                    <table class="table data-table" id="campground_list" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.campground.table.title')</th>
                                <th>@lang('labels.backend.access.campground.table.data_source')</th>
                                <th>@lang('labels.backend.access.campground.table.last_index')</th>
                                <th>@lang('labels.backend.access.campground.table.web_status')</th>
                                <th>@lang('labels.backend.access.campground.table.status')</th>
                                <th>@lang('labels.backend.access.campground.table.crawler_status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'campground_list';
    var list_url = '{{ route("admin.campground.list") }}';
    var order = [];
    var columns = [
        {data: 'title'},
        {data: 'data_source_id'},
        {data: 'last_indexed_time'},
        {data: 'web_status'},
        {data: 'status'},
        {data: 'crawler_status'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];
    var filterCustomData = true;
</script>
{!! script('js/custom_datatable.js') !!}
@endpush
