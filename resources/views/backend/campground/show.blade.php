@extends('backend.layouts.app')

@section('title', __('labels.backend.access.campground.management') . ' | ' . __('labels.backend.access.campground.view'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($campground, 'PATCH', route('admin.campground.update', $campground->id))->id('data_source_form')->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }}
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.campground.view')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body campground-form">
        <div class="row plr-50">
            <div class="col-md-12">
                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.title').':')->class('col-md-2 text-right font-bold')->for('title') }} 
                    {{ $campground->title }}
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.data_source').':')->class('col-md-2 text-right font-bold')->for('data_source') }} 
                    {{ $campground->getDataSource->crawler_name}}
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.url').':')->class('col-md-2 text-right font-bold')->for('url') }} 
                    {{ html()->a($campground->url, $campground->url)->attributes(['target'=>'_blank']) }}
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.last_index').':')->class('col-md-2 text-right font-bold')->for('last_index') }} 
                    {{ $campground->last_indexed_time->format(config('access.date_fromat_time'))}}
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.web_status').':')->class('col-md-2 text-right font-bold')->for('web_status') }} 
                    {{ $campground->web_status }}
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('labels.backend.access.campground.table.status')."<sup>*</sup>:")->class('col-md-2 text-right')->for('status') }}
                    {{ html()->select('status', ['Active'=>'Active', 'Inactive'=>'Inactive', 'Pending'=>'Pending'], $campground->status)->class('col-md-6 form-control d-inline') }}
                </div><!--form-group-->
            </div><!--col-->
            <div class="col-md-12 pt-3">
                <h4 class="card-title">
                    @lang('labels.backend.access.campground.management') Details
                </h4>
                <hr/>
                @php
                echo campground_detail_from_json(env('S3_BUCKET_URL').'/data_source/'.$campground->data_source_id.'/campgrounds/'.$campground->id.'/'.$campground->data_file_path, true);
                @endphp
            </div>
        </div><!--row-->
    </div>


    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.campground.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.submit')) }}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->closeModelForm() }}
@endsection