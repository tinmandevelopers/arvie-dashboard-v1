@extends('backend.layouts.app')

@section('title', __('labels.backend.access.check-availability-notification.management') . ' | ' .
    __('labels.backend.access.check-availability-notification.create'))

@section('breadcrumb-links')
@endsection

@section('content')
    <div id="overlay" style="display:none;">
    </div>
    <div class="" id="enquiry_result">

    </div>
        <form id="check_availability_notification_form" name="check_availability_notification_form">
        <div class="card">
            <div class="card-header clearfix">
                <div class="row">
                    <div class="col-4">
                        <h4 class="card-title">
                            @lang('labels.backend.access.check-availability-notification.create')
                        </h4>
                    </div>
                    <!--col-->
                </div>
                <!--row-->
            </div>
            <!--card-footer-->

            <div class="card-body data-source-form">
                <div class="row plr-50">
                    <div class="col-md-12 ">
                        <div class="row">

                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.campground_id') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('title') }}

                                <div class="col-md-12">
                                    {{ html()->select('campground_id')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.campground_id'))->id('campground_search')->attribute('maxlength', 191) }}
                                </div>
                                <!--col-->
                            </div>
                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.location'))->class('col-md-12 form-control-label')->for('title') }}

                                <div class="col-md-12">
                                    {{ html()->text('location')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.location'))->attribute('readonly', true) }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->
                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.check_in_date') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('check_in_date') }}

                                <div class="col-md-12">
                                    {{ html()->date('check_in_date')->id('check_in_date')->class('form-control')->placeholder(__('validation.attributes.check-availability-notification.check_in_date'))->required() }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->

                            <div class="form-group  col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.check_out_date') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('check_out_date') }}

                                <div class="col-md-12">
                                    {{ html()->date('check_out_date')->id('check_out_date')->class('form-control')->placeholder(__('validation.attributes.check-availability-notification.check_out_date'))->required() }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->
                            <div class="form-group col-md-6">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.reservation_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('reservation_type') }}

                                <div class="col-md-12">
                                    {{ html()->select('reservation_type', $params['reservation_type'])->class('form-control')->id('reservation_type') }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->

                            <div class="form-group col-md-6">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.adults') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('adults') }}

                                <div class="col-md-12">
                                    {{ html()->number('adults')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.adults'))->attribute('maxlength', 191) }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->

                            <div class="form-group col-md-6">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.duration') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('duration') }}

                                <div class="col-md-12">
                                    {{ html()->select('duration', ['48_hour'=>'48 Hours', '1_week'=>'1 Week', '1_month'=>'1 Month'])->class('col-md-6 form-control d-inline') }}
                                </div>
                                <!--col-->
                            </div>
                        </div>
                    </div>

                </div>
                <!--row-->
                <hr />
                <div class="row plr-50">
                    <div class="col-md-12">
                        <h5 class="card-sub-title">
                            @lang('labels.backend.access.check-availability-notification.other_detail')
                        </h5>
                    </div>

                    {{-- <div class="col-md-12 ">
                        <div class="form-group">
                        
                            <div class="col-md-12">
                            </div>
                        </div>
                        <!--col-->

                    </div> --}}
                    <!--form-group-->
                    <div class=" col-md-12 border rounded ">
                        <div class="col-md-12">
                            <h5 class="card-sub-title">
                                {{ html()->checkbox('details[search_type][custom_search]', 'custom_search', 'custom_search')->class('form-check-input')->id('custom_search') }}
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.custom_search'))->class('col-md-12 form-control-label')->for('towing_vehicle') }}
                            </h5>
                        </div>
                        <div id="custom_search_form" class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.equipment_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('equipment_type') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[custom][equipment_type]', $params['equipment_type'])->class('form-control equipment_type')->id('equipment_type')->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group ">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_vehicle') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_vehicle') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[custom][towing_vehicle]')->value('Yes')->checked(old('towing_vehicle', 'Yes'))->class('form-check-input custom-towing-vehicle') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[custom][towing_vehicle]')->value('No')->checked(old('towing_vehicle', 'No'))->class('form-check-input custom-towing-vehicle') }}
                                        </div>

                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.slide_out') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('slide_out') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[custom][slide_out]')->value('Yes')->checked(old('slide_out', 'Yes'))->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-control-label') }}
                                            {{ html()->radio('details[custom][slide_out]')->value('No')->checked(old('slide_out', 'No'))->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pet_friendly') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pet_friendly') }}

                                    <div class="col-md-12 ">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[custom][pet_friendly]')->value('Yes')->checked(old('pet_friendly', 'Yes'))->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[custom][pet_friendly]')->value('No')->checked(old('pet_friendly', 'No'))->class('form-check-input') }}
                                        </div>
                                    </div>

                                    <!--col-->
                                </div>

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pull_through') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pull_through') }}

                                    <div class="col-md-12">
                                        {{ html()->text('details[custom][pull_through]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.pull_through'))->attribute('maxlength', 50) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[custom][length]',0)->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="towingblock_custom" style="display:none;">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[custom][towing_length]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.towing_length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="rearblock_custom" style="display:none;">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.rear') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('rear') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[custom][rear]', $params['rear'])->class('form-control')->id('rear') }}
                                    </div>
                                    <!--col-->
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class=" col-md-12 border rounded">
                        <div class="col-md-12">
                            <h5 class="card-sub-title">
                                {{ html()->checkbox('details[search_type][user_1]', 'user_1', 'user_1')->class('form-check-input')->id('user_1') }}
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.user_1'))->class('col-md-12 form-control-label')->for('towing_vehicle') }}
                            </h5>
                        </div>
                        <div id="user_1_form" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.equipment_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('equipment_type') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_1][equipment_type]', $params['equipment_type'], $params['user_1']['equipment_type'])->class('form-control equipment_type')->id('equipment_type')->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                                <div class="form-group ">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_vehicle') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_vehicle') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_1][towing_vehicle]')->value('Yes')->checked($params['user_1']['towing_vehicle'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_1][towing_vehicle]')->value('No')->checked($params['user_1']['towing_vehicle'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>

                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.slide_out') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('slide_out') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_1][slide_out]')->value('Yes')->checked($params['user_1']['slide_out'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-control-label') }}
                                            {{ html()->radio('details[user_1][slide_out]')->value('No')->checked($params['user_1']['slide_out'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pet_friendly') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pet_friendly') }}

                                    <div class="col-md-12 ">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_1][pet_friendly]')->value('Yes')->checked($params['user_1']['pet_friendly'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_1][pet_friendly]')->value('No')->checked($params['user_1']['pet_friendly'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pull_through') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pull_through') }}

                                    <div class="col-md-12">
                                        {{ html()->text('details[user_1][pull_through]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.pull_through'))->attribute('maxlength', 50) }}
                                    </div>
                                    <!--col-->
                                </div>
                            </div>
                            <div class="col-md-6">


                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_1][length]', $params['user_1']['length'])->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="towingblock_user1" style="display:none;">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_1][towing_length]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.towing_length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="rearblock_user1">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.rear') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('rear') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_1][rear]', $params['rear'], $params['user_1']['rear'])->class('form-control')->id('rear') }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-12 border rounded ">
                        <div class="col-md-12">
                            <h5 class="card-sub-title">
                                {{ html()->checkbox('details[search_type][user_2]', 'user_2', 'user_2')->class('form-check-input')->id('user_2') }}
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.user_2'))->class('col-md-12 form-control-label')->for('towing_vehicle') }}
                            </h5>
                        </div>
                        <div id="user_2_form" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.equipment_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('equipment_type') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_2][equipment_type]', $params['equipment_type'],$params['user_2']['equipment_type'])->class('form-control equipment_type')->id('equipment_type')->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group ">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_vehicle') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_vehicle') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_2][towing_vehicle]')->value('Yes')->checked($params['user_2']['towing_vehicle'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_2][towing_vehicle]')->value('No')->checked($params['user_2']['towing_vehicle'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>

                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.slide_out') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('slide_out') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_2][slide_out]')->value('Yes')->checked($params['user_2']['slide_out'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-control-label') }}
                                            {{ html()->radio('details[user_2][slide_out]')->value('No')->checked($params['user_2']['slide_out'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pet_friendly') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pet_friendly') }}

                                    <div class="col-md-12 ">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_2][pet_friendly]')->value('Yes')->checked($params['user_2']['pet_friendly'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_2][pet_friendly]')->value('No')->checked($params['user_2']['pet_friendly'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pull_through') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pull_through') }}

                                    <div class="col-md-12">
                                        {{ html()->text('details[user_2][pull_through]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.pull_through'))->attribute('maxlength', 50) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_2][length]', $params['user_2']['length'])->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="towingblock_user2">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_2][towing_length]', $params['user_2']['towing_length'])->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.towing_length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="rearblock_user2">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.rear') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('rear') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_2][rear]', $params['rear'], $params['user_2']['rear'])->class('form-control')->id('rear') }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-12 border rounded">
                        <div class="col-md-12">
                            <h5 class="card-sub-title">
                                {{ html()->checkbox('details[search_type][user_3]', 'user_3', 'user_3')->class('form-check-input')->id('user_3') }}
                                {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.user_3'))->class('col-md-12 form-control-label')->for('towing_vehicle') }}
                            </h5>
                        </div>

                        <div id="user_3_form" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.equipment_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('equipment_type') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_3][equipment_type]', $params['equipment_type'], $params['user_3']['equipment_type'])->class('form-control equipment_type')->id('equipment_type')->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group ">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_vehicle') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_vehicle') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_3][towing_vehicle]')->value('Yes')->checked($params['user_3']['towing_vehicle'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_3][towing_vehicle]')->value('No')->checked($params['user_3']['towing_vehicle'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>

                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.slide_out') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('slide_out') }}

                                    <div class="col-md-12">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_3][slide_out]')->value('Yes')->checked($params['user_3']['slide_out'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-control-label') }}
                                            {{ html()->radio('details[user_3][slide_out]')->value('No')->checked($params['user_3']['slide_out'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pet_friendly') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pet_friendly') }}

                                    <div class="col-md-12 ">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('Yes'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_3][pet_friendly]')->value('Yes')->checked($params['user_3']['pet_friendly'] == 'Yes' ? true : false)->class('form-check-input') }}
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            {{ html()->label(__('No'))->class('form-check-label') }}
                                            {{ html()->radio('details[user_3][pet_friendly]')->value('No')->checked($params['user_3']['pet_friendly'] == 'No' ? true : false)->class('form-check-input') }}
                                        </div>
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.pull_through') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pull_through') }}

                                    <div class="col-md-12">
                                        {{ html()->text('details[user_3][pull_through]')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.pull_through'))->attribute('maxlength', 50) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                            </div>
                            <div class="col-md-6">


                                <div class="form-group">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_3][length]', $params['user_3']['length'])->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="towingblock_user3">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.towing_length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_length') }}

                                    <div class="col-md-12">
                                        {{ html()->number('details[user_3][towing_length]', $params['user_3']['towing_length'])->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability-notification.towing_length'))->attribute('maxlength', 191) }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->

                                <div class="form-group" id="rearblock_user3">
                                    {{ html()->label(__('validation.attributes.backend.access.check-availability-notification.rear') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('rear') }}

                                    <div class="col-md-12">
                                        {{ html()->select('details[user_3][rear]', $params['rear'], $params['user_3']['rear'])->class('form-control')->id('rear') }}
                                    </div>
                                    <!--col-->
                                </div>
                                <!--form-group-->
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer clearfix">
                    <div class="row">
                        <div class="col">
                            {{ form_cancel(route('admin.check-availability-notification'), __('buttons.general.cancel')) }}
                        </div>
                        <!--col-->

                        <div class="col text-right">
                            {{ html()->button(__('buttons.general.crud.submit'))->id('check_availability_notification_form_submit')->class('btn btn-common pull-right') }}
                        </div>
                        <!--col-->
                    </div>
                    <!--row-->
                </div>
                <!--card-footer-->
            </div>
            {{-- <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.check-availability-notification'), __('buttons.general.cancel')) }}
                    </div>
                    <!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.submit')) }}
                    </div>
                    <!--col-->
                </div>
                <!--row-->
            </div>
            <!--card-footer-->
        </div>
        <!--card-->
        {{ html()->form()->close() }} --}}
    </form>

@endsection

@push('before-styles')
    {{ style('css/select2.min.css') }}
@endpush

@push('after-scripts')
    {!! script('js/select2.min.js') !!}

    @php
        $path = storage_path('booking/bookingCheckAvailabilityFormData.json');
        $str = file_get_contents($path);
        $json = json_decode($str, true);
    @endphp

    <script>
        var userID = "{{ Auth::user()->id }}";
        var baseURL = "{{ URL('/') }}";
        var campgroundSearch = "{{ route('admin.campground.search') }}";
        var listURL = "{{ route('admin.check-availability-notification') }}";
        var formDataJson = {!! json_encode($json) !!};
    </script>
    {!! script('js/checkAvailabilityNotification.js') !!}
@endpush
