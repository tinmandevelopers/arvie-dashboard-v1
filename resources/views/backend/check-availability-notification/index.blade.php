@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.check-availability-notification.management'))

@section('breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12">
                    <h4 class="card-title mb-0 position-absolute w-70">
                        {{ __('labels.backend.access.check-availability-notification.management') }}
                        @if ($logged_in_user->isAdmin())
                            <a href="{{ route('admin.check-availability-notification.create') }}" class="btn ml-1"
                                data-toggle="tooltip"
                                title="@lang('labels.general.create_new')">@lang('labels.general.create_new')</a>
                        @endif
                        <a href="{{ route('admin.check-availability-notification.export') }}" data-toggle="tooltip"
                            data-placement="top" title="@lang('buttons.general.crud.view')"
                            class="btn btn-success float-right">
                            @lang('buttons.general.crud.export')
                        </a>
                    </h4>

                    <div class="table-responsive">
                        <table class="table data-table" id="checkavailability_notification_list" style="width:100%;">
                            <thead>
                                <tr>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.campground_title')
                                    </th>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.datasource_title')
                                    </th>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.day_of_week')
                                    </th>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.elapsed_time')
                                    </th>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.positive_hit')
                                    </th>
                                    <th>@lang('labels.backend.access.check-availability-notification.table.status')</th>
                                    <th>@lang('labels.general.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="odd">
                                    <td valign="top" colspan="8" class="dataTables_empty">
                                        @lang('labels.general.no_records_found')</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--col-->
            </div>
            <!--row-->
        </div>
        <!--card-body-->
    </div>
    <!--card-->
@endsection

@push('after-scripts')
    <script>
        var list_id = 'checkavailability_notification_list';
        var list_url = '{{ route('admin.check-availability-notification.list') }}';
        var order = [];
        var columns = [{
                data: 'campground_id'
            },
            {
                data: 'datasource_id'
            },
            {
                data: 'day_of_week'
            },
            {
                data: 'elapsed_time'
            },
            {
                data: 'positive_hit'
            },
            {
                data: 'status'
            },
            {
                data: 'actions',
                name: 'actions',
                class: 'action-btn',
                orderable: false
            }
        ];

        var filterCustomData = true;

    </script>
    {!! script('js/custom_datatable.js') !!}
@endpush
