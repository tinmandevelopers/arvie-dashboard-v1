@extends('backend.layouts.app')

@section('title', __('labels.backend.access.check-availability-notification.management') . ' | ' .
__('labels.backend.access.check-availability-notification.view'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-9">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.check-availability-notification.view')

                </h4>

            </div>
            <!--col-->
        </div>
        <!--row-->
    </div>
    <!--card-footer-->

    <div class="card-body enquiry-form">
        <div class="row plr-50">
            <div class="col-md-4 ">
                <div class="form-group">
                    <input name="log_id" type="hidden" value="{{ $log->id }}" id='dataSource'>
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.user_type') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->user_type }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.equipment_type') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->details->equipment_type }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.length') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->details->length }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability.table.check_in_date') . ': ')->class('col-md-6 font-bold')->for('check_in_date') }}</b>
                    {{ date(Config::get('access.date_fromat'), strtotime($log->check_in_date)) }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.booking_type') . ': ')->class('col-md-6 font-bold')->for('booking_type') }}</b>
                    {{ ($log->booking_type == 2) ? 'Booking Automatically' : 'Notify' }}
                </div>
                <!--form-group-->
                <hr>
            </div>
            <div class="col-md-4 ">
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.towing_vehicle') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->details->towing_vehicle }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.towing_length') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->details->towing_length }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.slide_out') . ': ')->class('col-md-6 font-bold')->for('campground_title') }}</b>
                    {{ $log->details->slide_out }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability.table.check_out_date') . ': ')->class('col-md-6 font-bold')->for('check_out_date') }}</b>
                    {{ date(Config::get('access.date_fromat'), strtotime($log->check_out_date)) }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.duration') . ': ')->class('col-md-6 font-bold')->for('duration') }}</b>
                    {{ str_replace('_',' ',$log->duration) }}
                </div>
                <!--form-group-->
                <hr>
            </div>
            <div class="col-md-4 ">
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.bookingenquiry.table.adults') . ': ')->class('col-md-6 font-bold')->for('adults') }}</b>
                    {{ $log->details->adults }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.pet_friendly') . ': ')->class('col-md-6 font-bold')->for('pet_friendly') }}</b>
                    {{ $log->details->pet_friendly }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.pull_through') . ': ')->class('col-md-6 font-bold')->for('pull_through') }}</b>
                    {{ $log->details->pull_through }}
                </div>
                <!--form-group-->
                <hr>
                <div class="form-group">
                    <b>{{ html()->label(__('labels.backend.access.check-availability-notification.table.no_of_days') . ': ')->class('col-md-6 font-bold')->for('no_of_days') }}</b>
                    {{ $log->no_of_days }}
                </div>
                <!--form-group-->
                <hr>
            </div>
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table data-table" id="checkavailability_notification_list" style="width:100%;">
                        <thead>
                            <tr>
<!--                                <th>@lang('labels.backend.access.check-availability-notification.table.status')
                                </th>-->
                                <th>@lang('labels.backend.access.check-availability-notification.table.log_time')
                                </th>
                                <th>@lang('labels.backend.access.check-availability-notification.table.availability')
                                </th>
                                <th>@lang('labels.backend.access.check-availability-notification.table.message')
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd">
                                <td valign="top" colspan="8" class="dataTables_empty">
                                    @lang('labels.general.no_records_found')</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!--row-->
    </div>

    <div class="card-footer clearfix">

    </div>
    <!--card-footer-->
</div>
<!--card-->
{{ html()->closeModelForm() }}
@endsection


@push('after-scripts')
<script>
    var list_id = 'checkavailability_notification_list';
    var list_url = '{{ route("admin.check-availability-notification.log") }}';
    var order = [[1, 'desc']];
    var columns = [
//        {
//            data: 'status'
//        },
        {
            data: 'log_time'
        },
        {
            data: 'result'
        },
        {
            data: 'message'
        }
    ];

    var filterCustomData = true;

</script>
{!! script('js/custom_datatable.js') !!}
@endpush
