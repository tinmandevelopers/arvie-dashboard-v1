@extends('backend.layouts.app')

@section('title', __('labels.backend.access.check-availability.management') . ' | ' .
    __('labels.backend.access.check-availability.create'))

@section('breadcrumb-links')
@endsection

@section('content')
    <div id="overlay" style="display:none;">
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="availablity_result">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Check Availablity Result</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="" id="enquiry_result">
                    </div>
                    <span id="detail">

                    </span>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{-- {{ html()->form()->id('check_availability_form')->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }} --}}
    <form id="check_availability_form" name="check_availability_form">
        <div class="card">
            <div class="card-header clearfix">
                <div class="row">
                    <div class="col-4">
                        <h4 class="card-title">
                            @lang('labels.backend.access.check-availability.create')
                        </h4>
                    </div>
                    <!--col-->
                </div>
                <!--row-->
            </div>
            <!--card-footer-->

            <div class="card-body data-source-form">
                <div class="row plr-50">
                    <div class="col-md-12 ">
                        <div class="row">

                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability.campground_id') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('title') }}

                                <div class="col-md-12">
                                    {{ html()->select('campground_id')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability.campground_id'))->id('campground_search')->attribute('maxlength', 191) }}
                                </div>
                                <!--col-->
                            </div>
                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability.location'))->class('col-md-12 form-control-label')->for('title') }}

                                <div class="col-md-12" id="location">

                                </div>
                                <!--col-->
                            </div>
                            {{-- <div class="form-group col-md-6 offset-md-6"></div> --}}
                            <!--form-group-->
                            <div class="form-group col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability.check_in_date') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('check_in_date') }}

                                <div class="col-md-12">
                                    {{ html()->date('check_in_date')->id('check_in_date')->class('form-control')->placeholder(__('validation.attributes.check-availability.check_in_date'))->required() }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->

                            <div class="form-group  col-md-6 ">
                                {{ html()->label(__('validation.attributes.backend.access.check-availability.check_out_date') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('check_out_date') }}

                                <div class="col-md-12">
                                    {{ html()->date('check_out_date')->id('check_out_date')->class('form-control')->placeholder(__('validation.attributes.check-availability.check_out_date'))->required() }}
                                </div>
                                <!--col-->
                            </div>
                            <!--form-group-->
                        </div>
                    </div>

                </div>
                <!--row-->
                <hr />
                <div class="row plr-50">
                    <div class="col-md-12">
                        <h5 class="card-sub-title">
                            @lang('labels.backend.access.check-availability.other_detail')
                        </h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.reservation_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('reservation_type') }}

                            <div class="col-md-12">
                                {{ html()->select('reservation_type', $params['reservation_type'])->class('form-control')->id('reservation_type') }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.adults') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('adults') }}

                            <div class="col-md-12">
                                {{ html()->number('adults')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability.adults'))->attribute('maxlength', 191) }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.pet_friendly') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('pet_friendly') }}

                            <div class="col-md-12 ">
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('Yes'))->class('form-check-label') }}
                                    {{ html()->radio('pet_friendly')->value('Yes')->checked(old('pet_friendly', 'Yes'))->class('form-check-input') }}
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('No'))->class('form-check-label') }}
                                    {{ html()->radio('pet_friendly')->value('No')->checked(old('pet_friendly', 'No'))->class('form-check-input') }}
                                </div>
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group ">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.towing_vehicle') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_vehicle') }}

                            <div class="col-md-12">
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('Yes'))->class('form-check-label') }}
                                    {{ html()->radio('towing_vehicle')->value('Yes')->checked(old('towing_vehicle', 'Yes'))->class('form-check-input') }}
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('No'))->class('form-check-label') }}
                                    {{ html()->radio('towing_vehicle')->value('No')->checked(old('towing_vehicle', 'No'))->class('form-check-input') }}
                                </div>

                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.slide_out') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('slide_out') }}

                            <div class="col-md-12">
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('Yes'))->class('form-check-label') }}
                                    {{ html()->radio('slide_out')->value('Yes')->checked(old('slide_out', 'Yes'))->class('form-check-input') }}
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    {{ html()->label(__('No'))->class('form-control-label') }}
                                    {{ html()->radio('slide_out')->value('No')->checked(old('slide_out', 'No'))->class('form-check-input') }}
                                </div>
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->



                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.equipment_type') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('equipment_type') }}

                            <div class="col-md-12">
                                {{ html()->select('equipment_type', $params['equipment_type'])->class('form-control equipment_type')->id('equipment_type')->attribute('maxlength', 191) }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('length') }}

                            <div class="col-md-12">
                                {{ html()->number('length', 0)->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability.length'))->id('equipment_length')->attribute('maxlength', 191) }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.pull_through'))->class('col-md-12 form-control-label')->for('pull_through') }}

                            <div class="col-md-12">
                                {{ html()->text('pull_through')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability.pull_through'))->attribute('maxlength', 50) }}
                            </div>
                            <!--col-->
                        </div>

                        <div class="form-group" id="towingblock" style="display:none;">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.towing_length') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('towing_length') }}

                            <div class="col-md-12">
                                {{ html()->number('towing_length')->class('form-control')->placeholder(__('validation.attributes.backend.access.check-availability.towing_length'))->attribute('maxlength', 191) }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->

                        <div class="form-group" id="rearblock" style="display:none;">
                            {{ html()->label(__('validation.attributes.backend.access.check-availability.rear') . '<sup>*</sup>')->class('col-md-12 form-control-label')->for('rear') }}

                            <div class="col-md-12">
                                {{ html()->select('rear', $params['rear'])->class('form-control')->id('rear') }}
                            </div>
                            <!--col-->
                        </div>
                        <!--form-group-->
                    </div>
                </div>

                <div class="card-footer clearfix">
                    <div class="row">
                        <div class="col">
                            {{ form_cancel(route('admin.check-availability.create'), __('buttons.general.cancel')) }}
                        </div>
                        <!--col-->

                        <div class="col text-right">
                            {{ html()->button(__('buttons.general.crud.submit'))->id('check_availability_form_submit')->class('btn btn-common pull-right') }}
                        </div>
                        <!--col-->
                    </div>
                    <!--row-->
                </div>
                <!--card-footer-->
            </div>
            <!--card-->
            {{-- {{ html()->form()->close() }} --}}
    </form>

@endsection

@push('before-styles')
    {{ style('css/select2.min.css') }}
@endpush

@push('after-scripts')


    {!! script('js/select2.min.js') !!}
    
    @php
        $path = storage_path('booking/bookingCheckAvailabilityFormData.json');
        $str = file_get_contents($path);
        $json = json_decode($str, true);
    @endphp

    <script>
        var userID = "{{ Auth::user()->id }}";
        var baseURL = "{{ URL('/') }}";
        var formDataJson = {!! json_encode($json) !!};
    </script>

    {!! script('js/checkAvailability.js') !!}
@endpush
