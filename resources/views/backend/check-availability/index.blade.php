@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.check-availability.log_all'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute w-70">
                    {{ __('labels.backend.access.check-availability.log_all') }}
                </h4>
                <div class="table-responsive">
                    <table class="table data-table" id="checkavailability_list" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.check-availability.table.campground_title')</th>
                                <th>@lang('labels.backend.access.check-availability.table.enquiry_from')</th>
                                <th>@lang('labels.backend.access.check-availability.table.check_in_date')</th>
                                <th>@lang('labels.backend.access.check-availability.table.check_out_date')</th>
                                <th>@lang('labels.backend.access.check-availability.table.created_at')</th>
                                <th>@lang('labels.backend.access.check-availability.table.availability')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'checkavailability_list';
    var list_url = '{{ route("admin.check-availability-log.list") }}';
    var order = [[4, 'desc']];
    var columns = [
        {data: 'campground_id'},
        {data: 'enquiry_from'},
        {data: 'check_in_date'},
        {data: 'check_out_date'},
        {data: 'created_at'},
        {data: 'result'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];
    
    var filterCustomData = true;
</script>
{!! script('js/custom_datatable.js') !!}
@endpush
