@extends('backend.layouts.app')

@section('title', __('labels.backend.access.check-availability.management') . ' | ' .
    __('labels.backend.access.check-availability.view'))

@section('breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-header clearfix">
            <div class="row">
                <div class="col-4">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.check-availability.view')
                    </h4>
                </div>
                <!--col-->
            </div>
            <!--row-->
        </div>
        <!--card-footer-->

        <div class="card-body enquiry-form">
            <div class="row plr-50">
                <div class="col-md-6 ">
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.campground_title') . ': ')->class('col-md-4 font-bold')->for('campground_title') }}</b>
                        {{ $log->campground->title }}
                    </div>
                    <!--form-group-->
                    <hr>
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.check_in_date') . ': ')->class('col-md-4 font-bold')->for('check_in_date') }}
                        </b>
                        {{ date(Config('access.date_fromat'), strtotime($log->check_in_date)) }}
                    </div>
                    <!--form-group-->
                    <hr>
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.check_out_date') . ': ')->class('col-md-4 font-bold')->for('check_out_date') }}
                        </b>
                        {{ date(Config('access.date_fromat'), strtotime($log->check_out_date)) }}
                    </div>
                    <!--form-group-->
                    <hr>
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.enquiry_from') . ': ')->class('col-md-4 font-bold')->for('enquiry_from') }}
                        </b>
                        {{ $log->enquiry_from }}
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.availability') . ': ')->class('col-md-4 font-bold')->for('enquiry_from') }}
                        </b>
                        {{ $log->result['availability'] ? 'Yes':'No' }}
                    </div>
                    <hr>
                    <div class="form-group">
                        <b>{{ html()->label(__('labels.backend.access.check-availability.table.message') . ': ')->class('col-md-4 font-bold')->for('enquiry_from') }}
                        </b>
                        {{ $log->result['message'] }}
                    </div>
                  
                    <!--form-group-->
                    <hr>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        @php
                            echo prepare_html((array) json_decode($log->details), 'booking');
                        @endphp
                    </div>
                </div>
                <!--row-->
            </div>

            <div class="card-footer clearfix">

            </div>
            <!--card-footer-->
        </div>
        <!--card-->
        {{ html()->closeModelForm() }}
    @endsection
