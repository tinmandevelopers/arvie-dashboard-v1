@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="container">
    @foreach($data as $key => $value)
    <div class="col-sm-12">
        
            <h4 class="title">{!! $value->title !!}</h4>
       
        <p class="text-muted">
            <strong>URL :</strong> {!! $value->link !!}
            <br/>
            <?php
            echo '<pre>';
            ?>
            <strong>Amenities :</strong> {!! json_encode($value->amenities) !!}
        </p>
    </div>
    @endforeach
</div>
@endsection
