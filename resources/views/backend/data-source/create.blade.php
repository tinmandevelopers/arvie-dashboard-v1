@extends('backend.layouts.app')

@section('title', __('labels.backend.access.data-source.management') . ' | ' . __('labels.backend.access.data-source.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.auth.data-source.store'))->id('data_source_form')->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }}
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title">
                    @lang('labels.backend.access.data-source.create')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->


    <div class="card-body data-source-form">
        <div class="row plr-50">
            <div class="col-md-6 ">
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.title')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('title') }}

                    <div class="col-md-12">
                        {{ html()->text('title')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.title'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.url')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('url') }}

                    <div class="col-md-12">
                        {{ html()->text('url')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.url_sample'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_name') }}

                    <div class="col-md-12">
                        {{ html()->text('crawler_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.crawler_name'))
                                    ->attribute('maxlength', 50)}}
                    </div><!--col-->
                </div><!--form-group-->
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_file')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_file') }}

                    <div class="col-md-12">
                        <div class="custom-file">
                            {{ html()->file('crawler_file')->class('form-control-file custom-file-input')->required() }}
                            {{ html()->label(__('validation.attributes.backend.access.data-source.choose_file'))->class('col-md-12 custom-file-label')->for('crawler_file') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.booking_file')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('booking_file') }}

                    <div class="col-md-12">
                        <div class="custom-file">
                            {{ html()->file('booking_file')->class('form-control-file custom-file-input')->required() }}
                            {{ html()->label(__('validation.attributes.backend.access.data-source.choose_file'))->class('col-md-12 custom-file-label')->for('booking_file') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_time')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_time') }}

                    <div class="col-md-12">
                        {{ html()->text('crawler_time')
                                    ->class('form-control')
                                    ->id('crawler_time')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.crawler_time_sample'))
                                    ->attribute('maxlength', 50)}}
                        <span id="crawler_readable_time"> </span>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
        </div><!--row-->
    </div>

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.auth.data-source.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.submit')) }}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->form()->close() }}
@endsection

@push('after-scripts')
{!! script('js/cronstrue.min.js') !!}
{!! script('js/datasources.js') !!}
@endpush
