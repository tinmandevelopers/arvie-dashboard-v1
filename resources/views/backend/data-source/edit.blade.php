@extends('backend.layouts.app')

@section('title', __('labels.backend.access.data-source.management') . ' | ' . __('labels.backend.access.data-source.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($dataSourceRequest, 'PATCH', route('admin.auth.data-source.update', $dataSourceRequest['id']))->id('data_source_form')->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }}
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.data-source.edit')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body data-source-form">
        <div class="row plr-50">
            <div class="col-md-6 ">
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.title')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('title') }}

                    <div class="col-md-12">
                        {{ html()->text('title')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.title'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.url')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('url') }}

                    <div class="col-md-12">
                        {{ html()->text('url')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.url_sample'))
                                    ->attribute('maxlength', 191)}}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_name') }}

                    <div class="col-md-12">
                        {{ html()->text('crawler_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.crawler_name'))
                                    ->attribute('maxlength', 50)}}
                    </div><!--col-->
                </div><!--form-group-->
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_file')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_file') }}

                    <div class="col-md-12">
                        @php($hide = '')
                        @if ($dataSourceRequest['crawler_file'] != '' && file_exists(public_path() . '/storage/' . $dataSourceRequest['crawler_file']))
                            <div class="image-preview form-control border-0">
                                @php($hide = 'hidden')
                                {{ html()->a(url('/storage/' . $dataSourceRequest['crawler_file']),__('buttons.general.crud.download') )->class('btn btn-primary btn-sm')->attribute('download') }}
                                {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                            </div>
                        @endif
                        <div class="custom-file {{$hide}}">
                            {{ html()->file('crawler_file')->class('form-control-file custom-file-input') }}
                            {{ html()->label(__('validation.attributes.backend.access.data-source.choose_file'))->class('col-md-12 custom-file-label')->for('crawler_file') }}
                            {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.booking_file')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('booking_file') }}

                    <div class="col-md-12">
                        @php($hide = '')
                        @if ($dataSourceRequest['booking_file'] != '' && file_exists(public_path() . '/storage/' . $dataSourceRequest['booking_file']))
                            <div class="image-preview form-control border-0">
                                @php($hide = 'hidden')
                                {{ html()->a(url('/storage/' . $dataSourceRequest['booking_file']), __('buttons.general.crud.download') )->class('btn btn-primary btn-sm')->attribute('download') }}
                                {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                            </div>
                        @endif
                        <div class="custom-file {{$hide}}">
                            {{ html()->file('booking_file')->class('form-control-file custom-file-input') }}
                            {{ html()->label(__('validation.attributes.backend.access.data-source.choose_file'))->class('col-md-12 custom-file-label')->for('booking_file') }}
                            {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                        </div>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ html()->label(__('validation.attributes.backend.access.data-source.crawler_time')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('crawler_time') }}

                    <div class="col-md-12">
                        {{ html()->text('crawler_time')
                                    ->class('form-control')
                                    ->id('crawler_time')
                                    ->placeholder(__('validation.attributes.backend.access.data-source.crawler_time_sample'))
                                    ->attribute('maxlength', 50)}}
                        <span id="crawler_readable_time"> </span>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
        </div><!--row-->
    </div>


    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.auth.data-source.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.submit')) }}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
{!! script('js/cronstrue.min.js') !!}
{!! script('js/datasources.js') !!}
@endpush
