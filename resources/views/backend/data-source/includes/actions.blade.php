<ul class="navbar-nav ml-auto mr-sm-3">
    <li class="dropdown">
        <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" class="text-center">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right mt-sm-2 mr-n3">
<!--            <span class="dropdown-item" id="start_crawler_{{$dataSource->id}}">
                <a data-toggle="modal" data-placement="top" title="@lang('buttons.general.crud.start_crawler')" class="start_crawler" data-id="{{$dataSource->id}}" >
                    <i class="fas fa-hourglass-start"></i> @lang('buttons.general.crud.start_crawler')
                </a>
            </span>-->
            <a href="{{ route('admin.auth.data-source.log',$dataSource) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.log')" class="dropdown-item">
                <i class="fas fa-eye"></i> @lang('buttons.general.log')
            </a>
            <a href="{{ route('admin.auth.data-source.edit', $dataSource) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.edit')" class="dropdown-item">
                <i class="fas fa-edit"></i> @lang('buttons.general.crud.edit')
            </a>
            <a href="{{ route('admin.auth.data-source.destroy', $dataSource) }}"
               data-method="delete"
               class="dropdown-item"
               data-trans-button-cancel="@lang('buttons.general.cancel')"
               data-trans-button-confirm="@lang('buttons.general.crud.delete')"
               data-trans-title="@lang('strings.backend.general.are_you_sure')"
               id="datasource_delete"
               >
                <i class="fas fa-trash"></i>  @lang('buttons.general.crud.delete')
            </a>

        </div>

    </li>
</ul>
