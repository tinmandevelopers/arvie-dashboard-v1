@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.data-source.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute">
                    {{ __('labels.backend.access.data-source.all') }}
                    @if ($logged_in_user->isAdmin())
                        <a href="{{ route('admin.auth.data-source.create') }}" class="btn ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')">@lang('labels.general.create_new')</a>
                    @endif
                </h4>

                <div class="table-responsive">
                    <table class="table data-table" id="data_source_list" style="width:100%;">
                        <thead>
                        <tr>
                                <th>@lang('labels.backend.access.data-source.table.title')</th>
                                <th>@lang('labels.backend.access.data-source.table.last_crawled')</th>
                                <th>@lang('labels.backend.access.data-source.table.crawler_log')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
<div class="console_model"></div>
@endsection

@push('after-scripts')
<script>
    $(document).on('click', '.start_crawler', function() {
        var dataSourceid=$(this).data('id');

        var postURL = "<?php echo url('admin/auth/data-source/'); ?>";
        $('.console_model').append('<div class="modal" tabindex="-1" role="dialog" id="crawler_console_'+dataSourceid+'">\n' +
            '            <div class="modal-dialog" role="document">\n' +
            '            <div class="modal-content">\n' +
            '            <div class="modal-header">\n' +
            '            <h5 class="modal-title">Crawler Output</h5>\n' +
            '        <button type="button" class="close close_crawler_console" onclick="closemodel('+dataSourceid+')"  data-dismiss="modal" aria-label="Close" >\n' +
            '            <span aria-hidden="true">&times;</span>\n' +
            '        </button>\n' +
            '        </div>\n' +
            '        <div class="modal-body"><iframe src="'+postURL+'/'+dataSourceid+'/crawler"  width="450" height="200"></iframe>\n' +
            '            </div>\n' +
            '            <div class="modal-footer">\n' +
            '                <button type="button" class="btn btn-secondary" onclick="closemodel('+dataSourceid+')" data-dismiss="modal">Close</button>\n' +
            '            </div>\n' +
            '            </div>\n' +
            '            </div>\n' +
            '            </div>');

         $('#crawler_console_'+dataSourceid).show();

         $('#start_crawler_'+dataSourceid).html('<a data-toggle="modal" onclick="openmodel('+dataSourceid+')" data-placement="top" title="@lang("buttons.general.crud.running_crawler")" class="" >\n' +
            '            <i class="fas fa-hourglass-start"></i> @lang("buttons.general.crud.running_crawler") \n' +
            '            </a>');

    });
    function openmodel(dataSourceid){
        $('#crawler_console_'+dataSourceid).show();
    }
    function closemodel(dataSourceid){
        $('#crawler_console_'+dataSourceid).hide();
    }

    var list_id = 'data_source_list';
    var list_url = '{{ route("admin.auth.data-source.list") }}';
    var order = [];
    var columns = [
        {data: 'title'},
        {data: 'last_crawled'},
        {data: 'crawler_log'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];

</script>
{!! script('js/custom_datatable.js') !!}
@endpush
