@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.data-source.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute">
                    {{ __('labels.backend.access.data-source.log_all') }}
                </h4>

                <div class="table-responsive">
                    <table class="table data-table" id="data_source_log_view" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.data-source.log_table.log_detail')</th>
                                <th>@lang('labels.backend.access.data-source.log_table.created_at')</th>
                                <th>@lang('labels.backend.access.data-source.log_table.updated_at')</th>
                                <th>@lang('labels.backend.access.data-source.log_table.status')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'data_source_log_view';
    var list_url = '{{ route('admin.auth.data-source.log-show',$dataSourceRequest['id'])}}';
    var order = [[1, 'desc']];
    var columns = [
        {data: 'log_file_path'},
        {data: 'created_at'},
        {data: 'updated_at'},
        {data: 'status'},
    ];
</script>
{!! script('js/custom_datatable.js') !!}
@endpush
