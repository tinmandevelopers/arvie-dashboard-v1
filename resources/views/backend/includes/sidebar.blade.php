<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link {{active_class(Route::is('admin/dashboard'))}}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/user/*'))}}" href="{{ route('admin.auth.user.index') }}">
                    <i class="nav-icon fas fa-users"></i>
                    @lang('labels.backend.access.users.management')
                </a>
            </li>
            @if ($logged_in_user->isAdmin())
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/profile-request*'))}}" href="{{ route('admin.auth.profile-request.index') }}">
                    <i class="nav-icon fas fa-user-check"></i>
                    @lang('labels.backend.access.profile-request.management')

                    @if ($pending_approval > 0)
                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
                </a>
            </li>
            @endif
            @if ($logged_in_user->isAdmin())
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/data-source*'))}}" href="{{ route('admin.auth.data-source.index') }}">
                    <i class="nav-icon fas fa-database"></i>
                    @lang('labels.backend.access.data-source.management')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/campground*'))}}" href="{{ route('admin.campground.index') }}">
                    <i class="nav-icon fas fa-campground"></i>
                    @lang('labels.backend.access.campground.management')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/campground*'))}}" href="{{ route('admin.campground.compare') }}">
                    <i class="nav-icon fas fa-bars"></i>
                    @lang('labels.backend.access.campground.compare')
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/booking/enquiry*'))}}" href="{{ route('admin.booking.enquiry') }}">
                    <i class="nav-icon fas fa-book"></i>
                    @lang('labels.backend.access.bookingenquiry.management')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/check-availability*'))}}" href="{{ route('admin.check-availability.create') }}">
                    <i class="nav-icon fas fa-check"></i>
                    @lang('labels.backend.access.check-availability.management')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/check-availability-log*'))}}" href="{{ route('admin.check-availability-log.index') }}">
                    <i class="nav-icon fas fa-history "></i>
                    @lang('labels.backend.access.check-availability.log_all')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(request()->is('admin/auth/check-availability-notification*'))}}" href="{{ route('admin.check-availability-notification') }}">
                    <i class="nav-icon fas fa-book"></i>
                    @lang('labels.backend.access.check-availability-notification.management')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('reports') }}">
                    <i class="nav-icon fas fa-book"></i>
                    @lang('labels.backend.access.reports')
                </a>
            </li>
            <li class="divider"></li>
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
