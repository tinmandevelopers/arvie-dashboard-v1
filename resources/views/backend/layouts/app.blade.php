<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="32400;url={{ route('frontend.auth.logout') }}" />
    <title>@yield('title', app_name())</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="512x512" href="{{ asset('img/favicon/android-icon-512x512.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">
    <link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}" />
    @yield('meta')

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap"
        rel="stylesheet">
    @stack('before-styles')
    {{ style(mix('css/backend.css')) }}

    {!! style('plugins/datatable/dataTables.bootstrap4.min.css') !!}

    {{ style('css/common.css') }}

    @stack('after-styles')
</head>

{{-- * Arvie BODY options, add following classes to body to change options
     * // Header options
     * 1. '.header-fixed'		- Fixed Header
     *
     * // Sidebar options
     * 1. '.sidebar-fixed'		- Fixed Sidebar
     * 2. '.sidebar-hidden'		- Hidden Sidebar
     * 3. '.sidebar-off-canvas'		- Off Canvas Sidebar
     * 4. '.sidebar-minimized'		- Minimized Sidebar (Only icons)
     * 5. '.sidebar-compact'		- Compact Sidebar
     *
     * // Aside options
     * 1. '.aside-menu-fixed'		- Fixed Aside Menu
     * 2. ''			        - Hidden Aside Menu
     * 3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu
     *
     * // Breadcrumb options
     * 1. '.breadcrumb-fixed'		- Fixed Breadcrumb
     *
     * // Footer options
     * 1. '.footer-fixed'		- Fixed footer --}}

<body class="app header-fixed sidebar-fixed aside-menu-off-canvas sidebar-lg-show">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.read-only')
            {!! Breadcrumbs::render() !!}

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div>
                    <!--content-header-->

                    @include('includes.partials.messages')
                    @yield('content')
                </div>
                <!--animated-->
            </div>
            <!--container-fluid-->
        </main>
        <!--main-->

        @include('backend.includes.aside')
    </div>
    <!--app-body-->

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/backend.js')) !!}
    {!! script('js/jquery.validate.min.js') !!}
    {!! script('js/plugins.js') !!}
    {!! script('plugins/datatable/jquery.dataTables.min.js') !!}
    {!! script('plugins/datatable/dataTables.bootstrap4.min.js') !!}
    {!! script('js/jquery.validate.min.js') !!}
    @stack('after-scripts')
</body>

</html>
