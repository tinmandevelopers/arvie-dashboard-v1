@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($profileRequest, 'PATCH', route('admin.auth.profile-request.update', $profileRequest['id']))->class('form-horizontal')->open() }}
<div class="card">
    <div class="card-header clearfix">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.profile-request.view')
                </h4>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->

    <div class="card-body user-form">
        <div class="row plr-50">
            <div class="col-md-12">
                <table class="table data-table" id="profile_request_list" style="width:100%;">
                    <thead>
                        <tr>
                            <th width="20%">Field</th>
                            <th width="40%">Current</th>
                            <th width="40%">Change</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($profileRequest as $key => $data)
                        @if(!in_array($key, $notDisplay))
                        <tr class="{{ $user[$key] != $data ? 'text-danger' : ''}}">
                            <td>{{ html()->label(__('validation.attributes.backend.access.users.'.str_replace(['kin_','medical_'],'',$key)))->class('col-md-12 form-control-label') }}</td>
                            <td>
                                @if($key == 'avatar_location' && $user['avatar_location'] != '' && file_exists(public_path() . '/storage/' . $user['avatar_location']) || $key == 'medical_membership_card' && $user['medical_membership_card'] != '' && file_exists(public_path() . '/storage/' . $user['medical_membership_card']))
                                <div class="image-preview">{{ html()->img(url('/storage/' . $user[$key]))->class("picture") }}</div>
                                @else
                                {{ $user[$key] }}
                                @endif
                            </td>
                            <td>
                                @if($key == 'avatar_location' && $data != '' && file_exists(public_path() . '/storage/' . $data) || $key == 'medical_membership_card' && $data != '' && file_exists(public_path() . '/storage/' . $data))
                                <div class="image-preview">{{ html()->img(url('/storage/' . $data))->class("picture") }}</div>
                                @else
                                {{ $data }}
                                @endif
                            </td>
                        </tr>
                        @endif
                        @empty
                    <td colspan="3">No Request detail</td>
                    @endforelse
                    </tbody>
                    @if(count($profileRequest) > 0)
                    <tfoot>
                        <tr>
                            <td>{{ html()->label(__('validation.attributes.backend.access.users.status'))->class('col-md-12 form-control-label') }}</td>
                            <td colspan="2">{{html()->select('status', ['Approved'=>'Approve', 'Rejected'=>'Reject'])->class('form-control')}}</td>
                        </tr>
                        <tr style="display: none">
                            <td>{{ html()->label(__('validation.attributes.backend.access.users.reason'))->class('col-md-12 form-control-label') }}</td>
                            <td colspan="2">
                                {{ html()->textarea('reason')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.reason'))
                                    ->attribute('rows', 5)}}</td>
                        </tr>
                    </tfoot>
                    @endif
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.auth.profile-request.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.submit')) }}
            </div><!--row-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
<script>
    $(document).ready(function () {
        $('#status').change(function () {
            if ($(this).val().toLowerCase() == 'rejected') {
                $('#reason').parents('tr').show();
            } else {
                $('#reason').parents('tr').hide();
            }
        })
    })
</script>
@endpush