<ul class="navbar-nav ml-auto mr-sm-3">
    <li class="dropdown">
        <a data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" class="text-center">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right mt-sm-2 mr-n3">
            <a href="{{ route('admin.auth.profile-request.edit', $user) }}" data-toggle="tooltip" data-placement="top" title="@lang('buttons.general.crud.view')" class="dropdown-item">
                <i class="fas fa-eye"></i> @lang('buttons.general.crud.view')
            </a>
        </div>
    </li>
</ul>