@if($user->isActive())
    <span class="button green-button">@lang('labels.general.active')</span>
@else
    <span class="button red-button">@lang('labels.general.inactive')</span>
@endif
