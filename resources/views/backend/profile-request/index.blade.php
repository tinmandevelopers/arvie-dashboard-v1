@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.profile-request.management'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0 position-absolute">
                    {{ __('labels.backend.access.profile-request.all') }}
                </h4>
            
                <div class="table-responsive">
                    <table class="table data-table" id="profile_request_list" style="width:100%;">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.profile-request.table.agent_id')</th>
                                <th>@lang('labels.backend.access.profile-request.table.first_name')</th>
                                <th>@lang('labels.backend.access.profile-request.table.email')</th>
                                <th>@lang('labels.backend.access.profile-request.table.request_date')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">@lang('labels.general.no_records_found')</td></tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
    var list_id = 'profile_request_list';
    var list_url = '{{ route("admin.auth.profile-request.list") }}';
    var order = [[3, 'desc']];
    var columns = [
        {data: 'id'},
        {data: 'first_name'},
        {data: 'email'},
        {data: 'created_at'},
        {data: 'actions', name: 'actions', class: 'action-btn', orderable: false}
    ];
</script>
{!! script('js/custom_datatable.js') !!}
@endpush