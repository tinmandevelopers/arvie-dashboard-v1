{{ html()->modelForm($user, 'POST', route('admin.auth.user.update', $user->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
@method('PATCH')

<div class="card-body user-form">
    <div class="row plr-50">
        <div class="col-md-12">
            <h5 class="card-sub-title">
                @lang('labels.backend.access.users.personal_details')
            </h5>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                {{ html()->hidden('form_name', 'update_profile_form')}}
                {{ html()->hidden('user_id', $user->id)}}

                {{ html()->label(__('validation.attributes.backend.access.users.first_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('first_name') }}

                <div class="col-md-12">
                    {{ html()->text('first_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->class('col-md-12 form-control-label')->for('last_name') }}

                <div class="col-md-12">
                    {{ html()->text('last_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.screen_name'))->class('col-md-12 form-control-label')->for('screen_name') }}

                <div class="col-md-12">
                    {{ html()->text('screen_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.screen_name'))
                                    ->attribute('maxlength', 191)}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.arvie_email')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('email') }}

              <div class="col-md-12">
                  {{html()->label($user->email)->class('form-control')}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.alternative_email')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('email') }}

                <div class="col-md-12">
                    {{ html()->email('alternative_email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.alternative_email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.contact_number')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('contact_number') }}

                <div class="col-md-12">
                    {{ html()->text('contact_number')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.contact_number'))
                                    ->attribute('maxlength', 10)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->
        </div><!--col-->

        <div class="col-md-6">
            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.address')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('address') }}

                <div class="col-md-12">
                    {{ html()->textarea('address')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.address'))
                                    ->attribute('rows', 5)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.dob')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('dob') }}

                <div class="col-md-12">
                    {{html()->label($user->dob)->class('form-control')}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.id_number')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('id_number') }}

                <div class="col-md-12">
                    {{html()->label($user->id_number)->class('form-control')}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.id_proof')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('id_proof') }}

                <div class="col-md-12">
                    @php($hide = '')
                    @if ($user->id_proof != '' && file_exists(public_path() . '/storage/' . $user->id_proof))
                    <div class="image-preview">
                        @php($hide = 'hidden')
                        {{ html()->img(url('/storage/' . $user->id_proof))->class("picture") }}
                    </div>
                    @else
                    <span>
                    No ID Proof</span>
                    @endif
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.income_tax_number')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('income_tax_number') }}

                <div class="col-md-12">
                    {{html()->label($user->income_tax_number)->class('form-control')}}
                </div><!--col-->
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
    <hr/>
    <div class="row plr-50">
        <div class="col-md-12">
            <h5 class="card-sub-title">
                @lang('labels.backend.access.users.personal_details_next_kin')
            </h5>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.first_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_first_name') }}

                <div class="col-md-12">
                    {{ html()->text('kin_first_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.last_name')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_last_name') }}

                <div class="col-md-12">
                    {{ html()->text('kin_last_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.contact_number')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_contact') }}

                <div class="col-md-12">
                    {{ html()->text('kin_contact')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.contact_number'))
                                    ->attribute('maxlength', 10)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.email')."<sup>*</sup>")->class('col-md-12 form-control-label')->for('kin_email') }}

                <div class="col-md-12">
                    {{ html()->email('kin_email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.membership_number'))->class('col-md-12 form-control-label')->for('medical_membership_number') }}

                <div class="col-md-12">
                    {{ html()->text('medical_membership_number')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.membership_number'))
                                    ->attribute('maxlength', 50)}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.provider'))->class('col-md-12 form-control-label')->for('medical_provider') }}

                <div class="col-md-12">
                    {{ html()->text('medical_provider')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.provider'))
                                    ->attribute('maxlength', 191)}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.insurance_plan'))->class('col-md-12 form-control-label')->for('medical_insurance_plan') }}

                <div class="col-md-12">
                    {{ html()->text('medical_insurance_plan')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.insurance_plan'))
                                    ->attribute('maxlength', 191)}}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.membership_card'))->class('col-md-12 form-control-label')->for('medical_membership_card') }}

                <div class="col-md-12">
                    @php($hide = '')
                    @if ($user->medical_membership_card != '' && file_exists(public_path() . '/storage/' . $user->medical_membership_card))
                    <div class="image-preview">
                        @php($hide = 'hidden')
                        {{ html()->img(url('/storage/' . $user->medical_membership_card))->class("picture") }}
                        {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                    </div>
                    @endif
                    <div class="custom-file {{$hide}}">
                        {{ html()->file('medical_membership_card')->class('form-control-file custom-file-input') }}
                        {{ html()->label(__('validation.attributes.backend.access.users.choose_file'))->class('col-md-12 custom-file-label')->for('medical_membership_card') }}
                        {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                    </div>
                </div><!--col-->
            </div><!--form-group-->
        </div><!--col-->

        <div class="col-md-6">

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.avatar_location'))->class('col-md-12 form-control-label')->for('avatar_location') }}

                <div class="col-md-12">
                    @php($hide = '')
                    @if ($user->avatar_location != '' && file_exists(public_path() . '/storage/' . $user->avatar_location))
                    <div class="image-preview">
                        @php($hide = 'hidden')
                        {{ html()->img(url('/storage/' . $user->avatar_location))->class("picture") }}
                        {{ html()->a('javascript:void(0);', 'Change')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().next().removeClass("hidden");') }}
                    </div>
                    @endif
                    <div class="custom-file {{$hide}}">
                        {{ html()->file('avatar_location')->class('form-control-file custom-file-input')}}
                        {{ html()->label(__('validation.attributes.backend.access.users.choose_file'))->class('col-md-12 custom-file-label')->for('avatar_location') }}
                        {{ html()->a('javascript:void(0);', 'Cancel')->class('btn btn-danger btn-sm')->attribute('onclick', '$(this).parent().addClass("hidden");$(this).parent().prev().removeClass("hidden");') }}
                    </div>
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.about_me'))->class('col-md-12 form-control-label')->for('about_me') }}

                <div class="col-md-12">
                    {{ html()->textarea('about_me')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.about_me'))
                                    ->attribute('rows', 5) }}
                </div><!--col-->
            </div><!--form-group-->

            <div class="form-group">
                {{ html()->label(__('validation.attributes.backend.access.users.specialized_in'))->class('col-md-12 form-control-label')->for('specialized_in') }}

                <div class="col-md-12">
                    {{html()->label($user->specialized_in)->class('form-control')}}
                </div><!--col-->
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
</div><!--card-body-->

<div class="card-footer">
    <div class="row">
        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.submit')) }}
        </div><!--row-->
    </div><!--row-->
</div><!--card-footer-->

{{ html()->closeModelForm() }}

@push('after-scripts')
    {!! script('js/users.js') !!}
@endpush
