<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Auth\UserController;
use App\Http\Controllers\Api\BookingEnquiryController;
use App\Http\Controllers\Api\CheckAvailabilityNotificationController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::group(['namespace' => 'Api'], function () {

    Route::post('login', [UserController::class, 'login'])->name('login');

    Route::group(['prefix' => 'booking/enquiry/', 'middleware' => ['throttle:200,1']], function () {
        Route::post('initiate', [BookingEnquiryController::class, 'initiateBookingEnquiry'])->name('initiate');

        Route::post('initiate/detail', [BookingEnquiryController::class, 'initiateDetailBookingEnquiry'])->name('detail');
        
        Route::post('initiate/update', [BookingEnquiryController::class, 'initiateUpdateBookingEnquiry'])->name('update');
    
        Route::post('initiate/file/upload', [BookingEnquiryController::class, 'initiateFileUploadBookingEnquiry'])->name('fileupload');
    });

    Route::group(['prefix' => 'booking/enquiry/', 'middleware' => ['auth:api','throttle:200,1']], function () {

        Route::post('create', [BookingEnquiryController::class, 'createBookingEnquiry'])->name('create');

        Route::post('update', [BookingEnquiryController::class, 'updateBookingEnquiry'])->name('update');

        Route::post('delete', [BookingEnquiryController::class, 'deleteBookingEnquiry'])->name('delete');

        Route::post('detail', [BookingEnquiryController::class, 'detailBookingEnquiry'])->name('detail');

    });

    Route::group(['prefix' => 'booking/availability/'], function () {
        Route::post('check', [BookingEnquiryController::class, 'checkBookingAvailability'])->name('check.availability');
    });
    
    Route::group(['prefix' => 'check-availability-notification', 'middleware' => ['throttle:200,1']], function () {
        
        Route::post('/store', [CheckAvailabilityNotificationController::class, 'store'])->name('check-availability-notification.store');
        
        Route::post('/update', [CheckAvailabilityNotificationController::class, 'update'])->name('check-availability-notification.update');
    
    });

});
