<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\CampgroundController;
use App\Http\Controllers\Backend\BookingEnquiryController;
use App\Http\Controllers\Backend\CheckAvailabilityNotificationController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('data-scrapping', [DashboardController::class, 'dataScrapping'])->name('data-scrapping');
Route::get('booking', [DashboardController::class, 'booking'])->name('booking');
Route::get('check-availability', [DashboardController::class, 'checkAvailablity'])->name('check-availability');
// Route::get('check-availability-notification', [CheckAvailabilityNotificationController::class, 'checkAvailablityNotification'])->name('check-availability-notification');

// Campground
Route::group(['middleware' => 'role:' . config('access.users.admin_role')], function () {
    Route::get('campground', [CampgroundController::class, 'index'])->name('campground.index');
    Route::post('campground-list', [CampgroundController::class, 'getCampgroundList'])->name('campground.list');
    Route::get('compare', [CampgroundController::class, 'compare'])->name('campground.compare');
    Route::get('actcamp/{id}', [CampgroundController::class, 'actcamp'])->name('campground.actcamp');
    Route::get('deactcamp', [CampgroundController::class, 'deactcamp'])->name('campground.deactcamp');
    Route::get('campground-search', [CampgroundController::class, 'search'])->name('campground.search');
    Route::get('equipement-list', [CampgroundController::class, 'equipementList'])->name('campground.equipement');

    Route::group(['prefix' => 'campground/{campground}'], function () {
        Route::get('view', [CampgroundController::class, 'show'])->name('campground.view');
        Route::get('edit', [CampgroundController::class, 'edit'])->name('campground.edit');
        Route::patch('/', [CampgroundController::class, 'update'])->name('campground.update');
        Route::delete('/', [CampgroundController::class, 'destroy'])->name('campground.destroy');
        Route::get('log', [CampgroundController::class, 'showLog'])->name('campground.log');
        Route::get('log-view', [CampgroundController::class, 'showLogList'])->name('campground.log-show');
        Route::get('booking', [CampgroundController::class, 'booking'])->name('campground.booking');
    });
});

// Booking Enquiry

Route::group(['prefix' => 'booking/enquiry'], function () {

    Route::get('/', [BookingEnquiryController::class, 'index'])->name('booking.enquiry');
    Route::post('/list', [BookingEnquiryController::class, 'getBookingEnquiryList'])->name('booking.enquiry.list');

    Route::group(['prefix' => '{enquiry}'], function () {
        Route::get('booking', [BookingEnquiryController::class, 'booking'])->name('booking.enquiry.booking');
        Route::get('view', [BookingEnquiryController::class, 'show'])->name('booking.enquiry.view');
    });
});

Route::group(['middleware' => 'role:' . config('access.users.admin_role')], function () {
    Route::group(['prefix' => 'check-availability'], function () {
        Route::get('/', [BookingEnquiryController::class, 'checkAvailabilityCreate'])->name('check-availability.create');
    });
});

Route::group(['middleware' => 'role:' . config('access.users.admin_role')], function () {
    Route::get('check-availability-log', [BookingEnquiryController::class, 'checkAvailabilityLogIndex'])->name('check-availability-log.index');
    Route::post('check-availability-log-list', [BookingEnquiryController::class, 'getcheckAvailabilityLogList'])->name('check-availability-log.list');
    Route::group(['prefix' => 'check-availability-log/{log}'], function () {
        Route::get('view', [BookingEnquiryController::class, 'showCheckAvailabilityLog'])->name('check-availability-log.view');
    });
});


Route::group(['prefix' => 'check-availability-notification'], function () {

    Route::get('/', [CheckAvailabilityNotificationController::class, 'index'])->name('check-availability-notification');
    Route::post('/list', [CheckAvailabilityNotificationController::class, 'getcheckAvailabilityNotificationList'])->name('check-availability-notification.list');
    Route::get('/create', [CheckAvailabilityNotificationController::class, 'create'])->name('check-availability-notification.create');

    Route::post('log', [CheckAvailabilityNotificationController::class, 'getLog'])->name('check-availability-notification.log');
    Route::get('export/', [CheckAvailabilityNotificationController::class, 'export'])->name('check-availability-notification.export');
    Route::group(['prefix' => '{log}'], function () {
        Route::delete('/', [CheckAvailabilityNotificationController::class, 'destroy'])->name('check-availability-notification.destroy');
        Route::get('view', [CheckAvailabilityNotificationController::class, 'show'])->name('check-availability-notification.view');
    });
});