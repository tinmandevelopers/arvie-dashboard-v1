<?php

use App\Http\Controllers\Backend\Auth\Role\RoleController;
use App\Http\Controllers\Backend\Auth\User\UserConfirmationController;
use App\Http\Controllers\Backend\Auth\User\UserController;
use App\Http\Controllers\Backend\Auth\User\UserPasswordController;
use App\Http\Controllers\Backend\Auth\User\UserSessionController;
use App\Http\Controllers\Backend\Auth\User\UserSocialController;
use App\Http\Controllers\Backend\Auth\User\UserStatusController;
use App\Http\Controllers\Backend\Auth\User\AccountController;
use App\Http\Controllers\Backend\Auth\User\ProfileController;
use App\Http\Controllers\Backend\Auth\User\UpdatePasswordController;
use App\Http\Controllers\Backend\Auth\User\ProfileRequestController;
use App\Http\Controllers\Backend\Auth\User\DataSourceController;

// All route names are prefixed with 'admin.auth'.
Route::group([
    'prefix' => 'auth',
    'as' => 'auth.',
    'namespace' => 'Auth',
        //'middleware' => 'role:'.config('access.users.admin_role'),
        ], function () {
    // User Management
    Route::group(['namespace' => 'User'], function () {
        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('user.account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('user.profile.update');

        // These routes can not be hit if the password is expired
        Route::group(['middleware' => 'password_expires'], function () {
            // Change Password Routes
            Route::patch('password/update', [UpdatePasswordController::class, 'update'])->name('password.update');
        });

        // User Status'
        Route::get('user/deactivated', [UserStatusController::class, 'getDeactivated'])->name('user.deactivated');
        Route::get('user/deleted', [UserStatusController::class, 'getDeleted'])->name('user.deleted');

        // User CRUD
        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::post('list', [UserController::class, 'getUsersList'])->name('users.list');
        Route::get('user/create', [UserController::class, 'create'])->name('user.create');
        Route::post('user', [UserController::class, 'store'])->name('user.store');

        // Specific User
        Route::group(['prefix' => 'user/{user}'], function () {
            // User
            Route::get('/', [UserController::class, 'show'])->name('user.show');
            Route::get('edit/{step?}', [UserController::class, 'edit'])->name('user.edit');
            Route::patch('/', [UserController::class, 'update'])->name('user.update');
            Route::delete('/', [UserController::class, 'destroy'])->name('user.destroy');

            // Account
            Route::get('account/confirm/resend', [UserConfirmationController::class, 'sendConfirmationEmail'])->name('user.account.confirm.resend');

            // Status
            Route::get('mark/{status}', [UserStatusController::class, 'mark'])->name('user.mark')->where(['status' => '[0,1]']);

            // Social
            Route::delete('social/{social}/unlink', [UserSocialController::class, 'unlink'])->name('user.social.unlink');

            // Confirmation
            Route::get('confirm', [UserConfirmationController::class, 'confirm'])->name('user.confirm');
            Route::get('unconfirm', [UserConfirmationController::class, 'unconfirm'])->name('user.unconfirm');

            // Password
            Route::get('password/change', [UserPasswordController::class, 'edit'])->name('user.change-password');
            Route::patch('password/change', [UserPasswordController::class, 'update'])->name('user.change-password.post');

            // Session
            Route::get('clear-session', [UserSessionController::class, 'clearSession'])->name('user.clear-session');

            // Deleted
            Route::get('delete', [UserStatusController::class, 'delete'])->name('user.delete-permanently');
            Route::get('restore', [UserStatusController::class, 'restore'])->name('user.restore');
        });


        // User Profile Request
        Route::group(['middleware' => 'role:' . config('access.users.admin_role')], function () {
            Route::get('profile-request', [ProfileRequestController::class, 'index'])->name('profile-request.index');
            Route::post('request-list', [ProfileRequestController::class, 'getProfileRequestList'])->name('profile-request.list');

            Route::group(['prefix' => 'profile-request/{profile_request}'], function () {
                Route::get('edit', [ProfileRequestController::class, 'edit'])->name('profile-request.edit');
                Route::patch('/', [ProfileRequestController::class, 'update'])->name('profile-request.update');
            });
        });

        // Data Source Request
        Route::group(['middleware' => 'role:' . config('access.users.admin_role')], function () {
            Route::get('data-source', [DataSourceController::class, 'index'])->name('data-source.index');
            Route::post('data-source-list', [DataSourceController::class, 'getDataSourceList'])->name('data-source.list');
            Route::get('data-source/create', [DataSourceController::class, 'create'])->name('data-source.create');
            Route::post('data-source', [DataSourceController::class, 'store'])->name('data-source.store');

            Route::group(['prefix' => 'data-source/{data_source}'], function () {
                Route::get('edit', [DataSourceController::class, 'edit'])->name('data-source.edit');
                Route::patch('/', [DataSourceController::class, 'update'])->name('data-source.update');
                Route::delete('/', [DataSourceController::class, 'destroy'])->name('data-source.destroy');
                Route::get('log', [DataSourceController::class, 'showLog'])->name('data-source.log');
                Route::post('log-view', [DataSourceController::class, 'showLogList'])->name('data-source.log-show');
                Route::get('booking', [DataSourceController::class, 'startBooking'])->name('data-source.booking');
                Route::get('crawler', [DataSourceController::class, 'startCrawler'])->name('data-source.crawler');
            });
        });
    });
});