<?php

Breadcrumbs::for('admin.auth.data-source.index', function ($trail) {
    $trail->push(__('labels.backend.access.data-source.management'), route('admin.auth.data-source.index'));
});

Breadcrumbs::for('admin.auth.data-source.create', function ($trail) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.create'), route('admin.auth.data-source.create'));
});

Breadcrumbs::for('admin.auth.data-source.edit', function ($trail, $id) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.view'), route('admin.auth.data-source.edit', $id));
});

Breadcrumbs::for('admin.auth.data-source.destroy', function ($trail, $id) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.deleted'), route('admin.auth.data-source.destroy', $id));
});

Breadcrumbs::for('admin.auth.data-source.log', function ($trail,$id) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.view'), route('admin.auth.data-source.log',$id));
});

Breadcrumbs::for('admin.auth.data-source.booking', function ($trail,$id) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.management'), route('admin.auth.data-source.booking',$id));
});

Breadcrumbs::for('admin.auth.data-source.crawler', function ($trail,$id) {
    $trail->parent('admin.auth.data-source.index');
    $trail->push(__('labels.backend.access.data-source.management'), route('admin.auth.data-source.crawler',$id));
});
