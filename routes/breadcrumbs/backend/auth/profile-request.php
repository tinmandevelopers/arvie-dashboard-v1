<?php

Breadcrumbs::for('admin.auth.profile-request.index', function ($trail) {
    $trail->push(__('labels.backend.access.profile-request.management'), route('admin.auth.profile-request.index'));
});

Breadcrumbs::for('admin.auth.profile-request.edit', function ($trail, $id) {
    $trail->parent('admin.auth.profile-request.index');
    $trail->push(__('labels.backend.access.profile-request.view'), route('admin.auth.profile-request.edit', $id));
});
