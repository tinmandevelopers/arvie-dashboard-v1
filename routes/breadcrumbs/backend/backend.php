<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.data-scrapping', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.data-scrapping'));
});

Breadcrumbs::for('admin.booking', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.booking'));
});



require __DIR__.'/auth.php';
require __DIR__.'/campground.php';
require __DIR__.'/bookingEnquiry.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/checkAvailabilityNotification.php';