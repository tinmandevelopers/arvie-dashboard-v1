<?php

Breadcrumbs::for('admin.booking.enquiry', function ($trail) {
    $trail->push(__('labels.backend.access.bookingenquiry.management'), route('admin.booking.enquiry'));
});

Breadcrumbs::for('admin.booking.enquiry.view', function ($trail, $id) {
    $trail->parent('admin.booking.enquiry');
    $trail->push(__('labels.backend.access.bookingenquiry.view'), route('admin.booking.enquiry.view', $id));
});

Breadcrumbs::for('admin.check-availability.create', function ($trail) {
    $trail->push(__('labels.backend.access.check-availability.management'), route('admin.check-availability.create'));
});

Breadcrumbs::for('admin.check-availability-log.index', function ($trail) {
    $trail->push(__('labels.backend.access.check-availability.log_all'), route('admin.check-availability-log.index'));
});

Breadcrumbs::for('admin.check-availability-log.view', function ($trail, $id) {
    $trail->parent('admin.check-availability-log.index');
    $trail->push(__('labels.backend.access.check-availability.view'), route('admin.check-availability-log.view', $id));
});
// Breadcrumbs::for('admin.campground.destroy', function ($trail, $id) {
//     $trail->parent('admin.campground.index');
//     $trail->push(__('labels.backend.access.campground.deleted'), route('admin.campground.destroy', $id));
// });

// Breadcrumbs::for('admin.campground.log', function ($trail,$id) {
//     $trail->parent('admin.campground.index');
//     $trail->push(__('labels.backend.access.campground.view'), route('admin.campground.log',$id));
// });



