<?php

Breadcrumbs::for('admin.campground.index', function ($trail) {
    $trail->push(__('labels.backend.access.campground.management'), route('admin.campground.index'));
});

Breadcrumbs::for('admin.campground.view', function ($trail, $id) {
    $trail->parent('admin.campground.index');
    $trail->push(__('labels.backend.access.campground.view'), route('admin.campground.view', $id));
});

Breadcrumbs::for('admin.campground.destroy', function ($trail, $id) {
    $trail->parent('admin.campground.index');
    $trail->push(__('labels.backend.access.campground.deleted'), route('admin.campground.destroy', $id));
});

Breadcrumbs::for('admin.campground.log', function ($trail,$id) {
    $trail->parent('admin.campground.index');
    $trail->push(__('labels.backend.access.campground.view'), route('admin.campground.log',$id));
});

Breadcrumbs::for('admin.campground.compare', function ($trail) {
    $trail->push(__('labels.backend.access.campground.compare'), route('admin.campground.compare'));
});
