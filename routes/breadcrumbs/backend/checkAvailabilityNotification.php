<?php

Breadcrumbs::for('admin.check-availability-notification', function ($trail) {
    $trail->push(__('labels.backend.access.check-availability-notification.management'), route('admin.check-availability-notification'));
});


Breadcrumbs::for('admin.check-availability-notification.create', function ($trail) {
    $trail->push(__('labels.backend.access.check-availability-notification.management'), route('admin.check-availability-notification.create'));
});

Breadcrumbs::for('admin.check-availability-notification.view', function ($trail, $id) {
    $trail->parent('admin.check-availability-notification');
    $trail->push(__('labels.backend.access.check-availability-notification.view'), route('admin.check-availability-notification.view', $id));
});

Breadcrumbs::for('admin.check-availability-notification.destroy', function ($trail) {
    $trail->push(__('labels.backend.access.check-availability-notification.management'), route('admin.check-availability-notification'));});
