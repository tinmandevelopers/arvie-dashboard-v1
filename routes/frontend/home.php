<?php

use App\Http\Controllers\Frontend\Auth\LoginController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', [LoginController::class, 'showLoginForm'])->name('index');
});
