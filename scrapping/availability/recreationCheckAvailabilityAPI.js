// https://www.recreation.gov/api/search/campsites?start=0&size=1000&fq=asset_id%3A232513&fq=campsite_type%3ATENT%20ONLY%20ELECTRIC&fq=campsite_type%3ATENT%20ONLY%20NONELECTRIC&fq=campsite_equipment_name%3ATent&start_date=2021-05-15T00:00:00.000Z&end_date=2021-05-18T00:00:00.000Z
const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
var fs = require('fs');
var express = require('express');

var url = args.url;
var filename = args.file_name;
const axios = require('axios');

var configPath = '../scrapping/temp/';
var filePathStore = 'scrapping/';

(async () => {

    let rawdata = fs.readFileSync(configPath + filename);
    var data = JSON.parse(rawdata);

    var campsiteType = '';
    var campsiteEquipement = data['details']['equipment_type'];
    if (data['details']['reservation_type'].toLowerCase() == 'rv') {
        campsiteType = '&fq=campsite_type%3ARV%20ELECTRIC&fq=campsite_type%3ARV%20NONELECTRIC';
    } else if (data['details']['reservation_type'].toLowerCase() == 'tent') {
        campsiteType = '&fq=campsite_type%3ATENT%20ONLY%20ELECTRIC&fq=campsite_type%3ATENT%20ONLY%20NONELECTRIC&fq=campsite_type%3AGROUP%20TENT%20ONLY%20AREA%20NONELECTRIC&';
    }

    campsiteEquipement = encodeURIComponent(campsiteEquipement)
    var id = url.substring(url.lastIndexOf('/') + 1);
    var startDate = data['check_in_date'].replaceAll('/', '-');
    var endDate = data['check_out_date'].replaceAll('/', '-');
    var availability = false;
    var comment = '';
    if (campsiteEquipement != '' && campsiteEquipement != '' && id != '' && startDate != '' && endDate != '') {
        var campsiteURL = 'https://www.recreation.gov/api/search/campsites?fq=asset_id%3A' + id + campsiteType + ' &fq=campsite_equipment_name%3A' + campsiteEquipement + '&start_date=' + startDate + 'T00:00:00.000Z&end_date=' + endDate + 'T00:00:00.000Z';
        // console.log(campsiteURL)
        await axios.get(campsiteURL)
            .then(async response => {
                var data = response.data;
                if (data.total > 0) {
                    availability = true;
                    comment = "Available";
                } else {
                    availability = false;
                    comment = "Not Available";
                }
            })
            .catch(async error => {
                console.log(error);
                availability = false;
                comment = error.message;
            });
    } else {
        availability = false;
        comment = 'Invalid Data';
    }

    var result = {
        'availability': availability,
        'message': comment,
    };

    console.log(result);
})();
