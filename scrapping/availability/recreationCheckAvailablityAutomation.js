//node scrapping/avlability/recreationCheckAvailablityAutomation.js --url=https://www.recreation.gov/api/camps/campgrounds/258887 --file_name=1626341385_checkAvailability.json
const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path');
const e = require("express");

var configPath = 'scrapping/temp/';
var filePathStore = 'scrapping/';

var url = args.url;
var filename = args.file_name;

var form_siteCategory = '';
var form_checkIn = '';
var form_checkOut = '';
var form_adults = '';
var form_petGroup = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_slideOutsGroup = '';
var debugLog = [];
var details = [];
var comment = "";
var availability = false;

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getDateValues(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var monthName = checkDate.toLocaleString("default", { month: "long" })
    var dayNumber = checkDate.getDate();
    var year = checkDate.getFullYear();

    var dateArr = new Array();

    dateArr['month'] = monthName;
    dateArr['year'] = year;
    dateArr['day'] = dayNumber;
    dateArr['month_year'] = monthName + " " + year;
    dateArr['original_date'] = dateVal;
    // var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateArr;
}

async function getEnquiryData() {
    let rawdata = fs.readFileSync(configPath + filename);
    var data = JSON.parse(rawdata);
    // console.log(data);
    form_siteCategory = data['details']['reservation_type'];
    form_checkIn = await getDateValues(data['check_in_date']);
    form_checkOut = await getDateValues(data['check_out_date']);
    // form_checkIn = data['check_in_date'].replace(/-/g, '/.');
    // form_checkOut = data['check_out_date'].replace(/-/g, '/');
    form_adults = data['details']['adults'];
    form_petGroup = data['details']['pet_friendly'];
    form_equipmentType = data['details']['equipment_type'];
    form_equipmentLength = data['details']['length'];
    form_slideOutsGroup = data['details']['slide_out'];


}

async function checkDate(indate, outdate) {

    var checkInDate = new Date(indate);
    var checkOutDate = new Date(outdate);
    var response = [];
    // Get today's date
    var todaysDate = new Date();

    // call setHours to take the time out of the comparison
    if (checkInDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
        response['message'] = "Check In Date is Less then current date";
        response['status'] = false;
    } else {
        if (checkOutDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
            response['message'] = "Check Out Date is Less then current date";
            response['status'] = false;
        } else {
            if (checkOutDate.setHours(0, 0, 0, 0) <= checkInDate.setHours(0, 0, 0, 0)) {
                response['message'] = "Check Out Date needs to be greter then check in date";
                response['status'] = false;
            } else {
                response['message'] = "Both date are Valid";
                response['status'] = true;
            }
        }
    }
    return response;
}

async function selectDatePicker(inMonthyear, inDayValue, inYearValue) {
    return new Promise(async function (resolve, reject) {
        // debugger;
        var calenderMonth = document.querySelectorAll('.CalendarMonth');
        var checkInFlag = false;
        for (var i = 0; i < calenderMonth.length; i++) {
            var datavisible = await calenderMonth[i].getAttribute("data-visible");
            if (datavisible) {

                var title = await calenderMonth[i].querySelector('div > strong').textContent;

                var titleYear = parseInt(new Date(title).getFullYear());
                var inputYear = parseInt(inYearValue);

                if (inputYear === titleYear) {
                    if (title == inMonthyear) {
                        var availDays = await calenderMonth[i].querySelectorAll('table > tbody > tr > td > div.rec-calendar-day > div >strong');
                        for (var d = 0; d < availDays.length; d++) {
                            var dayVal = await availDays[d].parentNode.parentNode.querySelector('div').textContent;
                            if (dayVal == inDayValue) {
                                await availDays[d].parentNode.parentNode.querySelector('div').click();
                                checkInFlag = true;
                                // console.log(dayVal+"=="+inDayValue)
                                resolve("true")
                            }
                        }
                    }
                } else {
                    resolve("false");
                }
            }
        }

        if (checkInFlag == false) {
            await document.querySelector('.sarsa-day-picker-range-controller-month-navigation-button.right').click();

            setTimeout(async function () {
                await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(function (result) {
                    if (result == "true") {
                        resolve("true")
                    } else {
                        resolve("false");
                    }
                });
            }, 5000)
        } else {
            resolve("true")
        }
    });
}

(async () => {
    try {
        await getEnquiryData();

        // for (var p = 0; p < campgrounds.length; p++) {

        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });

        debugLog.push('*************************');
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();
        debugLog.push(await dateTime() + ":" + 'Nevigating : ' + url);
        await page.setViewport({ width: 1366, height: 768 });

        await page.setDefaultNavigationTimeout(0);

        var browseUrl = url.replace("api/camps", "camping");
        // var browseUrl = campgrounds[p].link + 'reserve';
        debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

        await page.goto(browseUrl, { waitUntil: 'networkidle2' });

        debugLog.push(await dateTime() + ":" + "site available is found now selecting dates");

        var selectDates = await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(1) > button")

        let title = await page.evaluate(el => el.textContent, selectDates)
        console.log(title.trim());
        if (title == 'Select Dates ...') {
            await page.click('#campsite-filter-container > div > div > div:nth-child(1) > button');
        }

        debugLog.push(await dateTime() + ":" + "Verifying CheckIn/Out Date");

        page.on('console', msg => {
            for (let i = 0; i < msg._args.length; ++i)
                console.log(`${msg._args[i]}`)
        });

        var verifyCheckInDate = await checkDate(form_checkIn['original_date'], form_checkOut['original_date']);
        if (verifyCheckInDate['status'] == true) {
            debugLog.push(await dateTime() + ":" + "If date is valid then selecting date");

            var inMonthyear = form_checkIn['month_year'];
            var inDayValue = form_checkIn['day'];
            var inYearValue = form_checkIn['year'];
            var bothDateSelected = false;

            await page.addScriptTag({ content: `${selectDatePicker}` });
            var inDateSelect = await page.evaluate(async (inMonthyear, inDayValue, inYearValue) => {

                // return new Promise(function (resolve, reject) {
                var resultVal = false;
                await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(async function (result) {
                    resultVal = result
                });
                return resultVal;

            }, inMonthyear, inDayValue, inYearValue)

            if (inDateSelect == "true") {
                debugLog.push(await dateTime() + ":" + "If in date found then selcting out date");
                await sleep(1500)

                var outMonthyear = form_checkOut['month_year'];
                var outDayValue = form_checkOut['day'];
                var outYearValue = form_checkOut['year'];


                var outDateSelect = await page.evaluate(async (outMonthyear, outDayValue, outYearValue) => {

                    // return new Promise(function (resolve, reject) {
                    var resultVal = false;
                    await selectDatePicker(outMonthyear, outDayValue, outYearValue).then(async function (result) {
                        resultVal = result
                    });
                    return resultVal;

                }, outMonthyear, outDayValue, outYearValue)

                // console.log("outDateSelect:"+outDateSelect)
                if (outDateSelect == "false") {
                    debugLog.push(await dateTime() + ":" + "Check Out Date is Not Available");
                    comment = 'Check Out Date is Not Available';
                    console.log('Check Out Date is Not Available')
                    bookingStatus = false;
                }
                if (inDateSelect == "true" && outDateSelect == "true") {
                    bothDateSelected = true;
                    debugLog.push(await dateTime() + ":" + "Both date are available");

                }

                await page.click('#shared-filter-options-modal > div > div > div > div > div > button');
                await sleep(3000)

                if (await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(2) > button")) {
                    await page.click("#campsite-filter-container > div > div > div:nth-child(2) > button");


                    var equipAvailable = await page.evaluate(async (form_equipmentType) => {
                        var allEquip = document.querySelectorAll(".search-filter-check-item input:not([disabled])");
                        var found = false;
                        for (var a = 0; a < allEquip.length; a++) {
                            var str = allEquip[a].getAttribute('value');
                            console.log(str + "==" + form_equipmentType);
                            if (str == form_equipmentType) {
                                allEquip[a].parentNode.click()
                                found = true;
                            }
                        }
                        document.querySelector('#shared-filter-options-modal > div > div > div > div > div > button').click();
                        return found;
                    }, form_equipmentType)
                    console.log(equipAvailable);
                }

            } else {
                debugLog.push(await dateTime() + ":" + "Check in Date is Not Available");
                comment = 'Check in Date is Not Available';
                bookingStatus = false;
            }

        } else {
            debugLog.push(await dateTime() + ":" + "Date Error :" + verifyCheckInDate['message']);
            console.log("Date Error :" + verifyCheckInDate['message']);
            comment = verifyCheckInDate['message'];
            bookingStatus = false;
        }

        await sleep(2000);
        var siteList = await page.evaluate(async () => {

            var siteTotal = await document.querySelectorAll('.campsite-search-card-column');
            var found = [];
            for (var t = 0; t < siteTotal.length; t++) {
                var btnName = siteTotal[t].querySelector('.sarsa-button-inner-wrapper > span').textContent.trim();
                // console.log("title"+title);

                if (btnName == "Add to Cart") {
                    siteName = siteTotal[t].querySelector('.rec-flex-card-top-left-content-wrap').textContent.trim();
                    price = siteTotal[t].querySelector('.rec-flex-card-price-wrap').textContent.trim();
                    description = siteTotal[t].querySelector('.rec-card-description').textContent.trim();
                    found.push({
                        'title': siteName,
                        'price': price,
                        'other': description,
                    });

                }
            }
            await new Promise(function(resolve) {setTimeout(resolve, 1000)});
            if (await document.querySelector('.search-pagination-wrap .rec-pagination button >svg.rec-icon-arrow-forward')) {
                do {
                    await new Promise(function(resolve) {setTimeout(resolve, 1000)});
                    await document.querySelector('.search-pagination-wrap .rec-pagination button >svg.rec-icon-arrow-forward').parentElement.click();
                    var siteTotal = await document.querySelectorAll('.campsite-search-card-column');
                    for (var t = 0; t < siteTotal.length; t++) {
                        var btnName = siteTotal[t].querySelector('.sarsa-button-inner-wrapper > span').textContent.trim();
                        // console.log("title"+title);

                        if (btnName == "Add to Cart") {
                            siteName = siteTotal[t].querySelector('.rec-flex-card-top-left-content-wrap').textContent.trim();
                            price = siteTotal[t].querySelector('.rec-flex-card-price-wrap').textContent.trim();
                            description = siteTotal[t].querySelector('.rec-card-description').textContent.trim();

                            console.log("Matching siteName:" + siteName);
                            console.log("Price:" + price);
                            console.log("Description:" + description);

                            found.push({
                                'title': siteName,
                                'price': price,
                                'other': description,
                            });

                        }
                    }

                }while(!await document.querySelector('.search-pagination-wrap .rec-pagination button.rec-disabled-button >svg.rec-icon-arrow-forward'))

            }
            return found;
        });
        if (siteList.length > 0) {
            availability = true;
            comment = "Available";
            details = siteList;
        } else {
            availability = true;
            comment = "Not Available";
        }
    } catch (err) {
        availability = true;
        comment = "Not Available";

        debugLog.push(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = err.message;
    }

    var result = {
        'availability': availability,
        'message': comment,
        'details': details
    };

    console.log(JSON.stringify(result))
    // await browser.close();
})();

async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}
