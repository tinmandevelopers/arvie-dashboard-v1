const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

var configPath = '../scrapping/temp/';
var filePathStore = 'scrapping/';

// var url = args.url.replaceAll('@@', '&');
var url = args.url.split("@@").join("&");
var filename = args.file_name;
// console.log(url);
var bookingStatus = false;
var details = [];
var comment = '';

var form_siteCategory = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_vehicle = '';
var form_adults = '';
var form_login_username = 'dummayaccount@mailinator.com';
var form_login_password = 'Dummayaccount@123';

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        log(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}


async function getEnquiryData() {
    let rawdata = fs.readFileSync(configPath + filename);
    var data = JSON.parse(rawdata);

    form_siteCategory = data['details']['reservation_type'];
    form_equipmentType = data['details']['equipment_type'];
    form_equipmentLength = data['details']['length'];
    form_vehicle = data['details']['towing_length'];
    form_adults = data['details']['adults'];
    // console.log(data['details'])
}

(async () => {

    await getEnquiryData();

    try {
        var browser = await puppeteer.launch({
            headless: true,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
        });
        log(await browser.version());
        log(await dateTime() + ":" + await browser.version());
        // open a new page

        var page = await browser.newPage();
        await page.setViewport({width: 1366, height: 768});

        await page.setDefaultNavigationTimeout(0);
        
        try {          

            let browseUrl = url;
            //    console.log(browseUrl);
            log(await dateTime() + ":" + 'Nevigating : ' + browseUrl);
            await page.goto(browseUrl, {waitUntil: 'networkidle2'});
            await sleep(1000);
            if (await page.$('#sysError')) {
                let element = await page.$('.pageerror');
                let errorMsg = await page.evaluate(el => el.textContent, element)
                // comment = errorMsg;
                comment = "Not Available";
                bookingStatus = false;
                log(await dateTime() + ":" + errorMsg);

            } else {

                if (await page.$('#lookingfor')) {

                    var val = await getSelctBoxOptionValue('#lookingfor', form_siteCategory, page);

                    await page.select('#lookingfor', val);
                    if (val != '') {
                        await sleep(1000);
                        if (await page.$('.actionButton  button')) {
                            await page.click('.actionButton  button')
                            await sleep(1000);
                        }

                        await sleep(3000);
                        log(await dateTime() + ":" + "Scrolling page");
                        if (await page.$('#facility_map')) {
                            await page.evaluate(() => {
                                document.querySelector('#facility_map').scrollIntoView()
                            });
                        }
                        const bookTableTd = await page.evaluate(() => {
                            const rows = document.querySelectorAll('.table-responsive table tbody tr');
                            return Array.from(rows, row => {
                                const columns = row.querySelectorAll('td');
                                return Array.from(columns, column => column.innerText);
                            });
                        });
                        let getTdChild = 0;
                        if (bookTableTd && bookTableTd.length) {
                            var checkflag=false;
                            for (let a = 0; a < bookTableTd.length; a++) {
                                if(checkflag==false){
                                    let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');
                                    if (getBookTr.length) {
                                        getTdChild = a + 1;
                                        checkflag=true;
                                        break;
                                    }
                                }
                            }
                            if (getTdChild) {
//                                console.log(await dateTime() + ":" + "Click on book button");
                                await page.click('.table-responsive table tbody tr:nth-child(1) td:nth-child(7) button');
                                await sleep(2000);
                                var priceInfo = await page.evaluate(async () => {
                                    priceInfo = '';
                                    if (document.querySelector('.product-details__amount') !== null)
                                        priceInfo = document.querySelector('.product-details__amount').textContent + ' avg/night';
                                    return priceInfo;
                                });
                                await page.goBack();
                                await sleep(2000);

                                for (let a = 0; a < bookTableTd.length; a++) {
                                    let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');
                                    if (getBookTr.length) {
                                        var titleInfo = bookTableTd[a][0] + " Loop: " + bookTableTd[a][1];
                                        var maxpepople = (bookTableTd[a][3]) ? bookTableTd[a][3] : '-';
                                        var equipementLength = (bookTableTd[a][4]) ? bookTableTd[a][4] : '-';
                                        var otherInfo = "site type: " + bookTableTd[a][2] + " , Max # People: " + maxpepople + " , Equipment Length: " + equipementLength;
                                        details.push({
                                            'title': titleInfo,
                                            'description': otherInfo,
                                            'price': priceInfo
                                        });
                                    }
                                }
                                if (await page.$(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`)) {
                                    log(await dateTime() + ":" + "Click on book button");
                                    await page.click(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`);
                                    await sleep(3000);

                                    log(await dateTime() + ":" + "Scrolling page");
                                    await page.evaluate(() => {
                                        document.querySelector('.quickbook__datepager').scrollIntoView()
                                    });
                                    await sleep(1000);

                                    await page.click('.quickbook button');
                                    await sleep(3500);

                                    if (await page.$('#emailGroup input.TextBoxRenderer')) {
                                        log(await dateTime() + ":" + "Waiting for login page ");
                                        log(await dateTime() + ":" + "Storing email")
                                        const email = await page.waitForSelector("#emailGroup input.TextBoxRenderer");
                                        await page.click("#emailGroup input.TextBoxRenderer");
                                        await email.type(form_login_username);
                                    }

                                    if (await page.$('.PasswordBoxRenderer')) {
                                        log(await dateTime() + ":" + "Storing Password")
                                        const password = await page.waitForSelector(".PasswordBoxRenderer");
                                        await page.click(".PasswordBoxRenderer");
                                        await password.type(form_login_password);
                                        await sleep(3000)
                                    }
                                  
                                    if (await page.$('#signinbutton button')) {
                                        await page.click('#signinbutton button')
                                        await sleep(3000)
                                    } else {
                                        await sleep(2000);
                                    }

                                    await sleep(5000);
                                    // if (await page.$(".col-md-9 div:nth-child(2)")) {

                                    log(await dateTime() + ":" + "Scrolling EQUIPMENT section");
                                    // await page.evaluate(() => {
                                    //     document.querySelector('.col-md-9 div:nth-child(2)').scrollIntoView()
                                    // });
                                   await sleep(1000);
                                    // page.on('console', msg => {
                                    //     for (let i = 0; i < msg._args.length; ++i)
                                    //         console.log(`${msg._args[i]}`)
                                    // });
                                    var equipement=false;
                                    if (await page.$("#container  div.highlight__child-container div.input-holder-main select")) {

                                        var val = await getSelctBoxOptionValue("#container  div.highlight__child-container div.input-holder-main select",form_equipmentType, page);
                                        // console.log("equipemnet selectopnm value is:"+val)
                                        await page.select("#container  div.highlight__child-container div.input-holder-main select", val);
                                        if(val > 0){
                                            equipement=true;
                                        }else{
                                            equipement=false;
                                        }
                                    }else{
                                        equipement=true;
                                    }

                                    if (await page.$("#container  div.highlight__child-container div.input-holder-main input[id^=equipLength_primEquipTemplate]")) {
                                        const equipLength = await page.waitForSelector("#container  div.highlight__child-container div.input-holder-main input[id^=equipLength_primEquipTemplate]");
                                        await equipLength.type(form_equipmentLength);
                                        await sleep(1000);
                                        equipement=true;
                                    }   

                                    if (await page.$('select#equipmentType')) {
                                        log(await dateTime() + ":" + "select EQUIPMENT type");

                                        var val = await getSelctBoxOptionValue('#equipmentType', form_equipmentType, page);
                                        await page.select('#equipmentType', val);
                                        log(await page.select('#equipmentType', val))
                                        await sleep(1000);
                                        equipement=true;
                                    }
//                                    await sleep(1000);
                                    // await page.evaluate(() => {
                                    //     document.querySelector('.col-md-9 div:nth-child(2) > div:nth-child(4)').scrollIntoView()
                                    // });
//                                    await sleep(1000);

                                    if (await page.$('#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input')) {
                                        log(await dateTime() + ":" + "Selecting Equipment Length");
                                        const equipLength = await page.waitForSelector("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                        await page.click("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                        await equipLength.type(form_equipmentLength);
                                        equipement=true;
                                    }

                                    if (await page.$('#numOfVehicles')) {
                                        log(await dateTime() + ":" + "Selecting vehicles");
                                        const numOfVehicle = await page.waitForSelector("#numOfVehicles");
                                        await page.click("#numOfVehicles");
                                        await numOfVehicle.type(form_vehicle);
                                    }

                                    if (await page.$('#numOfOccup1')) {
                                        log(await dateTime() + ":" + "Storing Free");
                                        const numOfOccup1 = await page.waitForSelector("#numOfOccup1");
                                        await page.click("#numOfOccup1");
                                        await numOfOccup1.type(form_adults)
                                    } else {
                                        if (await page.$('#numOfOccupants')) {
                                            const numOfOccup1 = await page.waitForSelector("#numOfOccupants");
                                            await page.click("#numOfOccupants");
                                            await numOfOccup1.type(form_adults);
                                        }
                                    }
                                    await sleep(1000)

                                    if (await page.$('#agreementCheck')) {
                                        log(await dateTime() + ":" + "Selecting Tearms and Condition");
                                        await page.click("#agreementCheck");
                                        await sleep(1000)
                                    }


                                    log(await dateTime() + ":" + "Clicking on Continue to Cart button");
                                    await page.click('.btn-success')
                                    await sleep(3000);
                                    if (await page.$('.alert-danger') !== null) {
                                        if(equipement==true){
                                            comment = 'Available';
                                            bookingStatus = true;    
                                        }else{
                                            let element = await page.$('.alert-danger');
                                            let errorMsg = await page.evaluate(el => el.textContent, element)
    
                                            comment = "Not Available";
                                            bookingStatus = false;
                                            log(await dateTime() + ":" + errorMsg);    
                                        }
                                    } else {
                                        comment = 'Available';
                                        bookingStatus = true;
                                    }

                                    await sleep(1000);
                                    await page.goto("https://www.reserveamerica.com/explore/cart/view", {waitUntil: 'networkidle2'});
                                    if (await page.$('div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a') !== null) {
                                        await page.click("div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a")
                                        await sleep(1000);
                                        if (await page.$('div.modal-body > div.feedback-footer.row > div:nth-child(2) > button') !== null) {
                                            await page.click("div.modal-body > div.feedback-footer.row > div:nth-child(2) > button");
                                            await sleep(2000);
                                        }   
                                    }
                                    
                                } else {
                                    bookingStatus = false;
                                    comment = "Not Available";
                                    log(await dateTime() + ":" + "Not Available");
                                }
                            } else {
                                bookingStatus = false;
                                comment = "Not Available";
                                log(await dateTime() + ":" + "Not Available");
                            }
                        } else {
                            bookingStatus = false;
                            comment = "Not Available";
                            log(await dateTime() + ":" + "Not Available");
                        }

                    } else {
                        comment = "No Site Category Found";
                        bookingStatus = false;
                        log(await dateTime() + ":" + comment);
                    }
                } else {
                    comment = "Not Available";
                    bookingStatus = false;
                }

            }
        } catch (err) {
            log(await dateTime() + ":" + "Error Occurs While Checking Availablity")
            comment = 'Not Available';
            bookingStatus = false;
            log(err.message)

            await sleep(1000);
            await page.goto("https://www.reserveamerica.com/explore/cart/view", {waitUntil: 'networkidle2'});
            if (await page.$('div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a') !== null) {
                await page.click("div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a")
                await sleep(1000);
                if (await page.$('div.modal-body > div.feedback-footer.row > div:nth-child(2) > button') !== null) {
                    await page.click("div.modal-body > div.feedback-footer.row > div:nth-child(2) > button");
                    await sleep(2000);
                }   
            }
            // console.log(err)
        }

    } catch (err) {
        // log(err.message);
        // console.log(err)
        bookingStatus = false;
        log(await dateTime() + ":" + "Sometghing Went Wrong")
        // comment = err.message;
        comment = 'Not Available';

        await sleep(1000);
        await page.goto("https://www.reserveamerica.com/explore/cart/view", {waitUntil: 'networkidle2'});
        if (await page.$('div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a') !== null) {
            await page.click("div.col-md-9.col-xs-12 > div > div.pull-right.hidden-xs > a")
            await sleep(1000);
            if (await page.$('div.modal-body > div.feedback-footer.row > div:nth-child(2) > button') !== null) {
                await page.click("div.modal-body > div.feedback-footer.row > div:nth-child(2) > button");
                await sleep(2000);
            }   
        }
    }

    await browser.close();

    var result = {
        'availability': bookingStatus,
        'message': comment,
        'details': details
    };

    console.log(JSON.stringify(result))

})();


async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function log(txt) {
    // console.log(txt);
    // console.log('*************************');

}
async function getSelctBoxOptionValue(selector, title, page) {

    var value = await page.evaluate(async (title, selector) => {
        const example = document.querySelector(selector);
        const example_options = example.querySelectorAll('option');

        for (var i = 0; i < example_options.length; i++) {
            // console.log(example_options[i].text + '==' + title)
            if (example_options[i].text == title) {
                // console.log(example_options[i].value)
                return example_options[i].value;
            }
        }
        return '';
    }, title, selector);
    return value;
}