// Manually run commnad for Reference
// node scrapping/sunBookingReserve.js --campground_id={campground_id} --booking_id={booking_enquiry_id}
// node scrapping/sunBookingReserve.js --campground_id=666 --booking_id=11

const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

var configPath = '../scrapping/temp/';
var filePathStore = 'scrapping/';

var url = args.url;
var filename = args.file_name;

var bookingStatus = false;

var comment = '';
var debugLog = [];
var details = [];
// var form_siteCategory = 'Lodging';
var form_siteCategory = '';
var form_checkIn = '';
var form_checkOut = '';
var form_inFants = '';
var form_adults = '';
var form_kids = '';
var form_pets = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_slideOuts = '';

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function changeDateFormat(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var dayName = checkDate.toLocaleString("default", { weekday: "short" })
    var monthName = checkDate.toLocaleString("default", { month: "short" })
    var dayNumber = checkDate.getDate();

    var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateString;
}

async function getEnquiryData() {

    let rawdata = fs.readFileSync(configPath + filename);
    var data = JSON.parse(rawdata);

    form_siteCategory = data['details']['reservation_type'];
    form_checkIn = await changeDateFormat(data['check_in_date']);
    form_checkOut = await changeDateFormat(data['check_out_date']);
    // form_inFants = data['details']['infants'];
    form_adults = data['details']['adults'];
    // form_kids = data['details']['kids'];
    form_pets = data['details']['pet_friendly'];

    // console.log("pets"+form_pets);
    form_equipmentType = data['details']['equipment_type'];
    form_equipmentLength = data['details']['length'];
    form_slideOuts = (data['details']['slide_out'] == '0') ? '1' : data['details']['slide_out'];
    // form_firstName = data['name'];
    // form_lastName = data['name'];
    // form_address1 = data['address1'];
    // form_address2 = data['address2'];
    // form_countryCode = data['country_code'];
    // form_phone = data['phone'];
    // form_postcode = data['post_code'];
    // form_email = data['email'];
    // form_referal_resource = data['details']['referal_resource'];
    // form_reason_visit = data['details']['reason_visit'];
}

(async () => {
    try {
        await getEnquiryData();
        await campspot();
    } catch (err) {
        // console.log(err);
        bookingStatus = false;
        debugLog.push(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = 'Something Went Wrong';
        // console.log("Browser Closed");
    }

})();


async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function campspot() {

    var browser = await puppeteer.launch({
        headless: true,
        args: [
            '--start-maximized' // you can also use '--start-fullscreen'
        ]
        // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
    });
    // console.log('*************************');
    // console.log(await browser.version());
    debugLog.push(await dateTime() + ":" + await browser.version());
    // open a new page
    var page = await browser.newPage();

    try {
        // console.log('Nevigating : ' + campgrounds[p].title)
        // console.log('Nevigating : ' + url)
        debugLog.push(await dateTime() + ":" + 'Nevigating : ' + url);
        await page.setViewport({ width: 1366, height: 768 });

        await page.setDefaultNavigationTimeout(0);

        var browseUrl = url;
        // var browseUrl = campgrounds[p].link + 'reserve';
        debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

        await page.goto(browseUrl, { waitUntil: 'networkidle2' });

        await sleep(3000)

        debugLog.push(await dateTime() + ":" + 'Click on search availability link');

        const link = await page.$('#nav > div.nav-drop-classic-holder > div.search-block > div > a');             // declare object

        const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));    // declare promise
        await link.click();                             // click, a new tab opens

        debugLog.push(await dateTime() + ":" + 'New tab open');

        const newPage = await newPagePromise;           // open new tab /window, 
        await newPage.setViewport({ width: 1366, height: 768});
        await sleep(25000);

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input') !== null) {
            debugLog.push(await dateTime() + ":" + 'Fill check in date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input", form_checkIn);
        }

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input') !== null) {
            debugLog.push(await dateTime() + ":" + 'Fill check out date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input", form_checkOut);
        }

        await newPage.click('#search-form-label-dates');

        await newPage.evaluate(() => {
            document.querySelector('.home-search').scrollIntoView()
        })

        debugLog.push(await dateTime() + ":" + 'Fill guest details');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div') !== null) {
            await sleep(1000);
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
        }

        // for (var i = 0; i < form_inFants; i++) {
        //     await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(1) > td:nth-child(2) > div > button.app-increase-guest-count-button-0.guests-picker-menu-category-controls-stepper.mod-increase');
        // }
        // await sleep(1000);
        // for (var k = 0; k < form_kids; k++) {
        //     await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(2) > td:nth-child(2) > div > button.app-increase-guest-count-button-1.guests-picker-menu-category-controls-stepper.mod-increase');
        // }
        await sleep(1000);
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.app-increase-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-increase') != null) {
            if (form_adults > 2) {
                form_adults = form_adults - 2;
                for (var a = 0; a < form_adults; a++) {
                    await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.app-increase-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-increase');
                }
            } else {
                if (form_adults == 0) {
                    form_adults = 2;
                    for (var a = 0; a < form_adults; a++) {
                        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                    }
                }
                if (form_adults == 1) {
                    for (var a = 0; a < form_adults; a++) {
                        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                    }
                }
            }
        }

        await sleep(1000);
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.guests-picker-menu-category-controls-stepper.mod-increase.app-increase-pet-count-button') != null) {

            for (var p = 0; p < form_pets; p++) {
                await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr > td:nth-child(2) > div > button.guests-picker-menu-category-controls-stepper.mod-increase.app-increase-pet-count-button');
            }
            await sleep(1000);
        }
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div') !== null) {

            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
            await sleep(1000);
        }
        debugLog.push(await dateTime() + ":" + 'Click on search button');
        if(await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-submit > button')){
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-submit > button');
        }
        await sleep(1000);
        debugLog.push(await dateTime() + ":" + 'Checking error found or not');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > div.search-form-error-message') != null) {
            debugLog.push(await dateTime() + ":" + 'Error found in search page');

            let element = await newPage.$('.search-form-error-message');
            let elementText = await newPage.evaluate(el => el.textContent, element)
            // console.log(elementText);
            bookingStatus = false;
            comment = elementText;
        } else {
            await sleep(5000);
            debugLog.push(await dateTime() + ":" + 'Check campsite is found or not');
            if (await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div') !== null) {
                debugLog.push(await dateTime() + ":" + 'No campsite found');

                let element = await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div');
                let elementText = await newPage.evaluate(el => el.textContent, element)
                // console.log(elementText)
                comment = elementText;
                bookingStatus = false;
            } else {
                debugLog.push(await dateTime() + ":" + 'Campsite found');
                var sitefound = false;
                var equipmentfound=false;
                if (await newPage.$('.app-search-results-list') !== null) {
                    await sleep(1000);
                    debugLog.push(await dateTime() + ":" + 'Search campsite as par reservation type');
                    if (await newPage.$('.search-main  > div > div > div > div') != null) {

                        var test = await newPage.evaluate(async (form_siteCategory) => {
                            var sitetypeTotal = await document.querySelectorAll('.search-results .search-results-promoted-filters-item');
                            var found = false;
                            for (var t = 0; t < sitetypeTotal.length; t++) {
                                var title = sitetypeTotal[t].querySelector('button >h3:nth-child(1)').textContent.trim();

                                if (title == form_siteCategory) {

                                    sitetypeTotal[t].querySelector('button >h3:nth-child(1)').click();
                                    found = true;
                                }
                            }
                            return found;
                        }, form_siteCategory)
                        await sleep(2000)
                        if (!test) {
                            debugLog.push(await dateTime() + ":" + 'No campsite found as per reservation type');

                            // console.log('No site Category found...')
                            comment = 'No site Category found...';
                            bookingStatus = false;
                        } else {
                            debugLog.push(await dateTime() + ":" + 'Campsite found as per reservation type');

                            sitefound = true;
                            var selectEquipment = await newPage.evaluate(async (form_equipmentType) => {
                                if("#search-filters-equipment-types ~div >div > label"){
                                 var totalLbl = document.querySelectorAll("#search-filters-equipment-types ~div >div > label ");
                                 // console.log(totalLbl.length)
                                 var nameflag=false;
                                 for (var l = 0; l < totalLbl.length; l++) {
                                     var equipmentName = totalLbl[l].querySelector('div:nth-child(2)').textContent.trim();
                                        //  console.log("Equipement name:"+equipmentName);
                                        if(nameflag==false){
                                            if(form_equipmentType==equipmentName){
                                                totalLbl[l].querySelector('input').click();
                                                nameflag=true;
                                                
                                            }else{
                                                nameflag=false;
                                                
                                            }
                                        }
                                 }
                                 return nameflag;
                             }
                             },form_equipmentType);
                             debugLog.push("selectEquipment isss:"+selectEquipment);
                
                             await sleep(3000)
                             if(selectEquipment){
                                 equipmentfound=true;
                                 await newPage.click('body > app-root > div > main > search-results > div > section.search-main > div > list-view > search-results-list > div > ul:nth-child(1) > li > a');
                             }else{
                                 equipmentfound=false;
                             }
                            
                        }

                    }
                    await sleep(10000)
                }
                if (sitefound) {
                   if (equipmentfound) {
                    await sleep(1000);
                    debugLog.push(await dateTime() + ":" + 'Click on campsite to book');
                    if (await newPage.$('.site-locations-table>tr') !== null) {

                        let elements = await newPage.$$('.site-locations-table>tr.site-locations-table-site');
                        var titleInfo = "";
                        var aminitiesInfo = "";
                        var equipementInfo = "";
                        var equipementLenthInfo = "";
                        var slideoutsInfo = "";
                        var priceInfo = "";

                        for (const el of elements) {
                            if(await el.$('td.site-locations-table-site-select button') == null){
                                continue;
                            }
                            if (await el.$('td.site-locations-table-site-number') !== null) {
                                var title = await el.$('td.site-locations-table-site-number');
                                titleInfo = await newPage.evaluate(el => el.textContent, title);
                                titleInfo = titleInfo.replace(/\s+/g, ' ').trim();
                            }
                            if (await el.$('td.site-locations-table-site-amenities') !== null) {
                                var aminities = await el.$('td.site-locations-table-site-amenities');
                                aminitiesInfo = await newPage.evaluate(el => el.textContent, aminities);
                                aminitiesInfo = aminitiesInfo.replace(/\s+/g, ', ').replace(/,\s*$/, "").replace(/^,/, '');
                            }
                            if (await el.$('td.site-locations-table-site-rv-types') !== null) {
                                var equipement = await el.$('td.site-locations-table-site-rv-types');
                                equipementInfo = await newPage.evaluate(el => el.textContent, equipement);
                                equipementInfo = equipementInfo.replace(/\s+/g, ', ').replace(/,\s*$/, "").replace(/^,/, '');
                            }
                            if (await el.$('td.site-locations-table-site-max-rv-length') !== null) {
                                var equipementLenth = await el.$('td.site-locations-table-site-max-rv-length');
                                equipementLenthInfo = await newPage.evaluate(el => el.textContent, equipementLenth);
                                equipementLenthInfo = equipementLenthInfo.replace(/\s+/g, ', ').replace(/,\s*$/, "").replace(/^,/, '');
                            }
                            if (await el.$('td.site-locations-table-site-slide-outs') !== null) {
                                var slideouts = await el.$('td.site-locations-table-site-slide-outs');
                                slideoutsInfo = await newPage.evaluate(el => el.textContent, slideouts);
                                slideoutsInfo = slideoutsInfo.replace(/\s+/g, ', ').replace(/,\s*$/, "").replace(/^,/, '');
                            }
                            if (await newPage.$('.site-booking-form-price-table .app-average-per-night-price') !== null) {
                                var prices = await newPage.$('.site-booking-form-price-table .app-average-per-night-price');
                                priceInfo = await newPage.evaluate(el => el.textContent, prices);
                                priceInfo = priceInfo.replace(/\s+/g, ', ').replace(/,\s*$/, "").replace(/^,/, '');
                            }

                            details.push({
                                'title': titleInfo,
                                'other': {
                                    "amenities": aminitiesInfo,
                                    "equipment": equipementInfo,
                                    "equipment_length": equipementLenthInfo,
                                    "slide_outs": slideoutsInfo
                                },
                                "description": 'Site Amenities: '+ aminitiesInfo +' Max Length Allowed:'+equipementLenthInfo,
                                "price": priceInfo + ' avg/night'
                            });

                        }
                    }
                    await sleep(3000);
                    if (await newPage.$('#rv-type-select') !== null) {
                        // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                        await newPage.select('#rv-type-select', form_equipmentType)
                        await sleep(1000)
                    }
                    await sleep(1000)
                    if (await newPage.$('.site-booking-form-rv-length > input') !== null) {

                        await newPage.type('.site-booking-form-rv-length > input', form_equipmentLength)

                    }

                    if (await newPage.$('#rv-slide-out-select') !== null) {
                        // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                        await newPage.select('#rv-slide-out-select', form_slideOuts)
                        await sleep(1000)
                    }
                
                    // await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button');

                    //Checking if Location Field is Presernt or not
                    debugLog.push(await dateTime() + ":" + 'Checking if Location Field is Present or not');
                    // console.log('Checking if Location Field is Present or not')
                    // if (await newPage.$('.site-booking-form-location-selection') !== null) {
                    let element = await newPage.$('.site-booking-form-location-selection');
                    let elementText = await newPage.evaluate(el => el.textContent, element)
                    // console.log(elementText)
                    debugLog.push(await dateTime() + ":" + 'Checking if Location Field Has value or not');
                    // console.log('Checking if Location Field Has value or not')
                    if (elementText.trim() != '') {
                        // console.log('If Location Already inserted then clicking on add to cart button')
                        await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal > div > form > div.mobile-modal-add-to-cart > input');

                    } else {
                        debugLog.push(await dateTime() + ":" + 'If Location Field is Blanke the Clicking on location and selecting First Location');

                        // console.log('If Location Field is Blanke the Clicking on location and selecting First Location')
                        await newPage.click('.site-booking-form-location-button');
                        await sleep(1000)

                        await newPage.click('#locations > table > tr:nth-child(2) > td.site-locations-table-site-select > button');
                        await sleep(1000)
                        // await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button')
                        // await sleep(1000)
                        // await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input')

                    }
                    // //Clicking on Review Cart
                    // await newPage.click('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a');
                    // } else {
                    // console.log('Checking if Location Field is not present then Filling other values Like Equipement and related fields')
                    debugLog.push(await dateTime() + ":" + 'Checking if Location Field is not present then Filling other values Like Equipement and related fields');
                    await sleep(2000)
                    
                    // }
                        // console.log('in site list page')
                    await sleep(1000);
                    await newPage.click('.app-add-to-cart-button');

                    await sleep(1000);
                    // await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input');
                    await sleep(1000);
                    if (await newPage.$('.site-booking-form-error-message')) {

                        comment = 'Not Available';
                        bookingStatus = false;

                    } else {
                        comment = 'Available';
                        bookingStatus = true;
                    }
                } else {
                    comment = 'Not Available';
                    bookingStatus = false;
                }
                } else {
                    comment = 'Not Available';
                    bookingStatus = false;
                }
            }
            await sleep(1000)
        }
        // if (closeBrowser) {
        //     debugLog.push(await dateTime() + ":" + "Browser Closed ")
        //     await newPage.close();
        //     await browser.close();
        // }
    } catch (err) {
        // await newPage.close();
        // await browser.close();
        debugLog.push(await dateTime() + ":" + "Error Occurs  ")
        comment = "Not Available";
        bookingStatus = false;
        // console.log(err)
    }

    await browser.close();

    var result = {
        'availability': bookingStatus,
        'message': comment,
        'details': details,
    };

    console.log(JSON.stringify(result))


}