// https://www.recreation.gov/api/search/campsites?start=0&size=1000&fq=asset_id%3A232513&fq=campsite_type%3ATENT%20ONLY%20ELECTRIC&fq=campsite_type%3ATENT%20ONLY%20NONELECTRIC&fq=campsite_equipment_name%3ATent&start_date=2021-05-15T00:00:00.000Z&end_date=2021-05-18T00:00:00.000Z
const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
var fs = require('fs');
var express = require('express');

var url = args.url;
var filename = args.file_name;
const axios = require('axios');
var details = [];

// var configPath = '../scrapping/temp/';
// var filePathStore = 'scrapping/';

(async () => {
    var availability = false;
    var comment = '';
    try {
        // let rawdata = fs.readFileSync(configPath + filename);
        // var data = JSON.parse(rawdata);

        // var campsiteType = '';
        // var campsiteEquipement = data['details']['equipment_type'];
        // if (data['details']['reservation_type'].toLowerCase() == 'rv') {
        //     campsiteType = '&fq=campsite_type%3ARV%20ELECTRIC&fq=campsite_type%3ARV%20NONELECTRIC';
        // } else if (data['details']['reservation_type'].toLowerCase() == 'tent') {
        //     campsiteType = '&fq=campsite_type%3ATENT%20ONLY%20ELECTRIC&fq=campsite_type%3ATENT%20ONLY%20NONELECTRIC&fq=campsite_type%3AGROUP%20TENT%20ONLY%20AREA%20NONELECTRIC&';
        // }

        // campsiteEquipement = encodeURIComponent(campsiteEquipement)
        // var id = url.substring(url.lastIndexOf('/') + 1);
        // var startDate = data['check_in_date'].split("/").join("-");
        // var endDate = data['check_out_date'].split("/").join("-");

        // if (campsiteEquipement != '' && campsiteEquipement != '' && id != '' && startDate != '' && endDate != '') {
        // var campsiteURL = 'https://www.recreation.gov/api/search/campsites?fq=asset_id%3A' + id + campsiteType + ' &fq=campsite_equipment_name%3A' + campsiteEquipement + '&start_date=' + startDate + 'T00:00:00.000Z&end_date=' + endDate + 'T00:00:00.000Z';
        var campsiteURL = 'https://www.recreation.gov/api/search/campsites?fq=asset_id%3A233494&fq=campsite_equipment_name%3ATent&start_date=2021-06-13T00:00:00.000Z&end_date=2021-06-15T00:00:00.000Z';
                //https://www.recreation.gov/api/search/campsites?size=0&agg=campsite_type_of_use&fq=asset_id%3A233417&start=0
        var rateURL = 'https://www.recreation.gov/api/camps/campgrounds/233494/rates';
        var priceInfo = '';
        await axios.get(rateURL)
            .then(async response => {
                var data = response.data;
                priceInfo = (data.rates_list != 'undefined') ? '$'+Object.values(data.rates_list[0].rate_map)[0].per_night+" avg/night" : "";
            })

        // console.log(campsiteURL)
        await axios.get(campsiteURL)
            .then(async response => {
                var data = response.data;
                 console.log(data)
                if (data.total > 0) {
                    availability = true;
                    comment = "Available";
                    for (const cs of data.campsites) {
                        var titleInfo = '';
                        var attribute = '';
                        for (const attr of cs.attributes) {
                            attribute += attr.attribute_name + ": " + attr.attribute_value + ", ";
                        }
                        var titleInfo = 'Site: ' + cs.name + ', Loop: ' + cs.loop;
                        details.push({
                            'title': titleInfo,
                            'price': priceInfo,
                            'other': attribute,
                        });
                    }
                } else {
                    availability = false;
                    comment = "Not Available";
                }
            })
            .catch(async error => {
                // console.log(error);
                availability = false;
                comment = error.message;
            });
        // } else {
        //     availability = false;
        //     comment = 'Invalid Data';
        // }
    } catch (e) {
        availability = false;
        comment = e.message;
    }
    var result = {
        'availability': availability,
        'message': comment,
        'details': details,
    };

    console.log(JSON.stringify(result));
})();
