const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

var configPath = '../scrapping/temp/';
var filePathStore = 'scrapping/';

var url = 'https://www.reserveamerica.com/explore/alabama-coast-campground/PRCG/1066050/campsites?arrivalDate=2021-05-04&lengthOfStay=3&availStartDate=2021-05-04';
var url = 'https://www.reserveamerica.com/explore/cherry-creek-state-park/CO/50022/campsites?arrivalDate=2021-09-08&lengthOfStay=1&availStartDate=2021-09-08';
// var url = args.url.split("@@").join("&");
// var filename = args.file_name;
// console.log(url);
var bookingStatus = false;
var details = [];
var comment = '';

var form_siteCategory = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_vehicle = '';
var form_adults = '';
var form_login_username = 'dummayaccount@mailinator.com';
var form_login_password = 'Dummayaccount@123';

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        console.log(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}


async function getEnquiryData() {
    // let rawdata = fs.readFileSync(configPath + filename);
    // var data = JSON.parse(rawdata);

    form_siteCategory = 'RV sites';
    form_equipmentType = 'Fifth Wheel';
    form_equipmentLength = '10';
    form_vehicle = 'No';
    form_adults = '2';
}

(async () => {

    await getEnquiryData();

    try {
        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
        });
        console.log(await browser.version());
        console.log(await dateTime() + ":" + await browser.version());
        // open a new page

        var page = await browser.newPage();

        try {

            await page.setViewport({width: 1366, height: 768});

            await page.setDefaultNavigationTimeout(0);

            let browseUrl = url;
            //    console.log(browseUrl);
            console.log(await dateTime() + ":" + 'Nevigating : ' + browseUrl);
            await page.goto(browseUrl, {waitUntil: 'networkidle2'});
            await sleep(1000);
            if (await page.$('#sysError')) {
                let element = await page.$('.pageerror');
                let errorMsg = await page.evaluate(el => el.textContent, element)
                comment = errorMsg;
                bookingStatus = false;
                console.log(await dateTime() + ":" + errorMsg);

            } else {

                if (await page.$('#lookingfor')) {

                    var val = await getSelctBoxOptionValue('#lookingfor', form_siteCategory, page);

                    await page.select('#lookingfor', val);
                    if (val != '') {
                        await sleep(1000);
                        if (await page.$('.actionButton  button')) {
                            await page.click('.actionButton  button')
                            await sleep(1000);
                        }

                        await sleep(3000);
                        console.log(await dateTime() + ":" + "Scrolling page");
                        if (await page.$('#facility_map')) {
                            await page.evaluate(() => {
                                document.querySelector('#facility_map').scrollIntoView()
                            });
                        }
                        const bookTableTd = await page.evaluate(() => {
                            const rows = document.querySelectorAll('.table-responsive table tbody tr');
                            return Array.from(rows, row => {
                                const columns = row.querySelectorAll('td');
                                return Array.from(columns, column => column.innerText);
                            });
                        });
                        let getTdChild = 0;
                        if (bookTableTd && bookTableTd.length) {
                            for (let a = 0; a < bookTableTd.length; a++) {
                                let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');
                                if (getBookTr.length) {
                                    getTdChild = a + 1;
                                    break;
                                }
                            }
                            if (getTdChild) {
                                console.log(await dateTime() + ":" + "Click on book button");
                                await page.click('.table-responsive table tbody tr:nth-child(1) td:nth-child(7) button');
                                await sleep(2000);
                                var priceInfo = await page.evaluate(async () => {
                                    priceInfo = '';
                                    if(document.querySelector('.product-details__amount') !== null)
                                        priceInfo = document.querySelector('.product-details__amount').textContent+' avg/night';
                                    return priceInfo;
                                });
                                await page.goBack();
                                await sleep(2000);

                                for (let a = 0; a < bookTableTd.length; a++) {
                                    let getBookTr = bookTableTd[a].filter(book => book == 'Book Now');
                                    
                                    if (getBookTr.length) {
                                        var titleInfo = bookTableTd[a][0] + " Loop: " + bookTableTd[a][1];
                                        var maxpepople = (bookTableTd[a][3]) ? bookTableTd[a][3] : '-';
                                        var equipementLength = (bookTableTd[a][4]) ? bookTableTd[a][4] : '-';
                                        var otherInfo = "site type: " + bookTableTd[a][2] + " , Max # People: " + maxpepople + " , Equipment Length: " + equipementLength;
                                    
                                        details.push({
                                            'title': titleInfo,
                                            'price': priceInfo,
                                            'other': otherInfo,
                                        });
                                    }
                                }
                                if (await page.$(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`)) {
                                    console.log(await dateTime() + ":" + "Click on book button");
                                    await page.click(`.table-responsive table tbody tr:nth-child(${getTdChild}) td:nth-child(7) button`);
                                    await sleep(3000);

                                    console.log(await dateTime() + ":" + "Scrolling page");
                                    await page.evaluate(() => {
                                        document.querySelector('.quickbook__datepager').scrollIntoView()
                                    });
                                    await sleep(1000);

                                    await page.click('.quickbook button');
                                    await sleep(3500);

                                    if (await page.$('#emailGroup input.TextBoxRenderer')) {
                                        console.log(await dateTime() + ":" + "Waiting for login page ");
                                        console.log(await dateTime() + ":" + "Storing email")
                                        const email = await page.waitForSelector("#emailGroup input.TextBoxRenderer");
                                        await page.click("#emailGroup input.TextBoxRenderer");
                                        await email.type(form_login_username);
                                    }

                                    if (await page.$('.PasswordBoxRenderer')) {
                                        console.log(await dateTime() + ":" + "Storing Password")
                                        const password = await page.waitForSelector(".PasswordBoxRenderer");
                                        await page.click(".PasswordBoxRenderer");
                                        await password.type(form_login_password);
                                        await sleep(3000)
                                    }

                                    if (await page.$('#signinbutton button')) {
                                        await page.click('#signinbutton button')
                                        await sleep(3000)
                                    }
                                    await sleep(2000);
                                    // if (await page.$(".col-md-9 div:nth-child(2)")) {

                                    console.log(await dateTime() + ":" + "Scrolling EQUIPMENT section");
                                    // await page.evaluate(() => {
                                    //     document.querySelector('.col-md-9 div:nth-child(2)').scrollIntoView()
                                    // });
                                    await sleep(1000);
                                    // page.on('console', msg => {
                                    //     for (let i = 0; i < msg._args.length; ++i)
                                    //         console.log(`${msg._args[i]}`)
                                    // });
                                    if (await page.$('select#equipmentType')) {
                                        console.log(await dateTime() + ":" + "select EQUIPMENT type");

                                        var val = await getSelctBoxOptionValue('#equipmentType', form_equipmentType, page);
                                        await page.select('#equipmentType', val);
                                        console.log(await page.select('#equipmentType', val))
                                        await sleep(1000);
                                    }
                                    await sleep(1000);
                                    // await page.evaluate(() => {
                                    //     document.querySelector('.col-md-9 div:nth-child(2) > div:nth-child(4)').scrollIntoView()
                                    // });
                                    await sleep(1000);

                                    if (await page.$('#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input')) {
                                        console.log(await dateTime() + ":" + "Selecting Equipment Length");
                                        const equipLength = await page.waitForSelector("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                        await page.click("#container > div > main > div > div > div > div > div > div > div.col-md-9.col-xs-12 > div > div > div > div > div.content > div > div > div > div > div > div > div.form-group >input");
                                        await equipLength.type(form_equipmentLength);
                                    }

                                    if (await page.$('#numOfVehicles')) {
                                        console.log(await dateTime() + ":" + "Selecting vehicles");
                                        const numOfVehicle = await page.waitForSelector("#numOfVehicles");
                                        await page.click("#numOfVehicles");
                                        await numOfVehicle.type(form_vehicle);
                                    }

                                    if (await page.$('#numOfOccup1')) {
                                        console.log(await dateTime() + ":" + "Storing Free");
                                        const numOfOccup1 = await page.waitForSelector("#numOfOccup1");
                                        await page.click("#numOfOccup1");
                                        await numOfOccup1.type(form_adults)
                                    } else {
                                        if (await page.$('#numOfOccupants')) {
                                            const numOfOccup1 = await page.waitForSelector("#numOfOccupants");
                                            await page.click("#numOfOccupants");
                                            await numOfOccup1.type(form_adults);
                                        }
                                    }
                                    await sleep(1000)

                                    if (await page.$('#agreementCheck')) {
                                        console.log(await dateTime() + ":" + "Selecting Tearms and Condition");
                                        await page.click("#agreementCheck");
                                        await sleep(1000)
                                    }


                                    console.log(await dateTime() + ":" + "Clicking on Continue to Cart button");
                                    await sleep(1500);
                                    await page.click('.btn-success')
                                    await sleep(3000);
                                    if (await page.$('.alert-danger') !== null) {
                                        let element = await page.$('.alert-danger');
                                        let errorMsg = await page.evaluate(el => el.textContent, element)
                                        comment = errorMsg + "" + errorMsgVal;
                                        bookingStatus = false;
                                        console.log(await dateTime() + ":" + errorMsg);
                                    } else {
                                        comment = 'Available';
                                        bookingStatus = true;
                                        // await page.evaluate(() => {
                                        //     document.querySelector('.btn-success').scrollIntoView()
                                        // });

                                        // console.log(await dateTime() + ":" + "Waiting for review section");
                                        // await page.click('.btn-success')
                                        // await sleep(3000);

                                        // console.log(await dateTime() + ":" + "Waiting for Checkout section");
                                        // await page.evaluate(() => {
                                        //     document.querySelector('.encapsulate').scrollIntoView()
                                        // });

                                        // var name = form_name_on_credit_card.split(' ');

                                        // if (await page.$('#firstNamePRCG')) {
                                        //     console.log(await dateTime() + ":" + "Storing First Name");
                                        //     const firstname = await page.waitForSelector("#firstNamePRCG");
                                        //     await page.click("#firstNamePRCG");
                                        //     await firstname.type(name[0])
                                        // }

                                        // if (await page.$('#lastNamePRCG')) {
                                        //     console.log(await dateTime() + ":" + "Storing Second Name");
                                        //     const lastname = await page.waitForSelector("#lastNamePRCG");
                                        //     await page.click("#lastNamePRCG");
                                        //     await lastname.type(name[1])
                                        // }

                                        // if (await page.$('#cardTypePRCG')) {
                                        //     console.log(await dateTime() + ":" + "Selecting Card Type");
                                        //     await page.select('#cardTypePRCG', form_creditCardType)
                                        // }

                                        // if (await page.$('#cardNumberPRCG')) {
                                        //     console.log(await dateTime() + ":" + "Storing card number ");
                                        //     const cardNumber = await page.waitForSelector("#cardNumberPRCG");
                                        //     await page.click("#cardNumberPRCG");
                                        //     await cardNumber.type(form_creditCard)
                                        // }

                                        // if (await page.$('#expMonthPRCG')) {
                                        //     console.log(await dateTime() + ":" + "Selecting month");
                                        //     await page.select('#expMonthPRCG', form_creditCardExpMonth);
                                        // }

                                        // if (await page.$('#expYearPRCG')) {
                                        //     console.log(await dateTime() + ":" + "Selecting Year");
                                        //     await page.select('#expYearPRCG', form_creditCardExpYear);
                                        // }

                                        // console.log(await dateTime() + ":" + "Scrolling to acknowledgement checkbox");
                                        // await page.waitForSelector(".checkbox input");
                                        // await page.evaluate(() => {
                                        //     document.querySelector('.checkbox input').scrollIntoView()
                                        // });

                                        // await sleep(1000)
                                        // if (await page.$('#acknowledgement')) {
                                        //     console.log(await dateTime() + ":" + "Selecting acknowledgement");
                                        //     await page.click("#acknowledgement");
                                        //     await sleep(1000)
                                        // }

                                        // console.log(await dateTime() + ":" + "Clicking on Continue to Cart button");
                                        // await page.click('.btn-success')

                                        // await sleep(1000)

                                        // if (await page.$(".alert-danger")) {
                                        //     console.log(await dateTime() + ":" + "Selecting acknowledgement");
                                        //     let element = await page.$('.alert-danger');
                                        //     let errorMsg = await page.evaluate(el => el.textContent, element)
                                        //     // comment = errorMsg;
                                        //     comment = 'Booking done for this campground';
                                        //     bookingStatus = true;

                                        //     console.log(await dateTime() + ":" + errorMsg);
                                        //     browseClose = false;
                                        // } else {
                                        //     bookingStatus = true;
                                        //     comment = "Booking done for this campground";
                                        //     console.log(await dateTime() + ":" + "Order Confirm");
                                        //     browseClose = false;
                                        // }
                                    }
                                    // } else {
                                    //     bookingStatus = false;
                                    //     comment = "Sometghing went wrong when click on button.";
                                    // }
                                } else {
                                    bookingStatus = false;
                                    comment = "Not Available";
                                    console.log(await dateTime() + ":" + "Not Available");
                                }
                            } else {
                                bookingStatus = false;
                                comment = "Not Available";
                                console.log(await dateTime() + ":" + "Not Available");
                            }
                        } else {
                            bookingStatus = false;
                            comment = "Not Available";
                            console.log(await dateTime() + ":" + "Not Available");
                        }

                    } else {
                        comment = "No Site Category Found";
                        bookingStatus = false;
                        console.log(await dateTime() + ":" + comment);
                    }
                } else {
                    comment = "Not Available";
                    bookingStatus = false;
                }

            }
        } catch (err) {
            console.log(await dateTime() + ":" + "Error Occurs While Checking Availablity")
            comment = 'Not Available';
            bookingStatus = false;
            console.log(err.message)
            // console.log(err)
        }

    } catch (err) {
        console.log(err);
        bookingStatus = false;
        console.log(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = err.message;
    }

    // await browser.close();

    var result = {
        'availability': bookingStatus,
        'message': comment,
        'details': details
    };

    console.log(JSON.stringify(result))

})();


async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function log(txt) {
    // console.log(txt);
    // console.log('*************************');

}
async function getSelctBoxOptionValue(selector, title, page) {

    var value = await page.evaluate(async (title, selector) => {
        const example = document.querySelector(selector);
        const example_options = example.querySelectorAll('option');

        for (var i = 0; i < example_options.length; i++) {
            // console.log(example_options[i].text + '==' + title)
            if (example_options[i].text == title) {
                // console.log(example_options[i].value)
                return example_options[i].value;
            }
        }
        return '';
    }, title, selector);
    return value;
}