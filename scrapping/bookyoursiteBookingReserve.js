// Manually run commnad for Reference
// node scrapping/bookyoursiteBookingReserve.js --campground_id={campground_id} --booking_id={booking_enquiry_id}
// node scrapping/bookyoursiteBookingReserve.js --campground_id=4343 --booking_id=17

const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path');
const { resolve } = require("path");

//Prod
var configPath='../../../../../scrapping/';
var filePathStore='../scrapping/';
//Local
// var configPath = '../scrapping/';
// var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
const db = require(configPath + "models");
const CampGround = db.CampGround;
const BookingEnquiry = db.BookingEnquiry;

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var closeBrowser = true;
var bookingStatus = true;

var logInEmail = "qrnlgmdytsouvpicre@twzhhq.online";
var logInPassword = "Hello@007";

var form_siteCategory = '';
var form_checkIn = '';
var form_checkOut = '';
var form_group = '';
var form_inFants = '';
var form_adults = '';
var form_kids = '';
var form_pets = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_name_on_credit_card = '';
var form_slideOuts = '';
var form_VkrPostalCode = '';
var form_purchaseVkr_Group = '';
var form_firstName = '';
var form_lastName = '';
var form_address1 = '';
var form_address2 = '';
var form_countryCode = '';
var form_phone = '';
var form_city = '';
var form_state = '';
var form_postcode = '';
var form_vehicles = '';
var form_email = '';
var form_referal_resource = '';
var form_reason_visit = '';
var form_name_on_credit_card = "Testing user";
var form_creditCard = '4242424242424242';
var form_creditCardType = '';
var form_creditCardExpMonth = '02';
var form_creditCardExpYear = '34';
var form_creditCardCode = '111';
var comment = '';
var debugLog = [];

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getCampGroundByID() {
    debugLog.push(await dateTime() + ":" + 'Fetching campground detail by id');
    await CampGround.findByPk(campgroundId)
        .then(data => {

            siteUrl = data['url'];

        })
        .catch(async err => {
            bookingStatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground By Id');
            console.log('Error in Fetching Campground By Id');
        });
}

async function getDateValues(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var monthName = checkDate.toLocaleString("default", { month: "long" })
    var dayNumber = checkDate.getDate();
    var year = checkDate.getFullYear();

    var dateArr = new Array();

    dateArr['month'] = monthName;
    dateArr['year'] = year;
    dateArr['day'] = dayNumber;
    dateArr['month_year'] = monthName + " " + year;
    dateArr['original_date'] = dateVal;
    // var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateArr;
}

async function getEnquiryDataID() {
    debugLog.push(await dateTime() + ":" + 'Fetching booking enquiry detail by id');
    await BookingEnquiry.findByPk(bookingId)
        .then(async data => {

            // form_siteCategory = "Tent";
            form_checkIn = '2021-04-15';
            form_checkOut = '2021-04-20';
            // form_postcode = '35242';
            // form_countryCode = 'USA';
            // form_address1 = 'Alabama';
            // form_address2 = 'Anywhere';
            // form_city = 'Dothan';
            // form_state = 'AL';
            // form_vehicles = '2';
            // form_group = '1';
            form_equipmentLength = '15';
            form_equipmentType = "Tent";

            // data['details']=JSON.parse(data['details']);

            // form_siteCategory = data['details']['site_category'];
            // form_checkIn = await getDateValues(data['check_in_date']);
            // form_checkOut = await getDateValues(data['check_out_date']);
            // form_postcode = data['post_code'];
            // form_countryCode = data['country_code'];
            // form_address1 = data['address1'];
            // form_address2 = data['address2'];
            // form_city = data['city'];
            // form_state = data['state'];
            // form_vehicles = data['details']['vehicles'];
            // form_group = data['details']['groups'];
            // form_equipmentLength = data['details']['equipment_length'];
            // form_equipmentType = data['details']['equipment_type'];

        })
        .catch(async err => {
            bookingStatus = false;
            console.log(err.message)
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Enquiry By Id');
            console.log('Error in Fetching Enquiry By Id');
        });

    const enquiryUpdate = {
        index_status: 'InProgress',
        updated_at: Date.now(),
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })
}

async function convertDateFormat(indate, outdate) {
    var checkinDate = new Date(indate); // M-D-YYYY
    var checkoutDate = new Date(outdate); // M-D-YYYY

    var inmonthName = checkinDate.toLocaleString("default", { month: "long" })
    var indayNumber = checkinDate.getDate();
    var inyear = checkinDate.getFullYear();

    var outmonthName = checkoutDate.toLocaleString("default", { month: "long" })
    var outindayNumber = checkoutDate.getDate();
    var outyear = checkoutDate.getFullYear();

    var dateRange = inmonthName + " " + indayNumber + ", " + inyear + " - " + outmonthName + " " + outindayNumber + ", " + outyear;

    return dateRange;
}

async function clear(page, selector) {
    await page.evaluate(selector => {
        document.querySelector(selector).value = "";
    }, selector);
}

(async () => {
    try {

        await getCampGroundByID();
        await getEnquiryDataID();

        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });
        console.log('*************************');
        console.log(await browser.version());
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        try {
            // console.log('Nevigating : ' + campgrounds[p].title)
            console.log('Nevigating : ' + siteUrl)
            debugLog.push(await dateTime() + ":" + 'Nevigating : ' + siteUrl);
            await page.setViewport({ width: 1300, height: 768 });

            await page.setDefaultNavigationTimeout(0);

            var browseUrl = siteUrl;

            debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

            await page.goto(browseUrl);
            console.log('browseUrl:' + browseUrl)

            // page.on('console', msg => {
            //     for (let i = 0; i < msg._args.length; ++i)
            //         console.log(`${msg._args[i]}`)
            // });
            if (await page.$('.book-now-button')) {
                console.log('Clicking on book now buttton')
                await page.click('.book-now-button');

                await sleep(10000)

                if (await page.$('#accept-terms')) {
                    console.log('Accepting Terms and Conditions')
                    await page.click('#accept-terms');
                }

                if (await page.$('#q_date_range')) {
                    var dateRange = await convertDateFormat(form_checkIn, form_checkOut);
                    await clear(page,'#q_date_range');
                    const dateSelect = await page.waitForSelector("#q_date_range");
                    await dateSelect.type(dateRange)
                    await page.click("#availability-form > div > div > div > div > label");
                }

                await sleep(3000);
                
                if (await page.$('#q_unittype_id')) {
                    await page.select("select#q_unittype_id", "14962")
                    // await page.click("#q_unittype_id")
                    //   await page.evaluate((form_equipmentType) => {
                    //     $("#q_unittype_id option:contains("+form_equipmentType+")")[0].selected = true
                    //  },form_equipmentType)
                    //  await page.click("#q_unittype_id")
                }
                await sleep(1000);
                
                if (await page.$('#unittype_length')) {
                    await page.select("select#unittype_length", form_equipmentLength)
                    // await page.click("#unittype_length")
                    //   await page.evaluate((form_equipmentLength) => {
                    //     $("#unittype_length option:contains("+form_equipmentLength+")")[0].selected = true
                    //  },form_equipmentLength)
                    //  await page.click("#unittype_length")
                }
                await sleep(1000);
                
                if (await page.$('#show-availability')){
                    await page.click('#show-availability');
                }
                await sleep(5000);
                if (await page.$('.has-error .error-block')) {
                    let element = await page.$('.has-error .error-block');
                    let errorMsg = await page.evaluate(el => el.textContent, element)
                    console.log(errorMsg);
                }else{
                    if(await page.$('#sitetypes > div > div.bottom > div >ul.text-danger')){
                        let element = await page.$('#sitetypes > div > div.bottom > div >ul.text-danger');
                        let errorMsg = await page.evaluate(el => el.textContent, element)
                        console.log(errorMsg);
                    }else{  
                        
                        if(await page.$('#sitetypes > div:nth-child(1) > div.bottom > button.btn-primary.btn.reserve.pull-right')){
                            console.log('Campsite Found for reservation')

                            await page.click('#sitetypes > div:nth-child(1) > div.bottom > button.btn-primary.btn.reserve.pull-right')
                            
                            await sleep(3000)

                            

                        }else{
                            console.log('Campsite Not Found for reservation')
                        }
                        
                    }    
                }

            } else {
                console.log('Booking for this website not found')
            }
            // console.log(calenderMonth);
        } catch (err) {
            // await browser.close();
            debugLog.push(await dateTime() + ":" + "Sorry, an error has ocurred ")
            comment = 'Sorry, an error has ocurred';
            bookingStatus = false;
            console.log(err)
        }
        // await browser.close();

    } catch (err) {
        console.log(err);
        bookingStatus = false;
        // await browser.close();
        debugLog.push(await dateTime() + ":" + "Something Went Wrong")
        comment = 'Something Went Wrong';
        console.log("Browser Closed");
    }
    //    await storeBookingCommentAndLog();

})();

async function storeBookingCommentAndLog() {

    fs.writeFileSync(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json", JSON.stringify(debugLog));
    const filepath = path.resolve(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json");

    var s3result;
    var crawlerLogfilename = new Date().getTime() + '_booking_log.json';

    // var CampLogfilename=datasourceId+'_crawler_log.json';
    debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

    s3result = await s3.uploadFile(filepath, 'booking/' + campgroundId + '/' + bookingId + '/' + crawlerLogfilename);

    console.log(s3result)
    if (s3result != '') {
        console.log("successfully uploaded Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Successfully uploaded Booking Crawler log file on server")
        fs.unlinkSync(filepath)
    } else {
        crawlerLogfilename = '';
        console.log("Failed to upload Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Failed to upload Booking Crawler log file on server")
        crawlerstatus = false;
    }

    debugLog.push(await dateTime() + ":" + "Storing Comment And Update the Booking Enquiry Status")

    const enquiryUpdate = {
        index_status: 'Completed',
        comment: comment,
        log_file_path: crawlerLogfilename,
        updated_at: Date.now(),
        status: (bookingStatus == true) ? 'Success' : 'Failed',
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })

}

async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}
