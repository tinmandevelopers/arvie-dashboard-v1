const express = require('express');
const fs = require("fs");
const path = require('path');
const s3 = require(path.resolve("scrapping/helper/s3imageUpload"));
const args = require('yargs').argv;
const puppeteer = require("puppeteer");
const chalk = require("chalk");
const error = chalk.bold.red;
const success = chalk.keyword("green");
const log = console.log;

var debugLog = [];
var campGroundArr = [];
var crawlerstatus = true;
var datasourceId = args.datasource_id;
var campLogId = 0;
var crawlerLogId = 0;
var campID = 0;
var siteUrl = '';
var statusVal = args.status;
if (statusVal != undefined) {
    console.log('Status Argument Value : ' + statusVal)
    if (statusVal != 'pending') {
        statusVal = '';
    }
} else {
    statusVal = '';
}
console.log('Status Value Is :' + statusVal)

var app = express();

// Database Configuration
const db = require(path.resolve("scrapping/models/index.js"));
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const DataSource = db.DataSource;
const State = db.State;

(async () => {
    try {
        var browser = await puppeteer.launch({
            headless: true,
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });

        // open a new page
        var page = await browser.newPage();
        await page.setDefaultNavigationTimeout(0);

        debugLog.push("Start Time: " + await dateTime());
        console.log("Start Time: " + await dateTime());

        const dataSourceData = await getDatasourceByID();

        const storeLog = await storeCrawlerLog();

        if (statusVal == '') {
            const stateNames = await fetchAllState(page);

            await storeCampGrounds();
        }
        var result = await scrapeAll(page, async function (returnValue) {
            await browser.close();
            await storeCrawlerLogFile();
            debugLog.push(await dateTime() + ":" + "Browser Closed");
            console.log(success("Browser Closed"));
        });

        console.log("total Campground is" + campGroundArr.length);

    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        console.log(error(err));
        debugLog.push(await dateTime() + ":" + err);
        await browser.close();
        console.log(error("Browser Closed"));
        debugLog.push(await dateTime() + ":" + "Browser Closed");
        await storeCrawlerLogFile()
    }
    // console.log('Debug Log is');
    // console.log(await dateTime() + ":" + debugLog);
})();

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getDatasourceByID() {
    await DataSource.findByPk(datasourceId)
        .then(data => {
            siteUrl = data['url']
        })
        .catch(async err => {
            debugLog.push(await dateTime() + ":" + 'Error in Fetching DataSource By Id');
        });
}

async function storeCrawlerLog() {
    let response = [];
    const logData = {
        data_source_id: datasourceId,
        status: 'InProgress',
    };
    debugLog.push(await dateTime() + ":" + 'Storing Crawler Log');
    DataSourceCrawlerLog.create(logData)
        .then(async data => {

            response['status'] = 'success';
            response['content'] = data;
            // return data;
            crawlerLogId = data['id'];
            debugLog.push(await dateTime() + ":" + 'Crawler Log Stored Successfully');
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in crawler log storing');
            debugLog.push(await dateTime() + ":" + err.message);

            response['status'] = 'error';
            response['content'] = err;
            // return err;
            // console.log(err.message)
        });
    return response;
}

async function storeCampGrounds() {
    try {
        // console.log(campdata[0].camp_group)
        debugLog.push(await dateTime() + ":" + 'In store campground function');
        for (var key in campGroundArr) {
            // console.log('in key:' + key)
            // console.log(campGroundArr[key]['camp_group'])
            var gname = campGroundArr[key]['camp_group'];
            var cntname = campGroundArr[key]['country_name'];
            var groupurl = campGroundArr[key]['camp_group_url'];

            const campGroup = {
                name: gname,
                data_source_id: datasourceId,
                country: cntname,
                url: groupurl
            };

            const camp = campGroundArr[key]['campground'];

            // console.log("camp is ")
            // console.log(campGroundArr[key])
            // console.log(campGroundArr[key]['campground'])
            // debugLog.push(dateTime() + ":" + 'checking campgroup is added or not');
            await CampGroundGroup
                .findOne({ where: campGroup })
                .then(function (obj) {
                    // update
                    if (obj) {
                        return obj.update(campGroup)
                            .then(async data => {
                                for (const campkey in camp) {
                                    // console.log(camp[campkey]['url'])
                                    const cont = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url']
                                    };
                                    // debugLog.push(dateTime() + ":" + 'checking campground is added or not');
                                    await CampGround.findOne({ where: cont })
                                        .then(async function (obj) {
                                            // debugLog.push(dateTime() + ":" + 'updating campground ');
                                            const campdata = {
                                                data_source_id: datasourceId,
                                                campground_group_id: data['id'],
                                                title: camp[campkey]['title'],
                                                url: camp[campkey]['url'],
                                                status: 'Pending',
                                                index_status: 'Pending',
                                                last_indexed_time: Date.now(),
                                            };
                                            if (obj) {
                                                obj.update(campdata)
                                                    .then(async data => {
                                                    })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Updating Already Stored Campground');
                                                    });
                                            } else {
                                                await CampGround.create(campdata)
                                                    .then(async data => {
                                                    })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Creating Already Stored Campground');
                                                    });
                                            }
                                        })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            debugLog.push(await dateTime() + ":" + 'Error in Checking if Capground Already Stored or Not');
                                        });
                                }
                            })
                            .catch(async err => {
                                console.log(err)
                                crawlerstatus = false;
                                debugLog.push(await dateTime() + ":" + 'Error in Updating Campground');
                            });

                    } else {
                        // insert
                        // debugLog.push(dateTime() + ":" + ' campgroup creating');
                        return CampGroundGroup.create(campGroup, camp)
                            .then(async data => {
                                // debugLog.push(dateTime() + ":" + ' campgroup created');
                                for (var campkey in camp) {
                                    // console.log('in campground')
                                    const campground = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url'],
                                        status: 'Pending',
                                        index_status: 'Pending',
                                        last_indexed_time: Date.now(),
                                    };
                                    // debugLog.push(dateTime() + ":" + ' campground creating');
                                    await CampGround.create(campground)
                                        .then(async data => {
                                        })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            console.log('Error in Creating New Campground');
                                        });
                                }
                            })
                            .catch(async err => {
                                crawlerstatus = false;
                                console.log('Error in Storing New Campground');
                                console.log(err)
                                debugLog.push(await dateTime() + ":" + 'Error in Storing New Campground');
                            });
                    }
                })
                .catch(async err => {
                    crawlerstatus = false;

                    console.log(await dateTime() + ":" + 'Error in Storing Campground')
                    console.log(err)
                    debugLog.push(await dateTime() + ":" + 'Error in Storing Campground');
                });
        }
    } catch (e) {
        console.log(e)
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing All Campgrounds and Campground Groups');
    }
}

async function storeCrawlerLogFile() {
    try {
        fs.writeFileSync(path.resolve("scrapping/temp/crawler") + datasourceId + ".json", JSON.stringify(debugLog));
        const filepath = path.resolve("scrapping/temp/crawler") + datasourceId + ".json";

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFile(filepath, 'data_source/' + datasourceId + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Crawler log file to server");
            fs.unlinkSync(filepath)
        } else {

            crawlerLogfilename = '';
            console.log("Failed to upload Crawler log file to server");
        }

        const crawlerUpdate = {
            log_file_path: 'Log:' + crawlerLogfilename,
            status: (crawlerstatus == true) ? 'Success' : 'Failed',
            updated_at: Date.now(),
        };
        DataSourceCrawlerLog.update(crawlerUpdate, {
            where: { id: crawlerLogId }
        })
        var statusVal = (crawlerstatus == true) ? 'Success' : 'Failed';
        const dataSourceUpdate = {
            crawler_log: statusVal + ':' + crawlerLogfilename,
        };
        DataSource.update(dataSourceUpdate, {
            where: { id: datasourceId }
        })
    } catch (e) {
        console.log('Error in Updating DataSource and Storing Crawler File')
        console.log(e)
    }
}

var campArr = [];
async function fetchAllState(page) {
    try {
        await State.findAll()
            .then(state => {
                for (var l = 0; l < state.length; l++) {
                    var res = state[l]['state_name'].trim();
                    var stateStr = res.replace(" ", "+");

                    var campGroupUrl = siteUrl + 'campgrounds/United%20States%20Of%20America/' + stateStr;
                    campGroundArr[l] = {
                        'country_name': 'United States',
                        'camp_group': res,
                        'camp_group_url': campGroupUrl,
                        'campground': [],
                    }
                }
            })
            .catch(err => {
                console.log(err);
            });
        //        console.log(campGroundArr);
        for (var l = 0; l < campGroundArr.length; l++) {
       
            lastTimeFetch = false;
            var campground = await fetchCampgroundList(page, campGroundArr[l]['camp_group_url'], campGroundArr[l]['camp_group'], siteUrl);
            //            console.log(campground)
            //            console.log('final campground')
            campGroundArr[l]['campground'] = campground;
            console.log('added campground')
        }
        return campGroundArr;
        // console.log(stateArr);
    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in FetchGround States');
        console.log(err);
        // await browser.close();
        // console.log("Browser Closed");
        return [];
    }
}

var totalCamp = 0;
async function fetchCampgroundList(page, browseUrl, campState, siteUrl, start = 0) {
    try {

        console.log('URL is : ' + browseUrl);
        console.log('Navigating...');

        await page.goto(browseUrl), { waitUntil: 'networkidle2' };

        debugLog.push(await dateTime() + ":" + 'Navigating...' + browseUrl);
        debugLog.push(await dateTime() + ":" + 'Fetching CampGroup and Campground data');

        // page.on('console', msg => {
        //     for (let i = 0; i < msg._args.length; ++i)
        //         console.log(`${msg._args[i]}`)
        // });

        var campgroundData = await page.evaluate(({ siteUrl }) => {
            var campList = document.querySelectorAll('#catalogue-content > div > .row > .col-sm-4 > .campground > .info > h4 > a');

            console.log('length > ' + campList.length)

            var campval = [];
            for (var l = 0; l < campList.length; l++) {
                var camp_url = campList[l] && campList[l].getAttribute("href");

                campval.push({
                    'title': campList[l] && campList[l].innerText.trim(),
                    'url': (siteUrl + camp_url)
                });
            }
            return campval;
        }, { siteUrl });

        debugLog.push(await dateTime() + ":" + 'Fetched Campground List');
        debugLog.push(await dateTime() + ":" + 'Go to store data of campground');

        return campgroundData;
    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground List');

        console.log("Error in FetchGround List");
        console.log(err);
        return campArr;
    }
}

async function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
    //  will generate -- data

    //text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
    // used for - To avoid multiple sequential hyphens
}

async function campground_details() {
    try {
        let data;
        let descriptionArray = [];
        let accomodationArray = [];
        let telephoneArray = [];
        let labelArray = [];
        let reservePhone = '';
        let infoPhone = '';
        let seasonInfo = '';
        let address = '';
        let space = '';
        var descriptionArr = [];

        var name = await document.querySelector('#location-name span').textContent;
        var descriptionTag = await document.querySelectorAll('#description');
        var optionsTag = await document.querySelectorAll('#sites .site');
        var email = (typeof document.querySelector('#contact a[href^="mailto:"]') != 'undefined' && document.querySelector('#contact a[href^="mailto:"]') != null) ? await document.querySelector('#contact a[href^="mailto:"]').getAttribute('href').replace("mailto:", "") : '';
        var mapTag = await document.querySelector('#map');

        var mapLink = mapTag.getAttribute('style').replace('background-image: url("', '').replace('");', '');
        var mapImage = '';

        var seasonData = await document.querySelectorAll('#posted-rates table tr');
        for (var seasons of seasonData) {
            seasonInfo = seasonInfo + seasons.querySelector('tr th').textContent + '(' + seasons.querySelector('tr td').textContent + '),';
        }

        infoPhone = await document.querySelector('#contact a[href^="tel:"]').getAttribute('href').replace("tel:", "");
        address = await document.querySelector('#contact a[href^="https://maps"]').textContent;
        //return labelArray;

        if (typeof descriptionTag.querySelectorAll != 'undefined')
            descriptionArr = descriptionTag.querySelectorAll('p');
        for (var description of descriptionArr) {
            var desc = description.textContent;
            descriptionArray.push(desc.trim("\t"));
        }

        for (var accomodation of optionsTag) {
            accomodationArray.push(accomodation.textContent);
        }
        //Types of accomodation
        var slug = name
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;
        data = {
            name: name,
            slug: slug,
            description: descriptionArray,
            data: {
                amenities: [],
                recreation: {},
                local_attractions: [],
                rules_policies: [],
                deals: [],
            },
            contact: {
                physical_address: address.trim(),
                season_dates: seasonInfo,
                telephone_no: infoPhone,
                reserver_no: reservePhone,
                email: email,
                map_image: mapImage,
                map_link: mapLink,
            },
            info: '',
            images: [],
            accomodation_types: accomodationArray
        };
        // amenitiesArr.push(amenity.textContent);
        //  data['name'] = name;
        // data['amenities'] = amenitiesArr;
        return data;
    } catch (e) {
        crawlerstatus = false;
        console.log(e.message)
        debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground Details');
    }
}

async function funamenities() {
    try {
        const data = [];
        let amenitiesArr = [];

        //  let amenities = [];
        let amenities = await document.querySelectorAll('#amenities div.name');

        //let amenities =  await amenitiesUL.querySelectorAll('li.col-sm-6');

        for (var amenity of amenities) {
            amenitiesArr.push(amenity.textContent);
        }

        return amenitiesArr;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Amenity Details');
    }

}

async function funattractions() {
    try {
        let attraction = {};
        let attractionList = new Array();

        var attractionListTag = await document.querySelectorAll('#nearbies div.name');

        for (var attractionTag of attractionListTag) {
            attractionList.push(attractionTag.textContent);
        }

        return attractionList;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Attraction Details');
    }
}

async function funrulepolicy() {
    try {
        let rulePolicy = '';

        rulePolicy = await document.querySelector('.list-wrapper').innerHTML;

        return rulePolicy;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Rules and Policy Details');
    }
}

async function scrapeAll(page, callback) {
    try {
        var campgroundStatus = true;
        debugLog.push(await dateTime() + ":" + 'In Fetching campground detail.');
       
        var result = [];
        var successCamp = 0;
        var failedCamp = 0;
        await page.exposeFunction('uploadFileTos3', s3.uploadImage);
        CampGround.findAll({ where: { data_source_id: datasourceId } })
            .then(async data => {
                console.log("We got " + data.length + " campground(s) to scrape");
                for (var p = 0; p < data.length; p++) {
                    // if (data[p]['id'] == 4345) {
                        var crawlerLog = [];
                        try {
                            //                             console.log("title:"+data[p].title);
                            var campdata = data[p];
                            // console.log('title is '+ campdata['title'])
                            let campgrounds = '';
                            console.log(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");

                            debugLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                            crawlerLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");

                            campgrounds = campdata['id'];
                            var link = campdata['url'];
                            var title = campdata['title'];
                            console.log(await dateTime() + ": Scrap this > " + link)

                            debugLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                            crawlerLog.push(await dateTime() + ":" + "updating campground status to InProgress");

                            const campUpdate = {
                                index_status: "InProgress",
                            };
                            CampGround.update(campUpdate, {
                                where: { id: campdata['id'] }
                            }).then(data => {
                            })
                                .catch(async err => {
                                    crawlerstatus = false;
                                    console.log(err.message)
                                    debugLog.push(await dateTime() + ":" + 'Error in Updating CampGround Status To In Progress');
                                });


                            console.log(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            debugLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            crawlerLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            const campLogData = {
                                crawler_log_id: crawlerLogId,
                                campground_id: campdata['id'],
                                status: 'Pending',
                            };

                            CampgroundLog.create(campLogData)
                                .then(data => {
                                    campLogId = data['id'];
                                })
                                .catch(async err => {
                                    debugLog.push(await dateTime() + ":" + 'Error in New Creating CampGround Log');
                                    console.log(err.message)
                                });

                            debugLog.push(await dateTime() + ":" + "Nevigating..." + link);
                            await page.goto(link, { waitUntil: 'networkidle2' });

                            // page.on('console', msg => {
                            //     for (let i = 0; i < msg._args.length; ++i)
                            //         console.log(`${msg._args[i]}`)
                            // });

                            console.log(chalk.magenta("Navigating to " + title));
                            debugLog.push(await dateTime() + ":" + "Navigating to..." + title);

                            // await page.exposeFunction("uploadFileToS3", s3.uploadImage)
                            let campgroundDetails = await page.evaluate(campground_details);

                            // console.log(campgroundDetails);
                            debugLog.push(await dateTime() + ":" + "Navigating getting amenities");
                            crawlerLog.push(await dateTime() + ":" + "Navigating getting amenities");
                            console.log(chalk.yellow("Navigating for getting amenities"));
                            campgroundDetails['data']['amenities'] = await page.evaluate(funamenities);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for amenities");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for amenities");
                            //
                            // code for getting attractions
                            debugLog.push(await dateTime() + ":" + "Navigating for getting attractions");
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting attractions");
                            console.log(chalk.yellowBright("Navigating for getting attractions"));
                            campgroundDetails['data']['local_attractions'] = await page.evaluate(funattractions);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for attractions");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for attractions");

                            // code for getting images
                            // await page.goto(link + '/albums', {waitUntil: 'networkidle2'});
                            console.log(chalk.yellow("Navigating for getting Images"));
                            debugLog.push(await dateTime() + ":" + "Navigating for getting Images")
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting Images")

                            let imageList = new Array();

                            var images = await page.evaluate(async(datasourceId, campgrounds) => {
                                let imageList = new Array();
                                var imageListTag = await document.querySelectorAll('#image-carousel img');
                                var re = /(?:\.([^.]+))?$/;

                                for (var k = 0; k < imageListTag.length; k++) {
                                    var image_url = imageListTag[k].getAttribute('src');
                                    var nameArr = await image_url.split('/');
                                    var ext = await re.exec(image_url)[1];

                                    var slug_image = (ext != '') ? nameArr[nameArr.length - 1] : nameArr[nameArr.length - 1] +'.png';

                                    var obj = {
                                        image: await uploadFileTos3(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campgrounds + '/images/' + slug_image)
                                    }
                                    imageList.push(obj);
                                }
                                return imageList;
                            }, datasourceId, campgrounds);

                            campgroundDetails['images'] = images;

                            // Site details
                            var sitedetail = [];

                            console.log(chalk.yellow("Navigating for campsite Details"));
                            debugLog.push(await dateTime() + ":" + "Navigating for campsite Details")
                            crawlerLog.push(await dateTime() + ":" + "Navigating for campsite Details")
                            var bookButtonLink = await page.evaluate(async (datasourceId, campgrounds) => {
                                return (typeof document.querySelector('#title-bar > a.book-now-button') != 'undefined' && document.querySelector('#title-bar > a.book-now-button') != null) ? document.querySelector('#title-bar > a.book-now-button').getAttribute('href') : '';
                            });

                            if (bookButtonLink != '') {
                                await page.goto(bookButtonLink, { waitUntil: 'networkidle2' });
                                sitedetail = await page.evaluate(async (datasourceId, campgrounds) => {
                                    try {
                                    let elements = document.querySelectorAll("#sitetypes > .sitetype");
                                    var rvNameArr = ["RV", "RV Sites", "rv", "rv sites","rv park","rv park","RV Park","RV Resort","rv resort"];
                                    var lodgingNameArr = ["Lodging", "Lodging Sites", "lodging", "lodging sites"];
                                    var tentNameArr = ["Tent", "Tent Sites", "tent", "tent sites","Tenting","tenting",'Tents',"tents"];
                                   
                                    let data = [{ type: 'RV Sites', data: [] },
                                                { type: 'Lodging', data: [] },
                                                { type: 'Tent Sites', data: [] }
                                               ];
                                    for (var el = 0; el < elements.length; el++) {
                                        var obj = [];
                                        var campgroundName = title = elements[el].querySelector('#sitetypes > div > div > div.info > h3').textContent.trim();
                                        //                                        console.log(campgroundName);
                                        var summary = elements[el].querySelector('#sitetypes > div > div > div.info > .description').textContent;
                                        //                                        console.log('summary >' + summary);
                                        var image_url = elements[el].querySelector('#sitetypes > div > div > div.image.overflow-hidden > a').getAttribute('href');
                                        //                                        console.log('image_url' + image_url)
                                        var re = /(?:\.([^.]+))?$/;
                                        var nameArr = image_url.split('/');
                                        var ext = re.exec(image_url)[1];

                                        var slug_image = (ext != '') ? nameArr[nameArr.length - 1] : nameArr[nameArr.length - 1] + '.png';

                                        image_url = await uploadFileTos3(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campgrounds + '/images/campsites/' + slug_image)

                                        obj = {
                                            // campground: campgroundName,
                                            title: title,
                                            description: summary,
                                            images: image_url
                                        };
                                        var slug=campgroundName.split(" ");
                                        if (rvNameArr.includes(slug[0])) {
                                            data[0].data.push(obj);
                                        } else if (lodgingNameArr.includes(slug[0])) {
                                            data[1].data.push(obj);
                                        } else if (tentNameArr.includes(slug[0])) {
                                            data[2].data.push(obj);
                                        }
                                    }
                                    return data;
                                }catch(e){
                                   
                                    console.log(e.line)
                                }
                                }, campdata['data_source_id'], campdata['id']);

                            }
                            console.log(sitedetail)
                            // console.log( sitedetail['campsites'])
                            campgroundDetails['accomodation_types'] = sitedetail;

                            result.push(campgroundDetails);

                            fs.writeFileSync(path.resolve("scrapping/temp/output") + campdata['id'] + ".json", JSON.stringify(campgroundDetails));
                            const filepath = path.resolve("scrapping/temp/output" + campdata['id'] + ".json");
                            var s3result;
                            // var CampLogfilename=new Date().getTime()+'_campground_log.json';
                            var CampLogfilename = campdata['id'] + '_campground_log.json';
                            debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            crawlerLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            s3result = await s3.uploadFile(filepath, 'data_source/' + datasourceId + '/campgrounds/' + campgrounds + '/' + CampLogfilename);
                            // if(result.)
                            var logFileLocation = '';
                            console.log(s3result)
                            if (s3result != '') {
                                // logFileLocation = result['Location'];
                                logFileLocation = CampLogfilename;
                                fs.unlinkSync(filepath)
                                debugLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                                crawlerLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                            } else {
                                debugLog.push(await dateTime() + ":" + "error in upload file to server")
                                crawlerLog.push(await dateTime() + ":" + "error in upload file to server")
                            }

                            debugLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            crawlerLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            campgroundStatus = true;
                            
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, logFileLocation);
                            successCamp++;
                        } catch (e) {
                            console.log(e.message)
                            failedCamp++;
                            debugLog.push(await dateTime() + ":" + 'Error in Scrapping Individual Campground');
                            crawlerstatus = false;
                            campgroundStatus = false;
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '');
                        }

                    // }
                }
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);
                if (typeof callback === 'function') {
                    callback(result);
                }
                //            callback(result)
                // return result;

            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + 'Error in fetching All Campground Based on Datasource');
                crawlerstatus = false;
                console.log(err.message);
                if (typeof callback === 'function') {
                    callback(err);
                }
            });
    } catch (e) {
        debugLog.push(await dateTime() + ":" + 'Error in Scrap all Detail');
        crawlerstatus = false;
        console.log(e.message)
        callback(e)
    }
}

async function storeCampGroundLogFile(crawlerLog, camp_id, campStatus, logFileLocation) {
    try {
        const campUpdate = {
            data_file_path: logFileLocation,
            index_status: 'Completed',
            crawler_status: (campStatus == true) ? 'Success' : 'Failed',
        };
        CampGround.update(campUpdate, {
            where: { id: camp_id }
        })

        debugLog.push(await dateTime() + ":" + "updating campground log status and set log file path")
        crawlerLog.push(await dateTime() + ":" + "updating campground log status and set log file path")

        fs.writeFileSync(path.resolve("scrapping/temp/camp_crawler") + camp_id + ".json", JSON.stringify(crawlerLog));
        const filepath = path.resolve("scrapping/temp/camp_crawler") + camp_id + ".json";

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_campground_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFile(filepath, 'data_source/' + datasourceId + '/campgrounds/' + camp_id + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Successfully uploaded Campground Crawler log file on server")

            fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Failed to upload Campground Crawler log file on server")
            crawlerstatus = false;
        }

        const campLogDataUpdate = {
            status: (campStatus == true) ? 'Success' : 'Failed',
            log_file_path: crawlerLogfilename,
        };
        CampgroundLog.update(campLogDataUpdate, {
            where: { id: campLogId }
        })
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing CampGround Log/CrawlerLog File');
    }
}