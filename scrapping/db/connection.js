var mysql = require('mysql');
let dbconnected=false;
var connection = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "root",
    database: "arvie_booking"
});

connection.connect(function(err) {
    if (err) {
        dbconnected=false;
        console.error('error connecting: ' + err.stack);
        // return;
    }else {
        dbconnected=true;
    }

    console.log('connected as id ' + connection.threadId);
});
module.exports = {
    dbconnected,
};
