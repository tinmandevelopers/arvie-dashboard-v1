var express = require('express');
var fs = require('fs');

var app = express();


const puppeteer = require("puppeteer");
const chalk = require("chalk");
var fs = require("fs");
var s3 = require("../helper/s3imageUpload");
// MY OCD of colorful console.logs for debugging... IT HELPS
const error = chalk.bold.red;
const success = chalk.keyword("green");
const log = console.log;




//app.get('/scrape', function (req, res) {

(async () => {
    try {

        var browser = await puppeteer.launch({headless: false,
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });


        console.log(await browser.version());

        // open a new page
        var page = await browser.newPage();

        await page.setDefaultNavigationTimeout(0);

        var siteUrl = 'https://koa.com';
        var browseUrl = siteUrl + '/campgrounds/';

        console.log('Navigating...');

        var campgroundLinks = [];
       
         await page.goto(browseUrl);
         
         
         var campgroundLinks = await page.evaluate(({siteUrl}) => {
         var titleNodeList = document.querySelectorAll('a.link-action3');
         var titleLinkArray = [];
         
         for (var i = 0; i < titleNodeList.length; i++) {
         titleLinkArray[i] = {
         title: titleNodeList[i] && titleNodeList[i].children[0].innerText.trim(),
         link: titleNodeList[i] && siteUrl + titleNodeList[i].getAttribute("href"),
         };
         }
         return titleLinkArray;
         }, {siteUrl}); 
        // console.log(campgroundLinks);
        

        // fs.writeFileSync("scrapping/all_campgrounds.json", JSON.stringify(campgroundLinks));
         
        // console.log("after file write");
       // campgroundLinks = [];

        const result = await scrapeAll(page, campgroundLinks);


       // fs.writeFileSync("scrapping/output.json", JSON.stringify(result));

        await browser.close();
        console.log(success("Browser Closed"));



    } catch (err) {
        // Catch and display errors
        console.log(error(err));
        await browser.close();
        console.log(error("Browser Closed"));
    }

})();

async function convertToSlug(Text)
{
    return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
    //  will generate -- data

    //text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
    // used for - To avoid multiple sequential hyphens
}


async function campground_details(page) {
    let data;
    let amenitiesArr = [];
    let descriptionArray = [];
    let accomodationArray = [];
    let telephoneArray = [];
    let labelArray = [];
    let reservePhone = '';
    let infoPhone = '';
    let seasonInfo = '';
    let address = '';
    let space = '';

    // console.log(await this.convertToSlug("mansi patel"));


    var modals = await document.querySelectorAll('.modal.show');
    // return modals.length;
    if (modals.length > 0)
    {
        for (var modal of modals) {
            var closebutton = await modal.querySelector('div.modal-content button.close');
            await closebutton.click();
        }
    }

    var name = await document.getElementsByClassName('campground-name')[0].textContent;
    var descriptionTag = await document.querySelectorAll('.clp-section-content')[0];
    var optionsTag = await document.querySelectorAll('#Reservation_SiteCategory option');
    var email = await document.querySelector('#campgroundFooter a[href^="mailto:"]').getAttribute('href').replace("mailto:", "");
    var extraInfoTag = await document.querySelector('#campgroundFooter .col-sm-6').querySelectorAll('div');
    var mapTag = await document.querySelector('#campgroundFooter .col-lg-4.text-center');

    var mapLink = mapTag.querySelector('a').getAttribute('href');
    var mapImage = mapTag.querySelector('img').getAttribute('src');
    
     await page.goto('http://koa.com/campgrounds/decatur/recreation');
       // console.log(chalk.gray("Navigating for getting recreation"));

    //  return extraInfoTag;

//    for (var extraInfo of extraInfoTag) {
//        var label = extraInfo.nextElementSibling.textContent.toLowerCase();
//    }
    var label = '';
    var key = 0;
    // return extraInfoTag.length;
    for (var element of extraInfoTag) {

        //  var element = extraInfoTag[extraKey];
        //  return element.textContent+'mansi';
        //   return element;
//        if (element.nextElementSibling != 'undefined' && element.nextElementSibling != null) {
//
//        
//           // labelArray.push(label.tagName);
//        } else {
//           labelArray.push(element.tagName);
//        }
        //labelArray.push(element.tagName);
        // if (element != null && element.querySelector('b') != null) {
//            label = element.querySelector('b').textContent;
//
        label = element.querySelector('b').textContent;
        if (key == 0) {
            seasonInfo = label;
        } else if (key == 1 || key == 2) {
            if (label.toLowerCase().includes("reserve")) {
                reservePhone = element.querySelector('a').getAttribute('href').replace("tel:", "");
            } else if (label.toLowerCase().includes("info")) {
                infoPhone = element.querySelector('a').getAttribute('href').replace("tel:", "");
            }
        } else {
            if (address != "") {
                space = ', ';
            }

            address += space + label;
        }


        //  if (extraKey == 1 || extraKey == 2)



        //  }
        key++;
        //  telephoneArray.push(label);
    }

    //return labelArray;

    var descriptionArr = descriptionTag.querySelectorAll('p');
    // return descriptionArr;
    // let amenities = await document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');
    let amenities = [];

    for (var accomodation of optionsTag) {
        accomodationArray.push(accomodation.textContent);
    }
    // return descriptionArr;

    for (var description of descriptionArr) {
        var desc = description.textContent;
        descriptionArray.push(desc.trim("\t"));
    }

    // console.log(modals);

    //var products = document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');
    // console.log(products);
    for (var amenity of amenities) {
        amenitiesArr.push(amenity);
//        data.push({
//            product_name: product.querySelector('h3').textContent,
//            product_price: product.querySelector('.price_color').textContent,
//            product_availability: product.querySelector('.availability').textContent,
//            product_image: product.querySelector('.thumbnail').getAttribute("src"),
//            product_link: product.querySelector('h3 > a').getAttribute("href")
//        });
    }
    // return name;
    //  console.log(amenitiesArr);
    // console.log(name);

    //Index Campground Category images
    //Index Campsites, Rv and tenting photos

    //Local tab
    //Index title of local attractions with link

    //Index Rules and policies
    //Types of accomodation
    var slug = name
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;
    data = {name: name, slug: slug, description: descriptionArray, amenities: amenitiesArr, recreation: {}, info: '', physical_address: address.trim(), season_dates: seasonInfo, telephone_no: infoPhone, reserver_no: reservePhone, email: email, map_image: mapImage, map_link: mapLink, images: [], local_attractions: [], rules_policies: [], accomodation_types: accomodationArray};
    // amenitiesArr.push(amenity.textContent);
    //  data['name'] = name;
    // data['amenities'] = amenitiesArr;
    return data;
}

async function funamenities() {
    const data = [];
    let amenitiesArr = {amenities: [], unique_amenities: []};

//    var modals = await document.querySelectorAll('.modal.show');
//    // return modals.length;
//    if (modals.length > 0)
//    {
//        for (var modal of modals) {
//            var closebutton = await modal.querySelector('div.modal-content button.close');
//            await closebutton.click();
//        }
//    }
    //  let amenities = [];
    let amenities = await document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');

    //let amenities =  await amenitiesUL.querySelectorAll('li.col-sm-6');

    for (var amenity of amenities) {
        var ul = amenity.parentElement;
        //console.log("mansi" + amenity.parentElement.nodeName);
        //amenitiesArr.push(amenity.textContent);
        // amenitiesArr['amenities'] = [];
        // amenitiesArr.push(ul.previousElementSibling.nodeName);
        if (ul.previousElementSibling.nodeName == 'H2') {
            ul.previousElementSibling.textContent
            amenitiesArr['unique_amenities'].push(amenity.textContent);
        } else {
            amenitiesArr['amenities'].push(amenity.textContent);
        }

    }

    return amenitiesArr;

}

async function funrecreations() {
    var siteUrl = 'https://koa.com';
    let recreation = {};
    let recreationList = new Array();


//    var modals = await document.querySelectorAll('.modal.show');
//    // return modals.length;
//    if (modals.length > 0)
//    {
//        for (var modal of modals) {
//            var closebutton = await modal.querySelector('div.modal-content button.close');
//            await closebutton.click();
//        }
//    }
    var re = /(?:\.([^.]+))?$/;
    var recreationDiv = await document.querySelector('.secondary-layout-container');
    recreation.title = await recreationDiv.querySelector('h1').textContent;
    recreation.description = await recreationDiv.querySelector('.row.justify-content-center .col-md-8.text-left').textContent;
    recreation.description = recreation.description.trim("\t");

    var recreationListTag = await  document.querySelectorAll('.recreation .col-md-6.col-xl-4.mb-4');
    var url;
    for (var recreationTag of recreationListTag) {
        var picture = await  recreationTag.querySelector('picture source').getAttribute('srcset');
        url = picture.split("?");
//        var urlArr = url.split("/");
//        picture
        // return picture;
        picture = siteUrl + picture;
        let title = await  recreationTag.querySelector('.card-title').textContent;
        let  description = await  recreationTag.querySelector('.card-read-more p').textContent;
        var obj = {
            title: title,
            description: description,
            picture: picture,
        };
        var ext = re.exec(url[0])[1];
        // var imagename = (title.toLowerCase() + "." + ext);
        var slug_image = (title
                .toLowerCase()
                .replace(/ /g, '-')
                .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
        ;


        //  var imageUrl = 
        // return imageUrl;



        // return await uploadFileToS3(picture, 'recreations/' + slug_image,  function(err, res) {
//    if (err)
//        throw err;
//            else 
//               return 'mansipatel';
//
//       });

        obj.s3 = await uploadFileToS3(picture, 'recreations/' + slug_image);
//        const s3image = require('../helper/s3imageUpload');
//        s3image.uploadImage(picture, 'recreations/' + slug_image);

        recreationList.push(obj);
    }

    recreation.list = recreationList;

    return recreation;
}


async function funattractions() {
    var siteUrl = 'https://koa.com';
    let attraction = {};
    let attractionList = new Array();


//    var modals = await document.querySelectorAll('.modal.show');
//    // return modals.length;
//    if (modals.length > 0)
//    {
//        for (var modal of modals) {
//            var closebutton = await modal.querySelector('div.modal-content button.close');
//            await closebutton.click();
//        }
//    }


    var attractionListTag = await  document.querySelectorAll('.attractions');
    let  website = '';
    for (var attractionTag of attractionListTag) {
        var picture = await  attractionTag.querySelector('.card-img-container picture source').getAttribute('srcset');
        picture = siteUrl + picture;
        let title = await  attractionTag.querySelector('.card-title').textContent;
        let  description = await  attractionTag.querySelector('.card-text').innerHTML;
        if (attractionTag.querySelector('.w-100.mt-2 a') != null) {
            website = await  attractionTag.querySelector('.w-100.mt-2 a').getAttribute('href');
        }
        var obj = {
            title: title,
            description: description,
            picture: picture,
            website: website
        };
        attractionList.push(obj);
    }

    return attractionList;
}

async function funphotos() {
    var siteUrl = 'https://koa.com';
    let photo = {};
    let photoList = new Array();

    var photoListTag = await  document.querySelectorAll('.albums');

    for (var photoTag of photoListTag) {
        var picture = await  photoTag.querySelector('.card-img-container picture source').getAttribute('srcset');
        picture = siteUrl + picture;
        let title = await  photoTag.querySelector('.card-title').textContent;
        let  description = await  photoTag.querySelector('.card-text').innerHTML;
        let  website = await  photoTag.querySelector('.w-100.mt-2 a').getAttribute('href');
        var obj = {
            title: title,
            description: description,
            picture: picture,
            website: website
        };
        photoList.push(obj);
    }

    return photoList;
}

async function scrapeAll(page, rawdata) {
    // console.log(rawdata);
    let campgrounds = [];
    if (rawdata.length == 0) {
        let rawdata = fs.readFileSync('scrapping/campgrounds.json');
        campgrounds = JSON.parse(rawdata);
    } else {
        campgrounds = rawdata;
    }

    console.log(chalk.yellow("We got " + campgrounds.length + " campground(s) to scrape"));
    //return false;

    // console.log(campgrounds);

    const result = [];
    
    await page.exposeFunction("uploadFileToS3", s3.uploadImage)

    for (var p = 0; p < campgrounds.length; p++) {

        await page.goto(campgrounds[p].link);

        console.log(chalk.magenta("Navigating to " + campgrounds[p].title));

//        await page.evaluate(() => {
//  //var myFunc = function(element) { element.innerHTML = "baz" };
//  //var foo = document.querySelector('.bar');
//  var data = campground_details(page);
//  console.log(data);
//  return true;
//});
        


        let campgroundDetails = await page.evaluate(campground_details);


        // code for getting amenities
        await page.goto(campgrounds[p].link + 'amenities/');
        console.log(chalk.yellow("Navigating for getting amenities"));
        campgroundDetails['amenities'] = await page.evaluate(funamenities);

        // code for getting recreations
        await page.goto(campgrounds[p].link + 'recreation/');
        console.log(chalk.gray("Navigating for getting recreation"));

        //  console.log(chalk.red("mansi"));

        //console.log(await page.evaluate(funrecreations));
        campgroundDetails['recreation'] = await page.evaluate(funrecreations);

        // code for getting attractions
        await page.goto(campgrounds[p].link + 'local-area/');
        console.log(chalk.yellowBright("Navigating for getting attractions"));
        campgroundDetails['local_attractions'] = await page.evaluate(funattractions);

//        await page.evaluate((async() => {
//   console.log(campground_details);
//})());
        // campgrounds[p] = amenities;
        console.log(JSON.stringify(campgroundDetails));
        console.log(campgroundDetails);
        
          fs.writeFileSync("scrapping/campgrounds/"+campgroundDetails['slug']+".json", JSON.stringify(campgroundDetails));
        // console.log(`Found ${products.length} products on page ${p}`);

        // push products result array;
       // result.push(campgrounds[p]);

        // Click on next button
        //console.log('Click next...');
        //await page.click('.next a');
        //await sleep(1000);
    }
    return result;
}



//});

//app.listen('8001');
//
//console.log('Your node server start successfully....');
//
//exports = module.exports = app;