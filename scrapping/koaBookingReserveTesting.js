const puppeteer = require("puppeteer");
const chalk = require("chalk");
var fs = require('fs');

var form_checkIn = '2/10/2021';
var form_checkOut = '02/24/2021';
var form_adults = '2';
var form_kids = '2';
var form_free = '1';
var form_petGroup = '2';
var form_equipmentType = 'V';
var form_equipmentLength = '15';
var form_slideOutsGroup = '2';
var form_VkrPostalCode = '335565';
var form_purchaseVkr_Group = '2';
var form_firstName = "Testing";
var form_lastName = "T";
var form_address1 = "Street 2 , Albama";
var form_address2 = "USA";
var form_countryCode = "US";
var form_phone = "+19583591784";
var form_city = "Arizona";
var form_state = "california";
var form_postcode = "132454";
var form_email = "test@gmail.com";
var form_confirmEmail = "test@gmail.com";
var form_creditCard = "4005550000000019";
var form_creditCardType = "MC";
var form_creditCardExpMonth = "04";
var form_creditCardExpYear = "2025";
var form_creditCardCode = "123";

// var siteUrl = 'https://koa.com/campgrounds/temecula/';
// var siteUrl = 'https://koa.com/campgrounds/picacho-tucson-nw/';

// var siteUrl = 'https://koa.com/campgrounds/decatur/';
// var siteUrl = 'https://koa.com/campgrounds/gulf-shores/';
// var siteUrl = 'https://koa.com/campgrounds/ozark/';

// var siteUrl = 'https://koa.com/campgrounds/homer/';
// var siteUrl = 'https://koa.com/campgrounds/seward/';
// var siteUrl = 'https://koa.com/campgrounds/valdez/';
// var siteUrl = 'https://koa.com/campgrounds/clearwater/';

// var siteUrl = 'https://koa.com/campgrounds/carbondale/';
// var siteUrl='https://koa.com/campgrounds/moss-landing/';
// var siteUrl = 'https://koa.com/campgrounds/salinas/';

(async () => {
    try {
        let rawdata = fs.readFileSync('scrapping/koa_camp_link.json');
        campgrounds = JSON.parse(rawdata);
        var debugLog=[];
        for (var p = 331; p < campgrounds.length; p++) {

            var browser = await puppeteer.launch({
                headless: true,
                args: [
                    '--start-maximized' // you can also use '--start-fullscreen'
                ]
                // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
            });
            console.log('*************************');
            console.log(await browser.version());
            debugLog.push('*************************');

            // open a new page
            var page = await browser.newPage();

            try {
                console.log('Nevigating : ' + campgrounds[p].title)
                debugLog.push('Nevigating : ' + campgrounds[p].title);
                await page.setViewport({ width: 1366, height: 768 });

                await page.setDefaultNavigationTimeout(0);

                // var browseUrl = siteUrl + 'reserve';
                var browseUrl = campgrounds[p].link + 'reserve';

                await page.goto(browseUrl, { waitUntil: 'networkidle2' });

                await page.evaluate(() => {
                    document.querySelector('.nav-steps').scrollIntoView()
                })

                // await page.click("#Reservation_CheckInDate");
                // // await sleep(1000)
                // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(2) > td:nth-child(4)");
                // await sleep(1000)

                // await page.click("#Reservation_CheckOutDate");
                // // await sleep(1000)
                // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(4) > td:nth-child(4) > a");
                // await sleep(1000)

                if (await page.$('#Reservation_CheckInDate') !== null) {
                    await page.click("#Reservation_CheckInDate");
                    await page.type("#Reservation_CheckInDate", form_checkIn);
                    await sleep(1000)
                }

                if (await page.$('#Reservation_CheckOutDate') !== null) {
                    await page.click("#Reservation_CheckOutDate");
                    await page.type("#Reservation_CheckOutDate", form_checkOut);
                    await page.click(".reserve-top-text");
                    await sleep(1000);
                }

                if (await page.$('#Reservation_Adults') !== null) {
                    await page.click("#Reservation_Adults");
                    await page.type("#Reservation_Adults", form_adults);
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_Kids') !== null) {
                    await page.click("#Reservation_Kids");
                    await page.type('#Reservation_Kids', form_kids);
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_Free') !== null) {
                    await page.click("#Reservation_Free");
                    await page.type('#Reservation_Free', form_free);
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_Pets_Group') !== null) {
                    await page.click('#Reservation_Pets_Group > label:nth-child(' + form_petGroup + ')');
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_EquipmentType') !== null) {
                    await page.select('#Reservation_EquipmentType', form_equipmentType)
                    await sleep(1000)
                }

                if (await page.$('#Reservation_EquipmentLength') !== null) {
                    await page.click("#Reservation_EquipmentLength");
                    await page.type('#Reservation_EquipmentLength', form_equipmentLength)
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_SlideOuts_Group') !== null) {
                    await page.click('#Reservation_SlideOuts_Group > label:nth-child(' + form_slideOutsGroup + ')');
                    // await sleep(1000)
                }

                await page.evaluate(() => {
                    document.querySelector('#reserve-step-vkr').scrollIntoView()
                })


                // const VkrNumber = await page.waitForSelector("#Reservation_VkrNumber");
                // await page.click("#Reservation_VkrNumber");
                // await VkrNumber.type("12345");
                // await sleep(1000)

                if (await page.$('#Reservation_VkrPostalCode') !== null) {
                    await page.click("#Reservation_VkrPostalCode");
                    await page.type('#Reservation_VkrPostalCode', form_VkrPostalCode);
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_PurchaseVkr_Group') !== null) {
                    await page.click('#Reservation_PurchaseVkr_Group > label:nth-child(' + form_purchaseVkr_Group + ')');
                    await sleep(1000)
                }

                await page.click('#nextButton');
                await sleep(5000)

                if (await page.$(".validation-summary-errors") !== null) {
                    let element = await page.$('.validation-summary-errors');
                    let errorMsg = await page.evaluate(el => el.textContent, element)
                    console.log(errorMsg);
                    debugLog.push(errorMsg)
                } else {

                    await sleep(1000)
                    if (await page.$("#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button") !== null) {

                        await page.evaluate(() => {
                            document.querySelector('#site-types > div.row.no-padding > div > div').scrollIntoView()
                        })
                        await sleep(2000)

                        await page.click('#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button');
                        await sleep(5000)

                        await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                        await page.evaluate(() => {
                            document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                        })
                        await sleep(1000)

                        const firstname = await page.waitForSelector("#Reservation_FirstName");
                        await page.click("#Reservation_FirstName");
                        await firstname.type(form_firstName)
                        // await sleep(1000)

                        const lastname = await page.waitForSelector("#Reservation_LastName");
                        await page.click("#Reservation_LastName");
                        await lastname.type(form_lastName)
                        // await sleep(1000)

                        const address1 = await page.waitForSelector("#Reservation_Address1");
                        await page.click("#Reservation_Address1");
                        await address1.type(form_address1)
                        // await sleep(1000)

                        const address2 = await page.waitForSelector("#Reservation_Address2");
                        await page.click("#Reservation_Address2");
                        await address2.type(form_address2)
                        // await sleep(1000)

                        await page.select('#Reservation_CountryCode', form_countryCode)
                        // await sleep(1000)

                        const phone = await page.waitForSelector("#Reservation_PhoneNumber");
                        await page.click("#Reservation_PhoneNumber");
                        await phone.type(form_phone)
                        // await sleep(1000)

                        const city = await page.waitForSelector("#Reservation_City");
                        await page.click("#Reservation_City");
                        await city.type(form_city)
                        // await sleep(1000)

                        await page.waitForSelector("#reserve-step3-form");
                        await page.evaluate(() => {
                            document.querySelector('#reserve-step3-form').scrollIntoView()
                        })

                        const state = await page.waitForSelector("#Reservation_StateProvinceCode");
                        await page.click("#Reservation_StateProvinceCode");
                        await state.type(form_state)
                        // await sleep(1000)

                        const postcode = await page.waitForSelector("#Reservation_PostalCode");
                        await page.click("#Reservation_PostalCode");
                        await postcode.type(form_postcode)
                        // await sleep(1000)

                        const resemail = await page.waitForSelector("#Reservation_EmailAddress");
                        await page.click("#Reservation_EmailAddress");
                        await resemail.type(form_email)
                        // await sleep(1000)

                        const resconfemail = await page.waitForSelector("#Reservation_ConfirmEmailAddress");
                        await page.click("#Reservation_ConfirmEmailAddress");
                        await resconfemail.type(form_confirmEmail)
                        // await sleep(1000)

                        await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                        await page.evaluate(() => {
                            document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                        })

                        const creditcard = await page.waitForSelector("#Reservation_CreditCardNumber");
                        await page.click("#Reservation_CreditCardNumber");
                        await creditcard.type(form_creditCard)
                        // await sleep(1000)

                        await page.select('#Reservation_CreditCardType', form_creditCardType)
                        // await sleep(1000)

                        await page.select('#Reservation_CreditCardExpMonth', form_creditCardExpMonth)
                        // await sleep(1000)

                        await page.select('#Reservation_CreditCardExpYear', form_creditCardExpYear)
                        // await sleep(1000)

                        const cardcode = await page.waitForSelector("#Reservation_CreditCardSecurityCode");
                        await page.click("#Reservation_CreditCardSecurityCode");
                        await cardcode.type(form_creditCardCode)
                        // await sleep(1000)

                        await page.waitForSelector("#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox");
                        await page.evaluate(() => {
                            document.querySelector('#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox').scrollIntoView()
                        })

                        await page.click("#Reservation_TermsAgree");
                        await sleep(1000)

                        await page.click('#continueButton')
                        await sleep(3000)

                        await page.waitForSelector("#bookButton");
                        await page.evaluate(() => {
                            document.querySelector('#bookButton').scrollIntoView()
                        })

                        await sleep(1000)
                        await page.click('#bookButton')
                        await sleep(1000)
                        debugLog.push('Booking is Done for this campground')
                        console.log('Booking is Done for this campground')
                    } else {
                        var firstBlockSelector = '';

                        if (await page.$(".reserve-sitetype-unavailable") !== null) {
                            firstBlockSelector = '.reserve-sitetype-unavailable';
                        } else {
                            firstBlockSelector = '.reserve-sitetype-special';
                        }

                        await page.waitForSelector(firstBlockSelector);
                        await page.evaluate((firstBlockSelector) => {
                            document.querySelector(firstBlockSelector + '> div:nth-child(2)').scrollIntoView()
                        }, firstBlockSelector)
                        await sleep(1000)

                        await page.click(firstBlockSelector + ' > div:nth-child(2) > div >div:nth-child(1) > div >div:nth-child(2) > a')
                        await page.waitForSelector("#modal-window-iframe-lg-title");

                        const elementHandle = await page.$('#modal-window-iframe-lg > div > div > iframe');
                        const frame = await elementHandle.contentFrame();
                        // console.log('frame')
                        // console.log(frame)
                        await frame.waitForSelector('#availability-form-wrapper'),
                            await sleep(5000)

                        var firstavailableDate = '#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability';
                        if (await frame.$(firstavailableDate) != null) {
                            if (await frame.$(".validation-summary-errors") !== null) {
                                let element = await frame.$('.validation-summary-errors');
                                let errorMsgPopup = await frame.evaluate(el => el.textContent, element)
                                console.log(errorMsgPopup);
                                debugLog.push(errorMsgPopup);
                            }
                        }
                        var flag = false;
                        while (flag == false) {
                            if (await frame.$(firstavailableDate) != null) {
                                flag = true;
                                await frame.waitForSelector(firstavailableDate)
                                const dateValue = await frame.evaluate('document.querySelector("#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability").getAttribute("data-date")')
                                console.log("This CampGroud is now available from " + dateValue);
                                debugLog.push("This CampGroud is now available from " + dateValue);
                            } else {
                                if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button') != null) {
                                    if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button.fc-state-disabled') != null) {
                                        debugLog.push('No campgrounds are available for this Year')
                                        console.log('No campgrounds are available for this Year');
                                        flag = true;
                                    } else {
                                        await frame.waitForSelector('#calendar > div.fc-toolbar > div.fc-right > button');
                                        await frame.click('#calendar > div.fc-toolbar > div.fc-right > button');
                                    }
                                    await sleep(5000);
                                } else {
                                    debugLog.push('No campgrounds are available')
                                    console.log('No campgrounds are available');
                                    flag = true;
                                }
                            }
                        }
                    }
                }

                // await sleep(5000)
                // await browser.close();
            } catch (err) {
                debugLog.push(err)
                console.log(err)
            }
            await browser.close();
        }
    } catch (err) {
        console.log(err);
        debugLog.push(err)
        // await browser.close();
        console.log("Browser Closed");
        debugLog.push("Browser Closed")
    }
    fs.writeFileSync("scrapping/campground_booking.json", JSON.stringify(debugLog));
    // await browser.close();
})();

async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}


