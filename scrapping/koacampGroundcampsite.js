var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')
console.log(args);
var debugLog = [];
var crawlerstatus = true;
// console.log('Age: ' + args.age);
var datasourceId = args.datasource_id;
var app = express();
// const db = require("../../../../../scrapping/models/index.js");

var campLogId = 0;
const puppeteer = require("puppeteer");

//Prod
var configPath='../../../../../scrapping/';
var filePathStore='../scrapping/';
//Local
// var configPath = '../scrapping/';
// var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
const db = require(configPath + "models");

const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const DataSource = db.DataSource;

const chalk = require("chalk");
var fs = require("fs");
// var s3 = require("../../../../../scrapping/helper/s3imageUpload");
// MY OCD of colorful console.logs for debugging... IT HELPS
const error = chalk.bold.red;
const success = chalk.keyword("green");
const log = console.log;
var crawlerLogId = 0;
//app.get('/scrape', function (req, res) {
var campID = 0;
var siteUrl = '';

(async () => {
    try {

        const dataSourceData = await getDatasourceByID();

        const storeLog = await storeCrawlerLog();

        var browser = await puppeteer.launch({
            headless: true,
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });

        debugLog.push(await dateTime() + ":" + await browser.version());
        console.log(await browser.version());

        // open a new page
        var page = await browser.newPage();

        await page.setDefaultNavigationTimeout(0);

        console.log('URL is : ' + siteUrl);
        var browseUrl = siteUrl + '/campgrounds/';
        console.log('Navigating...');
        await page.goto(browseUrl), { waitUntil: 'networkidle2' };
        debugLog.push(await dateTime() + ":" + 'Navigating...' + browseUrl);

        debugLog.push(await dateTime() + ":" + 'Fetching CampGroup and Campground data');
        var campgroundData = await page.evaluate(({ siteUrl }) => {

            var rowList = document.querySelectorAll('#mainContent > div > .row');

            var rowArray = [{ 'camp_group': {} }];

            var country = '';
            var count = 0;
            for (var i = 0; i < rowList.length; i++) {
                if (i % 2 == 0) {
                    country = rowList[i].children[0].children[1].innerText.trim();
                } else {
                    var j = i + 2;
                    var campGroupList = document.querySelectorAll('#mainContent > div > div:nth-child(' + j + ') > div > ul > li  > a');
                    var campvalgroup = [];
                    for (var k = 0; k < campGroupList.length; k++) {
                        // campval[k]=  campGroupList[k] && campGroupList[k].innerText.trim();

                        var p = k + 1;
                        var campList = document.querySelectorAll(' #mainContent > div > div:nth-child(' + j + ') > div > ul > li:nth-child(' + p + ') > div > ul > li > div > a');

                        var campval = [];
                        for (var l = 0; l < campList.length; l++) {
                            var camp_url = campList[l] && campList[l].getAttribute("href");
                            var camptitle = campList[l] && campList[l].innerText;
                            camptitle =  camptitle.replace("Holiday","");
                            camptitle =  camptitle.replace("Journey","");
                            camptitle =  camptitle.replace("Resort","");
                            campval[l] = {
                                'title': camptitle.trim(),
                                'url': 'https://koa.com' + camp_url,
                            }
                        }
                        var camp_group_url = campGroupList[k] && campGroupList[k].getAttribute("href");
                        campvalgroup[k] = {
                            country_name: country,
                            camp_group: campGroupList[k] && campGroupList[k].innerText.trim(),
                            camp_group_url: 'https://koa.com' + camp_group_url,
                            campground: campval
                        };
                    }
                    rowArray[0].camp_group[count] = campvalgroup;
                    count++;
                }

            }
            return rowArray;

        }, { siteUrl });
        debugLog.push(await dateTime() + ":" + 'Fetched CampGroup and Campground data');

        // fs.writeFileSync("../scrapping/campground.json", JSON.stringify(campgroundData));
        debugLog.push(await dateTime() + ":" + 'Go to store data of campgroup and campground');

        await page.exposeFunction("uploadFileToS3", s3.uploadImage)

        const result = await storeCampground(campgroundData, page);
        const resultscrap = await scrapeAll(page, async function (returnValue) {
            storeCrawlerLogFile()
            browser.close();
            debugLog.push(await dateTime() + ":" + "Browser Closed");
            console.log(success("Browser Closed"));
        });



    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        console.log(error(err));
        debugLog.push(await dateTime() + ":" + err);
        await browser.close();
        console.log(error("Browser Closed"));
        debugLog.push(await dateTime() + ":" + "Browser Closed");
        await storeCrawlerLogFile()
    }
    // console.log('Debug Log is');
    // console.log(await dateTime() + ":" + debugLog);
})();

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getDatasourceByID() {
    await DataSource.findByPk(datasourceId)
        .then(data => {
            siteUrl = data['url']
        })
        .catch(async err => {
            debugLog.push(await dateTime() + ":" + 'Error in Fetching DataSource By Id');
        });
}

async function storeCrawlerLog() {
    let response = [];
    const logData = {
        data_source_id: datasourceId,
        status: 'InProgress',
    };
    debugLog.push(await dateTime() + ":" + 'Storing Crawler Log');
    DataSourceCrawlerLog.create(logData)
        .then(async data => {

            response['status'] = 'success';
            response['content'] = data;
            // return data;
            crawlerLogId = data['id'];
            debugLog.push(await dateTime() + ":" + 'Crawler Log Stored Successfully');
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in crawler log storing');
            debugLog.push(await dateTime() + ":" + err.message);

            response['status'] = 'error';
            response['content'] = err;
            // return err;
            // console.log(err.message)
        });
    return response;
}

async function storeCampground(campdata, page) {
    try {
        // console.log(campdata[0].camp_group)
        debugLog.push(await dateTime() + ":" + 'In store campground function');
        // for (var i = 0; i < campdata.length; i++) {
        var i = 0;
        for (var key in campdata[i].camp_group) {

            for (var property in campdata[i].camp_group[key]) {
                var gname = campdata[i].camp_group[key][property]['camp_group'];
                var cntname = campdata[i].camp_group[key][property]['country_name'];
                var groupurl = campdata[i].camp_group[key][property]['camp_group_url'];
                // console.log(campdata[i].camp_group[property]['camp_group'])
                var groupname = gname.replace("CAMPGROUNDS IN ", "");
                var country = cntname.replace("CAMPGROUNDS IN ", "");
                // console.log(newname)

                const campGroup = {
                    name: groupname,
                    data_source_id: datasourceId,
                    country: country,
                    url: groupurl
                };
                var condition = { name: groupname };
                const camp = campdata[i].camp_group[key][property]['campground'];
                // console.log("camp is ")
                // console.log(camp)
                // debugLog.push(dateTime() + ":" + 'checking campgroup is added or not');
                await CampGroundGroup
                    .findOne({ where: campGroup })
                    .then(function (obj) {
                        // update
                        if (obj) {
                            return obj.update(campGroup)
                                .then(data => {
                                    for (const campkey in camp) {
                                        // console.log(camp[campkey]['url'])
                                        const cont = {
                                            data_source_id: datasourceId,
                                            campground_group_id: data['id'],
                                            title: camp[campkey]['title'],
                                            url: camp[campkey]['url'],
                                            // status: 'Active',
                                            // index_status: 'Pending',
                                            // last_indexed_time:Date.now(),

                                        };
                                        // console.log(cont)
                                        // debugLog.push(dateTime() + ":" + 'checking campground is added or not');
                                        CampGround.findOne({ where: cont })
                                            .then(function (obj) {
                                                // debugLog.push(dateTime() + ":" + 'updating campground ');

                                                const campdata = {
                                                    data_source_id: datasourceId,
                                                    campground_group_id: data['id'],
                                                    title: camp[campkey]['title'],
                                                    url: camp[campkey]['url'],
                                                    status: 'Pending',
                                                    index_status: 'Pending',
                                                    last_indexed_time: Date.now(),

                                                };
                                                if (obj) {
                                                    obj.update(campdata)
                                                        .then(data => { })
                                                        .catch(async err => {
                                                            crawlerstatus = false;
                                                            debugLog.push(await dateTime() + ":" + 'Error in Updating Already Stored Campground');
                                                        });
                                                } else {
                                                    CampGround.create(campdata)
                                                        .then(data => { })
                                                        .catch(async err => {
                                                            crawlerstatus = false;
                                                            debugLog.push(await dateTime() + ":" + 'Error in Creating Already Stored Campground');
                                                        });
                                                }
                                            })
                                            .catch(async err => {
                                                crawlerstatus = false;
                                                debugLog.push(await dateTime() + ":" + 'Error in Checking if Capground Already Stored or Not');
                                            });
                                    }
                                })
                                .catch(async err => {
                                    crawlerstatus = false;
                                    debugLog.push(await dateTime() + ":" + 'Error in Updating Campground');
                                });

                        } else {
                            // insert
                            // debugLog.push(dateTime() + ":" + ' campgroup creating');
                            return CampGroundGroup.create(campGroup, camp)
                                .then(data => {
                                    // debugLog.push(dateTime() + ":" + ' campgroup created');
                                    for (var campkey in camp) {
                                        const campground = {
                                            data_source_id: datasourceId,
                                            campground_group_id: data['id'],
                                            title: camp[campkey]['title'],
                                            url: camp[campkey]['url'],
                                            status: 'Pending',
                                            index_status: 'Completed',
                                            last_indexed_time: Date.now(),

                                        };
                                        // debugLog.push(dateTime() + ":" + ' campground creating');
                                        CampGround.create(campground).then(data => { });
                                    }
                                })
                                .catch(async err => {
                                    crawlerstatus = false;
                                    debugLog.push(await dateTime() + ":" + 'Error in Storing New Campground');
                                });
                        }

                    })
                    .catch(async err => {
                        crawlerstatus = false;
                        debugLog.push(await dateTime() + ":" + 'Error in Storing Campground');
                    });
            }
        }

    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing All Campgrounds and Campground Groups');
    }

}

async function storeCrawlerLogFile() {
    try {
        fs.writeFileSync(filePathStore + "temp/crawler" + datasourceId + ".json", JSON.stringify(debugLog));
        const filepath = path.resolve(filePathStore + "temp/crawler" + datasourceId + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFile(filepath, 'data_source/' + datasourceId + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Crawler log file to server");
            fs.unlinkSync(filepath)
        } else {

            crawlerLogfilename = '';
            console.log("Failed to upload Crawler log file to server");
        }

        const crawlerUpdate = {
            log_file_path: 'Log:' + crawlerLogfilename,
            status: (crawlerstatus == true) ? 'Success' : 'Failed',
            updated_at: Date.now(),
        };
        DataSourceCrawlerLog.update(crawlerUpdate, {
            where: { id: crawlerLogId }
        })
        var statusVal = (crawlerstatus == true) ? 'Success' : 'Failed';
        const dataSourceUpdate = {
            crawler_log: statusVal + ':' + crawlerLogfilename,
        };
        DataSource.update(dataSourceUpdate, {
            where: { id: datasourceId }
        })
    } catch (e) {
        console.log('Error in Updating DataSource and Storing Crawler File')
        console.log(e)
    }
}

async function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
    //  will generate -- data

    //text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
    // used for - To avoid multiple sequential hyphens
}

async function campground_details() {
    try {
        let data;
        let amenitiesArr = [];
        let descriptionArray = [];
        let accomodationArray = [];
        let telephoneArray = [];
        let labelArray = [];
        let reservePhone = '';
        let infoPhone = '';
        let seasonInfo = '';
        let address = '';
        let space = '';

        // console.log(await this.convertToSlug("mansi patel"));


        var modals = await document.querySelectorAll('.modal.show');
        // return modals.length;
        if (modals.length > 0) {
            for (var modal of modals) {
                var closebutton = await modal.querySelector('div.modal-content button.close');
                await closebutton.click();
            }
        }

        var name = await document.getElementsByClassName('campground-name')[0].textContent;
        var descriptionTag = await document.querySelectorAll('.clp-section-content')[0];
        // var optionsTag = await document.querySelectorAll('#Reservation_SiteCategory option');
        var email = await document.querySelector('#campgroundFooter a[href^="mailto:"]').getAttribute('href').replace("mailto:", "");
        var extraInfoTag = await document.querySelector('#campgroundFooter .col-sm-6').querySelectorAll('div');
        var mapTag = await document.querySelector('#campgroundFooter .col-lg-4.text-center');

        var mapLink = mapTag.querySelector('a').getAttribute('href');
        var mapImage = mapTag.querySelector('img').getAttribute('src');

        //  return extraInfoTag;

        //    for (var extraInfo of extraInfoTag) {
        //        var label = extraInfo.nextElementSibling.textContent.toLowerCase();
        //    }
        var label = '';
        var key = 0;
        // return extraInfoTag.length;
        for (var element of extraInfoTag) {

            //  var element = extraInfoTag[extraKey];
            //  return element.textContent+'mansi';
            //   return element;
            //        if (element.nextElementSibling != 'undefined' && element.nextElementSibling != null) {
            //
            //
            //           // labelArray.push(label.tagName);
            //        } else {
            //           labelArray.push(element.tagName);
            //        }
            //labelArray.push(element.tagName);
            // if (element != null && element.querySelector('b') != null) {
            //            label = element.querySelector('b').textContent;
            //
            label = element.querySelector('b').textContent;
            if (key == 0) {
                seasonInfo = label;
            } else if (key == 1 || key == 2) {
                if (label.toLowerCase().includes("reserve")) {
                    reservePhone = element.querySelector('a').getAttribute('href').replace("tel:", "");
                } else if (label.toLowerCase().includes("info")) {
                    infoPhone = element.querySelector('a').getAttribute('href').replace("tel:", "");
                }
            } else {
                if (address != "") {
                    space = ', ';
                }

                address += space + label;
            }

            key++;
            //  telephoneArray.push(label);
        }
        var addArr = address.split(',');
        var newAddress=(addArr[addArr.length-1]) ? address.replace(addArr[addArr.length-1], "") : "";
        var pinState = (addArr[addArr.length-1]) ? addArr[addArr.length-1].trim().split(' ') : "";
        var state=(pinState[0]) ? pinState[0] : "";
        var zip=(pinState[1]) ? pinState[1] : "";

        var addressData = {
            address: newAddress,
            city: "",
            state: state,
            country: "",
            zip: zip,
            latitude: "",
            longitude: ""                          
        };
        //return labelArray;

        var descriptionArr = descriptionTag.querySelectorAll('p');
        // return descriptionArr;
        // let amenities = await document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');
        let amenities = [];

        // for (var accomodation of optionsTag) {
        //     accomodationArray.push(accomodation.textContent);
        // }
        // return descriptionArr;

        for (var description of descriptionArr) {
            var desc = description.textContent;
            descriptionArray.push(desc.trim("\t"));
        }

        // console.log(modals);

        //var products = document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');
        // console.log(products);
        for (var amenity of amenities) {
            amenitiesArr.push(amenity);
            //        data.push({
            //            product_name: product.querySelector('h3').textContent,
            //            product_price: product.querySelector('.price_color').textContent,
            //            product_availability: product.querySelector('.availability').textContent,
            //            product_image: product.querySelector('.thumbnail').getAttribute("src"),
            //            product_link: product.querySelector('h3 > a').getAttribute("href")
            //        });
        }
        // return name;
        //  console.log(amenitiesArr);
        // console.log(name);

        //Index Campground Category images
        //Index Campsites, Rv and tenting photos

        //Local tab
        //Index title of local attractions with link

        //Index Rules and policies
        //Types of accomodation
        var slug = name
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;
        data = {
            name: name,
            slug: slug,
            description: descriptionArray,
            data: {
                amenities: amenitiesArr,
                recreation: {},
                local_attractions: [],
                rules_policies: [],
                deals: [],
            },
            contact: {
                physical_address: address.trim(),
                season_dates: seasonInfo,
                telephone_no: infoPhone,
                reserver_no: reservePhone,
                email: email,
                map_image: mapImage,
                map_link: mapLink,
                address:addressData
            },
            info: '',
            images: [],
            accomodation_types: [],
        };
        // amenitiesArr.push(amenity.textContent);
        //  data['name'] = name;
        // data['amenities'] = amenitiesArr;
        return data;
    } catch (e) {
        crawlerstatus = false;
        console.log(e.message)
        // debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground Details');
    }
}

async function funamenities() {
    try {
        const data = [];
        let amenitiesArr = { amenities: [], unique_amenities: [] };

        //    var modals = await document.querySelectorAll('.modal.show');
        //    // return modals.length;
        //    if (modals.length > 0)
        //    {
        //        for (var modal of modals) {
        //            var closebutton = await modal.querySelector('div.modal-content button.close');
        //            await closebutton.click();
        //        }
        //    }
        //  let amenities = [];
        let amenities = await document.querySelectorAll('ul.gray-bullet-list li.col-sm-6');

        //let amenities =  await amenitiesUL.querySelectorAll('li.col-sm-6');

        for (var amenity of amenities) {
            var ul = amenity.parentElement;
            //console.log("mansi" + amenity.parentElement.nodeName);
            //amenitiesArr.push(amenity.textContent);
            // amenitiesArr['amenities'] = [];
            // amenitiesArr.push(ul.previousElementSibling.nodeName);
            if (ul.previousElementSibling.nodeName == 'H2') {
                ul.previousElementSibling.textContent
                amenitiesArr['unique_amenities'].push(amenity.textContent);
            } else {
                amenitiesArr['amenities'].push(amenity.textContent);
            }

        }

        return amenitiesArr;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Amenity Details');
    }

}

async function funrecreations() {
    try {
        var siteUrl = 'https://koa.com';
        let recreation = {};
        let recreationList = new Array();


        //    var modals = await document.querySelectorAll('.modal.show');
        //    // return modals.length;
        //    if (modals.length > 0)
        //    {
        //        for (var modal of modals) {
        //            var closebutton = await modal.querySelector('div.modal-content button.close');
        //            await closebutton.click();
        //        }
        //    }
        var re = /(?:\.([^.]+))?$/;
        var recreationDiv = await document.querySelector('.secondary-layout-container');
        recreation.title = await recreationDiv.querySelector('h1').textContent;
        recreation.description = await recreationDiv.querySelector('.row.justify-content-center .col-md-8.text-left').textContent;
        recreation.description = recreation.description.trim("\t");

        var recreationListTag = await document.querySelectorAll('.recreation .col-md-6.col-xl-4.mb-4');
        var url;
        var cnt = 0;
        var listlength = 1;
        for (var recreationTag of recreationListTag) {
            if (cnt < listlength) {
                var picture = await recreationTag.querySelector('picture source').getAttribute('srcset');
                url = picture.split("?");
                //        var urlArr = url.split("/");
                //        picture
                // return picture;
                picture = siteUrl + picture;
                let title = await recreationTag.querySelector('.card-title').textContent;
                let description = await recreationTag.querySelector('.card-read-more p').textContent;
                var obj = {
                    title: title,
                    description: description,
                    // picture: picture,
                };
                var ext = re.exec(url[0])[1];
                // var imagename = (title.toLowerCase() + "." + ext);
                var slug_image = (title
                    .toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
                ;


                //  var imageUrl =
                // return imageUrl;


                // return await uploadFileToS3(picture, 'recreations/' + slug_image,  function(err, res) {

                obj.image = await uploadFileToS3(picture, 'recreations/' + slug_image);
                //        const s3image = require('../helper/s3imageUpload');
                //        s3image.uploadImage(picture, 'recreations/' + slug_image);

                recreationList.push(obj);
            }

            recreation.list = recreationList;

            return recreation;
        }
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Recreation Details');
    }
}

async function funattractions() {
    try {
        var siteUrl = 'https://koa.com';
        let attraction = {};
        let attractionList = new Array();


        //    var modals = await document.querySelectorAll('.modal.show');
        //    // return modals.length;
        //    if (modals.length > 0)
        //    {
        //        for (var modal of modals) {
        //            var closebutton = await modal.querySelector('div.modal-content button.close');
        //            await closebutton.click();
        //        }
        //    }


        var attractionListTag = await document.querySelectorAll('.attractions');
        let website = '';
        for (var attractionTag of attractionListTag) {
            var picture = await attractionTag.querySelector('.card-img-container picture source').getAttribute('srcset');
            picture = siteUrl + picture;
            let title = await attractionTag.querySelector('.card-title').textContent;
            let description = await attractionTag.querySelector('.card-text').innerHTML;
            if (attractionTag.querySelector('.w-100.mt-2 a') != null) {
                website = await attractionTag.querySelector('.w-100.mt-2 a').getAttribute('href');
            }
            var obj = {
                title: title,
                description: description,
                picture: picture,
                website: website
            };
            attractionList.push(obj);
        }

        return attractionList;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Attraction Details');
    }
}

async function funrulepolicy() {
    try {
        let rulePolicy = '';

        rulePolicy = await document.querySelector('.list-wrapper').innerHTML;

        return rulePolicy;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in fetching Campground Rules and Policy Details');
    }
}


async function scrapeAll(page, callback) {
    try {

        var campgroundStatus = true;
        debugLog.push(await dateTime() + ":" + 'In Fetching campground detail.');
        var result = [];
        var successCamp = 0;
        var failedCamp = 0;
        CampGround.findAll({ where: { data_source_id: datasourceId } })
            .then(async data => {
                console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));

                for (var p = 0; p < data.length; p++) {
                    // if (data[p]['id'] == 16) {
                        var crawlerLog = [];
                        try {
                            // console.log("title:"+data[p].title);
                            var campdata = data[p];
                            // console.log('title is '+ campdata['title'])
                            let campgrounds = [];
                            debugLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                            crawlerLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");

                            var link = '';
                            var title = '';
                            link = campdata['url'];
                            title = campdata['title'];

                            debugLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                            crawlerLog.push(await dateTime() + ":" + "updating campground status to InProgress");

                            const campUpdate = {
                                index_status: "InProgress",
                            };
                            CampGround.update(campUpdate, {
                                where: { id: campdata['id'] }
                            }).then(data => { })
                                .catch(async err => {
                                    crawlerstatus = false;
                                    console.log(err.message)
                                    debugLog.push(await dateTime() + ":" + 'Error in Updating CampGround Status To In Progress');
                                });


                            debugLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            crawlerLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            const campLogData = {
                                crawler_log_id: crawlerLogId,
                                campground_id: campdata['id'],
                                status: 'Pending',
                            };

                            CampgroundLog.create(campLogData)
                                .then(data => {
                                    campLogId = data['id'];
                                })
                                .catch(async err => {
                                    debugLog.push(await dateTime() + ":" + 'Error in New Creating CampGround Log');
                                    console.log(err.message)
                                });

                            await page.setDefaultNavigationTimeout(0);
                            debugLog.push(await dateTime() + ":" + "Nevigating..." + link);
                            await page.goto(link, { waitUntil: 'networkidle2' });

                            console.log(chalk.magenta("Navigating to " + title));
                            debugLog.push(await dateTime() + ":" + "Navigating to..." + title);

                            page.on('console', msg => {
                                for (let i = 0; i < msg._args.length; ++i)
                                    console.log(`${msg._args[i]}`)
                            });
                            // await page.exposeFunction("uploadFileToS3", s3.uploadImage)
                            let campgroundDetails = await page.evaluate(campground_details);


                            var sitedetail = [];
                            sitedetail['campsites'] = await page.evaluate(async () => {

                                var sitelink = await document.querySelectorAll('#mainContent > section:nth-child(13) > div > div > div > div > a.btn.btn-blue-white');
                                var linkresult = [];
                                for (var i = 0; i < sitelink.length; i++) {

                                    var link = sitelink[i].getAttribute("href");
                                    var type = sitelink[i].textContent.trim();
                                    if (type == 'RV Sites' || type == 'Lodging' || type == 'Tent Sites') {
                                        linkresult[i] = { link: link, type: type, data: [] };
                                    }
                                }
                                return linkresult;
                            });
                            //  console.log(sitedetail)
                            // var siteContent = [];
                            for (var l = 0; l < sitedetail['campsites'].length; l++) {

                                await page.goto(link + sitedetail['campsites'][l]['link'], { waitUntil: 'networkidle2' });

                                // page.on('console', msg => {
                                //     for (let i = 0; i < msg._args.length; ++i)
                                //         console.log(`${msg._args[i]}`);
                                // });

                                sitedetail['campsites'][l]['link'] = '';
                                sitedetail['campsites'][l]['data'] = await page.evaluate(async (datasourceID, campId) => {
                                    // try {
                                    var link = 'https://koa.com/';
                                    let elements = document.querySelectorAll(".feature-details  > div:nth-child(3) > div");
                                    let holiday_element = document.querySelectorAll(".feature-details   > div.px-3.pb-3 ");
                                    let contact = document.querySelectorAll(".feature-details  > div.row.p-3 > div.col-10.col-lg-11  > div");
                                    let images = document.querySelectorAll('.reserve-sitetype-thumbnail-modal')
                                    var data = [];

                                    for (var el = 0; el < elements.length; el++) {
                                        // console.log('ekements ' + el)
                                        var campgroundName = contact[el].querySelector('div.col-12 > h3').textContent;
                                        // console.log(campgroundName);
                                        var title = elements[el].querySelector('div:nth-child(1) > h5').textContent;
                                        // console.log('titile' + title);
                                        var summary = elements[el].querySelector('div:nth-child(1) > p').textContent;
                                        // console.log('summaruy' + summary);
                                        var description = "";
                                        var amenities = "";
                                        var features = "";
                                        var others = "";
                                        if (elements[el].querySelector('div:nth-child(2) > div > div >h5') != null) {
                                            // console.log('inif')

                                            var totalRowdiv = elements[el].querySelectorAll(' div:nth-child(2) > div >div');

                                            for (var rd = 0; rd < totalRowdiv.length; rd++) {
                                                var heading = '';
                                                if (totalRowdiv[rd].querySelector('h5') != null) {
                                                    heading = totalRowdiv[rd].querySelector('h5').textContent.trim()
                                                    // console.log('heading is :' + heading);
                                                }
                                                var content = '';
                                                if (totalRowdiv[rd].querySelector('ul') != null) {
                                                    content = totalRowdiv[rd].querySelector('ul').textContent.trim()
                                                    // console.log('contentn is :' + content);
                                                }
                                                content = content.replace(/\t/g, "");
                                                // console.log('contentn is :' + content);
                                                if (heading == 'Description') {
                                                    description = content;
                                                }
                                                if (heading == 'Amenities') {
                                                    amenities = content;
                                                }
                                                if (heading == 'Features') {
                                                    features = content;
                                                }
                                                if (heading == 'Other') {
                                                    others = content;
                                                }
                                            }


                                        } else {
                                            // console.log('inelse')
                                            var heading = elements[el].querySelector('div:nth-child(2) > h5').textContent.trim();
                                            // console.log('heading:' + heading)
                                            if (heading == 'Description') {
                                                descriptionVal = elements[el].querySelectorAll('div > div > div > ul')
                                                for (var d = 0; d < descriptionVal.length; d++) {
                                                    description += descriptionVal[d].textContent
                                                }
                                                // description = elements[el].querySelector('div > div > div > ul').textContent
                                                // console.log('description'+description)
                                                description = (description != undefined) ? description.replace(/\t/g, "") : "";
                                            }
                                            if (heading == 'Amenities') {
                                                amenitiesVal = elements[el].querySelectorAll('div > div > div > ul')
                                                for (var a = 0; a < amenitiesVal.length; a++) {
                                                    amenities += amenitiesVal[a].textContent
                                                }
                                                // amenities = elements[el].querySelector('div > div > div > ul').textContent
                                                amenities = (amenities != undefined) ? amenities.replace(/\t/g, "") : '';
                                                // console.log('amenities' + amenities)

                                            }
                                            if (heading == 'Features') {

                                                featuresVal = elements[el].querySelectorAll('div > div > div > ul')
                                                for (var f = 0; f < featuresVal.length; f++) {
                                                    features += featuresVal[f].textContent
                                                }
                                                // features = elements[el].querySelector('div > div > div > ul').textContent
                                                // console.log('features'+features)
                                                features = (features != undefined) ? features.replace(/\t/g, "") : '';
                                            }
                                            if (heading == 'Other') {
                                                othersVal = elements[el].querySelectorAll('div > div > div > ul')
                                                for (var ov = 0; ov < othersVal.length; ov++) {
                                                    othersVal += othersVal[ov].textContent
                                                }

                                                // others = elements[el].querySelector('div > div > div > ul').textContent
                                                // console.log('others'+others)
                                                others = (others != undefined) ? others.replace(/\t/g, "") : '';
                                            }
                                        }

                                        var holiday = '';
                                        var address = '';
                                        var email = '';
                                        var reservationPhone = '';
                                        var infoPhone = '';

                                        if (holiday_element[el] != undefined && holiday_element[el].querySelector('div > div > p') != null) {
                                            holiday = holiday_element[el].querySelector('div > div > p').textContent;
                                        }
                                        // console.log('holiday' + holiday);

                                        if (contact[el] != undefined && contact[el].querySelector('div:nth-child(2) > ul > li:nth-child(1)') != null) {
                                            address = contact[el].querySelector('div:nth-child(2) > ul > li:nth-child(1)').textContent
                                        }
                                        // console.log('address' + address)
                                        if (contact[el] != undefined && contact[el].querySelector('div:nth-child(2) > ul > li:nth-child(2) > a') != null) {
                                            email = contact[el].querySelector('div:nth-child(2) > ul > li:nth-child(2) > a').textContent
                                        }
                                        // console.log('email' + email)
                                        if (contact[el] != undefined && contact[el].querySelector(' div:nth-child(3) > ul > li:nth-child(1)') != null) {
                                            reservationPhone = contact[el].querySelector(' div:nth-child(3) > ul > li:nth-child(1)').textContent
                                        }
                                        // console.log('reservationPhone' + reservationPhone)
                                        if (contact[el] != undefined && contact[el].querySelector(' div:nth-child(3) > ul > li:nth-child(2)') != null) {
                                            infoPhone = contact[el].querySelector(' div:nth-child(3) > ul > li:nth-child(2)').textContent
                                        }
                                        // console.log('infoPhone' + infoPhone)
                                        var image_url = [];
                                        if (images[el].querySelectorAll('div > div > div > picture') != null) {
                                            var imageList = images[el].querySelectorAll('div > div >div > picture');
                                            var re = /(?:\.([^.]+))?$/;
                                            for (var img = 0; img < imageList.length; img++) {
                                                var urlval = imageList[img].querySelector('img').getAttribute('src');
                                                var urlarr = urlval.split("?");
                                                // var siteUrl = 'https://koa.com';
                                                var urlpath = link + urlarr[0];
                                                var filename = urlarr[0].substring(urlval.lastIndexOf('/') + 1).toLowerCase();
                                                image_url.push({
                                                    image: await uploadFileToS3(urlpath, 'data_source/' + datasourceID + '/campgrounds/' + campId + '/images/campsites/' + filename),
                                                })
                                            }
                                            if (imageList.length < 1) {
                                                var urlval = images[el].querySelector('img').getAttribute('src');
                                                var urlarr = urlval.split("?");
                                                // var siteUrl = 'https://koa.com';
                                                var urlpath = link + urlarr[0];

                                                var filename = urlarr[0].substring(urlval.lastIndexOf('/') + 1);

                                                if (urlval != null) {
                                                    image_url.push({
                                                        image: await uploadFileToS3(urlpath, 'data_source/' + datasourceID + '/campgrounds/' + campId + '/images/campsites/' + filename),
                                                    })
                                                }
                                            }
                                        }

                                        data[el] = {
                                            campground: campgroundName,
                                            title: title,
                                            summary: summary,
                                            description: description,
                                            features: features,
                                            amenities: amenities,
                                            others: others,
                                            holiday: holiday,
                                            address: address,
                                            email: email,
                                            reservation_phone: reservationPhone,
                                            information_phone: infoPhone,
                                            images: image_url
                                        };

                                    }
                                    // console.log(data);
                                    return data;
                                    // } catch (e) {
                                    //     console.log(e.message)
                                    // }
                                }, campdata['data_source_id'], campdata['id']);


                            }
                            // console.log(sitedetail)
                            // console.log( sitedetail['campsites'])
                            campgroundDetails['accomodation_types'] = sitedetail['campsites'];
                            // console.log(campgroundDetails["accomodation_types"])
                            // console.log(campgroundDetails)
                            // code for getting deals
                            await page.goto(link + 'hot-deals/', { waitUntil: 'networkidle2' });
                            console.log(chalk.yellow("Navigating for deals"));
                            debugLog.push(await dateTime() + ":" + "Navigating for deals");
                            crawlerLog.push(await dateTime() + ":" + "Navigating for deals");

                            var datasourceID = campdata['data_source_id'];
                            var campID = campdata['id'];
                            var addressDetail="";
                            campgroundDetails['data']['deals'] = await page.evaluate(async (datasourceID, campID) => {
                                var re = /(?:\.([^.]+))?$/;
                                var siteUrl = 'https://koa.com';

                                var manuname = await document.querySelector('#campgroundSecondaryNav > ul > li:nth-child(2) > a').innerText.trim();
                                if (manuname == 'DEALS') {
                                    var result = [];
                                    var title = await document.querySelector('#mainContent > div.container.secondary-layout-container.pt-4.pb-4 > div.row.text-center > div > h1').innerText.trim();
                                    var allDeals = await document.querySelectorAll(".hot-deals > div > div ");
                                    for (var d = 0; d < allDeals.length; d++) {
                                        var picture = await allDeals[d].querySelector('div.card-img-top.d-block.text-center > img').getAttribute('src');
                                        var description = await document.querySelector('div.card-body > div.card-read-more').innerHTML;
                                        var url = picture.split("?");
                                        var ext = re.exec(url[0])[1];
                                        var onlyurl = url[0];
                                        picture = siteUrl + onlyurl;
                                        var dealobj = {
                                            title: title,
                                            description: description,
                                            // picture: picture,
                                            image: await uploadFileToS3(picture, 'data_source/' + datasourceID + '/campgrounds/' + campID + '/images/deals/' + new Date().getTime() + '.' + ext)
                                        };
                                        result[d] = dealobj;
                                    }
                                    return result;
                                } else {
                                    return [];
                                }
                            }, datasourceID, campID);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for deals");


                            await page.goto(link + 'amenities/', { waitUntil: 'networkidle2' });
                            debugLog.push(await dateTime() + ":" + "Navigating getting amenities");
                            crawlerLog.push(await dateTime() + ":" + "Navigating getting amenities");
                            console.log(chalk.yellow("Navigating for getting amenities"));
                            campgroundDetails['data']['amenities'] = await page.evaluate(funamenities);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for amenities");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for amenities");

                            // code for getting recreations
                            await page.goto(link + 'recreation/', { waitUntil: 'networkidle2' });
                            debugLog.push(await dateTime() + ":" + "Navigating for getting recreation");
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting recreation");
                            console.log(chalk.gray("Navigating for getting recreation"));
                            campgroundDetails['data']['recreation'] = await page.evaluate(funrecreations);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for recreation");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for recreation");

                            // // code for getting attractions
                            await page.goto(link + 'local-area/', { waitUntil: 'networkidle2' });
                            debugLog.push(await dateTime() + ":" + "Navigating for getting attractions");
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting attractions");
                            console.log(chalk.yellowBright("Navigating for getting attractions"));
                            campgroundDetails['data']['local_attractions'] = await page.evaluate(funattractions);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for attractions");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for attractions");

                            // code for getting rules and policies
                            await page.goto(link + 'general-information/', { waitUntil: 'networkidle2' });
                            debugLog.push(await dateTime() + ":" + "Navigating for getting rules and policies");
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting rules and policies");
                            console.log(chalk.yellow("Navigating for getting rules and policies"));
                            campgroundDetails['data']['rules_policies'] = await page.evaluate(funrulepolicy);
                            debugLog.push(await dateTime() + ":" + "Navigation Done for rules and policies");
                            crawlerLog.push(await dateTime() + ":" + "Navigation Done for rules and policies");
                            // console.log("Navigating for getting rules and policies done"+campdata['id']);

                            // code for getting images
                            await page.goto(link + 'albums/', { waitUntil: 'networkidle2' });
                            console.log(chalk.yellow("Navigating for getting Images"));
                            debugLog.push(await dateTime() + ":" + "Navigating for getting Images")
                            crawlerLog.push(await dateTime() + ":" + "Navigating for getting Images")
                            campgroundDetails['images'] = await page.evaluate(async (link) => {

                                let imageList = new Array();

                                var imageListTag = await document.querySelectorAll(' #mainContent > div.container-fluid.pt-4.pb-4 > div.row.albums.justify-content-center > div > a');
                                for (var k = 0; k < imageListTag.length; k++) {
                                    if (k < 2) {
                                        var img_url = imageListTag[k] && imageListTag[k].getAttribute("href");
                                        img_url = img_url.replace("../", "")
                                        var obj = {
                                            title: imageListTag[k] && imageListTag[k].innerText.trim(),
                                            url: link + img_url,
                                            imagedata: '',
                                        };
                                        imageList.push(obj);
                                    }
                                }

                                return imageList;

                            }, link);
                            debugLog.push(await dateTime() + ":" + "Fetching album wise images")
                            crawlerLog.push(await dateTime() + ":" + "Fetching album wise images")
                            //
                            for (var m = 0; m < campgroundDetails['images'].length; m++) {
                                if (m < 2) {
                                    for (var key in campgroundDetails['images']) {
                                        var img_url = campgroundDetails['images'][key]['url'];
                                        var albumname = campgroundDetails['images'][key]['title'];

                                        await page.goto(img_url, { waitUntil: 'networkidle2' });

                                        var datasourceID = campdata['data_source_id'];
                                        var campID = campdata['id'];

                                        var resultImage = await page.evaluate(async (albumname, datasourceID, campID) => {
                                            let imageArr = new Array();
                                            var imageLink = await document.querySelectorAll('#mainContent > div.container-fluid.pt-4.pb-4 > div.row.photos.justify-content-center > div > div > a');
                                            var re = /(?:\.([^.]+))?$/;
                                            for (var l = 0; l < imageLink.length; l++) {
                                                if (l < 2) {
                                                    var image_url = imageLink[l] && imageLink[l].getAttribute('href');

                                                    var url = image_url.split("?");
                                                    image_url = 'https://koa.com' + url[0];
                                                    var ext = re.exec(url[0])[0];
                                                    var album = albumname.replace(" ", "_")
                                                    var title = imageLink[l] && imageLink[l].innerText.trim();
                                                    var slug_image = await (title
                                                        .toLowerCase()
                                                        .replace(/ /g, '-')
                                                        .replace(/[^\w-]+/g, '') + ext).toLowerCase();
                                                    var imgobj = {
                                                        title: title,
                                                        // source_url: image_url,
                                                        album: albumname,
                                                        image: await uploadFileToS3(image_url, 'data_source/' + datasourceID + '/campgrounds/' + campID + '/images/' + album + "/" + slug_image),
                                                        // s3: '',
                                                    };
                                                    imageArr.push(imgobj);
                                                }
                                            }
                                            return imageArr;

                                        }, albumname, datasourceID, campID);
                                        // console.log('resultImage');
                                        // console.log(resultImage)
                                        campgroundDetails['images'][key]['imagedata'] = resultImage;

                                    }
                                }
                            }
                            // console.log(campgroundDetails)
                            debugLog.push(await dateTime() + ":" + "Fetching album wise images done")
                            crawlerLog.push(await dateTime() + ":" + "Fetching album wise images done")

                            result.push(campgroundDetails);

                            fs.writeFileSync(filePathStore + "temp/output" + campdata['id'] + ".json", JSON.stringify(campgroundDetails));
                            const filepath = path.resolve(filePathStore + "temp/output" + campdata['id'] + ".json");
                            var s3result;
                            // var CampLogfilename=new Date().getTime()+'_campground_log.json';
                            var CampLogfilename = campdata['id'] + '_campground_log.json';
                            debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            crawlerLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            s3result = await s3.uploadFile(filepath, 'data_source/' + campdata['data_source_id'] + '/campgrounds/' + campdata['id'] + '/' + CampLogfilename);
                            // if(result.)
                            var logFileLocation = '';
                            console.log(s3result)
                            if (s3result != '') {
                                // logFileLocation = result['Location'];
                                logFileLocation = CampLogfilename;
                                fs.unlinkSync(filepath)
                                debugLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                                crawlerLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                            } else {
                                debugLog.push(await dateTime() + ":" + "error in upload file to server")
                                crawlerLog.push(await dateTime() + ":" + "error in upload file to server")
                            }

                            debugLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            crawlerLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")

                            campgroundStatus = true;
                            addressDetail=campgroundDetails['contact']['address'];
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, logFileLocation,addressDetail);
                            successCamp++;

                        } catch (e) {
                            failedCamp++;
                            console.log(e)
                            debugLog.push(await dateTime() + ":" + 'Error in Scrapping Individual Campground');
                            crawlerstatus = false;
                            campgroundStatus = false;
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '','');
                        }
                    // }
                }
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);
                callback(result)
                // return result;

            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + 'Error in fetching All Campground Based on Datasource');
                crawlerstatus = false;
                console.log(err);
                callback(err)
            });
    } catch (e) {
        debugLog.push(await dateTime() + ":" + 'Error in Scrap all Detail');
        crawlerstatus = false;
        callback(e)
    }
}

async function storeCampGroundLogFile(crawlerLog, camp_id, campStatus, logFileLocation,address="") {
    try {
        const campUpdate = {
            data_file_path: logFileLocation,
            index_status: 'Completed',
            crawler_status: (campStatus == true) ? 'Success' : 'Failed',
            location:JSON.stringify(address),
        };
        CampGround.update(campUpdate, {
            where: { id: camp_id }
        })

        debugLog.push(await dateTime() + ":" + "updating campground log status and set log file path")
        crawlerLog.push(await dateTime() + ":" + "updating campground log status and set log file path")

        fs.writeFileSync(filePathStore + "temp/camp_crawler" + camp_id + ".json", JSON.stringify(crawlerLog));
        const filepath = path.resolve(filePathStore + "temp/camp_crawler" + camp_id + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_campground_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFile(filepath, 'data_source/' + datasourceId + '/campgrounds/' + camp_id + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Successfully uploaded Campground Crawler log file on server")

            fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Failed to upload Campground Crawler log file on server")
            crawlerstatus = false;
        }

        const campLogDataUpdate = {
            status: (campStatus == true) ? 'Success' : 'Failed',
            log_file_path: crawlerLogfilename,
        };
        CampgroundLog.update(campLogDataUpdate, {
            where: { id: campLogId }
        })
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing CampGround Log/CrawlerLog File');
    }
}
