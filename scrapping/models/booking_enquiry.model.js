const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
   
    const bookingEnquiry = sequelize.define("booking_enquiry", {
        campground_id: {
            type: DataTypes.BIGINT(20),
        },
        check_in_date: {
            field: 'check_in_date',
            type: Sequelize.DATE,
        },
        check_out_date: {
            field: 'check_out_date',
            type: Sequelize.DATE,
        },
        name: {
            type: Sequelize.STRING
        },
        address1: {
            type: Sequelize.STRING
        },
        address2: {
            type: Sequelize.STRING
        },
        country_code: {
            type: Sequelize.STRING
        },
        phone: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        state: {
            type: Sequelize.STRING
        },
        post_code: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        details: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        index_status: {
            type: Sequelize.STRING
        },
        comment: {
            type: Sequelize.STRING
        },
        log_file_path: {
            type: Sequelize.STRING
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: true,
        freezeTableName: true
    }
    );
    
    return bookingEnquiry;
};
