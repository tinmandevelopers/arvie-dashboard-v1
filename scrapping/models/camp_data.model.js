const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
    const Campdata = sequelize.define("camp_data", {
        campground_id: {
            type: DataTypes.BIGINT(20),
        },
        data_source_id: {
            type: DataTypes.BIGINT(20),
        },
        camp_data: {
            type: Sequelize.STRING
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
        deleted_at: {
            field: 'deleted_at',
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: true,
        freezeTableName: true
    }
    );

    return Campdata;
};
