const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
    const Campground = sequelize.define("campgrounds", {
        data_source_id: {
            type: DataTypes.BIGINT(20),
        },
        campground_group_id: {
            type: DataTypes.BIGINT(20),
        },
        title: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        location: {
            type: Sequelize.STRING
        },
        data_file_path: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        index_status: {
            type: Sequelize.STRING
        },
        crawler_status: {
            type: Sequelize.STRING
        },
        last_indexed_time: {
            field: 'last_indexed_time',
            type: Sequelize.DATE,
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
        deleted_at: {
            field: 'deleted_at',
            type: Sequelize.DATE,
        },
    },
        {
            timestamps: true,
            freezeTableName: true
        }
    );

    return Campground;
};
