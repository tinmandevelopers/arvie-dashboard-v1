const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
    const CampgroundGroup = sequelize.define("campgrounds_group", {
        data_source_id: {
            type: DataTypes.BIGINT(11),
        },
        name: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        country: {
            type: Sequelize.STRING
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: true,
        freezeTableName: true
    }
    );

    return CampgroundGroup;
};
