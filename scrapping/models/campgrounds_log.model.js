const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
    const CampgroundLog = sequelize.define("campgrounds_log", {
        crawler_log_id: {
            type: DataTypes.BIGINT(20),
        },
        campground_id: {
            type: DataTypes.BIGINT(20),
        },
        status: {
            type: Sequelize.STRING
        },
        log_file_path: {
            type: Sequelize.STRING
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
        deleted_at: {
            field: 'deleted_at',
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: true,
        freezeTableName: true
    }
    );

    return CampgroundLog;
};
