const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de
    const dataSourceCrawlerLog = sequelize.define("data_source_crawler_log", {
        data_source_id: {
            type: DataTypes.BIGINT(20),
        },
        log_file_path: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        createdAt: {
            field: 'created_at',
            type: Sequelize.DATE,
        },
        updatedAt: {
            field: 'updated_at',
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: true,
        freezeTableName: true
    }
    );

    return dataSourceCrawlerLog;
};
