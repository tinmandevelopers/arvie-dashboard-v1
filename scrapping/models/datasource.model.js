const {DataTypes} = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    sequelize.de

    const DataSource = sequelize.define("data_sources", {
            title: {
                type: Sequelize.STRING
            },
            url: {
                type: Sequelize.STRING
            },
            crawler_name: {
                type: Sequelize.STRING
            },
            crawler_file: {
                type: Sequelize.STRING
            },
            booking_file: {
                type: Sequelize.STRING
            },
            crawler_time: {
                type: Sequelize.STRING
            },
            last_crawled: {
                type: Sequelize.DATE
            },
            crawler_log: {
                type: Sequelize.STRING
            },
            status: {
                type: Sequelize.STRING
            },
            comment: {
                type: Sequelize.STRING
            },
            createdAt: {
                field: 'created_at',
                type: Sequelize.DATE,
            },
            updatedAt: {
                field: 'updated_at',
                type: Sequelize.DATE,
            },
            deleted_at: {
                field: 'deleted_at',
                type: Sequelize.DATE,
            },
        },
        {
            timestamps: true,
            freezeTableName: true
        }
    );

    return DataSource;
};
