const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;

db.CampGroundGroup = require("./campgrounds_group.model.js")(sequelize, Sequelize);
db.CampGround = require("./campgrounds.model.js")(sequelize, Sequelize);
db.DataSourceCrawlerLog = require("./data_source_crawler_log.model.js")(sequelize, Sequelize);
db.CampgroundLog = require("./campgrounds_log.model.js")(sequelize, Sequelize);
db.DataSource = require("./datasource.model.js")(sequelize, Sequelize);
db.State = require("./state.model.js")(sequelize, Sequelize);
db.BookingEnquiry = require("./booking_enquiry.model.js")(sequelize, Sequelize);
db.CampData = require("./camp_data.model.js")(sequelize, Sequelize);

module.exports = db;
