const axios = require('axios');
var fs = require('fs');
var express = require('express');
const puppeteer = require("puppeteer");
//Prod
// var configPath='../../../../../scrapping/';
// var filePathStore='../scrapping/';
//Local
var configPath = '../scrapping/';
var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
// var s3 = require("../scrapping/helper/s3imageUpload");
const chalk = require("chalk");
const db = require(configPath + "models");
// const db = require("../scrapping/models/index.js");
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const CampData = db.CampData;
const DataSource = db.DataSource;
const State = db.State;

const path = require('path')
const args = require('yargs').argv;

var debugLog = [];
var crawlerstatus = true;
var datasourceId = args.datasource_id;


(async () => {
  
    var condition = { data_source_id: datasourceId ,status: "Active" , web_status: "Listed"};
    var log=[];
    await CampGround.findAll({ where: condition })
    .then(async data => {
  
        console.log('total length is')
        console.log(data.length)
        console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));

        for (var p = 0; p < data.length; p++) {
            // console.log(data[p]['id'])
            // if (data[p]['id'] == 1269) {

                try {
                    var campdata = data[p];
                    
                    console.log(campdata['id']);
                    await axios.get(campdata['url'])
                    .then(async function (response) {
                        let rowList = [];
                        var address1 = response.data.campground.addresses[0].address1;
                        var address2 = response.data.campground.addresses[0].address2;
                        var address3 = response.data.campground.addresses[0].address3;
                        var city = response.data.campground.addresses[0].city;
                        var country_code = response.data.campground.addresses[0].country_code;
                        var state_code = response.data.campground.addresses[0].state_code;
                        var postal_code = response.data.campground.addresses[0].postal_code;
                        var address = address1 + ', ' + address2 + ', ' + address3;
                        var facility_latitude = response.data.campground.facility_latitude;
                        var facility_longitude = response.data.campground.facility_longitude;
                        
                        var rowData = {
                            address: address,
                            city: city,
                            state: state_code,
                            country: country_code,
                            zip: postal_code,
                            latitude: facility_latitude,
                            longitude: facility_longitude                          
                        };
                        console.log(rowData);

                        const campDataStore = {
                            campground_id: data[p]['id'],
                            data_source_id: campdata['data_source_id'],
                            camp_data:JSON.stringify(rowData),
                        };

                        await CampData.create(campDataStore)
                        .then(async data => {
                            log.push('Success :'+data[p]['id']);
                        })
                        .catch(async err => {
                            log.push('Error:'+data[p]['id']);
                            console.log('Error in New Creating CampGround data:'+data[p]['id']);
                            console.log(err.message)
                        });

                    });

                } catch (e) {
                    console.log(e);
                }
            // }
            }

            fs.writeFileSync(filePathStore + "temp/recreationAddress.txt", JSON.stringify(log));
    })
    .catch(async err => {
        debugLog.push(await dateTime() + ":" + "Error in fetching All Campground Based on Datasource");
        console.log(err);

    });


    })();