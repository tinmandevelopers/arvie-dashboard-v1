// Manually run commnad for Reference
// node scrapping/sunBookingReserve.js --campground_id={campground_id} --booking_id={booking_enquiry_id}
// node scrapping/recreationBookingReserve.js --campground_id=812 --booking_id=14

const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path');
const { resolve } = require("path");

//Prod
var configPath='../../../../../scrapping/';
var filePathStore='../scrapping/';
//Local
// var configPath = '../scrapping/';
// var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
const db = require(configPath + "models");
const CampGround = db.CampGround;
const BookingEnquiry = db.BookingEnquiry;

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var closeBrowser = true;
var bookingStatus = true;

var logInEmail = "qrnlgmdytsouvpicre@twzhhq.online";
var logInPassword = "Hello@007";

var form_siteCategory = '';
var form_checkIn = '';
var form_checkOut = '';
var form_group = '';
var form_inFants = '';
var form_adults = '';
var form_kids = '';
var form_pets = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_name_on_credit_card = '';
var form_slideOuts = '';
var form_VkrPostalCode = '';
var form_purchaseVkr_Group = '';
var form_firstName = '';
var form_lastName = '';
var form_address1 = '';
var form_address2 = '';
var form_countryCode = '';
var form_phone = '';
var form_city = '';
var form_state = '';
var form_postcode = '';
var form_vehicles = '';
var form_email = '';
var form_referal_resource = '';
var form_reason_visit = '';
var form_name_on_credit_card = "Testing user";
var form_creditCard = '4242424242424242';
var form_creditCardType = '';
var form_creditCardExpMonth = '02';
var form_creditCardExpYear = '34';
var form_creditCardCode = '111';
var comment = '';
var debugLog = [];

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getCampGroundByID() {
    debugLog.push(await dateTime() + ":" + 'Fetching campground detail by id');
    await CampGround.findByPk(campgroundId)
        .then(data => {

            siteUrl = data['url'].replace(/api[/]camps/g, "camping");

        })
        .catch(async err => {
            bookingStatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground By Id');
            console.log('Error in Fetching Campground By Id');
        });
}

async function getDateValues(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var monthName = checkDate.toLocaleString("default", { month: "long" })
    var dayNumber = checkDate.getDate();
    var year = checkDate.getFullYear();

    var dateArr = new Array();

    dateArr['month'] = monthName;
    dateArr['year'] = year;
    dateArr['day'] = dayNumber;
    dateArr['month_year'] = monthName + " " + year;
    dateArr['original_date'] = dateVal;
    // var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateArr;
}

async function getEnquiryDataID() {
    debugLog.push(await dateTime() + ":" + 'Fetching booking enquiry detail by id');
    await BookingEnquiry.findByPk(bookingId)
        .then(async data => {

            // form_siteCategory = "Tent";
            // form_checkIn = await getDateValues('2021-04-15');
            // form_checkOut =await getDateValues('2021-02-20);
            // form_postcode = '35242';
            // form_countryCode = 'USA';
            // form_address1 = 'Alabama';
            // form_address2 = 'Anywhere';
            // form_city = 'Dothan';
            // form_state = 'AL';
            // form_vehicles = '2';
            // form_group = '1';
            // form_equipmentLength = '15';
            // form_equipmentType = "Trailer";

            // data['details']=JSON.parse(data['details']);

            form_siteCategory = data['details']['site_category'];
            form_checkIn = await getDateValues(data['check_in_date']);
            form_checkOut = await getDateValues(data['check_out_date']);
            form_postcode = data['post_code'];
            form_countryCode = data['country_code'];
            form_address1 = data['address1'];
            form_address2 = data['address2'];
            form_city = data['city'];
            form_state = data['state'];
            form_vehicles = data['details']['vehicles'];
            form_group = data['details']['groups'];
            form_equipmentLength = data['details']['equipment_length'];
            form_equipmentType = data['details']['equipment_type'];
            // form_inFants = data['details']['infants'];
            // form_adults = data['details']['adults'];
            // form_kids = data['details']['kids'];
            // form_pets = data['details']['pet'];
            // form_equipmentType = data['details']['equipment_type'];
            // form_equipmentLength = data['details']['equipment_length'];
            // form_slideOuts = data['details']['slide_outs'];
            // form_firstName = data['name'];
            // form_lastName = data['name'];
            // form_address1 = data['address1'];
            // form_address2 = data['address2'];
            // form_countryCode = data['country_code'];
            // form_phone = data['phone'];
            // form_postcode = data['post_code'];
            // form_email = data['email'];
            // form_referal_resource = data['details']['referal_resource'];
            // form_reason_visit = data['details']['reason_visit'];

        })
        .catch(async err => {
            bookingStatus = false;
            console.log(err.message)
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Enquiry By Id');
            console.log('Error in Fetching Enquiry By Id');
        });

    const enquiryUpdate = {
        index_status: 'InProgress',
        updated_at: Date.now(),
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })
}

async function checkDate(indate, outdate) {
    
    var checkInDate = new Date(indate);
    var checkOutDate = new Date(outdate);
    var response = [];
    // Get today's date
    var todaysDate = new Date();

    // call setHours to take the time out of the comparison
    if (checkInDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
        response['message'] = "Check In Date is Less then current date";
        response['status'] = false;
    } else {
        if (checkOutDate.setHours(0, 0, 0, 0) < todaysDate.setHours(0, 0, 0, 0)) {
            response['message'] = "Check Out Date is Less then current date";
            response['status'] = false;
        } else {
            if (checkOutDate.setHours(0, 0, 0, 0) <= checkInDate.setHours(0, 0, 0, 0)) {
                response['message'] = "Check Out Date needs to be greter then check in date";
                response['status'] = false;
            } else {
                response['message'] = "Both date are Valid";
                response['status'] = true;
            }
        }
    }
    return response;
}

async function selectDatePicker(inMonthyear, inDayValue, inYearValue) {
    return new Promise(async function (resolve, reject) {
        // debugger;
        var calenderMonth = document.querySelectorAll('.CalendarMonth');
        var checkInFlag = false;
        for (var i = 0; i < calenderMonth.length; i++) {
            var datavisible = await calenderMonth[i].getAttribute("data-visible");
            if (datavisible) {

                var title = await calenderMonth[i].querySelector('div > strong').textContent;

                var titleYear = parseInt(new Date(title).getFullYear());
                var inputYear = parseInt(inYearValue);

                if (inputYear === titleYear) {
                    if (title == inMonthyear) {
                        var availDays = await calenderMonth[i].querySelectorAll('table > tbody > tr > td > div.rec-calendar-day > div >strong');
                        for (var d = 0; d < availDays.length; d++) {
                            var dayVal = await availDays[d].parentNode.parentNode.querySelector('div').textContent;
                            if (dayVal == inDayValue) {
                                await availDays[d].parentNode.parentNode.querySelector('div').click();
                                checkInFlag = true;
                                // console.log(dayVal+"=="+inDayValue)
                                resolve("true")
                            }
                        }
                    }
                } else {
                    resolve("false");
                }
            }
        }

        if (checkInFlag == false) {
            await document.querySelector('.sarsa-day-picker-range-controller-month-navigation-button.right').click();
            
            setTimeout(async function () {
                await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(function (result) {
                    if (result=="true") {
                        resolve("true")
                    }else{
                        resolve("false");
                    }
                });
            }, 5000)
        }else{
            resolve("true")
        }
    });
}

(async () => {
    try {

        await getCampGroundByID();
        await getEnquiryDataID();

        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });
        console.log('*************************');
        console.log(await browser.version());
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        try {
            // console.log('Nevigating : ' + campgrounds[p].title)
            console.log('Nevigating : ' + siteUrl)
            debugLog.push(await dateTime() + ":" + 'Nevigating : ' + siteUrl);
            await page.setViewport({ width: 1300, height: 768 });

            await page.setDefaultNavigationTimeout(0);

            var browseUrl = siteUrl;
            // var browseUrl = campgrounds[p].link + 'reserve';
            debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

            await page.goto(browseUrl);
            // console.log('browseUrl:' + browseUrl)
            await sleep(3000)

            // page.on('console', msg => {
            //     for (let i = 0; i < msg._args.length; ++i)
            //         console.log(`${msg._args[i]}`)
            // });

            if (await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(3) > button")) {
                debugLog.push(await dateTime() + ":" + "Clicking Add More Filters");
                page.click("#campsite-filter-container > div > div > div:nth-child(3) > button");

                await sleep(1000);

                var siteCatAvailable = await page.evaluate(async (form_siteCategory) => {
                    var allcampSite = document.querySelectorAll("[id^='campsite-type']:not([disabled]");
                    var found = false;
                    for (var a = 0; a < allcampSite.length; a++) {
                        var str = allcampSite[a].getAttribute('value').split(" ");
                        console.log(str[0] + "==" + form_siteCategory);
                        if (str[0] == form_siteCategory) {
                            allcampSite[a].parentNode.click()
                            found = true;
                        }
                    }
                    document.querySelector('#shared-filter-options-modal > div > div > div > div > div > button').click();
                    return found;
                }, form_siteCategory)
                debugLog.push(await dateTime() + ":" + "Checking if site available is found or not");
                if (siteCatAvailable) {

                    debugLog.push(await dateTime() + ":" + "site available is found now selecting dates");

                    var selectDates = await page.waitForSelector("#campsite-filter-container > div > div > div:nth-child(1) > button")

                    let title = await page.evaluate(el => el.textContent, selectDates)
                    console.log(title.trim());
                    if (title == 'Select Dates ...') {
                        await page.click('#campsite-filter-container > div > div > div:nth-child(1) > button');
                    }

                    debugLog.push(await dateTime() + ":" + "Verifying CheckIn/Out Date");

                    var verifyCheckInDate = await checkDate(form_checkIn['original_date'], form_checkOut['original_date']);
                    // Testing
                    // page.on('console', msg => {
                    //     for (let i = 0; i < msg._args.length; ++i)
                    //         console.log(`${msg._args[i]}`);
                    // });

                    // console.log(verifyCheckInDate);

                    if (verifyCheckInDate['status'] == true) {
                        debugLog.push(await dateTime() + ":" + "If date is valid then selecting date");

                        var inMonthyear = form_checkIn['month_year'];
                        var inDayValue = form_checkIn['day'];
                        var inYearValue = form_checkIn['year'];
                        var bothDateSelected = false;
                        
                        await page.addScriptTag({ content: `${selectDatePicker}` });
                        var inDateSelect = await page.evaluate(async (inMonthyear, inDayValue, inYearValue) => {

                            // return new Promise(function (resolve, reject) {
                            var resultVal = false;
                            await selectDatePicker(inMonthyear, inDayValue, inYearValue).then(async function (result) {
                                resultVal = result
                            });
                            return resultVal;

                        }, inMonthyear, inDayValue, inYearValue)

                        if (inDateSelect == "true") {
                            debugLog.push(await dateTime() + ":" + "If in date found then selcting out date");
                            await sleep(1500)
                        
                            var outMonthyear = form_checkOut['month_year'];
                            var outDayValue = form_checkOut['day'];
                            var outYearValue = form_checkOut['year'];


                            var outDateSelect = await page.evaluate(async (outMonthyear, outDayValue, outYearValue) => {

                                // return new Promise(function (resolve, reject) {
                                var resultVal = false;
                                await selectDatePicker(outMonthyear, outDayValue, outYearValue).then(async function (result) {
                                    resultVal = result
                                });
                                return resultVal;

                            }, outMonthyear, outDayValue, outYearValue)

                            // console.log("outDateSelect:"+outDateSelect)
                            if (outDateSelect == "false") {
                                debugLog.push(await dateTime() + ":" + "Check Out Date is Not Available");
                                comment='Check Out Date is Not Available';
                                console.log('Check Out Date is Not Available')
                                bookingStatus=false;
                            }
                            if (inDateSelect == "true" && outDateSelect == "true") {
                                bothDateSelected = true;
                                debugLog.push(await dateTime() + ":" + "Both date are available");

                            }

                        } else {
                            debugLog.push(await dateTime() + ":" + "Check in Date is Not Available");
                            comment='Check in Date is Not Available';
                            console.log('Check in Date is Not Available');
                            bookingStatus=false;
                        }

                        await page.click('#shared-filter-options-modal > div > div > div > div > div > button');
                        await sleep(3000)
                        if (bothDateSelected) {

                            if (await page.$(".campsite-search-card-column:nth-child(1) .rec-flex-card-price-button-wrap:nth-child(1) > button:not([disabled]") !== null) {
                                debugLog.push(await dateTime() + ":" + "Selecting First campsite");
                                
                                await page.click('.campsite-search-card-column:nth-child(1) .rec-flex-card-price-button-wrap:nth-child(1) > button');
                                debugLog.push(await dateTime() + ":" + "Filing Login Details");
                                if (await page.$("#rec-acct-sign-in-email-address") !== null) {
                                    const loginemail = await page.waitForSelector("#rec-acct-sign-in-email-address");
                                    await page.click("#rec-acct-sign-in-email-address");
                                    await loginemail.type(logInEmail)
                                }

                                if (await page.$("#rec-acct-sign-in-password") !== null) {
                                    const loginpass = await page.waitForSelector("#rec-acct-sign-in-password");
                                    await page.click("#rec-acct-sign-in-password");
                                    await loginpass.type(logInPassword)
                                }

                                if (await page.$(".flex-grid.justify-content-center > div > div > div > form > button") !== null) {
                                    await page.click('.flex-grid.justify-content-center > div > div > div > form > button');
                                }
                                await sleep(2000)
                                console.log('checking Login is success or not')
                                if(await page.$(".sarsa-alert.error .alert-body") !== null){
                                    
                                    let element = await page.$('.sarsa-alert.error .alert-body');
                                    let errorMsg = await page.evaluate(el => el.textContent, element)
                                    console.log(errorMsg)
                                    debugLog.push(await dateTime() + ":" + errorMsg);
                                    comment=errorMsg;
                                    bookingStatus=false;
                                    if(await page.$(".rec-acct-signin-modal .rec-close-modal") !== null){
                                        await page.click(".rec-acct-signin-modal .rec-close-modal");
                                    }
                                }else{
                                    debugLog.push(await dateTime() + ":" +"Login is successful");
                                    await sleep(5000)
                                    console.log('checking Login is success');
                                    debugLog.push(await dateTime() + ":" +"Login is successful so now filling form details");

                                    if (await page.$(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']") !== null) {
                                        const postalcode = await page.waitForSelector(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']");
                                        await page.click(".camp-group-leader-details-form > div.flex-grid  >div:nth-child(3) input[type='text']");
                                        await postalcode.type(form_postcode)
                                        debugLog.push(await dateTime() + ":" +"Adding Post Code");
                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(2) > div> div > div> div > select") !== null) {
                                        await page.select('.camp-group-leader-address-form > div:nth-child(2) > div> div > div> div > select', form_countryCode)
                                        debugLog.push(await dateTime() + ":" +"selecting Country");
                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input") !== null) {
                                        const address = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input");
                                        await page.click(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(1) > div > div input");
                                        await address.type(form_address1)
                                        debugLog.push(await dateTime() + ":" +"Adding Address1");
                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input") !== null) {
                                        const address2 = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input");
                                        await page.click(".camp-group-leader-address-form > div:nth-child(3) > div:nth-child(2) > div > div input");
                                        await address2.type(form_address2)
                                        debugLog.push(await dateTime() + ":" +"Adding Address2");
                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input") !== null) {
                                        const city = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input");
                                        await page.click(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(1) > div > div input");
                                        await city.type(form_city)
                                        debugLog.push(await dateTime() + ":" +"Adding city");

                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div >select") !== null) {
                                        await page.select('.camp-group-leader-address-form > div:nth-child(4) > div:nth-child(2) > div > div > div >select', form_state)
                                        debugLog.push(await dateTime() + ":" +"Selecting State");
                                    }
    
                                    if (await page.$(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input") !== null) {
                                        const postcode = await page.waitForSelector(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input");
                                        await page.click(".camp-group-leader-address-form > div:nth-child(4) > div:nth-child(3) > div >div >div input");
                                        await postcode.type(form_postcode)
                                        debugLog.push(await dateTime() + ":" +"Adding Post Code");
                                    }
    
                                    if (await page.$(".camp-group-size-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)") !== null) {
    
                                        for (var i = 0; i < parseInt(form_group); i++) {
                                            await page.click(".camp-group-size-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)");
                                        }
                                        debugLog.push(await dateTime() + ":" +"Adding campgroup");

                                    }
    
                                    if (await page.$(".camp-camping-equipment-form > div > div > div >div> label") !== null) {
                                        var selectEquipment = await page.evaluate(async (form_equipmentType, form_equipmentLength) => {
                                            var totalLbl = document.querySelectorAll(".camp-camping-equipment-form > div > div > div >div");
                                            console.log(totalLbl.length)
                                            for (var l = 0; l < totalLbl.length; l++) {
                                                var equipmentName = totalLbl[l].querySelector('label > span:nth-child(3)').firstChild.nodeValue;
    
                                                if (equipmentName.trim() == form_equipmentType.trim()) {
    
                                                    totalLbl[l].querySelector('label > span:nth-child(3)').click();
                                                    if (totalLbl[l].querySelector('input[id^=equip_' + totalLbl[l].querySelector('label > input:checked').value + '_length]') !== null) {
    
                                                        totalLbl[l].querySelector('input[id^=equip_' + totalLbl[l].querySelector('label > input:checked').value + '_length]').value = form_equipmentLength;
                                                        totalLbl[l].querySelector('input[id^=equip_' + totalLbl[l].querySelector('label > input:checked').value + '_length]').setAttribute('value', form_equipmentLength);
    
                                                    }
    
                                                }
                                            }
    
                                        }, form_equipmentType, form_equipmentLength);
                                        debugLog.push(await dateTime() + ":" +"Adding equipment");

                                    }
                                    await sleep(1000);
    
                                    if (await page.$(".camp-vehicle-info-form  input") !== null) {
                                        for (var i = 0; i < parseInt(form_vehicles); i++) {
                                            await page.click(".camp-vehicle-info-form > div > div > div > div > div.sarsa-number-field > button:nth-child(3)");
                                        }
                                        debugLog.push(await dateTime() + ":" +"Adding Vehicle Information");

                                    }
                                    await sleep(1000);
    
                                    if (await page.$(".rec-order-detail-need-to-know.sarsa-need-to-know > div > label >span") !== null) {
                                        await page.click(".rec-order-detail-need-to-know.sarsa-need-to-know > div > label >span");
                                        debugLog.push(await dateTime() + ":" +"Selecting Terms and conditions");
                                    }
                                    await sleep(1000);
    
                                    console.log('Proceeed to cart')
                                    if (await page.$("#action-bar-submit") !== null) {
                                        await page.click("#action-bar-submit");
                                        debugLog.push(await dateTime() + ":" +"Click on proceed to cart");
                                    }
    
                                    console.log('Proceeed to Payment')
                                    await sleep(5000);
                                    if (await page.$(".cart-order-summary-actions > button.rec-button-primary-large") !== null) {
                                        await page.click(".cart-order-summary-actions > button.rec-button-primary-large");
                                        debugLog.push(await dateTime() + ":" +"Click on proceed to Payment");

                                    }
    
                                    if (await page.$(".sarsa-form-validation-alert .alert-body") !== null) {
                                        let element = await page.$('.sarsa-form-validation-alert .alert-body');
                                        let errorMsg = await page.evaluate(el => el.textContent, element)
                                        console.log(errorMsg)
                                        debugLog.push(await dateTime() + ":" +errorMsg);
                                        bookingStatus=false;

                                    } else {
    
                                        await sleep(5000);
                                        if (await page.$(".rec-cart-form-field input[name='name']") !== null) {
                                            const nameOnCard = await page.waitForSelector(".rec-cart-form-field input[name='name']");
                                            await page.click(".rec-cart-form-field input[name='name']");
                                            await nameOnCard.type(form_name_on_credit_card)
                                            debugLog.push(await dateTime() + ":" +"Adding Card Name");
                                        }
                                        await sleep(1000)
                                        if (await page.$(".rec-cart-form-field input[name='number']") !== null) {
                                            const cardNumber = await page.waitForSelector(".rec-cart-form-field input[name='number']");
                                            await page.click(".rec-cart-form-field input[name='number']");
                                            await cardNumber.type(form_creditCard)
                                            debugLog.push(await dateTime() + ":" +"Adding Card Number");
                                        }
                                        await sleep(1000)
                                        if (await page.$(".rec-cart-form-field select[name='month']") !== null) {
                                            await page.select(".rec-cart-form-field select[name='month']", form_creditCardExpMonth)
                                            debugLog.push(await dateTime() + ":" +"Adding Card Month");
                                        }
    
                                        if (await page.$(".rec-cart-form-field select[name='year']") !== null) {
                                            await page.select(".rec-cart-form-field select[name='year']", form_creditCardExpYear)
                                            debugLog.push(await dateTime() + ":" +"Adding Card Year");
                                        }
    
                                        if (await page.$(".rec-cart-form-field input[name='cvc']") !== null) {
                                            const cardNo = await page.waitForSelector(".rec-cart-form-field input[name='cvc']");
                                            await page.click(".rec-cart-form-field input[name='cvc']");
                                            await cardNo.type(form_creditCardCode)
                                            debugLog.push(await dateTime() + ":" +"Adding Card Code");
                                        }
                                        await sleep(1000)
    
                                        if (await page.$("#page-body > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > button:not([disabled]") !== null) {
                                            await page.click("#page-body > div > div > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > button")
                                            debugLog.push(await dateTime() + ":" +"Clicking on next");
                                            if (await page.$("#page-body > div > div > div > div > div.flex-col-md-8 > div.ml-1.mr-2 > div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md") !== null) {
                                                await page.click("#page-body > div > div > div > div > div.flex-col-md-8 > div.ml-1.mr-2 > div.flex-grid.justify-content-end.mt-3.mr-half > button.sarsa-button.ml-1.sarsa-button-primary.sarsa-button-md")
                                                debugLog.push(await dateTime() + ":" +"Clicking on Confirm Button");
                                                await sleep(5000)
        
                                                if (await page.$(".cart-procesing-modal-text") !== null) {
                                                    let element = await page.$('.cart-procesing-modal-text');
                                                    let message = await page.evaluate(el => el.textContent, element)
                                                    console.log(message);
                                                    comment=message;
                                                    debugLog.push(await dateTime() + ":" +message);
                                                    bookingStatus=true;
                                                }
                                            }
                                        }else{
                                            debugLog.push(await dateTime() + ":" +"Invalid Payment Details");
                                            console.log('Invalid Payment Details')
                                            comment='Invalid Payment Details';
                                            bookingStatus=false;

                                        }
        
                                    }
                                }


                            } else {
                                console.log('if campsite not found the fetching message')
                                if (await page.$(".rec-notification-body") !== null) {
                                    let element = await page.$('.rec-notification-body');
                                    let errorMsg = await page.evaluate(el => el.textContent, element)
                                    console.log(errorMsg);
                                    comment=errorMsg;
                                    debugLog.push(await dateTime() + ":" +errorMsg);
                                    bookingStatus=false;
                                }else{
                                    debugLog.push(await dateTime() + ":" +"There are no campsites are availble");
                                    console.log("There are no campsites are availble");
                                    comment='There are no campsites are availble';
                                    bookingStatus=false;
                                }
                            }
                        }
                    } else {
                        debugLog.push(await dateTime() + ":" +"Date Error :" + verifyCheckInDate['message']);
                        console.log("Date Error :" + verifyCheckInDate['message']);
                        comment=verifyCheckInDate['message'];
                        bookingStatus=false;
                    }
                }
                else {
                    debugLog.push(await dateTime() + ":" +"No Site Category available");
                    console.log('No Site Category available')
                    comment='No Site Category available';
                    bookingStatus=false;
                }
            }


            // console.log(calenderMonth);
        } catch (err) {
            await browser.close();
            debugLog.push(await dateTime() + ":" + "Sorry, an error has ocurred ")
            comment = 'Sorry, an error has ocurred';
            bookingStatus = false;
            console.log(err)
        }
            await browser.close();

    } catch (err) {
        console.log(err);
        bookingStatus = false;
        await browser.close();
        debugLog.push(await dateTime() + ":" + "Something Went Wrong")
        comment = 'Something Went Wrong';
        console.log("Browser Closed");
    }
       await storeBookingCommentAndLog();

})();

async function storeBookingCommentAndLog() {

    fs.writeFileSync(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json", JSON.stringify(debugLog));
    const filepath = path.resolve(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json");

    var s3result;
    var crawlerLogfilename = new Date().getTime() + '_booking_log.json';

    // var CampLogfilename=datasourceId+'_crawler_log.json';
    debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

    s3result = await s3.uploadFile(filepath, 'booking/' + campgroundId + '/' + bookingId + '/' + crawlerLogfilename);

    console.log(s3result)
    if (s3result != '') {
        console.log("successfully uploaded Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Successfully uploaded Booking Crawler log file on server")
        fs.unlinkSync(filepath)
    } else {
        crawlerLogfilename = '';
        console.log("Failed to upload Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Failed to upload Booking Crawler log file on server")
        crawlerstatus = false;
    }

    debugLog.push(await dateTime() + ":" + "Storing Comment And Update the Booking Enquiry Status")

    const enquiryUpdate = {
        index_status: 'Completed',
        comment: comment,
        log_file_path: crawlerLogfilename,
        updated_at: Date.now(),
        status: (bookingStatus == true) ? 'Success' : 'Failed',
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })

}

async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}
