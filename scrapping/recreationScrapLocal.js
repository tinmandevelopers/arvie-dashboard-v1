const axios = require('axios');
var fs = require('fs');
var express = require('express');
const puppeteer = require("puppeteer");
//Prod
// var configPath='../../../../../scrapping/';
// var filePathStore='../scrapping/';
//Local
var configPath = '../scrapping/';
var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
// var s3 = require("../scrapping/helper/s3imageUpload");
const chalk = require("chalk");
const db = require(configPath + "models");
// const db = require("../scrapping/models/index.js");
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const DataSource = db.DataSource;
const State = db.State;

const path = require('path')
const args = require('yargs').argv;

var debugLog = [];
var crawlerstatus = true;
var datasourceId = args.datasource_id;
var statusVal = '';
statusVal = args.status;
if (statusVal != undefined) {
    console.log('Status Argument Value : ' + statusVal)
    if (statusVal != 'pending') {
        statusVal = '';
    }
} else {
    statusVal = '';
}
console.log('Status Value Is :' + statusVal)

var size = 20;
var campGroundArr = [];
var lastTimeFetch = false;
var crawlerLogId = 0;
var siteUrl = '';
(async () => {

    try {

        var startdatetime = new Date();
        console.log("Start Time: " + startdatetime);

        const dataSourceData = await getDatasourceByID();

        const storeLog = await storeCrawlerLog();
        if (statusVal == '') {
            // const stateNames = await fetchAllState();

            // await storeCampGrounds();
        }
        var result = await scrapeAll();

        await storeCrawlerLogFile();
        // campGroundArr.push({ 'start_at': startdatetime });
        // var datetime = new Date();
        // console.log("End Time: " + datetime);
        // campGroundArr.push({ 'end_at': datetime });

        console.log("total Campground is" + campGroundArr.length);
        // fs.writeFileSync("../scrapping/all_recreation_campgrounds_all.json", JSON.stringify(campGroundArr));

    } catch (error) {
        console.log(error);
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + error);
        await storeCrawlerLogFile()
    }

})();

async function storeCrawlerLogFile() {
    try {

        fs.writeFileSync(filePathStore + "temp/crawler" + datasourceId + ".json", JSON.stringify(debugLog));
        const filepath = path.resolve(filePathStore + "temp/crawler" + datasourceId + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Crawler log file to server");
            // fs.unlinkSync(filepath)
        } else {

            crawlerLogfilename = '';
            console.log("Failed to upload Crawler log file to server");
        }

        const crawlerUpdate = {
            log_file_path: 'Log:' + crawlerLogfilename,
            status: (crawlerstatus == true) ? 'Success' : 'Failed',
            updated_at: Date.now(),
        };
        await DataSourceCrawlerLog.update(crawlerUpdate, {
            where: { id: crawlerLogId }
        })
        var statusVal = (crawlerstatus == true) ? 'Success' : 'Failed';
        const dataSourceUpdate = {
            crawler_log: statusVal + ':' + crawlerLogfilename,
        };

        await DataSource.update(dataSourceUpdate, {
            where: { id: datasourceId }
        });

    } catch (e) {
        console.log('Error in Updating DataSource and Storing Crawler File')
        console.log(e)
    }
}

async function getDatasourceByID() {
    await DataSource.findByPk(datasourceId)
        .then(async data => {
            siteUrl = data['url']
            var lastchar = siteUrl.charAt(siteUrl.length - 1);
            var slace = (lastchar == '/') ? "" : "/";
            siteUrl = siteUrl + slace;
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching DataSource By Id');
        });
}

async function storeCrawlerLog() {
    let response = [];
    const logData = {
        data_source_id: datasourceId,
        status: 'InProgress',
    };
    debugLog.push(await dateTime() + ":" + 'Storing Crawler Log');
    await DataSourceCrawlerLog.create(logData)
        .then(async data => {

            response['status'] = 'success';
            response['content'] = data;
            // return data;
            crawlerLogId = data['id'];
            debugLog.push(await dateTime() + ":" + 'Crawler Log Stored Successfully');
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in crawler log storing');
            debugLog.push(await dateTime() + ":" + err.message);

            response['status'] = 'error';
            response['content'] = err;
            // return err;
            // console.log(err.message)
        });
    return response;
}

var campArr = [];
async function fetchAllState() {
    try {

        await State.findAll()
            .then(state => {
                // console.log(JSON.stringify(state));

                for (var l = 0; l < state.length; l++) {

                    var res = state[l]['state_name'].trim();
                    var stateStr = res.replace(" ", "+");

                    var campGroupUrl = "https://www.recreation.gov/api/search?fq=entity_type%3Acampground&fq=state_code_s%3A" + stateStr + "&fq=reservable%3A1";
                    campGroundArr[l] = {
                        'country_name': 'United States',
                        'camp_group': res,
                        'camp_group_url': campGroupUrl,
                        'campground': [],
                    }

                }
            })
            .catch(err => {
                console.log(err);
            });

        for (var l = 0; l < campGroundArr.length; l++) {
            // for (var l = 0; l < 2; l++) {
            console.log('in loop coming');

            console.log(campGroundArr[l]['camp_group'])
            campArr = [];
            lastTimeFetch = false;
            campground = await fetchCampgroundList(campGroundArr[l]['camp_group_url'], 0);
            // console.log('final campground')
            campGroundArr[l]['campground'] = campArr;
            // console.log(campArr);
        }
        return campGroundArr;
        // console.log(stateArr);

    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in FetchGround States');
        console.log(err);
        // await browser.close();
        // console.log("Browser Closed");
        return [];
    }

}
var totalCamp = 0;

async function fetchCampgroundList(campGroupUrl, start = 0) {
    try {
        console.log(campGroupUrl + '&start=' + start + '&size=' + size);
        await axios.get(campGroupUrl + '&start=' + start + '&size=' + size)
            .then(async response => {
                totalCamp = response.data.total;
                if (totalCamp > 0) {
                    for (var i = 0; i < response.data.results.length; i++) {
                        var campId = response.data.results[i].entity_id;
                        campArr.push({
                            'title': response.data.results[i].name,
                            'url': 'https://www.recreation.gov/api/camps/campgrounds/' + campId
                        })

                    }
                }
                // console.log(campArr);
                start = start + size;
                // console.log('in lasttimefetchvalue : '+lastTimeFetch)
                if (start < totalCamp) {
                    await fetchCampgroundList(campGroupUrl, start)
                } else {
                    // if (lastTimeFetch == false) {
                    //     lastTimeFetch = true;
                    //     console.log('in lasttimefetch')
                    //     await fetchCampgroundList(campGroupUrl, start)
                    // } else {
                    return campArr;
                    // }
                    // return campArr;
                }

            })
            .catch(error => {
                console.log("Error in FetchGround List Api");
                console.log(error);
                // crawlerstatus = false;
                // debugLog.push(await dateTime() + ":" + "Error in FetchGround List Api");
                return campArr;
            })

    } catch (err) {
        // Catch and display errors
        // crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground List');

        console.log("Error in FetchGround List");
        console.log(err);
        return campArr;
    }

}

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
    }
}

async function storeCampGrounds() {
    try {

        // console.log(campdata[0].camp_group)
        debugLog.push(await dateTime() + ":" + 'In store campground function');
        for (var key in campGroundArr) {
            // console.log('in key:' + key)
            // console.log(campGroundArr[key]['camp_group'])
            var gname = campGroundArr[key]['camp_group'];
            var cntname = campGroundArr[key]['country_name'];
            var groupurl = campGroundArr[key]['camp_group_url'];

            const campGroup = {
                name: gname,
                data_source_id: datasourceId,
                country: cntname,
                url: groupurl
            };

            const camp = campGroundArr[key]['campground'];

            // console.log("camp is ")
            // console.log(campGroundArr[key])
            // console.log(campGroundArr[key]['campground'])
            // debugLog.push(dateTime() + ":" + 'checking campgroup is added or not');
            await CampGroundGroup
                .findOne({ where: campGroup })
                .then(function (obj) {
                    // update
                    if (obj) {
                        return obj.update(campGroup)
                            .then(async data => {
                                for (const campkey in camp) {
                                    // console.log(camp[campkey]['url'])
                                    const cont = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url'],
                                        // status: 'Active',
                                        // index_status: 'Pending',
                                        // last_indexed_time:Date.now(),

                                    };
                                    // console.log(cont)
                                    // debugLog.push(dateTime() + ":" + 'checking campground is added or not');
                                    await CampGround.findOne({ where: cont })
                                        .then(async function (obj) {
                                            // debugLog.push(dateTime() + ":" + 'updating campground ');

                                            const campdata = {
                                                data_source_id: datasourceId,
                                                campground_group_id: data['id'],
                                                title: camp[campkey]['title'],
                                                url: camp[campkey]['url'],
                                                status: 'Pending',
                                                index_status: 'Pending',
                                                last_indexed_time: Date.now(),
                                            };
                                            if (obj) {
                                                // console.log('updated data')
                                                // console.log(obj)

                                                obj.update(campdata)
                                                    .then(async data => { })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Updating Already Stored Campground');
                                                    });
                                            } else {
                                                await CampGround.create(campdata)
                                                    .then(async data => { })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Creating Already Stored Campground');
                                                    });
                                            }
                                        })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            debugLog.push(await dateTime() + ":" + 'Error in Checking if Capground Already Stored or Not');
                                        });
                                }
                            })
                            .catch(async err => {
                                console.log(err)
                                crawlerstatus = false;
                                debugLog.push(await dateTime() + ":" + 'Error in Updating Campground');
                            });

                    } else {
                        // insert
                        // debugLog.push(dateTime() + ":" + ' campgroup creating');
                        return CampGroundGroup.create(campGroup, camp)
                            .then(async data => {
                                // debugLog.push(dateTime() + ":" + ' campgroup created');
                                for (var campkey in camp) {
                                    // console.log('in campground')
                                    const campground = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url'],
                                        status: 'Pending',
                                        index_status: 'Pending',
                                        last_indexed_time: Date.now(),

                                    };
                                    // debugLog.push(dateTime() + ":" + ' campground creating');
                                    await CampGround.create(campground).then(async data => { })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            console.log('Error in Creating New Campground');
                                        });
                                }
                            })
                            .catch(async err => {
                                crawlerstatus = false;
                                console.log('Error in Storing New Campground');
                                console.log(err)
                                debugLog.push(await dateTime() + ":" + 'Error in Storing New Campground');
                            });
                    }

                })
                .catch(async err => {
                    crawlerstatus = false;
                    console.log(await dateTime() + ":" + 'Error in Storing Campground')
                    console.log(err)
                    debugLog.push(await dateTime() + ":" + 'Error in Storing Campground');
                });
        }


    } catch (e) {
        console.log(e)
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing All Campgrounds and Campground Groups');
    }

}

async function concateString(value, comma = true) {
    var comaVal = (comma == true) ? ', ' : '';
    var text = '';
    text = (value != '' && value != null) ? value + comaVal : '';

    return text;
}

var campGroundID = 0;
async function scrapeAll() {
    var result = [];
    try {

        var campgroundStatus = true;
        debugLog.push(await dateTime() + ":" + 'In Fetching campground detail.');

        var successCamp = 0;
        var failedCamp = 0;
        var condition;
        console.log(statusVal)
        if (statusVal != '') {
            condition = { data_source_id: datasourceId, index_status: statusVal };
        } else {
            // console.log('in else')

            condition = { data_source_id: datasourceId ,status: "Active" , web_status: "Listed"};
        }

        await CampGround.findAll({ where: condition })
            .then(async data => {
                console.log('total length is')
                console.log(data.length)
                console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));

                for (var p = 0; p < data.length; p++) {

                    if (data[p]['id'] == 812) {

                        console.log(chalk.yellow("Navigating : " + data[p].title));
                        var crawlerLog = [];
                        try {
                            // console.log("title:"+data[p].title);
                            var campdata = data[p];
                            // console.log('title is '+ campdata['title'])
                            let campgrounds = [];
                            debugLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                            crawlerLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");

                            var link = '';
                            var title = '';
                            link = campdata['url'];
                            title = campdata['title'];
                            campGroundID = campdata['id'];
                            var addressDetail="";
                            var campId = link.split("/").pop();

                            debugLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                            crawlerLog.push(await dateTime() + ":" + "updating campground status to InProgress");

                            const campUpdate = {
                                index_status: "InProgress",
                            };
                            await CampGround.update(campUpdate, {
                                where: { id: campdata['id'] }
                            }).then(async data => { })
                                .catch(async err => {
                                    crawlerstatus = false;
                                    console.log(err.message)
                                    debugLog.push(await dateTime() + ":" + 'Error in Updating CampGround Status To In Progress');
                                });


                            debugLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            crawlerLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            const campLogData = {
                                crawler_log_id: crawlerLogId,
                                campground_id: campdata['id'],
                                status: 'Pending',
                            };

                            await CampgroundLog.create(campLogData)
                                .then(async data => {
                                    campLogId = data['id'];
                                })
                                .catch(async err => {
                                    debugLog.push(await dateTime() + ":" + 'Error in New Creating CampGround Log');
                                    console.log(err.message)
                                });

                            let campgroundDetails = await fetchCampDetail(link, campId, campGroundID);
                            // console.log('campgroundDetails Values')
                            // console.log(campgroundDetails)
                            result.push(campgroundDetails);

                            fs.writeFileSync(filePathStore + "temp/output" + campdata['id'] + ".json", JSON.stringify(campgroundDetails));
                            const filepath = path.resolve(filePathStore + "temp/output" + campdata['id'] + ".json");
                            var s3result;
                            // var CampLogfilename=new Date().getTime()+'_campground_log.json';
                            var CampLogfilename = campdata['id'] + '_campground_log.json';
                            debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            crawlerLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            s3result = await s3.uploadFileLocal(filepath, 'data_source/' + campdata['data_source_id'] + '/campgrounds/' + campdata['id'] + '/' + CampLogfilename);
                            // if(result.)
                            var logFileLocation = '';
                            console.log(s3result)
                            if (s3result != '') {
                                // logFileLocation = result['Location'];
                                logFileLocation = CampLogfilename;
                                // fs.unlinkSync(filepath)
                                debugLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                                crawlerLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                            } else {
                                debugLog.push(await dateTime() + ":" + "error in upload file to server")
                                crawlerLog.push(await dateTime() + ":" + "error in upload file to server")
                            }

                            debugLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            crawlerLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")

                            campgroundStatus = true;
                            addressDetail=campgroundDetails['contact']['address'];
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, logFileLocation.length,addressDetail);
                            successCamp++;


                        } catch (e) {
                            console.log(e);
                            failedCamp++;
                            debugLog.push(await dateTime() + ":" + "Error in Scrapping Individual Campground");
                            crawlerstatus = false;
                            campgroundStatus = false;
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '','');
                        }
                    }
                }
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);

            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + "Error in fetching All Campground Based on Datasource");
                crawlerstatus = false;
                console.log(err);
                //    return err;
            });
    } catch (e) {
        console.log(e);
        debugLog.push(await dateTime() + ":" + "Error in Scrap all Detail");
        crawlerstatus = false;
        // return e;
    }
    return result;
}

var campsiteArr = [];
async function fetchCampDetail(url, campId, campGroundID) {
    //Fetching Campground Detail Id Wise
    var campData;
    await axios.get(url)
        .then(async response => {
            
            var slug = response.data.campground.facility_name.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
            var address = '';
            var addressData = '';
            var addressCity = '';
            var addressCountry = '';
            var addressState = '';
            var addressZip = '';
            var addressLat = '';
            var addressLong = '';
            for (var a = 0; a < response.data.campground.addresses.length; a++) {
                // console.log(response.data.campground.addresses[a]);
                var address1 = await concateString(response.data.campground.addresses[a].address1);
                var address2 = await concateString(response.data.campground.addresses[a].address2);
                var address3 = await concateString(response.data.campground.addresses[a].address3);
                var city = await concateString(response.data.campground.addresses[a].city);
                var country_code = await concateString(response.data.campground.addresses[a].country_code, false);
                var state_code = await concateString(response.data.campground.addresses[a].state_code, false);
                var postal_code = await concateString(response.data.campground.addresses[a].postal_code, false);
                if (a == 0) {
                    address +=  address1 + '' + address2 + '' + address3 + '' + city + '' + state_code + ' ' + postal_code;
                    addressData = address1 + '' + address2 + '' + address3;
                    addressCity = response.data.campground.addresses[a].city;
                    addressCountry = response.data.campground.addresses[a].country_code;
                    addressState = response.data.campground.addresses[a].state_code;
                    addressZip = response.data.campground.addresses[a].postal_code;
                    addressLat = response.data.campground.facility_latitude;
                    addressLong = response.data.campground.facility_longitude;
                } else {
                    address +=  address1 + '' + address2 + '' + address3 + '' + city + '' + state_code + ' ' + postal_code;
                }

            }

            var addrData = {
                address: addressData,
                city: addressCity,
                state: addressState,
                country: addressCountry,
                zip: addressZip,
                latitude: addressLat,
                longitude: addressLong                          
            };

            var campGroundImage = [];
            //Fetching Campground Images 
            console.log('Image URL : https://www.recreation.gov/api/media/public/asset/' + campId)
            await axios.get('https://www.recreation.gov/api/media/public/asset/' + campId)
                .then(async response => {
                    //Fetching Campsite
                    var re = /(?:\.([^.]+))?$/;
                    for (var ci = 0; ci < response.data.result.length; ci++) {
                        var image_url = response.data.result[ci].url;
                        var album = response.data.result[ci].title;
                        album = album.replace(/ /g, "_");

                        var time = new Date().getTime();
                        var ext = await re.exec(image_url)[1];

                        var slug_image = album + "_" + time + "." + ext;
                        // console.log('slug is ');
                        // console.log(slug_image);
                        var campImage = {
                            title: response.data.result[ci].title,
                            // url:response.data.result[ci].url,
                            imagedata: [
                                {
                                    title: response.data.result[ci].description,
                                    album: response.data.result[ci].title,
                                    image: await s3.uploadImageLocal(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campGroundID + '/images/' + album + "/" + slug_image)
                                }]
                        }
                        campGroundImage[ci] = campImage;
                    }
                })
                .catch(async error => {
                    console.log(error);
                    console.log('Error in ' + campId);
                    debugLog.push(await dateTime() + ":" + "Error in Fetching CampGround Details By id:" + campId);
                    crawlerstatus = false;

                })


            var amenities = (response.data.campground.amenities != undefined) ? Object.values(response.data.campground.amenities) : '';

            var campsitedata = [{ type: 'RV Sites', data: [] },
            { type: 'Lodging', data: [] },
            { type: 'Tent Sites', data: [] }
            ];

            console.log("Fetching campsites for Tent Sites")
            campsiteArr = [];
            var campSiteUrl = '';
            // https://www.recreation.gov/api/search/campsites?start=0&size=1000&fq=asset_id%3A232555&fq=campsite_type%3ARV%20ELECTRIC&fq=campsite_type%3ARV%20NONELECTRIC&include_non_site_specific_campsites=true

            campSiteUrl = siteUrl + 'api/search/campsites?fq=asset_id%3A' + campId + '&fq=campsite_type%3ARV%20ELECTRIC&fq=campsite_type%3ARV%20NONELECTRIC'
            var rvArr = await fetchCampsite(campSiteUrl, 0);
            // console.log('Rv Site')
            // console.log(rvArr)
            campsitedata[0].data = rvArr;

            campSiteUrl = siteUrl + 'api/search/campsites?fq=asset_id%3A' + campId + '&fq=campsite_type%3ATENT%20ONLY%20ELECTRIC&fq=campsite_type%3ATENT%20ONLY%20NONELECTRIC'
            var tentArr = await fetchCampsite(campSiteUrl, 0);
            // console.log('tent Site')
            // console.log(tentArr)
            campsitedata[2].data = tentArr;

            //Prepare Json Structure of Campground Data
            campData = {
                id: campId,
                name: response.data.campground.facility_name,
                slug: slug,
                description: response.data.campground.facility_description_map.Overview,
                data: {
                    amenities: amenities,
                    recreation: { title: "", description: response.data.campground.facility_description_map.Facilities },
                    local_attractions: response.data.campground.facility_description_map['Nearby Attractions'],
                    rules_policies: response.data.campground.facility_rules,
                    deals: '',
                },
                contact: {
                    physical_address: address,
                    latitude: response.data.campground.facility_latitude,
                    longitude: response.data.campground.facility_longitude,
                    season_dates: "Open All Year",
                    telephone_no: response.data.campground.facility_phone,
                    reserver_no: response.data.campground.facility_phone,
                    email: response.data.campground.facility_email,
                    map_image: '',
                    map_link: '',
                    address:addrData
                },
                images: campGroundImage,
                accomodation_types: campsitedata,
                // campsites: campSiteArr,
            };
        })
        .catch(async error => {
            console.log(error);
            console.log('Error in ' + campId);
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + "Error in Fetching CampGround Details");
        })
    // console.log('campData')
    // console.log(campData)
    return campData;
}

var totalCamp = 0;

async function fetchCampsite(campSiteUrl, campstart = 0) {

    console.log(campSiteUrl + '&rcp=' + campstart);
    var re = /(?:\.([^.]+))?$/;
    await axios.get(campSiteUrl + '&start=' + campstart + '&size=' + size)
        .then(async response => {

            totalCamp = response.data.total;

            if (totalCamp > 0) {
                for (var i = 0; i < response.data.campsites.length; i++) {
                    // console.log('camp siteUrl: ' + siteUrl + 'api/camps/campsites/' + response.data.campsites[i].campsite_id);
                    await axios.get(siteUrl + 'api/camps/campsites/' + response.data.campsites[i].campsite_id)
                        .then(async responseCampsite => {
                            var features = '';
                            if (responseCampsite.data.campsite.attributes) {
                                var featuresArr = responseCampsite.data.campsite.attributes;

                                var comma = ', ';
                                for (var j = 0; j < featuresArr.length; j++) {

                                    if ((j + 1) == featuresArr.length) {
                                        comma = '.';
                                    }
                                    features += featuresArr[j].attribute_name + ": " + featuresArr[j].attribute_value + comma

                                }
                            }
                            var amenities = [];
                            if (responseCampsite.data.campsite.amenities) {
                                var amenitiesArr = responseCampsite.data.campsite.amenities;
                                for (var a = 0; a < amenitiesArr.length; a++) {
                                    amenities.push(amenitiesArr[a].attribute_name + ': ' + amenitiesArr[a].attribute_value)
                                }
                            }
                            var campimage = null;
                            if (response.data.campsites[i].preview_image_url) {
                                var image_url = response.data.campsites[i].preview_image_url;
                                //                                        console.log('image_url' + image_url)

                                var nameArr = image_url.split('/');
                                var ext = re.exec(image_url)[1];

                                var slug_image = (ext != '') ? nameArr[nameArr.length - 1] : nameArr[nameArr.length - 1] + '.png';
                                campimage = { image: await s3.uploadImageLocal(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campGroundID + '/images/campsites/' + slug_image) };
                            }
                            campsiteArr.push({
                                'campground': response.data.campsites[i].parent_asset_name,
                                'title': 'Site: ' + responseCampsite.data.campsite.campsite_name + ', LOOP: ' + responseCampsite.data.campsite.loop,
                                'summary': 'organization : ' + response.data.campsites[i].org_name + ' | Near ' + response.data.campsites[i].city + ', ' + response.data.campsites[i].state_code,
                                'description': 'Site: ' + response.data.campsites[i].name + ', LOOP: ' + response.data.campsites[i].loop + ' ,' + 'organization : ' + response.data.campsites[i].org_name + ' | Near ' + response.data.campsites[i].city + ', ' + response.data.campsites[i].state_code,
                                'features': features,
                                'amenities': amenities,
                                'address': 'Site: ' + response.data.campsites[i].name + ', LOOP: ' + response.data.campsites[i].loop + ' | Near ' + response.data.campsites[i].city + ', ' + response.data.campsites[i].state_code + ', ' + response.data.campsites[i].country_code,
                                'email': '',
                                'reservation_phone': '',
                                'information_phone': '',
                                'images': [campimage],
                            })

                        })
                        .catch(async error => {
                            console.log(error);
                            console.log('Error in ' + campId);
                            debugLog.push(await dateTime() + ":" + "Error in Fetching CampGround Site Details By id:" + campId);
                            crawlerstatus = false;
                        })

                }

            }

            campstart = campstart + size;

            if (campstart < totalCamp) {
                await fetchCampsite(campSiteUrl, campstart);
            }


        })
        .catch(async error => {
            console.log(error);
            console.log('Error in ' + campId);
            debugLog.push(await dateTime() + ":" + "Error in Fetching Camp Sites:" + campId);
            crawlerstatus = false;
            campsiteArr = [];
        })
    return campsiteArr;
}

async function storeCampGroundLogFile(crawlerLog, camp_id, campStatus, logFileLocation, address="") {
    try {
        const campUpdate = {
            data_file_path: logFileLocation,
            index_status: 'Completed',
            crawler_status: (campStatus == true) ? 'Success' : 'Failed',
            location:JSON.stringify(address),
        };
        await CampGround.update(campUpdate, {
            where: { id: camp_id }
        })

        debugLog.push(await dateTime() + ":" + "updating campground log status and set log file path")
        crawlerLog.push(await dateTime() + ":" + "updating campground log status and set log file path")

        fs.writeFileSync(filePathStore + "temp/camp_crawler" + camp_id + ".json", JSON.stringify(crawlerLog));
        const filepath = path.resolve(filePathStore + "temp/camp_crawler" + camp_id + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_campground_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/campgrounds/' + camp_id + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Successfully uploaded Campground Crawler log file on server")

            // fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Failed to upload Campground Crawler log file on server")
            crawlerstatus = false;
        }

        const campLogDataUpdate = {
            status: (campStatus == true) ? 'Success' : 'Failed',
            log_file_path: crawlerLogfilename,
        };
        await CampgroundLog.update(campLogDataUpdate, {
            where: { id: campLogId }
        })
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing CampGround Log/CrawlerLog File');
    }
}

