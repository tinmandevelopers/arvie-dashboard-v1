const axios = require('axios');
var fs = require('fs');

var total = 0;
var start = 4;
var size = 6;
var campGroundArr = [];
campGroundArr = [];
var lastTimeFetch = false;
(async () => {

    try {
        var datetime = new Date();
        console.log("Start Time: " + datetime);
        campGroundArr.push({ 'start_at': datetime });
        $result = await fetchCampground(start);
        console.log("total Campground is" + campGroundArr.length);
        fs.writeFileSync("scrapping/all_recreation_campgrounds_all.json", JSON.stringify(campGroundArr));
        var datetime = new Date();
        console.log("End Time: " + datetime);
        campGroundArr.push({ 'end_at': datetime });

    } catch (error) {
        console.log(error);
    }

})();

async function fetchCampground(start) {
    console.log("*********************");
    console.log("start from : " + start);
    //Fetching Campground List
    await axios.get('https://www.recreation.gov/api/search?fq=entity_type%3Acampground&start=' + start + '&size=' + size)
        .then(async response => {

            total = response.data.total;
            resultArr = response.data.results;

            for (var i = 0; i < response.data.results.length; i++) {
                console.log('fetching data for id : ' + response.data.results[i].entity_id);
                var accomodationTypes=response.data.results[i].campsite_equipment_name;
                var campId = response.data.results[i].entity_id;

                //Fetching Campground Detail Id Wise
                await axios.get('https://www.recreation.gov/api/camps/campgrounds/' + campId)
                    .then(async response => {
                        var campData;
                        var slug = response.data.campground.facility_name.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
                        var address = '';
                        for (var a = 0; a < response.data.campground.addresses.length; a++) {

                            var address1 = await concateString(response.data.campground.addresses[a].address1);
                            var address2 = await concateString(response.data.campground.addresses[a].address2);
                            var address3 = await concateString(response.data.campground.addresses[a].address3);
                            var city = await concateString(response.data.campground.addresses[a].city);
                            var country_code = await concateString(response.data.campground.addresses[a].country_code, false);
                            var postal_code = await concateString(response.data.campground.addresses[a].postal_code, false);
                            if (a == 0) {
                                address += 'Address: ' + address1 + '' + address2 + '' + address3 + '' + city + '' + country_code + '-' + postal_code;
                            } else {
                                address += 'Other Address: ' + address1 + '' + address2 + '' + address3 + '' + city + '' + country_code + '-' + postal_code;
                            }

                        }
                        var campSiteImage=[];
                        //Fetching Campground Images
                        await axios.get('https://www.recreation.gov/api/media/public/asset/' + campId)
                            .then(async response => {
                                //Fetching Campsite

                                for (var ci = 0; ci < response.data.result.length; ci++) {
                                    var campImage={
                                        title:response.data.result[ci].title,
                                        url:response.data.result[ci].url,
                                        imageData:[{title:response.data.result[ci].description,album:response.data.result[ci].title,image:response.data.result[ci].url}]
                                    }
                                    campSiteImage[ci]=campImage;
                                }
                            })
                            .catch(error => {
                                console.log(error);
                                console.log('Error in ' + campId);
                            })


                        var campsite = (response.data.campground.campsites != undefined) ? response.data.campground.campsites : [];
                        var amenities = (response.data.campground.amenities != undefined) ? Object.values(response.data.campground.amenities) : '';

                        var campSiteArr = {};
                        //Fetching Campsite
                        for (var c = 0; c < campsite.length; c++) {
                            await axios.get('https://www.recreation.gov/api/camps/campsites/' + campsite[c])
                                .then(async response => {
                                    campSiteArr[c] = response.data.campsite;

                                })
                                .catch(error => {
                                    console.log(error);
                                    console.log('Error in ' + campId);
                                })
                        }

                        //Prepare Json Structure of Campground Data
                        campData = {
                            id:campId,
                            name: response.data.campground.facility_name,
                            slug: slug,
                            description: response.data.campground.facility_description_map.Overview,
                            data: {
                                amenities: amenities,
                                recreation: { title:"",description:response.data.campground.facility_description_map.Facilities},
                                local_attractions: response.data.campground.facility_description_map['Nearby Attractions'],
                                rules_policies: response.data.campground.facility_rules,
                                deals: '',
                            },
                            contact: {
                                physical_address: address,
                                season_dates: "Open All Year",
                                telephone_no: response.data.campground.facility_phone,
                                reserver_no: response.data.campground.facility_phone,
                                email: response.data.campground.facility_email,
                                map_image: '',
                                map_link: '',
                            },
                            images: campSiteImage,
                            accomodation_types: accomodationTypes,
                            campsites: campSiteArr,
                        };

                        campGroundArr.push(campData);
                        console.log(response.data.campground.facility_name);
                    })
                    .catch(error => {
                        console.log(error);
                        console.log('Error in ' + campId);
                    })

            }

            // start = start + size;

            // if (start < total) {
            //     fetchCampground(start)
            // } else {
            //     if (lastTimeFetch == false) {
            //         lastTimeFetch = true;
            //         fetchCampground(start)
            //     } else {
            //         return campGroundArr;
            //     }
            // }
        })
        .catch(error => {
            console.log(error);
        })

}

async function concateString(value, comma = true) {
    var comaVal = (comma == true) ? ', ' : '';
    var text = '';
    text = (value != '' && value != null) ? value + comaVal : '';

    return text;
}


