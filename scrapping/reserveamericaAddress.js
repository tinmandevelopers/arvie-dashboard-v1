const axios = require('axios');
var fs = require('fs');
var express = require('express');
const puppeteer = require("puppeteer");
//Prod
// var configPath='../../../../../scrapping/';
// var filePathStore='../scrapping/';
//Local
var configPath = '../scrapping/';
var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
// var s3 = require("../scrapping/helper/s3imageUpload");
const chalk = require("chalk");
const db = require(configPath + "models");
// const db = require("../scrapping/models/index.js");
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const CampData = db.CampData;
const DataSource = db.DataSource;
const State = db.State;

const path = require('path')
const args = require('yargs').argv;

var debugLog = [];
var crawlerstatus = true;
var datasourceId = args.datasource_id;


(async () => {
  
    var condition = { data_source_id: datasourceId ,status: "Active" , web_status: "Listed"};
    var log=[];
    await CampGround.findAll({ where: condition })
    .then(async data => {
  
        console.log('total length is')
        console.log(data.length)
        console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));
        var browser = await puppeteer.launch({
            headless: true,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });
        console.log('*************************');
        console.log(await browser.version());
       
        var page = await browser.newPage();
        await page.goto("https://www.reserveamerica.com/", { waitUntil: 'networkidle2' });
        var count=0;
        for (var p = 0; p < data.length; p++) {
            // console.log(data[p]['id'])
            if (data[p]['id'] > 17545) {
                if(count > 10){
                    count=0;
                    await page.goto("https://www.reserveamerica.com/", { waitUntil: 'networkidle2' });
                }
                try {
                    var campdata = data[p];
                    
                    console.log(campdata['id']);
                    var url=campdata['url'];
                    await page.goto(url, { waitUntil: 'networkidle2' });

                    let element = await page.$('body > pre');
                    let campContent = await page.evaluate(el => el.textContent, element)
            
                    var campDetail = JSON.parse(campContent)
                    console.log(campDetail);

                    var addresss = '';
            
                        let rowList = [];
                        var address1 = campDetail.contactAddress.address.addrLine1;
                        var address2 = campDetail.contactAddress.address.addrLine2;
                        var address3 = "";
                        var city = campDetail.contactAddress.address.city;
                        var country_code = campDetail.contactAddress.address.countryCode;
                        var state_code = campDetail.contactAddress.address.stateProvinceCode;
                        var postal_code = campDetail.contactAddress.address.cityStateZip;
                        var address = address1 + ', ' + address2 + ', ' + address3;
                        var facility_latitude = campDetail.coordinates.latitude;
                        var facility_longitude = campDetail.coordinates.longitude;
                        
                        var rowData = {
                            address: address,
                            city: city,
                            state: state_code,
                            country: country_code,
                            zip: postal_code,
                            latitude: facility_latitude,
                            longitude: facility_longitude                          
                        };
                        console.log(rowData);

                        const campDataStore = {
                            campground_id: data[p]['id'],
                            data_source_id: campdata['data_source_id'],
                            camp_data:JSON.stringify(rowData),
                        };

                        await CampData.create(campDataStore)
                        .then(async data => {
                            log.push('Success :'+data[p]['id']);
                        })
                        .catch(async err => {
                            log.push('Error:'+data[p]['id']);
                            console.log('Error in New Creating CampGround data:'+data[p]['id']);
                            console.log(err.message)
                        });

                    // });
                    count++;

                } catch (e) {
                    console.log(e);
                }
            }
            }

            fs.writeFileSync(filePathStore + "temp/recreationAddress.txt", JSON.stringify(log));
    })
    .catch(async err => {
        console.log("Error in fetching All Campground Based on Datasource");
        console.log(err);

    });


    })();