const puppeteer = require("puppeteer");
const express = require('express');
const args = require('yargs').argv;
const path = require('path')

var bookignStatus = true;
var comment = '';
var debugLog = [];
var browseClose = true;

let siteUrl = 'https://www.reserveamerica.com/';
let searchData = 'Alaska';
let arrivalDate = '2021-08-01';
let departDate = '2021-08-08';
let userEmail = 'dummayaccount@mailinator.com';
let userPassword = 'Letsdoit@123';

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}


(async () => {
    try {
        var browser = await puppeteer.launch({
            headless: false,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
        });
        console.log('*************************');
        console.log(await browser.version());
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        try {
            console.log('Nevigating : ' + siteUrl)
            debugLog.push(await dateTime() + ":" + 'Nevigating : ' + siteUrl);
            await page.setViewport({ width: 1366, height: 768 });

            await page.setDefaultNavigationTimeout(0);

            let browseUrl = siteUrl;
            debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);
            await page.goto(browseUrl, { waitUntil: 'networkidle2' });

            if (await page.$('#home_hero') !== null) {

                debugLog.push(await dateTime() + ":" + "Scrolling page");
                await page.click(".searchForm > div> div > div > .form-control");
                await page.type('.searchForm > div> div > div > .form-control', searchData)
                await page.click('.searchForm > div> .searchbar__button > button');
                await sleep(3000);
                if (await page.$(".search-help-error") !== null) {
                    debugLog.push(await dateTime() + ":" + "Fetching validation errors ");

                    let element = await page.$('.search-help-error');
                    let errorMsg = await page.evaluate(el => el.textContent, element)
                    comment = errorMsg;
                    bookignStatus = false;
                    console.log(errorMsg);
                } else {
                    debugLog.push(await dateTime() + ":" + "Waiting for next page ");
                    await sleep(1000);
                    if (await page.$('#arrivaldatelg-md') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing arrival date");
                        await page.click("#arrivaldatelg-md");
                        await page.type("#arrivaldatelg-md", arrivalDate);
                        await sleep(1000)
                    }

                    if (await page.$('#departuredatelg-md') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing depart date");
                        await page.click("#departuredatelg-md");
                        await page.type("#departuredatelg-md", departDate);
                        await sleep(1000);
                    }
                    await sleep(1000);
                    if (await page.$(".facility-summary-card:first-child .facility-summary-card__detail .facility-summary-card__title") !== null) {
                        debugLog.push(await dateTime() + ":" + "Clicking on link ");
                        await page.evaluate(() => {
                            document.querySelector('.facility-summary-card:first-child .facility-summary-card__detail a').click();
                        });
                        await sleep(4000);
                        debugLog.push(await dateTime() + ":" + "Waiting for detail page ");
                        if (await page.$(".facility-content-container") !== null) {
                            await page.evaluate(() => {
                                document.querySelector('.facility-content-container').scrollIntoView();
                            });
                            debugLog.push(await dateTime() + ":" + "Scrolling detail page");
                            await sleep(750);
                            await page.click('.product-search-form__contents .actionButton button');
                            await sleep(1000);
                            await page.evaluate(() => {
                                document.querySelector('.section-height').scrollIntoView();
                            });
                            debugLog.push(await dateTime() + ":" + "Scrolling to availability slot section");

                            await page.click('.table-responsive table tbody td:nth-child(7) button')
                            await sleep(3000);
                            /*
                            debugLog.push(await dateTime() + ":" + "Waiting for book page");
                            await page.evaluate(() => {
                                document.querySelector('.date-pager').scrollIntoView();
                            });
                            await sleep(500);
                            await page.click('.quickbook button');
                            await sleep(2000);
                            debugLog.push(await dateTime() + ":" + "Waiting for login page ");
                            if (await page.$("#existing_cust") !== null) {
                                debugLog.push(await dateTime() + ":" + "Storing email")
                                const email = await page.waitForSelector("#emailGroup input.TextBoxRenderer");
                                await page.click("#emailGroup input.TextBoxRenderer");
                                await email.type(userEmail);

                                debugLog.push(await dateTime() + ":" + "Storing Password")
                                const password = await page.waitForSelector("#emailGroup input.PasswordBoxRenderer");
                                await page.click("#emailGroup input.PasswordBoxRenderer");
                                await password.type(userPassword);

                                await page.click('#signinbutton button')
                                await sleep(3000)
                            }
                            */
                        }
                    }
                }
                return false;
            } else {
                debugLog.push(await dateTime() + ":" + "Fetching Error Message")
                let errorMsgCamp = 'Site is Under Maintenance'
                console.log(errorMsgCamp);
                bookignStatus = false;
                comment = errorMsgCamp;
            }
        } catch (err) {
            debugLog.push(await dateTime() + ":" + "Sorry, an error has ocurred ")
            comment = 'Sorry, an error has ocurred';
            bookignStatus = false;
            console.log(err)
        }
        await browser.close();
    } catch (err) {
        console.log(err);
        bookignStatus = true;
        debugLog.push(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = 'Sometghing Went Wrong';
        // await browser.close();
        console.log("Browser Closed");
    }
})();


async function sleep(ms) {
    console.log("debugLog", debugLog);
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}