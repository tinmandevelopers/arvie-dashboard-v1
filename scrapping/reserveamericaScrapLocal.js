const axios = require('axios');
var fs = require('fs');
var express = require('express');
const puppeteer = require("puppeteer");
//Prod
// var configPath = '../../../../../scrapping/';
// var filePathStore = '../scrapping/';
//Local
var configPath = '../scrapping/';
var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
// var s3 = require("../scrapping/helper/s3imageUpload");
const chalk = require("chalk");
const db = require(configPath + "models");
// const db = require("../scrapping/models/index.js");
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const DataSource = db.DataSource;
const State = db.State;

const path = require('path')
const args = require('yargs').argv;

var debugLog = [];
var crawlerstatus = true;
var datasourceId = args.datasource_id;
var statusVal = '';
statusVal = args.status;
if (statusVal != undefined) {
    console.log('Status Argument Value : ' + statusVal)
    if (statusVal != 'pending') {
        statusVal = '';
    }
} else {
    statusVal = '';
}
console.log('Status Value Is :' + statusVal)

var size = 20;
var campGroundArr = [];
var lastTimeFetch = false;
var crawlerLogId = 0;
var siteUrl = '';
(async () => {

    try {

        var startdatetime = new Date();
        console.log("Start Time: " + startdatetime);

        const dataSourceData = await getDatasourceByID();

        const storeLog = await storeCrawlerLog();

        var browser = await puppeteer.launch({
            headless: true,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });
        console.log('*************************');
        console.log(await browser.version());
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        await page.setViewport({ width: 1366, height: 768 });

        await page.setDefaultNavigationTimeout(0);

        var browseUrl = siteUrl;

        await page.goto(browseUrl, { waitUntil: 'networkidle2' });

        if (statusVal == '') {

            // const stateNames = await fetchAllState(page);

            // await storeCampGrounds();
        }
        var result = await scrapeAll(page);
        browser.close();
        await storeCrawlerLogFile();
        // campGroundArr.push({ 'start_at': startdatetime });
        // var datetime = new Date();
        // console.log("End Time: " + datetime);
        // campGroundArr.push({ 'end_at': datetime });

        // console.log("total Campground is" + campGroundArr.length);
        // fs.writeFileSync(filePathStore + "/temp/all_reserveamerica_campgrounds_all.json", JSON.stringify(campGroundArr));

    } catch (error) {
        console.log(error);
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + error);
        await storeCrawlerLogFile()
    }

})();

async function storeCrawlerLogFile() {
    try {

        fs.writeFileSync(filePathStore + "temp/crawler" + datasourceId + ".json", JSON.stringify(debugLog));
        const filepath = path.resolve(filePathStore + "temp/crawler" + datasourceId + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Crawler log file to server");
            // fs.unlinkSync(filepath)
        } else {

            crawlerLogfilename = '';
            console.log("Failed to upload Crawler log file to server");
        }

        const crawlerUpdate = {
            log_file_path: 'Log:' + crawlerLogfilename,
            status: (crawlerstatus == true) ? 'Success' : 'Failed',
            updated_at: Date.now(),
        };
        await DataSourceCrawlerLog.update(crawlerUpdate, {
            where: { id: crawlerLogId }
        })
        var statusVal = (crawlerstatus == true) ? 'Success' : 'Failed';
        const dataSourceUpdate = {
            crawler_log: statusVal + ':' + crawlerLogfilename,
        };

        await DataSource.update(dataSourceUpdate, {
            where: { id: datasourceId }
        });

    } catch (e) {
        console.log('Error in Updating DataSource and Storing Crawler File')
        console.log(e)
    }
}

async function getDatasourceByID() {
    await DataSource.findByPk(datasourceId)
        .then(async data => {
            siteUrl = data['url']
            var lastchar = siteUrl.charAt(siteUrl.length - 1);
            var slace = (lastchar == '/') ? "" : "/";
            siteUrl = siteUrl + slace;
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching DataSource By Id');
        });
}

async function storeCrawlerLog() {
    let response = [];
    const logData = {
        data_source_id: datasourceId,
        status: 'InProgress',
    };
    debugLog.push(await dateTime() + ":" + 'Storing Crawler Log');
    await DataSourceCrawlerLog.create(logData)
        .then(async data => {

            response['status'] = 'success';
            response['content'] = data;
            // return data;
            crawlerLogId = data['id'];
            debugLog.push(await dateTime() + ":" + 'Crawler Log Stored Successfully');
        })
        .catch(async err => {
            crawlerstatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in crawler log storing');
            debugLog.push(await dateTime() + ":" + err.message);

            response['status'] = 'error';
            response['content'] = err;
            // return err;
            // console.log(err.message)
        });
    return response;
}

var campArr = [];
var campgroundDataArr = [];
async function fetchAllState(page) {
    try {

        await State.findAll()
            .then(async state => {
                // console.log(JSON.stringify(state));

                for (var l = 0; l < state.length; l++) {
                    if (state[l]['id'] == 2) {
                        var res = state[l]['state_name'].trim();
                        var stateCode = state[l]['state_code'].trim();
                        var stateStr = res.replace(" ", "%20");
                        console.log('Fetching Campground For ' + res + ' Campgroup.')
                        var campGroupUrl = siteUrl + 'explore/search-results?searchTerm=' + stateStr + '&type=state&stateCode=' + stateCode;

                        var campGroupFetchUrl = siteUrl + 'jaxrs-json/search?stype=state&tstc=' + stateCode;
                        campgroundDataArr = [];

                        await fetchCampgroundList(campGroupFetchUrl, page, 0);

                        campGroundArr[l] = {
                            'country_name': 'United States',
                            'camp_group': res,
                            'camp_group_url': campGroupUrl,
                            'campground': campgroundDataArr,
                        }

                    }
                }
            })
            .catch(err => {
                console.log(err);
            });

        return campGroundArr;

    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in FetchGround States');
        console.log(err);
        // await browser.close();
        // console.log("Browser Closed");
        return [];
    }

}
var totalCamp = 0;


async function fetchCampgroundList(campGroupUrl, page, start = 0) {
    try {

        console.log(campGroupUrl + '&rcp=' + start);

        await page.goto(campGroupUrl + '&rcp=' + start, { waitUntil: 'networkidle2' });

        let element = await page.$('body > pre');
        let campData = await page.evaluate(el => el.textContent, element)

        var dataArr = JSON.parse(campData)

        totalCamp = dataArr.totalPages;

        for (var i = 0; i < dataArr.records.length; i++) {

            var campId = dataArr.records[i].details.id;

            countryId = dataArr.records[i].details.contrCode;

            var campUrl = siteUrl + 'jaxrs-json/facilities/' + countryId + '/' + campId + '?temp_facility_details_only=true';

            campgroundDataArr.push({
                'title': dataArr.records[i].name,
                'url': campUrl
            })

        }
        start = start + 1;

        if (start < totalCamp) {
            await fetchCampgroundList(campGroupUrl, page, start)
        } else {
            return campgroundDataArr;
        }

    } catch (err) {
        // Catch and display errors
        // crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground List');

        console.log("Error in FetchGround List");
        console.log(err);
        //  browser.close();
        return null;
    }

}

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
    }
}

async function storeCampGrounds() {
    try {

        // console.log(campdata[0].camp_group)
        debugLog.push(await dateTime() + ":" + 'In store campground function');
        for (var key in campGroundArr) {
            // console.log('in key:' + key)
            // console.log(campGroundArr[key]['camp_group'])
            var gname = campGroundArr[key]['camp_group'];
            var cntname = campGroundArr[key]['country_name'];
            var groupurl = campGroundArr[key]['camp_group_url'];

            const campGroup = {
                name: gname,
                data_source_id: datasourceId,
                country: cntname,
                url: groupurl
            };

            const camp = campGroundArr[key]['campground'];

            // console.log("camp is ")
            // console.log(campGroundArr[key])
            // console.log(campGroundArr[key]['campground'])
            // debugLog.push(dateTime() + ":" + 'checking campgroup is added or not');
            await CampGroundGroup
                .findOne({ where: campGroup })
                .then(function (obj) {
                    // update
                    if (obj) {
                        return obj.update(campGroup)
                            .then(async data => {
                                for (const campkey in camp) {
                                    // console.log(camp[campkey]['url'])
                                    const cont = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url'],
                                        // status: 'Active',
                                        // index_status: 'Pending',
                                        // last_indexed_time:Date.now(),

                                    };
                                    // console.log(cont)
                                    // debugLog.push(dateTime() + ":" + 'checking campground is added or not');
                                    await CampGround.findOne({ where: cont })
                                        .then(async function (obj) {
                                            // debugLog.push(dateTime() + ":" + 'updating campground ');

                                            const campdata = {
                                                data_source_id: datasourceId,
                                                campground_group_id: data['id'],
                                                title: camp[campkey]['title'],
                                                url: camp[campkey]['url'],
                                                status: 'Pending',
                                                index_status: 'Pending',
                                                last_indexed_time: Date.now(),
                                            };
                                            if (obj) {
                                                // console.log('updated data')
                                                // console.log(obj)

                                                obj.update(campdata)
                                                    .then(async data => { })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Updating Already Stored Campground');
                                                    });
                                            } else {
                                                await CampGround.create(campdata)
                                                    .then(async data => { })
                                                    .catch(async err => {
                                                        console.log(err)
                                                        crawlerstatus = false;
                                                        debugLog.push(await dateTime() + ":" + 'Error in Creating Already Stored Campground');
                                                    });
                                            }
                                        })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            debugLog.push(await dateTime() + ":" + 'Error in Checking if Capground Already Stored or Not');
                                        });
                                }
                            })
                            .catch(async err => {
                                console.log(err)
                                crawlerstatus = false;
                                debugLog.push(await dateTime() + ":" + 'Error in Updating Campground');
                            });

                    } else {
                        // insert
                        // debugLog.push(dateTime() + ":" + ' campgroup creating');
                        return CampGroundGroup.create(campGroup, camp)
                            .then(async data => {
                                // debugLog.push(dateTime() + ":" + ' campgroup created');
                                for (var campkey in camp) {
                                    // console.log('in campground')
                                    const campground = {
                                        data_source_id: datasourceId,
                                        campground_group_id: data['id'],
                                        title: camp[campkey]['title'],
                                        url: camp[campkey]['url'],
                                        status: 'Pending',
                                        index_status: 'Pending',
                                        last_indexed_time: Date.now(),

                                    };
                                    // debugLog.push(dateTime() + ":" + ' campground creating');
                                    await CampGround.create(campground).then(async data => { })
                                        .catch(async err => {
                                            console.log(err)
                                            crawlerstatus = false;
                                            console.log('Error in Creating New Campground');
                                        });
                                }
                            })
                            .catch(async err => {
                                crawlerstatus = false;
                                console.log('Error in Storing New Campground');
                                console.log(err)
                                debugLog.push(await dateTime() + ":" + 'Error in Storing New Campground');
                            });
                    }

                })
                .catch(async err => {
                    crawlerstatus = false;
                    console.log(await dateTime() + ":" + 'Error in Storing Campground')
                    console.log(err)
                    debugLog.push(await dateTime() + ":" + 'Error in Storing Campground');
                });
        }


    } catch (e) {
        console.log(e)
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing All Campgrounds and Campground Groups');
    }

}

async function concateString(value, comma = true) {
    var comaVal = (comma == true) ? ', ' : '';
    var text = '';
    text = (value != '' && value != null) ? value + comaVal : '';

    return text;
}

var campGroundID = 0;
async function scrapeAll(page) {
    var result = [];
    try {

        var campgroundStatus = true;
        debugLog.push(await dateTime() + ":" + 'In Fetching campground detail.');

        var successCamp = 0;
        var failedCamp = 0;
        var condition;
        console.log(statusVal)
        if (statusVal != '') {
            condition = { data_source_id: datasourceId, index_status: statusVal };
        } else {
            condition = { data_source_id: datasourceId };
        }

        // await page.exposeFunction('uploadFileTos3', s3.uploadImageLocal);
        await CampGround.findAll({ where: condition })
            .then(async data => {
                console.log('total length is')
                console.log(data.length)
                console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));
                var cnt = 0;
                for (var p = 0; p < data.length; p++) {
                    if (cnt == 48) {
                        await page.goto(siteUrl, { waitUntil: 'networkidle2' });
                        cnt = 0;
                    }
                    cnt++;
                    // if (data[p].id == 9702) {
                    // console.log("title:" + data[p].title);
                    var crawlerLog = [];
                    try {
                        // console.log("title:"+data[p].title);
                        var campdata = data[p];
                        // console.log('title is '+ campdata['title'])
                        let campgrounds = [];
                        debugLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                        crawlerLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                        console.log(chalk.yellow("We got " + campdata['title'] + " campground for scrape"));

                        console.log("Nevigating URL:" + campdata['url'])
                        var link = '';
                        var title = '';
                        link = campdata['url'];
                        title = campdata['title'];

                        var campId = campdata['id'];
                        campGroundID = campdata['id'];
                        var addressDetail="";
                        debugLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                        crawlerLog.push(await dateTime() + ":" + "updating campground status to InProgress");

                        const campUpdate = {
                            index_status: "InProgress",
                        };
                        await CampGround.update(campUpdate, {
                            where: { id: campdata['id'] }
                        }).then(async data => { })
                            .catch(async err => {
                                crawlerstatus = false;
                                console.log(err.message)
                                debugLog.push(await dateTime() + ":" + 'Error in Updating CampGround Status To In Progress');
                            });


                        debugLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                        crawlerLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                        const campLogData = {
                            crawler_log_id: crawlerLogId,
                            campground_id: campdata['id'],
                            status: 'Pending',
                        };

                        await CampgroundLog.create(campLogData)
                            .then(async data => {
                                campLogId = data['id'];
                            })
                            .catch(async err => {
                                debugLog.push(await dateTime() + ":" + 'Error in New Creating CampGround Log');
                                console.log(err.message)
                            });
                        // page.on('console', msg => {
                        //     for (let i = 0; i < msg._args.length; ++i)
                        //         console.log(`${msg._args[i]}`)
                        // });


                        let campgroundDetails = await fetchCampDetail(link, campId, datasourceId, siteUrl, page);
                        // console.log('campgroundDetails')
                        // console.log(campgroundDetails)
                        if (campgroundDetails == undefined) {
                            failedCamp++;
                            debugLog.push(await dateTime() + ":" + "Error in Scrapping Campground :" + link);
                            crawlerstatus = false;
                            campgroundStatus = false;
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '');
                        } else {
                            result.push(campgroundDetails);

                            fs.writeFileSync(filePathStore + "temp/output" + campdata['id'] + ".json", JSON.stringify(campgroundDetails));
                            const filepath = path.resolve(filePathStore + "temp/output" + campdata['id'] + ".json");
                            var s3result;
                            // var CampLogfilename=new Date().getTime()+'_campground_log.json';
                            var CampLogfilename = campdata['id'] + '_campground_log.json';
                            debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            crawlerLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            s3result = await s3.uploadFileLocal(filepath, 'data_source/' + campdata['data_source_id'] + '/campgrounds/' + campdata['id'] + '/' + CampLogfilename);
                            // if(result.)
                            var logFileLocation = '';
                            console.log(s3result)
                            if (s3result != '') {
                                // logFileLocation = result['Location'];
                                logFileLocation = CampLogfilename;
                                // fs.unlinkSync(filepath)
                                debugLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                                crawlerLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                            } else {
                                debugLog.push(await dateTime() + ":" + "error in upload file to server")
                                crawlerLog.push(await dateTime() + ":" + "error in upload file to server")
                            }

                            debugLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            crawlerLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")

                            campgroundStatus = true;
                            addressDetail=campgroundDetails['contact']['address'];
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, logFileLocation,addressDetail);
                            successCamp++;

                        }
                    } catch (e) {
                        console.log(e);
                        failedCamp++;
                        debugLog.push(await dateTime() + ":" + "Error in Scrapping Individual Campground");
                        crawlerstatus = false;
                        campgroundStatus = false;
                        await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '','');
                    }
                    // }
                }
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);

            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + "Error in fetching All Campground Based on Datasource");
                crawlerstatus = false;
                console.log(err);
                //    return err;
            });
    } catch (e) {
        console.log(e);
        debugLog.push(await dateTime() + ":" + "Error in Scrap all Detail");
        crawlerstatus = false;
        // return e;
    }
    return result;
}

let rvsiteFetch = false;
var campsiteArr = [];
var rvKey = 0;
var tentKey = 0;
var lodgingKey = 0;
var campsitedata = [{ type: 'RV Sites', data: [] },
{ type: 'Lodging', data: [] },
{ type: 'Tent Sites', data: [] }
];
async function fetchCampDetail(url, campId, datasourceId, siteUrl, page) {
    //Fetching Campground Detail Id Wise
    var campData;
    try {
        console.log('campground: ' + url)
        await page.goto(url, { waitUntil: 'networkidle2' });

        let element = await page.$('body > pre');
        let campContent = await page.evaluate(el => el.textContent, element)

        var campDetail = JSON.parse(campContent)

        var imageList = [];

        var re = /(?:\.([^.]+))?$/;
        if (campDetail.id != undefined) {
            // await page.addScriptTag({ content: `${s3}` });
            //Fetching Campground Images
            if (campDetail.originalPhotos) {
                for (var i = 0; i < campDetail.originalPhotos.length; i++) {
                    var image_url = siteUrl + campDetail.originalPhotos[i].photoURL;
                    var nameArr = await image_url.split('/');
                    var ext = await re.exec(image_url)[1];

                    var slug_image = (ext != '') ? (i + 1) + '.' + ext : (i + 1) + '.png';

                    var lastURLSegment = image_url.substr(image_url.lastIndexOf('/') + 1);

                    var lasturl = encodeURIComponent(lastURLSegment);

                    var newurl = image_url.split('/').slice(0, -1).join('/') + '/' + lasturl;
                    slug_image = slug_image.split(" ").join("_")
                    var obj = {
                        title: campDetail.originalPhotos[i].description,
                        // url: image_url,
                        // slug_image:slug_image,
                        // ext:ext,
                        image: await s3.uploadImageLocal(newurl, 'data_source/' + datasourceId + '/campgrounds/' + campId + '/images/' + slug_image),

                    }
                    imageList.push(obj);
                }
            }

            //Fetching Campground Wise Campsites
            if (rvsiteFetch == false) {
                // console.log('in function rv fetch coming');

                var resTypeUrl = siteUrl + 'jaxrs-json/config/facility?contractCode=' + campDetail.contrCode + '&facilityID=' + campDetail.id;
                await page.goto(resTypeUrl, { waitUntil: 'networkidle2' });
                let element = await page.$('body > pre');
                let resSitetypeContent = await page.evaluate(el => el.textContent, element)

                var resSitetype = JSON.parse(resSitetypeContent);
                // console.log('resSitetype');
                if (resSitetype.campingCfg != undefined) {
                    if (resSitetype.campingCfg.campsitesSearch.applicableLookingFor) {
                        rvsiteFetch = true;

                        var resTypeArr = resSitetype.campingCfg.campsitesSearch.applicableLookingFor;
                        var rvNameArr = ["RV", "RV sites", "rv", "rv sites", "rv park", "rv park", "RV Park", "RV Resort", "rv resort"];
                        var lodgingNameArr = ["Cabins or lodgings", "cabins or lodgings", "Lodging", "Lodging Sites", "lodging", "lodging sites"];
                        var tentNameArr = ["Tent", "Tent Sites", "tent", "tent sites", "Tenting", "tenting", 'Tents', "tents"];

                        for (var r = 0; r < resTypeArr.length; r++) {
                            // console.log("Title:" + resTypeArr[r]['label'])
                            if (rvNameArr.includes(resTypeArr[r]['label'])) {
                                rvKey = resTypeArr[r]['key'];
                            } else if (tentNameArr.includes(resTypeArr[r]['label'])) {
                                tentKey = resTypeArr[r]['key'];
                            } else if (lodgingNameArr.includes(resTypeArr[r]['label'])) {
                                lodgingKey = resTypeArr[r]['key'];
                            }
                        }
                    }
                }
            }


            if (rvKey > 0) {
                campsiteArr = [];
                totalCampPage = 0;
                var rvArr;
                var campSiteUrl = siteUrl + 'jaxrs-json/products/' + campDetail.contrCode + '/' + campDetail.id + '?pa99999=' + rvKey;

                rvArr = await fetchCampsite(campSiteUrl, page, 0);
                campsitedata[0].data = rvArr;
                // console.log('RV Sites');
                // console.log(rvArr)
            }
            if (lodgingKey > 0) {
                campsiteArr = [];
                totalCampPage = 0;
                var campSiteUrl = siteUrl + 'jaxrs-json/products/' + campDetail.contrCode + '/' + campDetail.id + '?pa99999=' + lodgingKey;
                var lodgingArr = await fetchCampsite(campSiteUrl, page, 0);
                campsitedata[1].data = lodgingArr;
                // console.log('Lodging');
                // console.log(lodgingArr)
            }
            if (tentKey > 0) {
                campsiteArr = [];
                totalCampPage = 0;
                var campSiteUrl = siteUrl + 'jaxrs-json/products/' + campDetail.contrCode + '/' + campDetail.id + '?pa99999=' + tentKey;
                var tentArr = await fetchCampsite(campSiteUrl, page, 0);
                campsitedata[2].data = tentArr;
                // console.log('Tent');
                // console.log(tentArr)
            }

            var slug = campDetail.name
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-');

            var description = '';
            if (campDetail.description) {
                var descArr = campDetail.description.notes;
                descArr.forEach(function (entry) {
                    description += entry.content + "<br>";
                });
            }
            var amenitiesVal = [];
            if (campDetail.amenities) {
                var amenityArr = campDetail.amenities;
                amenityArr.forEach(function (entry) {
                    amenitiesVal.push(entry.name);
                });
            }
            var addresss = '';
            var addressData;
            if (campDetail.contactAddress.address) {
                addresss = campDetail.contactAddress.address.addrLine1 + ', ' + campDetail.contactAddress.address.addrLine2 + ', ' + campDetail.contactAddress.address.city + ', ' + campDetail.contactAddress.address.stateProvince + ' ' + campDetail.contactAddress.address.country + '-' + campDetail.contactAddress.address.cityStateZip;
                var address1 = campDetail.contactAddress.address.addrLine1;
                var address2 = campDetail.contactAddress.address.addrLine2;
                var address3 = "";
                var city = campDetail.contactAddress.address.city;
                var country_code = campDetail.contactAddress.address.countryCode;
                var state_code = campDetail.contactAddress.address.stateProvinceCode;
                var postal_code = campDetail.contactAddress.address.cityStateZip;
                var address = address1 + ', ' + address2 + ', ' + address3;
                var facility_latitude = campDetail.coordinates.latitude;
                var facility_longitude = campDetail.coordinates.longitude;
                
                addressData = {
                    address: address,
                    city: city,
                    state: state_code,
                    country: country_code,
                    zip: postal_code,
                    latitude: facility_latitude,
                    longitude: facility_longitude                          
                };
            }
            
            var reservationPhone = '';

            if (campDetail.contactPhones != null) {
                reservationPhone = campDetail.contactPhones[0].phone.number;
            }

            var nearByfacility = [];
            if (campDetail.nearbyFacilities) {
                var nearByfacilityArr = campDetail.nearbyFacilities;
                nearByfacilityArr.forEach(function (entry) {
                    nearByfacility.push(entry.name);
                });
            }

            //Prepare Json Structure of Campground Data
            campData = {
                id: campDetail.id,
                name: campDetail.name,
                slug: slug,
                description: description,
                data: {
                    amenities: { amenities: amenitiesVal },
                    recreation: {},
                    local_attractions: nearByfacility,
                    rules_policies: [],
                    deals: [],
                },
                contact: {
                    physical_address: addresss,
                    season_dates: campDetail.seasons,
                    telephone_no: campDetail.contactAddress.phoneNumber,
                    reserver_no: reservationPhone,
                    email: campDetail.contactAddress.emailAddress,
                    map_image: campDetail.staticMapURL,
                    map_link: '',
                    coordinates: campDetail.coordinates,
                    address:addressData
                },
                images: imageList,
                accomodation_types: campsitedata,
            };
        }
        return campData;
    } catch (e) {
        debugLog.push(await dateTime() + ":" + 'Error in Scrap Campground  Detail');
        crawlerstatus = false;
        console.log(e);
        console.log(e.message)
        return campData;

    }
}

var totalCampPage = 0;

async function fetchCampsite(campSiteUrl, page, campstart = 0) {

    try {

        console.log(campSiteUrl + '&rcp=' + campstart);

        await page.goto(campSiteUrl + '&rcp=' + campstart, { waitUntil: 'networkidle2' });

        var re = /(?:\.([^.]+))?$/;
        let element = await page.$('body > pre');
        let resSitetypeContent = await page.evaluate(el => el.textContent, element)

        var dataArr = JSON.parse(resSitetypeContent)

        if (dataArr.records != null) {
            totalCampPage = dataArr.totalPages;
            for (var i = 0; i < dataArr.records.length; i++) {
                var image_url = dataArr.records[i].details.imageURL;

                var imageData;

                if (image_url != null) {
                    image_url = siteUrl + image_url;
                    var nameArr = image_url.split('/');
                    var ext = re.exec(image_url)[1];

                    var slug_image = (ext != '') ? dataArr.records[i].id + '.' + ext : dataArr.records[i].id + '.png';
                    imageData = await s3.uploadImageLocal(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campGroundID + '/images/campsites/' + slug_image);
                    // await s3.uploadImageLocal(image_url, 'data_source/' + datasourceId + '/campgrounds/' + campGroundID + '/images/campsites/' + slug_image);
                } else {
                    imageData = image_url;
                }
                // console.log(imageData);
                campsiteArr.push({
                    'campground': dataArr.records[i].details.loopName,
                    'title': dataArr.records[i].details.loopName + ' ' + dataArr.records[i].details.prodGrpName,
                    'summary': dataArr.records[i].details.siteDescription,
                    'address': dataArr.records[i].details.loopName,
                    'email': '',
                    'reservation_phone': '',
                    'information_phone': '',
                    'images': [{ image: imageData }],
                })

            }

        }
        console.log('Start:' + campstart + ' End:' + totalCampPage)
        campstart = campstart + 1;

        if (campstart < totalCampPage) {
            await fetchCampsite(campSiteUrl, page, campstart);
        }
        // console.log('in else coming');
        // console.log(campsiteArr)
        return campsiteArr;
    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground List');
        console.log("Error in FetchGround List");
        console.log(err);
        //  browser.close();
        return campsiteArr;
    }

}

async function storeCampGroundLogFile(crawlerLog, camp_id, campStatus, logFileLocation,address="") {
    try {
        const campUpdate = {
            data_file_path: logFileLocation,
            index_status: 'Completed',
            crawler_status: (campStatus == true) ? 'Success' : 'Failed',
            location:JSON.stringify(address),
        };
        await CampGround.update(campUpdate, {
            where: { id: camp_id }
        })

        debugLog.push(await dateTime() + ":" + "updating campground log status and set log file path")
        crawlerLog.push(await dateTime() + ":" + "updating campground log status and set log file path")

        fs.writeFileSync(filePathStore + "temp/camp_crawler" + camp_id + ".json", JSON.stringify(crawlerLog));
        const filepath = path.resolve(filePathStore + "temp/camp_crawler" + camp_id + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_campground_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/campgrounds/' + camp_id + '/crawler/' + crawlerLogfilename);
      
        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Successfully uploaded Campground Crawler log file on server")

            // fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Campground Crawler log file on server");
            debugLog.push(await dateTime() + ":" + "Failed to upload Campground Crawler log file on server")
            crawlerstatus = false;
        }

        const campLogDataUpdate = {
            status: (campStatus == true) ? 'Success' : 'Failed',
            log_file_path: crawlerLogfilename,
        };
        await CampgroundLog.update(campLogDataUpdate, {
            where: { id: campLogId }
        })
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Storing CampGround Log/CrawlerLog File');
    }
}

