// Manually run commnad for Reference
// node scrapping/sunBookingReserve.js --campground_id={campground_id} --booking_id={booking_enquiry_id}
// node scrapping/sunBookingReserve.js --campground_id=666 --booking_id=11

const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

//Prod
var configPath='../../../../../scrapping/';
var filePathStore='../scrapping/';
//Local
// var configPath = '../scrapping/';
// var filePathStore = 'scrapping/';

var s3 = require(configPath + "helper/s3imageUpload");
const db = require(configPath + "models");
const CampGround = db.CampGround;
const BookingEnquiry = db.BookingEnquiry;

var campgroundId = args.campground_id;
var bookingId = args.booking_id;
var closeBrowser = true;
var bookingStatus = true;
// var form_siteCategory = 'Lodging';
var form_siteCategory = 'RV Sites';

var form_checkIn = '';
var form_checkOut = '';
var form_inFants = '';
var form_adults = '';
var form_kids = '';
var form_pets = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_slideOuts = '';
var form_VkrPostalCode = '';
var form_purchaseVkr_Group = '';
var form_firstName = '';
var form_lastName = '';
var form_address1 = '';
var form_address2 = '';
var form_countryCode = '';
var form_phone = '';
var form_city = '';
var form_state = '';
var form_postcode = '';
var form_email = '';
var form_referal_resource = '';
var form_reason_visit = '';
var form_creditCard = '4242424242424242';
var form_creditCardType = '';
var form_creditCardExpMonth = '02';
var form_creditCardExpYear = '34';
var form_creditCardCode = '111';
var comment = '';
var debugLog = [];

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getCampGroundByID() {
    debugLog.push(await dateTime() + ":" + 'Fetching campground detail by id');
    await CampGround.findByPk(campgroundId)
        .then(data => {
            siteUrl = data['url']

        })
        .catch(async err => {
            bookingStatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Campground By Id');
            console.log('Error in Fetching Campground By Id');
        });
}

async function changeDateFormat(dateVal) {
    var checkDate = new Date(dateVal); // M-D-YYYY

    var dayName = checkDate.toLocaleString("default", { weekday: "short" })
    var monthName = checkDate.toLocaleString("default", { month: "short" })
    var dayNumber = checkDate.getDate();

    var dateString = dayName + ', ' + monthName + ' ' + dayNumber;
    return dateString;
}

async function getEnquiryDataID() {
    debugLog.push(await dateTime() + ":" + 'Fetching booking enquiry detail by id');
    await BookingEnquiry.findByPk(bookingId)
        .then(async data => {

            form_siteCategory = data['details']['site_category'];
            form_checkIn = await changeDateFormat(data['check_in_date']);
            form_checkOut = await changeDateFormat(data['check_out_date']);
            form_inFants = data['details']['infants'];
            form_adults = data['details']['adults'];
            form_kids = data['details']['kids'];
            form_pets = data['details']['pet'];
            form_equipmentType = data['details']['equipment_type'];
            form_equipmentLength = data['details']['equipment_length'];
            form_slideOuts = data['details']['slide_outs'];
            form_firstName = data['name'];
            form_lastName = data['name'];
            form_address1 = data['address1'];
            form_address2 = data['address2'];
            form_countryCode = data['country_code'];
            form_phone = data['phone'];
            form_postcode = data['post_code'];
            form_email = data['email'];
            form_referal_resource = data['details']['referal_resource'];
            form_reason_visit = data['details']['reason_visit'];

        })
        .catch(async err => {
            bookingStatus = false;
            debugLog.push(await dateTime() + ":" + 'Error in Fetching Enquiry By Id');
            console.log('Error in Fetching Enquiry By Id');
        });

    const enquiryUpdate = {
        index_status: 'InProgress',
        updated_at: Date.now(),
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })
}

(async () => {
    try {

        await getCampGroundByID();
        await getEnquiryDataID();

        await campspot();

    } catch (err) {
        console.log(err);
        bookingStatus = false;
        debugLog.push(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = 'Sometghing Went Wrong';
        console.log("Browser Closed");
    }
       await storeBookingCommentAndLog();

})();

async function storeBookingCommentAndLog() {

    fs.writeFileSync(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json", JSON.stringify(debugLog));
    const filepath = path.resolve(filePathStore + "temp/booking_enquiry_crawler_" + bookingId + ".json");

    var s3result;
    var crawlerLogfilename = new Date().getTime() + '_booking_log.json';

    // var CampLogfilename=datasourceId+'_crawler_log.json';
    debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

    s3result = await s3.uploadFile(filepath, 'booking/' + campgroundId + '/' + bookingId + '/' + crawlerLogfilename);

    console.log(s3result)
    if (s3result != '') {
        console.log("successfully uploaded Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Successfully uploaded Booking Crawler log file on server")
        fs.unlinkSync(filepath)
    } else {
        crawlerLogfilename = '';
        console.log("Failed to upload Booking Crawler log file on server");
        debugLog.push(await dateTime() + ":" + "Failed to upload Booking Crawler log file on server")
        crawlerstatus = false;
    }

    debugLog.push(await dateTime() + ":" + "Storing Comment And Update the Booking Enquiry Status")

    const enquiryUpdate = {
        index_status: 'Completed',
        comment: comment,
        log_file_path: crawlerLogfilename,
        updated_at: Date.now(),
        status: (bookingStatus == true) ? 'Success' : 'Failed',
    };
    BookingEnquiry.update(enquiryUpdate, {
        where: { id: bookingId }
    })

}

async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

async function campspot() {
    var browser = await puppeteer.launch({
        headless: false,
        args: [
            '--start-maximized' // you can also use '--start-fullscreen'
        ]
        // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
    });
    console.log('*************************');
    console.log(await browser.version());
    debugLog.push(await dateTime() + ":" + await browser.version());
    // open a new page
    var page = await browser.newPage();

    try {
        // console.log('Nevigating : ' + campgrounds[p].title)
        console.log('Nevigating : ' + siteUrl)
        debugLog.push(await dateTime() + ":" + 'Nevigating : ' + siteUrl);
        await page.setViewport({ width: 1366, height: 768 });

        await page.setDefaultNavigationTimeout(0);

        var browseUrl = siteUrl;
        // var browseUrl = campgrounds[p].link + 'reserve';
        debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

        await page.goto(browseUrl, { waitUntil: 'networkidle2' });

        await sleep(3000)

        debugLog.push(await dateTime() + ":" + 'Click on search availability link');

        const link = await page.$('#nav > div.nav-drop-classic-holder > div.search-block > div > a');             // declare object

        const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page())));    // declare promise
        await link.click();                             // click, a new tab opens

        debugLog.push(await dateTime() + ":" + 'New tab open');

        const newPage = await newPagePromise;           // open new tab /window, 
        
        await sleep(25000);

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input') !== null) {
            debugLog.push(await dateTime() + ":" + 'Fill check in date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div > label:nth-child(1) > input", form_checkIn);        }

        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input') !== null) {
            debugLog.push(await dateTime() + ":" + 'Fill check out date');
            await newPage.type("body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > aggredator > div > div.aggredator-field > label:nth-child(3) > input", form_checkOut);
        }

        await newPage.click('#search-form-label-dates');

        await newPage.evaluate(() => {
            document.querySelector('.home-search').scrollIntoView()
        })

        debugLog.push(await dateTime() + ":" + 'Fill guest details');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div') !== null) {
            await sleep(1000);
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
        }
        
        for (var i = 0; i < form_inFants; i++) {
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(1) > td:nth-child(2) > div > button.app-increase-guest-count-button-0.guests-picker-menu-category-controls-stepper.mod-increase');
        }
        await sleep(1000);
        for (var k = 0; k < form_kids; k++) {
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(2) > td:nth-child(2) > div > button.app-increase-guest-count-button-1.guests-picker-menu-category-controls-stepper.mod-increase');
        }
        await sleep(1000);
        if (form_adults > 2) {
            form_adults = form_adults - 2;
            for (var a = 0; a < form_adults; a++) {
                await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-increase-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-increase');
            }
        } else {
            if (form_adults == 0) {
                form_adults = 2;
                for (var a = 0; a < form_adults; a++) {
                    await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                }
            }
            if (form_adults == 1) {
                for (var a = 0; a < form_adults; a++) {
                    await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(3) > td:nth-child(2) > div > button.app-decrease-guest-count-button-2.guests-picker-menu-category-controls-stepper.mod-decrease');
                }
            }
        }
        await sleep(1000);
        for (var p = 0; p < form_pets; p++) {
            await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > div > table > tr:nth-child(4) > td:nth-child(2) > div > button.guests-picker-menu-category-controls-stepper.mod-increase.app-increase-pet-count-button');
        }
        await sleep(1000);
       

        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-guests > guest-categories-selectors > div > button > div');
        await sleep(1000);
        debugLog.push(await dateTime() + ":" + 'Click on search button');
        await newPage.click('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-submit > button');
        await sleep(1000);

        debugLog.push(await dateTime() + ":" + 'Checking error found or not');
        if (await newPage.$('body > app-root > div > main > search > section.home-splash.mod-sun > div > div.home-search > div > div.search-form-dates > div.search-form-error-message') != null) {
            debugLog.push(await dateTime() + ":" + 'Error found in search page');

            let element = await newPage.$('.search-form-error-message');
            let elementText = await newPage.evaluate(el => el.textContent, element)
            console.log(elementText);
            bookingStatus = false;
            comment=elementText;
        } else {
            await sleep(5000);
            debugLog.push(await dateTime() + ":" + 'Check campsite is found or not');
            if (await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div') !== null) {
                debugLog.push(await dateTime() + ":" + 'No campsite found');

                let element = await newPage.$('body > app-root > div > main > search-results > div > section.search-main > div > list-view > div > div');
                let elementText = await newPage.evaluate(el => el.textContent, element)
                console.log(elementText)
                comment=elementText;
                bookingStatus = false;
            } else {
                debugLog.push(await dateTime() + ":" + 'Campsite found');
                var sitefound = false;

                if (await newPage.$('.app-search-results-list') !== null) {
                    await sleep(1000);
                    debugLog.push(await dateTime() + ":" + 'Search campsite as par reservation type');
                    if (await newPage.$('.search-main  > div > div > div > div') != null) {

                        var test = await newPage.evaluate(async (form_siteCategory) => {
                            var sitetypeTotal = await document.querySelectorAll('.search-main  > div > div > div > div');
                            var found = false;
                            for (var t = 0; t < sitetypeTotal.length; t++) {
                                var title = sitetypeTotal[t].querySelector('button >h3:nth-child(1)').textContent.trim();

                                if (title == form_siteCategory) {

                                    sitetypeTotal[t].querySelector('button >h3:nth-child(1)').click();
                                    found = true;
                                }
                            }
                            return found;
                        }, form_siteCategory)
                        await sleep(2000)
                        if (!test) {
                            debugLog.push(await dateTime() + ":" + 'No campsite found as per reservation type');

                            console.log('No site Category found...')
                            comment='No site Category found...';
                            bookingStatus = false;
                        } else {
                            debugLog.push(await dateTime() + ":" + 'Campsite found as per reservation type');

                            sitefound = true;
                            await newPage.click('body > app-root > div > main > search-results > div > section.search-main > div > list-view > search-results-list > div > ul:nth-child(1) > li > a');
                        }

                    }
                    await sleep(10000)
                }
                if (sitefound) {
                    await sleep(1000);
                    debugLog.push(await dateTime() + ":" + 'Click on campsite to book');

                    await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button');
                    await sleep(3000);

                    //Checking if Location Field is Presernt or not
                    debugLog.push(await dateTime() + ":" + 'Checking if Location Field is Present or not');
                    console.log('Checking if Location Field is Present or not')
                    if (await newPage.$('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div:nth-child(3) > div:nth-child(2) > div > button') !== null) {
                        let element = await newPage.$('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div:nth-child(3) > div:nth-child(2) > div > button');
                        let elementText = await newPage.evaluate(el => el.textContent, element)
                        // console.log(elementText)
                        debugLog.push(await dateTime() + ":" + 'Checking if Location Field Has value or not');
                        console.log('Checking if Location Field Has value or not')
                        if (elementText.trim() != '') {
                            console.log('If Location Already inserted then clicking on add to cart button')
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal > div > form > div.mobile-modal-add-to-cart > input');
                            
                        } else {
                            debugLog.push(await dateTime() + ":" + 'If Location Field is Blanke the Clicking on location and selecting First Location');
                            
                            console.log('If Location Field is Blanke the Clicking on location and selecting First Location')
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div:nth-child(3) > div:nth-child(2) > div > button');
                            await sleep(1000)

                            await newPage.click('#locations > table > tr:nth-child(2) > td.site-locations-table-site-select > button');
                            await sleep(1000)
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking-mobile > a > button')
                            await sleep(1000)
                            await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input')

                        }
                        // //Clicking on Review Cart
                        // await newPage.click('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a');
                    } else {
                        console.log('Checking if Location Field is not present then Filling other values Like Equipement and related fields')
                        debugLog.push(await dateTime() + ":" + 'Checking if Location Field is not present then Filling other values Like Equipement and related fields');

                        if (await newPage.$('#rv-type-select') !== null) {
                            // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.select('#rv-type-select', form_equipmentType)
                            await sleep(1000)
                        }
                        await sleep(1000)
                        if (await newPage.$('.app-rv-length-input') !== null) {

                            await newPage.type('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.site-booking-form-section.site-booking-form-rv.app-booking-form-rv > div:nth-child(2) > label.site-booking-form-rv-length.app-rv-length-filter > input', form_equipmentLength)

                        }

                        if (await newPage.$('#rv-slide-out-select') !== null) {
                            // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                            await newPage.select('#rv-slide-out-select', form_slideOuts)
                            await sleep(1000)
                        }
                    }
                    await sleep(1000);
                    await newPage.click('body > app-root > div > main > site > section.site-content > div.site-booking.mobile-modal.is-visible > div > form > div.mobile-modal-add-to-cart > input');
                    // After Filing Those detail Popup is open so decline it
                    debugLog.push(await dateTime() + ":" + 'After Filing Those detail Popup is open so decline it');

                    if (await newPage.$('body > app-root > div > main > site > lock-site-modal > div > div > div.site-lock-modal-actions > button.site-lock-modal-actions-button.mod-decline') !== null) {
                        // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                        await newPage.click('body > app-root > div > main > site > lock-site-modal > div > div > div.site-lock-modal-actions > button.site-lock-modal-actions-button.mod-decline')
                        await sleep(1000)
                    }
                    if (await newPage.$('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a') !== null) {
                        // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                        await newPage.click('body > app-root > div > main > view-add-ons > div > section > div > div.cart-confirmation-item-actions > a')
                        await sleep(1000)
                    }
                    await sleep(1000)
                    debugLog.push(await dateTime() + ":" + 'clicking on Checkout Button');

                    console.log('clicking on Checkout Button')
                    if (await newPage.$('body > app-root > div > main > shopping-cart > div > section.cart-summary > div > a') !== null) {
                        // console.log('in button');
                        // debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                        await newPage.click('body > app-root > div > main > shopping-cart > div > section.cart-summary > div > a')
                        await sleep(1000)
                    }
                    await sleep(10000)

                    console.log('Filing Guest Information');
                    debugLog.push(await dateTime() + ":" + 'Filing Guest Information');

                    if (await newPage.$('#first-name') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filing first name');

                        const firstname = await newPage.waitForSelector("#first-name");
                        await newPage.click("#first-name");
                        await firstname.type(form_firstName)
                    }
                    if (await newPage.$('#guest-full-name-input') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filing guest full name');

                        const firstname = await newPage.waitForSelector("#guest-full-name-input");
                        await newPage.click("#guest-full-name-input");
                        await firstname.type(form_firstName)
                    }
                    // await sleep(1000)

                    // debugLog.push(await dateTime() + ":" + "Storing Last Name")
                    if (await newPage.$('#last-name') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filing last name');
                        const lastname = await newPage.waitForSelector("#last-name");
                        await newPage.click("#last-name");
                        await lastname.type(form_lastName)
                        // await sleep(1000)
                    }

                    if (await newPage.$('#guest-country') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Selecting guest country');
                        await newPage.select('#guest-country', form_countryCode)
                        await sleep(1000)
                    }

                    if (await newPage.$('#country-select') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Selecting country');
 
                        // debugLog.push(await dateTime() + ":" + "Selecting Card Type");
                        await newPage.select('#country-select', form_countryCode)
                        await sleep(1000)
                    }

                    if (await newPage.$('#guest-address-line-1') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filling guest address 1');
                        // debugLog.push(await dateTime() + ":" + "Storing Address 1")
                        const address1 = await newPage.waitForSelector("#guest-address-line-1");
                        await newPage.click("#guest-address-line-1");
                        await address1.type(form_address1)
                        // await sleep(1000)
                    }
                    if (await newPage.$('#address-line-1') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filling address 1');
  
                        // debugLog.push(await dateTime() + ":" + "Storing Address 1")
                        const address1 = await newPage.waitForSelector("#address-line-1");
                        await newPage.click("#address-line-1");
                        await address1.type(form_address1)
                        // await sleep(1000)
                    }

                    if (await newPage.$('#address-line-2') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filling address 2');
  
                        // debugLog.push(await dateTime() + ":" + "Storing Address 2")
                        const address2 = await newPage.waitForSelector("#address-line-2");
                        await newPage.click("#address-line-2");
                        await address2.type(form_address2)
                        // await sleep(1000)
                    }

                    if (await newPage.$('#guest-postal-code-input') !== null) {
                        debugLog.push(await dateTime() + ":" + 'Filling guest postal code');
  
                        const postCode = await newPage.waitForSelector("#guest-postal-code-input");
  
                        await newPage.click("#guest-postal-code-input");
                        await postCode.type(form_postcode)
                        // await sleep(1000)
                    }
                    if (await newPage.$('#postal-code') !== null) {

                        debugLog.push(await dateTime() + ":" + 'Filling post code');
                        const postCode = await newPage.waitForSelector("#postal-code");
                        await newPage.click("#postal-code");
                        await postCode.type(form_postcode)
                        // await sleep(1000)
                    }


                    if (await newPage.$('#guest-phone-number-input') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing Guest Phone Number")
                        const phoneNo = await newPage.waitForSelector("#guest-phone-number-input");
                        await newPage.click("#guest-phone-number-input");
                        await phoneNo.type(form_phone)
                        // await sleep(1000)
                    }
                    if (await newPage.$('#phone-number') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing Phone Number")
                        const phoneNo = await newPage.waitForSelector("#phone-number");
                        await newPage.click("#phone-number");
                        await phoneNo.type(form_phone)
                        // await sleep(1000)
                    }

                    if (await newPage.$('#guest-email-input') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing guest email")
                        const email = await newPage.waitForSelector("#guest-email-input");
                        await newPage.click("#guest-email-input");
                        await email.type(form_email)
                        // await sleep(1000)
                    }
                    if (await newPage.$('#email-address') !== null) {
                        debugLog.push(await dateTime() + ":" + "Storing email")
                        const email = await newPage.waitForSelector("#email-address");
                        await newPage.click("#email-address");
                        await email.type(form_email)
                        // await sleep(1000)
                    }
                    if (await newPage.$('#referral-source') !== null) {
                        debugLog.push(await dateTime() + ":" + "Selecting referral source");
                        await newPage.select('#referral-source', form_referal_resource)
                        await sleep(1000)
                    }
                    if (await newPage.$('#reason-for-visit') !== null) {
                        debugLog.push(await dateTime() + ":" + "Selecting reason for visit");
                        await newPage.select('#reason-for-visit', form_reason_visit)
                        await sleep(1000)
                    }
                    await sleep(5000)
                    console.log('click on Submit Button')
                    debugLog.push(await dateTime() + ":" + "click on Submit Button");
                    if (await newPage.$('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(2) > div > div.checkout-form-submit > button') !== null) {
                        await newPage.click('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(2) > div > div.checkout-form-submit > button');
                    }

                    await sleep(1000)
                    debugLog.push(await dateTime() + ":" + "Checking error in form");
                    if (await newPage.$('.checkout-form-error-summary-content') != null) {
                        debugLog.push(await dateTime() + ":" + "Error in form filling");

                        let element = await newPage.$('.checkout-form-error-summary-content');
                        let elementText = await newPage.evaluate(el => el.textContent, element)
                        console.log(elementText)
                        comment=elementText;
                        bookingStatus = false;
                    }

                    if (await newPage.$('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail.app-guest-information > form > button') !== null) {
                        debugLog.push(await dateTime() + ":" + "Click on submit ");
                        await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail.app-guest-information > form > button');
                    }

                    // await sleep(30000)
                    debugLog.push(await dateTime() + ":" + "Click on checkout form ");
                    if (await newPage.$('#app-payment-amount-fragment > div > div.checkout-form-submit > button')) {
                        await newPage.click('#app-payment-amount-fragment > div > div.checkout-form-submit > button')
                        closeBrowser=false;
                        console.log("Booking Successfully Done.")
                        debugLog.push(await dateTime() + ":" + "Booking Successfully Done.");

                        comment="Booking Successfully Done."
                        bookingStatus = true;
                    }
                    // await sleep(30000)

                    // const frameHandle = await newPage.$("#tokenFrame");
                    // const frame = await frameHandle.contentFrame();
                    // // const elementHandle = await newPage.$('body > app-root > div > main > checkout > div > form > card-connect-checkout > section:nth-child(4) > div > div > form >label>iframe > #document');
                    // // const frame = await elementHandle.contentFrame();
                    // console.log('frame is')
                    // console.log(frame)
                    // if (frame != null) {

                    //     if (await frame.$('#ccnumfield') !== null) {
                    //         console.log('ccnumber filling')
                    //         const cardNumber = await frame.waitForSelector("#ccnumfield");
                    //         await frame.click("#ccnumfield");
                    //         await cardNumber.type(form_creditCard)
                    //     }


                    // } else {
                    //     console.log('in else')
                    //     if (await newPage.$('#ccnumfield') !== null) {
                    //         console.log('ccnumber filling')

                    //         const cardNumber = await newPage.waitForSelector("#ccnumfield");
                    //         await newPage.click("#ccnumfield");
                    //         await cardNumber.type(form_creditCard)
                    //     }


                    // }

                    // const expMonth = await newPage.waitForSelector("body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > div > div.checkout-field-cc-expiration > input.checkout-field-input.mod-cc-exp.app-cc-exp-month.ng-pristine.ng-untouched.ng-valid-min.ng-valid-max.ng-invalid.ng-invalid-required");
                    // await newPage.click("#body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > div > div.checkout-field-cc-expiration > input.checkout-field-input.mod-cc-exp.app-cc-exp-month.ng-pristine.ng-untouched.ng-valid-min.ng-valid-max.ng-invalid.ng-invalid-required");
                    // await expMonth.type(form_creditCardExpMonth)

                    // const expYear = await newPage.waitForSelector("body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > div > div.checkout-field-cc-expiration > input.checkout-field-input.mod-cc-exp.app-cc-exp-year.ng-pristine.ng-untouched.ng-valid-min.ng-valid-max.ng-invalid.ng-invalid-required");
                    // await newPage.click("body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > div > div.checkout-field-cc-expiration > input.checkout-field-input.mod-cc-exp.app-cc-exp-year.ng-pristine.ng-untouched.ng-valid-min.ng-valid-max.ng-invalid.ng-invalid-required");
                    // await expYear.type(form_creditCardExpYear)

                    // const cardCode = await newPage.waitForSelector("body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > label:nth-child(4) > input");
                    // await newPage.click("body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > section > div > div:nth-child(2) > label:nth-child(4) > input");
                    // await cardCode.type(form_creditCardCode)

                    // await sleep(3000)
                    // console.log('click on continue order review')
                    // await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > form > button');

                    // await sleep(3000)
                    // await newPage.click('#agree-to-terms-and-conditions');
                    // await sleep(1000)

                    // await newPage.click('body > div.ng-scope > ng-view > div > div > section.new-consumer-checkout-detail > div > button');
                }
            }
            await sleep(1000)
        }
        if (closeBrowser) {
            debugLog.push(await dateTime() + ":" + "Browser Closed ")
            await newPage.close();
            await browser.close();
        }
    } catch (err) {
        // await newPage.close();
        await browser.close();
        debugLog.push(await dateTime() + ":" + "Sorry, an error has ocurred ")
        comment = 'Sorry, an error has ocurred';
        bookingStatus = false;
        console.log(err)
    }
}