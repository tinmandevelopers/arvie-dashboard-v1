var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')
console.log(args);
var debugLog = [];
var crawlerstatus = true;


//Prod
// var configPath='../../../../../scrapping/';
// var filePathStore='../scrapping/';
//Local'
var configPath = '../scrapping/';
var filePathStore = 'scrapping/';


var s3 = require(configPath + "helper/s3imageUpload");
const db = require(configPath + "models");
// console.log('Age: ' + args.age);
var datasourceId = args.datasource_id;
var app = express();
// const db = require("../../../../../scrapping/models/index.js");
const CampGroundGroup = db.CampGroundGroup;
const CampGround = db.CampGround;
const DataSourceCrawlerLog = db.DataSourceCrawlerLog;
const CampgroundLog = db.CampgroundLog;
const DataSource = db.DataSource;

const puppeteer = require("puppeteer");
const chalk = require("chalk");
var fs = require("fs");
// var s3 = require("../../../../../scrapping/helper/s3imageUpload");
// MY OCD of colorful console.logs for debugging... IT HELPS
const error = chalk.bold.red;
const success = chalk.keyword("green");
const log = console.log;
var crawlerLogId = 0;
var campLogId = 0;
//app.get('/scrape', function (req, res) {
var campID = 0;
var siteUrl = '';

(async () => {
    try {
        const dataSourceData = await getDatasourceByID();

        const storeLog = await storeCrawlerLog();

        var browser = await puppeteer.launch({
            headless: true,
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });

        debugLog.push(await dateTime() + ":" + await browser.version());
        console.log(await browser.version());

        // open a new page
        var page = await browser.newPage();

        await page.setDefaultNavigationTimeout(0);

        console.log('URL is : ' + siteUrl);
        var browseUrl = siteUrl + '/resorts/';
        console.log('Navigating...');
        await page.goto(browseUrl);
        debugLog.push(await dateTime() + ":" + 'Navigating...' + browseUrl);

        debugLog.push(await dateTime() + ":" + 'Fetching CampGroup and Campground data');
        // var campgroundData = await page.evaluate(({ siteUrl }) => {

        //     var rowList = document.querySelectorAll('#resortlistsectionscrolldiv > div.container > div > div > h2');

        //     var rowArray = [{ 'camp_group': {} }];

        //     for (var i = 0; i < rowList.length; i++) {
        //         const country = rowList[i] && rowList[i].innerText.trim()
        //         var count = i + 1;
        //         var campGroup = document.querySelectorAll('#resortlistsectionscrolldiv > div.container > div > div:nth-child(' + count + ') > div > div > h3 > a');

        //         var campvalgroup = [];
        //         for (var j = 0; j < campGroup.length; j++) {
        //             var jcnt = j + count;
        //             var campGround;
        //             if (i == 0) {
        //                 campGround = document.querySelectorAll('#resortlistsectionscrolldiv > div.container > div > div:nth-child(' + count + ') > div > div > div > div > ul > li > a');
        //             } else {
        //                 campGround = document.querySelectorAll('#resortlistsectionscrolldiv > div.container > div > div:nth-child(' + count + ') > div:nth-child(' + jcnt + ') > div > div > div > ul > li > a');
        //             }

        //             var campgroundVal = [];
        //             for (var k = 0; k < campGround.length; k++) {
        //                 campgroundVal[k] = {
        //                     title: campGround[k] && campGround[k].innerText.trim(),
        //                     url: campGround[k] && campGround[k].getAttribute("href"),
        //                 }
        //             }
        //             campvalgroup[j] = {
        //                 country_name: country,
        //                 camp_group: campGroup[j] && campGroup[j].innerText.trim(),
        //                 camp_group_url: campGroup[j] && campGroup[j].getAttribute("href"),
        //                 campground: campgroundVal,
        //             };

        //         }
        //         rowArray[0].camp_group[i] = campvalgroup;
        //     }
        //     return rowArray;


        // }, { siteUrl });
        debugLog.push(await dateTime() + ":" + 'Fetched CampGroup and Campground data');

        // console.log('campground Data');
        // console.log(JSON.stringify(campgroundData));

        // fs.writeFileSync("hackernews1.json", JSON.stringify(news));
        // fs.writeFileSync("../scrapping/suncampground.json", JSON.stringify(campgroundData));
        // debugLog.push(await dateTime() + ":" + 'Go to store data of campgroup and campground');
        await page.exposeFunction("uploadFileToS3", s3.uploadImageLocal)
        await page.exposeFunction("convertToSlug", this.fnConvertToSlug)

        // const result = await storeCampground(campgroundData, page);
        // const resultscrap = await scrapeAll(page);
        const resultscrap = await scrapeAll(page, async function (returnValue) {
            storeCrawlerLogFile()
            browser.close();
            console.log("Browser Closed");
            debugLog.push(await dateTime() + ":" + "Browser Closed");
        });

    } catch (err) {
        // Catch and display errors
        crawlerstatus = false;
        console.log(error(err));
        debugLog.push(await dateTime() + ":" + err);
        await storeCrawlerLogFile()
        await browser.close();
        console.log(error("Browser Closed"));
        debugLog.push(await dateTime() + ":" + "Browser Closed");
    }
    // console.log('Debug Log is');
    // console.log(await dateTime() + ":" + debugLog);
})();

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Getting DateTime');
    }
}

async function getDatasourceByID() {
    await DataSource.findByPk(datasourceId)
        .then(data => {
            siteUrl = data['url']
        })
        .catch(async err => {
            debugLog.push(await dateTime() + ":" + 'Error in Fetching DataSource By Id');
        });
}

async function storeCrawlerLog() {
    try {
        let response = [];
        const logData = {
            data_source_id: datasourceId,
            status: 'InProgress',
        };
        debugLog.push(await dateTime() + ":" + 'Storing Crawler Log');
        DataSourceCrawlerLog.create(logData)
            .then(async data => {

                response['status'] = 'success';
                response['content'] = data;
                // return data;
                crawlerLogId = data['id'];
                debugLog.push(await dateTime() + ":" + 'Crawler Log Stored Successfully');
            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + 'Error in crawler log storing');
                debugLog.push(await dateTime() + ":" + err.message);

                response['status'] = 'error';
                response['content'] = err;
                // return err;
                // console.log(err.message)
            });
        return response;
    } catch (e) {
        debugLog.push(await dateTime() + ":" + 'Error in Storing Crawler Log Data');
        console.log(e)
        crawlerstatus = false;
    }
}

async function storeCampground(campdata, page) {
    try {

        // console.log(campdata[0].camp_group)
        debugLog.push(await dateTime() + ":" + 'In store campground function');
        for (var i = 0; i < campdata.length; i++) {
            for (var key in campdata[i].camp_group) {

                for (var property in campdata[i].camp_group[key]) {
                    var gname = campdata[i].camp_group[key][property]['camp_group'];
                    var cntname = campdata[i].camp_group[key][property]['country_name'];
                    var groupurl = campdata[i].camp_group[key][property]['camp_group_url'];
                    // console.log(campdata[i].camp_group[property]['camp_group'])
                    var groupname = gname.replace("CAMPGROUNDS IN ", "");
                    var country = cntname.replace("CAMPGROUNDS IN ", "");
                    // console.log(newname)

                    const campGroup = {
                        name: groupname,
                        data_source_id: datasourceId,
                        country: country,
                        url: groupurl
                    };
                    var condition = { name: groupname };
                    const camp = campdata[i].camp_group[key][property]['campground'];
                    // console.log("camp is ")
                    // console.log(camp)
                    // debugLog.push(dateTime() + ":" + 'checking campgroup is added or not');
                    await CampGroundGroup
                        .findOne({ where: campGroup })
                        .then(function (obj) {
                            // console.log(obj)
                            // update
                            if (obj) {
                                // debugLog.push(dateTime() + ":" + 'campgroup updating');

                                return obj.update(campGroup)
                                    .then(data => {
                                        for (const campkey in camp) {
                                            // console.log(camp[campkey]['url'])
                                            const cont = {
                                                data_source_id: datasourceId,
                                                campground_group_id: data['id'],
                                                title: camp[campkey]['title'],
                                                url: camp[campkey]['url'],
                                                // status: 'Active',
                                                // index_status: 'Pending',
                                                // last_indexed_time:Date.now(),

                                            };
                                            // console.log(cont)
                                            // debugLog.push(dateTime() + ":" + 'checking campground is added or not');
                                            CampGround.findOne({ where: cont })
                                                .then(function (obj) {
                                                    // debugLog.push(dateTime() + ":" + 'updating campground ');

                                                    const campdata = {
                                                        data_source_id: datasourceId,
                                                        campground_group_id: data['id'],
                                                        title: camp[campkey]['title'],
                                                        url: camp[campkey]['url'],
                                                        status: 'Pending',
                                                        index_status: 'Pending',
                                                        last_indexed_time: Date.now(),

                                                    };
                                                    if (obj) {
                                                        // console.log('found')
                                                        // console.log(campdata)

                                                        obj.update(campdata).then(data => { })
                                                            .catch(async err => {
                                                                debugLog.push(await dateTime() + ":" + "Error in updating Campground While Campground is Already stored");
                                                                console.log(err.message)
                                                            });
                                                        // debugLog.push(dateTime() + ":" + ' campground updated');

                                                    } else {
                                                        // console.log('not found')
                                                        // console.log(campdata)
                                                        // debugLog.push(dateTime() + ":" + ' campground created');
                                                        CampGround.create(campdata).then(data => { })
                                                            .catch(async err => {
                                                                debugLog.push(await dateTime() + ":" + "Error in creting Campground While Campground is not Found");
                                                                console.log(err.message)
                                                            });
                                                    }


                                                })


                                        }
                                    })
                                    .catch(async err => {
                                        debugLog.push(await dateTime() + ":" + "Error in updating Campground While alredy stored");
                                        console.log(err.message)
                                    });

                            } else {

                                // insert
                                // debugLog.push(dateTime() + ":" + ' campgroup creating');
                                return CampGroundGroup.create(campGroup, camp)
                                    .then(data => {
                                        // debugLog.push(dateTime() + ":" + ' campgroup created');
                                        for (var campkey in camp) {
                                            const campground = {
                                                data_source_id: datasourceId,
                                                campground_group_id: data['id'],
                                                title: camp[campkey]['title'],
                                                url: camp[campkey]['url'],
                                                status: 'Pending',
                                                index_status: 'Completed',
                                                last_indexed_time: Date.now(),

                                            };
                                            // debugLog.push(dateTime() + ":" + ' campground creating');

                                            CampGround.create(campground)
                                                .then(data => { })
                                                .catch(async err => {
                                                    debugLog.push(await dateTime() + ":" + "Error in Creating New Campground");
                                                    console.log(err.message)
                                                });

                                        }
                                    })
                                    .catch(async err => {
                                        debugLog.push(await dateTime() + ":" + 'Error in creating new campground while not Campground Detail found in Database');
                                        console.log(err.message)
                                    });
                            }

                        }).catch(async err => {
                            debugLog.push(await dateTime() + ":" + 'error in checking Capground is stored or not.' + err.message);
                            console.log(err.message)
                        });
                }
            }

        }

    } catch (e) {
        crawlerstatus = false;
        console.log(e)

        debugLog.push(await dateTime() + ":" + 'Error in Storing Campground');
    }

}

async function storeCrawlerLogFile() {
    try {
        fs.writeFileSync(filePathStore + "temp/crawler" + datasourceId + ".json", JSON.stringify(debugLog));
        const filepath = path.resolve(filePathStore + "temp/crawler" + datasourceId + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Crawler log file to server");
            // fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Crawler log file to server");
        }

        const crawlerUpdate = {
            log_file_path: 'Log:' + crawlerLogfilename,
            status: (crawlerstatus == true) ? "Success" : "Failed",
            updated_at: Date.now(),
        };
        DataSourceCrawlerLog.update(crawlerUpdate, {
            where: { id: crawlerLogId }
        })
        var statusVal = (crawlerstatus == true) ? 'Success' : 'Failed';
        const dataSourceUpdate = {
            crawler_log: statusVal + ':' + crawlerLogfilename,
        };
        DataSource.update(dataSourceUpdate, {
            where: { id: datasourceId }
        })
    } catch (e) {
        console.log("Error in Updating Datasource Crawler File")
        console.log(e)
    }
}


async function fnConvertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '')
        ;
    //  will generate -- data

    //text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
    // used for - To avoid multiple sequential hyphens
}


async function campground_details() {
    try {
        let dev = {};
        let returndata;
        var re = /(?:\.([^.]+))?$/;
        let amenitiesArr = { amenities: [] };
        let localAttractionsArr = [];
        var offers = [];
        let photosArray = [];
        let accomodationArray = [];
        dev.single_page = true;

        let contact = {};
        let data = {};

        var linksTag = await document.querySelector('#nav .subnav_area_bg ul');


        //return linksTag;

        var name = await document.querySelector('.intro-section .title-block .container span.title').textContent;

        var slug = name
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;


        var main_image = '';

        if (await document.querySelector('.intro-section .templateseobgheroimg') != null) {
            main_image = await document.querySelector('.intro-section .templateseobgheroimg').getAttribute('src');
        } else {
            var style = getComputedStyle(await document.querySelector('.intro-section .bg-stretch'));
            main_image = style.backgroundImage.slice(4, -1).replace(/"/g, "");

            //          var style = await document.querySelector('.intro-section .bg-stretch').style.backgroundImage;
            //         var main_image = style.backgroundImage;
        }

        var description = await document.querySelector('.about-section .container .text-holder').innerHTML;

        var contactTag = await document.querySelector('.contacts-nav .contacts-list');


        var phone = contactTag.querySelector('a[href^="tel:"]').getAttribute('href').replace("tel:", "");
        var email = contactTag.querySelector('a[href^="mailto:"]').getAttribute('href').replace("mailto:", "");
        var addressTag = contactTag.querySelector('address');
        var map_link = addressTag.parentElement.getAttribute('href');
        var resport_map = await document.querySelectorAll('.resortlinkright .resortlinkother a.map-link')[0].getAttribute('href');

        contact.telephone_no = phone;
        contact.email = email;
        contact.physical_address = addressTag.textContent.trim();
        contact.map_link = map_link;
        contact.resport_map = resport_map;

        var addressDetail=addressTag.textContent.trim()
        var addArr = addressDetail.split(',');
        var newAddress=(addArr[addArr.length-1]) ? addressDetail.replace(addArr[addArr.length-1], "") : "";
        var pinState = (addArr[addArr.length-1]) ? addArr[addArr.length-1].trim().split(' ') : "";
        var state=(pinState[0]) ? pinState[0] : "";
        var zip=(pinState[1]) ? pinState[1] : "";

        var addressData = {
            address: newAddress,
            city: "",
            state: state,
            country: "",
            zip: zip,
            latitude: "",
            longitude: ""                          
        };
        contact.address = addressData;
        
        // return contact;

        var seasonInfo = await document.querySelector('.resortlink ul').innerHTML;

        //return linksTag;
        if (linksTag.classList.contains("checkselenium")) {

            dev.single_page = false;

            var aTags = await linksTag.querySelectorAll('li a');
            var searchText = "Ways to Stay";
            dev.accomodation_link = '';

            for (var i = 0; i < aTags.length; i++) {
                if (aTags[i].textContent.toLowerCase() == searchText.toLowerCase()) {
                    dev.accomodation_link = aTags[i].getAttribute('href');
                    break;
                }
            }

            // return dev;

            var photos = await document.querySelectorAll('.about-section .container .images-container .images-box .img-holder');
            // var lazyLoad = await document.querySelectorAll('.about-section .container .images-container .images-box .img-holder .lazyloaded');

            //   if (lazyLoad.length > 0) {
            // return 1;
            //  await window.scrollTo(0,document.body.scrollHeight);
            //var scrollingElement = await (document.scrollingElement || document.body);
            //scrollingElement.scrollTop = scrollingElement.scrollHeight;
            //await document.getElementsByClassName("blog-section")[0].scrollIntoView();
            //  }

            // return photos;
            // var titleArr = new Array();
            var imgIndex = 1;


            for (var photo of photos) {
                //return photo.nodeName;
                // return await photo.querySelector('picture > source').getAttribute('data-srcset');
                if (await photo.querySelector('.destination-img') != null) {
                    var title = 'gallery_' + imgIndex;
                    var url = '';
                    if (await photo.querySelector('picture > source') != null) {
                        var url = await photo.querySelector('picture > source').getAttribute('data-srcset');
                    }
                } else {
                    // return 'else';
                    var title = await photo.querySelector('img').getAttribute('title');
                    var url = await photo.querySelector('img').getAttribute('data-src');
                }
                // titleArr.push(title)

                var ext = await re.exec(url)[1];
                var slug_image = await (title
                    .toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
                ;

                var s3 = '';
                // var s3 = await uploadFileToS3(url, 'sun/' + slug_image);

                await photosArray.push({ title: title, image: url });
                imgIndex++;
            }

            // return photosArray;
        } else {
            // return 1;
            var photos = await document.querySelectorAll('.about-section .container .gallery-block .slick-track .slick-slide a');
            var titleArr = new Array();
            var imgIndex = 1;
            for (var photo of photos) {
                if (await photo.firstElementChild.nodeName == 'IMG') {
                    var title = await photo.getAttribute('title');
                    var url = await photo.querySelector('img').getAttribute('src');
                } else {
                    var title = 'gallery_' + imgIndex;
                    titleArr.push(title);
                    var url = await photo.querySelector('div').getAttribute('data-bgset');
                }

                titleArr.push(title)

                var ext = await re.exec(url)[1];
                var slug_image = await (title
                    .toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
                ;

                var s3 = '';
                // var s3 = await uploadFileToS3(url, 'sun/' + slug_image);

                await photosArray.push({ title: title, image: url });
                imgIndex++;
            }
            //return titleArr;
            //return photosArray;


            var amenities = await document.querySelectorAll('#amenities-folder ul li');


            for (var amenity of amenities) {
                amenitiesArr['amenities'].push(amenity.textContent.trim());
            }

            data.amenities = amenitiesArr;


            var local_attractions = await document.querySelectorAll('#attractions-anchor .attractions-holder ul li');


            for (var attraction of local_attractions) {
                localAttractionsArr.push(attraction.textContent.trim());
            }

            data.local_attractions = localAttractionsArr;


            var accomodationTag = await document.querySelectorAll('.ways_to_stay_bg:not([style*="display:none"]) .container ul li a');


            for (var accomodation of accomodationTag) {
                var acc_type = accomodation.querySelector('h5').textContent;
                var id = accomodation.getAttribute('href').replace('#', '');

                var accomodationTag = await document.getElementById(id);

                var all_accomodation = accomodationTag.querySelectorAll('.full_ways_stay_div');

                var accoData = new Array();

                for (var a_acc of all_accomodation) {

                    var acc_data = {};
                    acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
                    acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
                    acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

                    var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
                    var amArray = new Array();
                    for (var amenitiy of amenities) {
                        amArray.push(amenitiy.textContent.trim());
                    }

                    acc_data.amenities = amArray;

                    var imageArray = new Array();
                    var images = a_acc.querySelectorAll('.waystostay_slider_item a img');
                    for (var image of images) {
                        imageArray.push({
                            title: image.getAttribute('title'),
                            description: image.getAttribute('description'),
                            image: image.getAttribute('data-src')
                        });
                    }


                    acc_data.images = imageArray;
                }
                accoData.push(acc_data);
                accomodationArray.push({ type: acc_type, data: accoData });
            }
            // return accomodationArray;

            var offerTag = await document.querySelector('.special-offers-list');

            // return offerTag;
            //return offerTag.children.length;
            //return offerTag.childNodes;
            if (offerTag.children.length > 0) {
                if (!offerTag.classList.contains("slick-slider")) {
                    //return offerTag.nodeName;
                    if (offerTag.nodeName == 'UL') {
                        for (var offer of offerTag) {
                            var offer = offer.querySelector('img');
                            var link = offer.querySelector('.holder a').getAttribute('href');
                            var offer_description = offer.querySelector('.holder p').textContent;
                            offers.push({
                                title: offer.getAttribute('title'),
                                description: offer_description,
                                image: offer.getAttribute('src'),
                                link: link
                            });
                        }
                    } else {

                        //                    var offer_img_tag = offerTag.querySelector('img');
                        //                    var offer_image = offer_img_tag.getAttribute('data-src');
                        //                      return offer_image;
                        //                    var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');
                        //
                        //                    var offer_description = offerTag.querySelector('.holder p').textContent;
                        //                    offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});

                        if (offerTag.querySelector('img') != null) {
                            var offer_img_tag = offerTag.querySelector('img');
                            var offer_image = offer_img_tag.getAttribute('data-src');
                            var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                            var offer_description = offerTag.querySelector('.holder p').textContent;
                            offers.push({
                                title: offer_img_tag.getAttribute('title'),
                                description: offer_description,
                                image: offer_image,
                                link: link
                            });
                        } else {
                            var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                            //  return offer_img_tag;
                            var offer_image = offer_img_tag.getAttribute('data-bgset');
                            var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                            var offer_description = offerTag.querySelector('.holder p').textContent;
                            offers.push({
                                title: offer_img_tag.getAttribute('title'),
                                description: offer_description,
                                image: offer_image,
                                link: link
                            });
                        }
                    }

                } else {
                    // return 1;
                    if (offerTag.nodeName == 'UL') {
                    } else {
                        if (offerTag.querySelector('img') != null) {
                            var offer_img_tag = offerTag.querySelector('img');
                            var offer_image = offer_img_tag.getAttribute('data-src');
                            var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                            var offer_description = offerTag.querySelector('.holder p').textContent;
                            offers.push({
                                title: offer_img_tag.getAttribute('title'),
                                description: offer_description,
                                image: offer_image,
                                link: link
                            });
                        } else {
                            var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                            //  return offer_img_tag;
                            var offer_image = offer_img_tag.getAttribute('data-bgset');
                            var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                            var offer_description = offerTag.querySelector('.holder p').textContent;
                            offers.push({
                                title: offer_img_tag.getAttribute('title'),
                                description: offer_description,
                                image: offer_image,
                                link: link
                            });
                        }


                    }
                }

            }

        }


        // return offers;

        //    offer.title = await document.querySelector('.offers-section .specialoffer_btns .text-block').innerHTML;
        //     offer.description = await document.querySelector('.offers-section .specialoffer_btns .text-block').innerHTML;


        returndata = {
            name: name,
            slug: slug,
            description: description,
            main_image: main_image,
            data: data,
            contact: contact,
            season_dates: seasonInfo,
            images: photosArray,
            accomodation_types: accomodationArray,
            offers: offers,
            dev: dev
        };
        return returndata;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Campground Details");
        console.log(e)
    }
}


async function funamenities() {
    try {
        let amenitiesArr = { amenities: [], unique_amenities: [] };
        let amenities = await document.querySelectorAll('.amenities-container .content-box .title-bar span');
        let unique_amenities = await document.querySelectorAll('.amenities-block ul.accordion li a');


        for (var amenity of amenities) {
            amenitiesArr['amenities'].push(amenity.textContent);
        }

        for (var amenity of unique_amenities) {
            amenitiesArr['unique_amenities'].push(amenity.textContent);
        }

        return amenitiesArr;
    } catch (e) {
        debugLog.push(await dateTime() + ":" + "Error in Fetching Campground Amenities");
        crawlerstatus = false;
        console.log(e)
    }

}

async function funaccomodationtypes() {
    try {
        let accomodationArray = new Array();
        var accomodations = await document.querySelectorAll('.ways_to_stay_bg ul.tabs_headings_tabs li');
        //  var other_accomodations = await document.querySelector('.ways_to_stay_bg:not([style*="display:none"]) ul li');
        var accoData = new Array();

        for (var accomodationtag of accomodations) {
            var accomodation = await accomodationtag.querySelector('a');
            var acc_type = await accomodation.querySelector('h5').textContent;

            var acc_data = {};

            if (accomodationtag.classList.contains("ui-tabs-active")) {

                var all_accomodation = await document.querySelectorAll('.ways_to_stay_bg .full_ways_stay_div');

                accoData = new Array();

                //  return all_accomodation;

                for (var a_acc of all_accomodation) {

                    acc_data = {};
                    acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
                    //return  acc_data.title;
                    acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
                    acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

                    var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
                    var amArray = new Array();
                    for (var amenitiy of amenities) {
                        amArray.push(amenitiy.textContent.trim());
                    }

                    acc_data.amenities = amArray;

                    // return acc_data.amenities;

                    var imageArray = new Array();
                    var images = a_acc.querySelectorAll('.waystostay_slider_item a img');

                    //return images;
                    for (var image of images) {
                        imageArray.push({
                            title: image.getAttribute('title'),
                            description: image.getAttribute('description'),
                            image: image.getAttribute('data-src')
                        });
                    }


                    acc_data.images = imageArray;

                    // return acc_data;
                }
                accoData.push(acc_data);
            } else {
                accoData = [];
            }


            accomodationArray.push({ type: acc_type, url: accomodation.getAttribute('href'), data: accoData });
        }
        return accomodationArray;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Accomodation Types Details");
        console.log(e)
    }
}

async function funaccomodationdetails() {
    try {
        let accomodationArray = new Array();


        var all_accomodation = await document.querySelectorAll('.ways_to_stay_bg .full_ways_stay_div');

        //  return all_accomodation;

        for (var a_acc of all_accomodation) {

            var acc_data = {};
            acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
            //return  acc_data.title;
            acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
            acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

            var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
            var amArray = new Array();
            for (var amenitiy of amenities) {
                amArray.push(amenitiy.textContent.trim());
            }

            acc_data.amenities = amArray;

            // return acc_data.amenities;

            var imageArray = new Array();
            var images = a_acc.querySelectorAll('.waystostay_slider_item a img');

            //return images;
            for (var image of images) {
                imageArray.push({
                    title: image.getAttribute('title'),
                    description: image.getAttribute('description'),
                    image: image.getAttribute('data-src')
                });
            }


            acc_data.images = imageArray;

            accomodationArray.push(acc_data);

            // return acc_data;
        }


        // accomodationArray.push({type: acc_type, url: accomodation.getAttribute('href'), data: accoData});
        //  }
        return accomodationArray;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Accomodation Details");
        console.log(e)
    }
}

async function funattractions() {
    try {
        let attractionList = new Array();


        var attractionListTag = await document.querySelectorAll('.lcl_attrac_panel_bg .lcl_attrac_panel');

        let website = '';
        for (var attractionTag of attractionListTag) {
            var picture = await attractionTag.querySelector('.lcl_img img').getAttribute('data-src');
            let title = await attractionTag.querySelector('.lcl_text_area h1').textContent;
            let address = await attractionTag.querySelector('.lcl_text_area h2').textContent;
            let description = await attractionTag.querySelectorAll('.lcl_text_area p')[1].innerHTML;

            var obj = {
                title: title,
                address: address,
                description: description,
                picture: picture,
                website: website
            };
            attractionList.push(obj);
        }

        return attractionList;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Attraction Details");
        console.log(e)
    }
}

async function funoffers() {
    try {
        //  return 2;
        var offers = new Array();
        var offerTag = await document.querySelector('.special-offers-list');
        //return offerTag;
        //return offerTag.childNodes;
        if (offerTag.children.length > 0) {
            //  return 1;
            if (!offerTag.classList.contains("slick-slider")) {

                if (offerTag.nodeName == 'UL') {
                    //return 1;
                    var offerLi = await offerTag.querySelectorAll('li');

                    for (var offer of offerLi) {
                        var offer_img_tag = offer.querySelector('img');
                        var link = offer.querySelector('.holder a').getAttribute('href');
                        var offer_description = offer.querySelector('.holder p').textContent;
                        offers.push({
                            title: offer_img_tag.getAttribute('title'),
                            description: offer_description,
                            image: offer_img_tag.getAttribute('src'),
                            link: link
                        });
                    }
                } else {

                    var offer_img_tag = offerTag.querySelector('img');
                    var offer_image = offer_img_tag.getAttribute('data-src');
                    var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                    var offer_description = offerTag.querySelector('.holder p').textContent;
                    offers.push({
                        title: offer_img_tag.getAttribute('title'),
                        description: offer_description,
                        image: offer_image,
                        link: link
                    });
                }

            } else {
                //  return 5;
                if (offerTag.nodeName == 'UL') {
                    //  return 6;
                    var offerLi = await offerTag.querySelectorAll('li');
                    //  return offerLi;
                    for (var offer of offerLi) {
                        if (offerTag.querySelector('picture') != null) {
                            var offer_img_tag = offer.querySelector('div.bg-stretch');
                            var offer_image = offer_img_tag.getAttribute('data-bgset');
                            //return offer;
                            var link = offer.querySelector('.holder a').getAttribute('href');

                            var offer_description = offer.querySelector('.holder p').textContent;
                        } else {
                            //return 88;
                            var offer_image_tag = offer.querySelector('img');
                            //   return offer;
                            var offer_image = offer_image_tag.getAttribute('src');
                            // return offer_image;
                            var link = offer.querySelector('.holder a').getAttribute('href');
                            var offer_description = offer.querySelector('.holder p').textContent;
                        }

                        if (offer_description == '') {
                            var offer_description = offer.querySelector('.holder div').textContent;
                        }

                        offers.push({
                            title: offer.getAttribute('title'),
                            description: offer_description,
                            image: offer_image,
                            link: link
                        });
                        return offers;
                    }
                } else {
                    // return 7;
                    if (offerTag.querySelector('picture') != null) {
                        //  return 8;]
                        var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                        //  return offer_img_tag;
                        var offer_image = offer_img_tag.getAttribute('data-bgset');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({
                            title: offer_img_tag.getAttribute('title'),
                            description: offer_description,
                            image: offer_image,
                            link: link
                        });
                    } else {
                        //  return 9;
                        var offer_img_tag = offerTag.querySelector('img');
                        var offer_image = offer_img_tag.getAttribute('data-src');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({
                            title: offer_img_tag.getAttribute('title'),
                            description: offer_description,
                            image: offer_image,
                            link: link
                        });
                    }


                }
            }

        }

        return offers;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Offers Details");
        console.log(e)
    }
}

async function scrapeAll(page, callback) {
    try {

        debugLog.push(await dateTime() + ":" + 'In Fetching campground detail function.');
        var campgroundStatus = true;

        let campgrounds = [];
        // console.log(campgrounds);
        var result = [];
        var successCamp = 0;
        var failedCamp = 0;
        CampGround.findAll({ where: { data_source_id: datasourceId } })
            .then(async data => {
                console.log(chalk.yellow("We got " + data.length + " campground(s) to scrape"));

                for (var p = 0; p < data.length; p++) {
                    // console.log("title:"+data[p].title);
                    var crawlerLog = [];
                    var campdata = data[p];
                    try {
                        if (campdata['id'] == 716) {
                            debugLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");
                            crawlerLog.push(await dateTime() + ":" + "We got " + campdata['title'] + " campground for scrape");

                            var link = '';
                            var title = '';
                            link = campdata['url'];
                            title = campdata['title'];
                            debugLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                            crawlerLog.push(await dateTime() + ":" + "updating campground status to InProgress");
                            const campdataVal = {
                                index_status: "InProgress",
                            };
                            CampGround.update(campdataVal, {
                                where: { id: campdata['id'] }
                            })
                                .then(data => {
                                })
                                .catch(async err => {
                                    debugLog.push(await dateTime() + ":" + "Error in Updating Campground Status to Inprogress ");
                                    console.log(err.message)
                                });
                            debugLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            crawlerLog.push(await dateTime() + ":" + "Creating Campgroundlog Data with status Pending");
                            const campLogData = {
                                crawler_log_id: crawlerLogId,
                                campground_id: campdata['id'],
                                status: 'Pending',
                            };

                            CampgroundLog.create(campLogData)
                                .then(data => {
                                    campLogId = data['id'];
                                })
                                .catch(async err => {
                                    debugLog.push(await dateTime() + ":" + "Error in Creating Campground Log");
                                    console.log(err.message)
                                });

                            await page.setDefaultNavigationTimeout(0);
                            debugLog.push(await dateTime() + ":" + "Nevigating..." + link);
                            crawlerLog.push(await dateTime() + ":" + "Nevigating..." + link);
                            await page.goto(link, { waitUntil: 'networkidle2' });

                            await autoScroll(page);

                            console.log(chalk.magenta("Navigating to " + title));
                            debugLog.push(await dateTime() + ":" + "Nevigating..." + title);
                            crawlerLog.push(await dateTime() + ":" + "Nevigating..." + title);

                            let campgroundDetails = await page.evaluate(campground_details);

                            if (campgroundDetails['main_image'] != '') {
                                var iurl = campgroundDetails['main_image'];
                                var filename = iurl.substring(iurl.lastIndexOf('/') + 1).toLowerCase();
                                var datasourceID = campdata['data_source_id'];
                                var campID = campdata['id'];
                                campgroundDetails['main_image'] = await s3.uploadImageLocal(iurl, 'data_source/' + datasourceID + '/campgrounds/' + campID + '/images/mainimage/' + filename);
                            }

                            if (typeof (campgroundDetails['dev']['single_page']) != undefined && !campgroundDetails['dev']['single_page']) {
                                // code for getting amenities
                                debugLog.push(await dateTime() + ":" + "Navigating getting amenities");
                                crawlerLog.push(await dateTime() + ":" + "Navigating getting amenities");
                                await page.goto(link + 'amenities/', { waitUntil: 'networkidle2' });
                                console.log(chalk.yellow("Navigating for getting amenities"));
                                campgroundDetails['data']['amenities'] = await page.evaluate(funamenities);
                                debugLog.push(await dateTime() + ":" + "Navigation Done for amenities");
                                crawlerLog.push(await dateTime() + ":" + "Navigation Done for amenities");

                                // code for getting local attractions
                                debugLog.push(await dateTime() + ":" + "Navigating getting attraction");
                                crawlerLog.push(await dateTime() + ":" + "Navigating getting attraction");
                                await page.goto(link + 'local-attractions/', { waitUntil: 'networkidle2' });
                                console.log(chalk.yellow("Navigating for getting local attractions"));
                                campgroundDetails['data']['local_attractions'] = await page.evaluate(funattractions);
                                debugLog.push(await dateTime() + ":" + "Navigating Done for attraction");
                                crawlerLog.push(await dateTime() + ":" + "Navigating Done for attraction");

                                // code for getting local attractions
                                debugLog.push(await dateTime() + ":" + "Navigating getting offers");
                                crawlerLog.push(await dateTime() + ":" + "Navigating getting offers");
                                await page.goto(link + 'special-offers/', { waitUntil: 'networkidle2' });
                                await page.waitForSelector('.special-offers-list', { visible: true })
                                console.log(chalk.gray("Navigating for getting offers"));
                                campgroundDetails['offers'] = await page.evaluate(funoffers);
                                debugLog.push(await dateTime() + ":" + "Navigating Done for offers");
                                crawlerLog.push(await dateTime() + ":" + "Navigating Done for offers");

                                // console.log(JSON.stringify(campgroundDetails['offers']));

                                // code for getting accomodation types
                                if (campgroundDetails['dev']['accomodation_link'] != "") {

                                    await page.goto(campgroundDetails['dev']['accomodation_link'], { waitUntil: 'networkidle2' });
                                    console.log(chalk.yellow("Navigating for getting accomodation types"));
                                    debugLog.push(await dateTime() + ":" + "Navigating for getting accomodation types");
                                    crawlerLog.push(await dateTime() + ":" + "Navigating for getting accomodation types");

                                    campgroundDetails['accomodation_types'] = await page.evaluate(funaccomodationtypes);

                                    for (var j = 0; j < campgroundDetails['accomodation_types'].length; j++) {
                                        if (campgroundDetails['accomodation_types'][j]['data'].length == 0) {
                                            await page.goto(campgroundDetails['accomodation_types'][j]['url'], { waitUntil: 'networkidle2' });
                                            campgroundDetails['accomodation_types'][j]['data'] = await page.evaluate(funaccomodationdetails);
                                        }
                                    }
                                    debugLog.push(await dateTime() + ":" + "Navigating Done for accomodation types");
                                    crawlerLog.push(await dateTime() + ":" + "Navigating Done for accomodation types");

                                }
                            }
                            // console.log(campgroundDetails['accomodation_types'])

                            for (var j = 0; j < campgroundDetails['accomodation_types'].length; j++) {

                                if (campgroundDetails['accomodation_types'][j]['data'].length > 0) {
                                    for (var d = 0; d < campgroundDetails['accomodation_types'][j]['data'].length; d++) {
                                        if (campgroundDetails['accomodation_types'][j]['data'][d]['images']) {
                                            if (campgroundDetails['accomodation_types'][j]['data'][d]['images'].length > 0) {
                                                for (var k = 0; k < campgroundDetails['accomodation_types'][j]['data'][d]['images'].length; k++) {
                                                    var iurl = campgroundDetails['accomodation_types'][j]['data'][d]['images'][k]['image'];
                                                    var no = k + 1 + 1 + d;
                                                    var filename = no + '_' + iurl.substring(iurl.lastIndexOf('/') + 1).toLowerCase();
                                                    campgroundDetails['accomodation_types'][j]['data'][d]['images'][k]['image'] = await s3.uploadImageLocal(iurl, 'data_source/' + datasourceID + '/campgrounds/' + campID + '/images/campsites/' + filename);
                                                }
                                            }
                                        } else {

                                        }
                                    }

                                }
                            }
                            debugLog.push(await dateTime() + ":" + "Storing images on s3 bucket")
                            crawlerLog.push(await dateTime() + ":" + "Storing images on s3 bucket")
                            var datasourceID = campdata['data_source_id'];
                            var campID = campdata['id'];
                            var addressDetail="";
                            for (var m = 0; m < campgroundDetails['images'].length; m++) {
                                if (m < 1) {
                                    var re = /(?:\.([^.]+))?$/;
                                    for (var key in campgroundDetails['images']) {
                                        var img_url = campgroundDetails['images'][key]['image'];
                                        var album = campgroundDetails['images'][key]['title'];
                                        // var albumname = album.replace(" ", "_");
                                        var albumname = album.replace(/[^A-Z0-9]+/ig, "-");
                                        var ext = await re.exec(img_url)[1];
                                        album = album.replace(/[^A-Z0-9]+/ig, "-");
                                        var slug_image = await (album
                                            .toLowerCase()
                                            .replace(/ /g, '-')
                                            .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();

                                        // slug_image=slug_image.split("/").join("-");
                                        campgroundDetails['images'][key]['image'] = await s3.uploadImageLocal(img_url, 'data_source/' + datasourceID + '/campgrounds/' + campID + '/images/' + albumname + "/" + slug_image);
                                    }
                                }
                            }
                            debugLog.push(await dateTime() + ":" + "Storing images on s3 bucket Done")
                            crawlerLog.push(await dateTime() + ":" + "Storing images on s3 bucket Done")
                            // console.log(campgroundDetails["images"])

                            result.push(campgroundDetails);
                            // console.log(result);
                            // debugLog.push(await dateTime() + ":" + "Fetching campground detail")

                            fs.writeFileSync(filePathStore + "/temp/output" + campdata['id'] + ".json", JSON.stringify(campgroundDetails));
                            const filepath = path.resolve(filePathStore + "/temp/output" + campdata['id'] + ".json");
                            var s3result;
                            // var CampLogfilename = new Date().getTime() + '_campground_log.json';
                            var CampLogfilename = campdata['id'] + '_campground_log.json';
                            debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            crawlerLog.push(await dateTime() + ":" + "uploading campground detail json file to server")
                            s3result = await s3.uploadFileLocal(filepath, 'data_source/' + campdata['data_source_id'] + '/campgrounds/' + campdata['id'] + '/' + CampLogfilename);
                            // if(result.)
                            var logFileLocation = '';
                            console.log(s3result)
                            if (s3result != '') {
                                // logFileLocation = result['Location'];
                                // fs.unlinkSync(filepath)
                                logFileLocation = CampLogfilename;
                                debugLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                                crawlerLog.push(await dateTime() + ":" + "successfully uploaded file to server")
                            } else {
                                debugLog.push(await dateTime() + ":" + "error in upload file to server")
                                crawlerLog.push(await dateTime() + ":" + "error in upload file to server")
                            }

                            debugLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")
                            crawlerLog.push(await dateTime() + ":" + "updating campground staus to completed and set log file path")

                            campgroundStatus = true;
                            addressDetail=campgroundDetails['contact']['address'];
                            await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, logFileLocation,addressDetail);
                        }
                        successCamp++;

                        // return result;
                    } catch (e) {
                        failedCamp++;
                        crawlerstatus = false;
                        console.log(e)
                        campgroundStatus = false;
                        debugLog.push(await dateTime() + ":" + "Error in Fetching Individual Campground Detail");

                        await storeCampGroundLogFile(crawlerLog, campdata['id'], campgroundStatus, '','');

                    }
                }
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);
                callback(result)
            })
            .catch(async err => {
                debugLog.push(await dateTime() + ":" + "Error in Fetching All Campground Based On Datasource");
                console.log(err);
                crawlerstatus = false;
                callback(err)
                debugLog.push('Success:' + successCamp + ',Failed:' + failedCamp);

            });

    } catch (e) {
        console.log(e)
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Fetching Scrapping All Campground Details");
        callback(e)
    }
    // await storeCampGroundLogFile(crawlerLog, campdata['id']);
}

async function autoScroll(page) {
    try {
        await page.evaluate(async () => {
            await new Promise((resolve, reject) => {
                var totalHeight = 0;
                var distance = 100;
                var timer = setInterval(() => {
                    var scrollHeight = document.body.scrollHeight;
                    window.scrollBy(0, distance);
                    totalHeight += distance;

                    if (totalHeight >= scrollHeight) {
                        clearInterval(timer);
                        resolve();
                    }
                }, 100);
            });
        });
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + "Error in Scrolling Page");
    }

}

async function storeCampGroundLogFile(crawlerLog, camp_id, campStatus, logFileLocation,address="") {
    try {
        const campUpdate = {
            data_file_path: logFileLocation,
            index_status: 'Completed',
            crawler_status: (campStatus == true) ? 'Success' : 'Failed',
            location:JSON.stringify(address),
        };
        CampGround.update(campUpdate, {
            where: { id: camp_id }
        })

        debugLog.push(await dateTime() + ":" + "updating campground log status and set log file path")

        fs.writeFileSync(filePathStore + "temp/camp_crawler" + camp_id + ".json", JSON.stringify(crawlerLog));
        const filepath = path.resolve(filePathStore + "temp/camp_crawler" + camp_id + ".json");

        var s3result;
        var crawlerLogfilename = new Date().getTime() + '_campground_crawler_log.json';

        // var CampLogfilename=datasourceId+'_crawler_log.json';
        debugLog.push(await dateTime() + ":" + "uploading campground detail json file to server")

        s3result = await s3.uploadFileLocal(filepath, 'data_source/' + datasourceId + '/campgrounds/' + camp_id + '/crawler/' + crawlerLogfilename);

        console.log(s3result)
        if (s3result != '') {
            console.log("successfully uploaded Campground Crawler log file on server");
            // fs.unlinkSync(filepath)
        } else {
            crawlerLogfilename = '';
            console.log("Failed to upload Campground Crawler log file on server");
        }

        const campLogDataUpdate = {
            status: (campStatus == true) ? 'Success' : 'Failed',
            log_file_path: crawlerLogfilename,
        };
        CampgroundLog.update(campLogDataUpdate, {
            where: { id: campLogId }
        })
    } catch (e) {
        crawlerstatus = false;
        console.log(e)
    }
}
