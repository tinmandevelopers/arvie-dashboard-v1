// node scrapping/sunavailabilitylink.js --datasource_id=2

const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

//Prod
// var configPath='../../../../../scrapping/';
// var filePathStore='../scrapping/';
//Local
var configPath = '../scrapping/';
var filePathStore = 'scrapping/temp/';
const db = require(configPath + "models");

const CampGround = db.CampGround;
var datasourceId = args.datasource_id;

var cond = { "data_source_id": datasourceId };
var links = [];
CampGround.findAll({ where: cond })
    .then(async data => {

        console.log('**************************')
        console.log("start At : " + new Date())

        for (i = 0; i < data.length; i++) {

            var browser = await puppeteer.launch({
                headless: true,
                // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
            });

            try {

                console.log(await browser.version());

                // open a new page
                var page = await browser.newPage();

                await page.setDefaultNavigationTimeout(0);

                var browseUrl = data[i]['url'];
                console.log('Navigating...' + browseUrl);
                await page.goto(browseUrl), { waitUntil: 'networkidle2' };
                console.log('in come')
                // let element = await frame.$('.validation-summary-errors');
                // let errorMsgPopup = await frame.evaluate(el => el.textContent, element)
                // console.log(errorMsgPopup);

                const hrefs = await page.$$eval('#nav > div.nav-drop-classic-holder > div.search-block > div > a', links => links.map(link => link.href));
                links.push(hrefs);
                await browser.close();
            } catch (err) {
                console.log(err)
                await browser.close();
            }

        }


        fs.writeFile(filePathStore + "sunlinks.json", JSON.stringify(links), function (err) {
            if (err) throw err;
            console.log("Saved!");
        });
        console.log(links);
        console.log('**************************')
        console.log("End At : " + new Date())

    })
    .catch(err => {
        console.log(err);

    });


