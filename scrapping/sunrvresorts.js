var express = require('express');
var fs = require('fs');

var app = express();


const puppeteer = require("puppeteer");
const chalk = require("chalk");
var fs = require("fs");
var s3 = require("../helper/s3imageUpload");
// MY OCD of colorful console.logs for debugging... IT HELPS
const error = chalk.bold.red;
const success = chalk.keyword("green");
const log = console.log;




//app.get('/scrape', function (req, res) {

(async () => {
    try {

        var browser = await puppeteer.launch({headless: false, args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
                    // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });


        console.log(await browser.version());

        // open a new page
        var page = await browser.newPage();

        await page.setViewport({width: 1366, height: 768});

        await page.setDefaultNavigationTimeout(0);

        var siteUrl = 'https://www.sunrvresorts.com';
        var browseUrl = siteUrl + '/resorts/';

        console.log('Navigating...');

        var campgroundLinks = [];

        await page.goto(browseUrl);

        var campgroundLinks = await page.evaluate(({siteUrl}) => {
            var titleNodeList = document.querySelectorAll('.columns .col ul li a');
            var titleLinkArray = [];

            //titleNodeList.length = 15;

            for (var i = 0; i < titleNodeList.length; i++) {
                //for (var i = 30; i < 60; i++) {
                titleLinkArray[i] = {
                    title: titleNodeList[i] && titleNodeList[i].innerText.trim(),
                    link: titleNodeList[i] && titleNodeList[i].getAttribute("href"),
                };
            }
            return titleLinkArray;
        }, {siteUrl});
        // console.log(campgroundLinks);


        fs.writeFileSync("scrapping/all_sun_campgrounds.json", JSON.stringify(campgroundLinks));

        // console.log("after file write");
        //campgroundLinks = [];

        const result = await scrapeAll(page, campgroundLinks);


        // fs.writeFileSync("scrapping/output.json", JSON.stringify(result));

        await browser.close();
        console.log(success("Browser Closed"));



    } catch (err) {
        // Catch and display errors
        console.log(error(err));
        await browser.close();
        console.log(error("Browser Closed"));
    }

})();

async function fnConvertToSlug(Text)
{
    return Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
    //  will generate -- data

    //text.toLowerCase().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
    // used for - To avoid multiple sequential hyphens
}


async function campground_details() {
    let dev = {};
    let returndata;
    var re = /(?:\.([^.]+))?$/;
    let amenitiesArr = {amenities: []};
    let localAttractionsArr = [];
    var offers = [];
    let photosArray = [];
    let accomodationArray = [];
    dev.single_page = true;

    let contact = {};
    let data = {};

    var linksTag = await document.querySelector('#nav .subnav_area_bg ul');


    //return linksTag;

    var name = await document.querySelector('.intro-section .title-block .container span.title').textContent;

    var slug = name
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-')
            ;


    var main_image = '';

    if (await document.querySelector('.intro-section .templateseobgheroimg') != null) {
        main_image = await document.querySelector('.intro-section .templateseobgheroimg').getAttribute('src');
    } else {
        var style = getComputedStyle(await document.querySelector('.intro-section .bg-stretch'));
        main_image = style.backgroundImage.slice(4, -1).replace(/"/g, "");

//          var style = await document.querySelector('.intro-section .bg-stretch').style.backgroundImage;
//         var main_image = style.backgroundImage;
    }

    var description = await document.querySelector('.about-section .container .text-holder').innerHTML;

    var contactTag = await document.querySelector('.contacts-nav .contacts-list');


    var phone = contactTag.querySelector('a[href^="tel:"]').getAttribute('href').replace("tel:", "");
    var email = contactTag.querySelector('a[href^="mailto:"]').getAttribute('href').replace("mailto:", "");
    var addressTag = contactTag.querySelector('address');
    var map_link = addressTag.parentElement.getAttribute('href');
    var resport_map = await document.querySelectorAll('.resortlinkright .resortlinkother a.map-link')[0].getAttribute('href');

    contact.telephone_no = phone;
    contact.email = email;
    contact.physical_address = addressTag.textContent.trim();
    contact.map_link = map_link;
    contact.resport_map = resport_map;

    // return contact;

    var seasonInfo = await document.querySelector('.resortlink ul').innerHTML;

    //return linksTag;
    if (linksTag.classList.contains("checkselenium")) {

        dev.single_page = false;

        var aTags = await linksTag.querySelectorAll('li a');
        var searchText = "Ways to Stay";
        dev.accomodation_link = '';

        for (var i = 0; i < aTags.length; i++) {
            if (aTags[i].textContent.toLowerCase() == searchText.toLowerCase()) {
                dev.accomodation_link = aTags[i].getAttribute('href');
                break;
            }
        }

        // return dev;

        var photos = await document.querySelectorAll('.about-section .container .images-container .images-box .img-holder');
        // var lazyLoad = await document.querySelectorAll('.about-section .container .images-container .images-box .img-holder .lazyloaded');

        //   if (lazyLoad.length > 0) {
        // return 1;
        //  await window.scrollTo(0,document.body.scrollHeight);
//var scrollingElement = await (document.scrollingElement || document.body);
        //scrollingElement.scrollTop = scrollingElement.scrollHeight;
        //await document.getElementsByClassName("blog-section")[0].scrollIntoView();
        //  }

        // return photos;
        // var titleArr = new Array();
        var imgIndex = 1;




        for (var photo of photos) {
            //return photo.nodeName;
            // return await photo.querySelector('picture > source').getAttribute('data-srcset');
            if (await photo.querySelector('.destination-img') != null) {
                var title = 'gallery_' + imgIndex;
                var url = '';
                if (await photo.querySelector('picture > source') != null) {
                    var url = await photo.querySelector('picture > source').getAttribute('data-srcset');
                }
            } else {
                // return 'else';
                var title = await photo.querySelector('img').getAttribute('title');
                var url = await photo.querySelector('img').getAttribute('data-src');
            }
            // titleArr.push(title)

            var ext = await re.exec(url)[1];
            var slug_image = await (title
                    .toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
            ;

            var s3 = '';
            // var s3 = await uploadFileToS3(url, 'sun/' + slug_image);

            await photosArray.push({title: title, url: url, s3: s3});
            imgIndex++;
        }

        // return photosArray;
    } else {
        // return 1;
        var photos = await document.querySelectorAll('.about-section .container .gallery-block .slick-track .slick-slide a');
        var titleArr = new Array();
        var imgIndex = 1;
        for (var photo of photos) {
            if (await photo.firstElementChild.nodeName == 'IMG') {
                var title = await photo.getAttribute('title');
                var url = await photo.querySelector('img').getAttribute('src');
            } else {
                var title = 'gallery_' + imgIndex;
                titleArr.push(title);
                var url = await photo.querySelector('div').getAttribute('data-bgset');
            }

            titleArr.push(title)

            var ext = await re.exec(url)[1];
            var slug_image = await (title
                    .toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '') + "." + ext).toLowerCase();
            ;

            var s3 = '';
            // var s3 = await uploadFileToS3(url, 'sun/' + slug_image);

            await photosArray.push({title: title, url: url, s3: s3});
            imgIndex++;
        }
        //return titleArr;
        //return photosArray;


        var amenities = await document.querySelectorAll('#amenities-folder ul li');


        for (var amenity of amenities) {
            amenitiesArr['amenities'].push(amenity.textContent.trim());
        }

        data.amenities = amenitiesArr;



        var local_attractions = await document.querySelectorAll('#attractions-anchor .attractions-holder ul li');


        for (var attraction of local_attractions) {
            localAttractionsArr.push(attraction.textContent.trim());
        }

        data.local_attractions = localAttractionsArr;



        var accomodationTag = await document.querySelectorAll('.ways_to_stay_bg:not([style*="display:none"]) .container ul li a');


        for (var accomodation of accomodationTag) {
            var acc_type = accomodation.querySelector('h5').textContent;
            var id = accomodation.getAttribute('href').replace('#', '');

            var accomodationTag = await document.getElementById(id);

            var all_accomodation = accomodationTag.querySelectorAll('.full_ways_stay_div');

            var accoData = new Array();

            for (var a_acc of all_accomodation) {

                var acc_data = {};
                acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
                acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
                acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

                var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
                var amArray = new Array();
                for (var amenitiy of amenities) {
                    amArray.push(amenitiy.textContent.trim());
                }

                acc_data.amenities = amArray;

                var imageArray = new Array();
                var images = a_acc.querySelectorAll('.waystostay_slider_item a img');
                for (var image of images) {
                    imageArray.push({title: image.getAttribute('title'), description: image.getAttribute('description'), image: image.getAttribute('data-src')});
                }


                acc_data.images = imageArray;
            }
            accoData.push(acc_data);
            accomodationArray.push({type: acc_type, data: accoData});
        }
        // return accomodationArray;

        var offerTag = await document.querySelector('.special-offers-list');

        // return offerTag;
//return offerTag.children.length;
//return offerTag.childNodes;
        if (offerTag.children.length > 0) {
            if (!offerTag.classList.contains("slick-slider")) {
                //return offerTag.nodeName;
                if (offerTag.nodeName == 'UL') {
                    for (var offer of offerTag) {
                        var offer = offer.querySelector('img');
                        var link = offer.querySelector('.holder a').getAttribute('href');
                        var offer_description = offer.querySelector('.holder p').textContent;
                        offers.push({title: offer.getAttribute('title'), description: offer_description, image: offer.getAttribute('src'), link: link});
                    }
                } else {

//                    var offer_img_tag = offerTag.querySelector('img');
//                    var offer_image = offer_img_tag.getAttribute('data-src');
//                      return offer_image;
//                    var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');
//
//                    var offer_description = offerTag.querySelector('.holder p').textContent;
//                    offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});

                    if (offerTag.querySelector('img') != null) {
                        var offer_img_tag = offerTag.querySelector('img');
                        var offer_image = offer_img_tag.getAttribute('data-src');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                    } else {
                        var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                        //  return offer_img_tag;
                        var offer_image = offer_img_tag.getAttribute('data-bgset');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                    }
                }

            } else {
                // return 1;
                if (offerTag.nodeName == 'UL') {
                } else {
                    if (offerTag.querySelector('img') != null) {
                        var offer_img_tag = offerTag.querySelector('img');
                        var offer_image = offer_img_tag.getAttribute('data-src');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                    } else {
                        var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                        //  return offer_img_tag;
                        var offer_image = offer_img_tag.getAttribute('data-bgset');
                        var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                        var offer_description = offerTag.querySelector('.holder p').textContent;
                        offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                    }


                }
            }

        }

    }





    // return offers;

//    offer.title = await document.querySelector('.offers-section .specialoffer_btns .text-block').innerHTML;
//     offer.description = await document.querySelector('.offers-section .specialoffer_btns .text-block').innerHTML;


    returndata = {name: name, slug: slug, description: description, main_image: main_image, data: data, contact: contact, season_dates: seasonInfo, images: photosArray, accomodation_types: accomodationArray, offers: offers, dev: dev};
    return returndata;
}

async function funamenities() {
    let amenitiesArr = {amenities: [], unique_amenities: []};
    let amenities = await document.querySelectorAll('.amenities-container .content-box .title-bar span');
    let unique_amenities = await document.querySelectorAll('.amenities-block ul.accordion li a');


    for (var amenity of amenities) {
        amenitiesArr['amenities'].push(amenity.textContent);
    }

    for (var amenity of unique_amenities) {
        amenitiesArr['unique_amenities'].push(amenity.textContent);
    }

    return amenitiesArr;

}

async function funaccomodationtypes() {
    let accomodationArray = new Array();
    var accomodations = await document.querySelectorAll('.ways_to_stay_bg ul.tabs_headings_tabs li');
    //  var other_accomodations = await document.querySelector('.ways_to_stay_bg:not([style*="display:none"]) ul li');
    var accoData = new Array();

    for (var accomodationtag of accomodations) {
        var accomodation = await accomodationtag.querySelector('a');
        var acc_type = await accomodation.querySelector('h5').textContent;

        var acc_data = {};

        if (accomodationtag.classList.contains("ui-tabs-active")) {

            var all_accomodation = await document.querySelectorAll('.ways_to_stay_bg .full_ways_stay_div');

            accoData = new Array();

            //  return all_accomodation;

            for (var a_acc of all_accomodation) {

                acc_data = {};
                acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
                //return  acc_data.title;
                acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
                acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

                var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
                var amArray = new Array();
                for (var amenitiy of amenities) {
                    amArray.push(amenitiy.textContent.trim());
                }

                acc_data.amenities = amArray;

                // return acc_data.amenities;

                var imageArray = new Array();
                var images = a_acc.querySelectorAll('.waystostay_slider_item a img');

                //return images;
                for (var image of images) {
                    imageArray.push({title: image.getAttribute('title'), description: image.getAttribute('description'), image: image.getAttribute('data-src')});
                }


                acc_data.images = imageArray;

                // return acc_data;
            }
            accoData.push(acc_data);
        } else {
            accoData = [];
        }


        accomodationArray.push({type: acc_type, url: accomodation.getAttribute('href'), data: accoData});
    }
    return accomodationArray;
}

async function funaccomodationdetails() {
    let accomodationArray = new Array();


    var all_accomodation = await document.querySelectorAll('.ways_to_stay_bg .full_ways_stay_div');

    //  return all_accomodation;

    for (var a_acc of all_accomodation) {

        var acc_data = {};
        acc_data.title = a_acc.querySelector('.waystostay_slider_right_area h2').textContent;
        //return  acc_data.title;
        acc_data.sub_title = a_acc.querySelector('.waystostay_slider_right_area h3').textContent;
        acc_data.description = a_acc.querySelector('.waystostay_slider_right_area p').innerHTML.trim("\n");

        var amenities = a_acc.querySelectorAll('.waystostay_slider_right_area ul li');
        var amArray = new Array();
        for (var amenitiy of amenities) {
            amArray.push(amenitiy.textContent.trim());
        }

        acc_data.amenities = amArray;

        // return acc_data.amenities;

        var imageArray = new Array();
        var images = a_acc.querySelectorAll('.waystostay_slider_item a img');

        //return images;
        for (var image of images) {
            imageArray.push({title: image.getAttribute('title'), description: image.getAttribute('description'), image: image.getAttribute('data-src')});
        }


        acc_data.images = imageArray;

        accomodationArray.push(acc_data);

        // return acc_data;
    }




    // accomodationArray.push({type: acc_type, url: accomodation.getAttribute('href'), data: accoData});
    //  }
    return accomodationArray;
}

async function funattractions() {

    let attractionList = new Array();



    var attractionListTag = await  document.querySelectorAll('.lcl_attrac_panel_bg .lcl_attrac_panel');

    let  website = '';
    for (var attractionTag of attractionListTag) {
        var picture = await  attractionTag.querySelector('.lcl_img img').getAttribute('data-src');
        let title = await  attractionTag.querySelector('.lcl_text_area h1').textContent;
        let address = await  attractionTag.querySelector('.lcl_text_area h2').textContent;
        let  description = await  attractionTag.querySelectorAll('.lcl_text_area p')[1].innerHTML;

        var obj = {
            title: title,
            address: address,
            description: description,
            picture: picture,
            website: website
        };
        attractionList.push(obj);
    }

    return attractionList;
}

async function funoffers() {

    //  return 2;
    var offers = new Array();
    var offerTag = await document.querySelector('.special-offers-list');
//return offerTag;
//return offerTag.childNodes;
    if (offerTag.children.length > 0) {
        //  return 1;
        if (!offerTag.classList.contains("slick-slider")) {

            if (offerTag.nodeName == 'UL') {
                //return 1;
                var offerLi = await offerTag.querySelectorAll('li');

                for (var offer of offerLi) {
                    var offer_img_tag = offer.querySelector('img');
                    var link = offer.querySelector('.holder a').getAttribute('href');
                    var offer_description = offer.querySelector('.holder p').textContent;
                    offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_img_tag.getAttribute('src'), link: link});
                }
            } else {

                var offer_img_tag = offerTag.querySelector('img');
                var offer_image = offer_img_tag.getAttribute('data-src');
                var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                var offer_description = offerTag.querySelector('.holder p').textContent;
                offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
            }

        } else {
            //  return 5;
            if (offerTag.nodeName == 'UL') {
                //  return 6;
                var offerLi = await offerTag.querySelectorAll('li');
                //  return offerLi;
                for (var offer of offerLi) {
                    if (offerTag.querySelector('picture') != null) {
                        var offer_img_tag = offer.querySelector('div.bg-stretch');
                        var offer_image = offer_img_tag.getAttribute('data-bgset');
                        //return offer;
                        var link = offer.querySelector('.holder a').getAttribute('href');

                        var offer_description = offer.querySelector('.holder p').textContent;
                    } else {
                        //return 88;
                        var offer_image_tag = offer.querySelector('img');
                        //   return offer;
                        var offer_image = offer_image_tag.getAttribute('src');
                        // return offer_image;
                        var link = offer.querySelector('.holder a').getAttribute('href');
                        var offer_description = offer.querySelector('.holder p').textContent;
                    }

                    if (offer_description == '') {
                        var offer_description = offer.querySelector('.holder div').textContent;
                    }

                    offers.push({title: offer.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                    return offers;
                }
            } else {
                // return 7;
                if (offerTag.querySelector('picture') != null) {
                    //  return 8;]
                    var offer_img_tag = offerTag.querySelector('div.bg-stretch');
                    //  return offer_img_tag;
                    var offer_image = offer_img_tag.getAttribute('data-bgset');
                    var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                    var offer_description = offerTag.querySelector('.holder p').textContent;
                    offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                } else {
                    //  return 9;
                    var offer_img_tag = offerTag.querySelector('img');
                    var offer_image = offer_img_tag.getAttribute('data-src');
                    var link = offerTag.querySelector('.customlistyling .box .holder a').getAttribute('href');

                    var offer_description = offerTag.querySelector('.holder p').textContent;
                    offers.push({title: offer_img_tag.getAttribute('title'), description: offer_description, image: offer_image, link: link});
                }


            }
        }

    }

    return offers;
}


async function scrapeAll(page, rawdata) {
    // console.log(rawdata);
    let campgrounds = [];
    if (rawdata.length == 0) {
        let rawdata = fs.readFileSync('scrapping/all_sun_campgrounds.json');
        campgrounds = JSON.parse(rawdata);
    } else {
        campgrounds = rawdata;
    }

    console.log(chalk.yellow("We got " + campgrounds.length + " campground(s) to scrape"));
    //return false;

    // console.log(campgrounds);

    const result = [];

    await page.exposeFunction("uploadFileToS3", s3.uploadImage)
    await page.exposeFunction("convertToSlug", this.fnConvertToSlug)

    for (var p = 0; p < campgrounds.length; p++) {

        await page.goto(campgrounds[p].link, {waitUntil: 'networkidle2'});

        await autoScroll(page);

        console.log(chalk.red("Navigating to " + campgrounds[p].title));

//        await page.evaluate(() => {
//  //var myFunc = function(element) { element.innerHTML = "baz" };
//  //var foo = document.querySelector('.bar');
//  var data = campground_details(page);
//  console.log(data);
//  return true;
//});
//let campgroundDetails;


        let campgroundDetails = await page.evaluate(campground_details);

        // console.log(campgroundDetails);


        if (typeof (campgroundDetails['dev']['single_page']) != undefined && !campgroundDetails['dev']['single_page']) {
            // code for getting amenities
            await page.goto(campgrounds[p].link + 'amenities/', {waitUntil: 'networkidle2'});
            console.log(chalk.yellow("Navigating for getting amenities"));
            campgroundDetails['data']['amenities'] = await page.evaluate(funamenities);

            // code for getting local attractions
            await page.goto(campgrounds[p].link + 'local-attractions/', {waitUntil: 'networkidle2'});
            console.log(chalk.yellow("Navigating for getting local attractions"));
            campgroundDetails['data']['local_attractions'] = await page.evaluate(funattractions);

            // code for getting local attractions
            await page.goto(campgrounds[p].link + 'special-offers/');
            await page.waitForSelector('.special-offers-list', {visible: true})
            //await autoScroll(page);
            //console.log(chalk.gray("Navigating for getting offers"));
            // console.log(await page.evaluate(funoffers));
            campgroundDetails['offers'] = await page.evaluate(funoffers);
            // console.log(campgroundDetails['offers']);


            //console.log(JSON.stringify(campgroundDetails['offers']));

            // code for getting accomodation types
            if (campgroundDetails['dev']['accomodation_link'] != "") {

                await page.goto(campgroundDetails['dev']['accomodation_link'], {waitUntil: 'networkidle2'});
                console.log(chalk.yellow("Navigating for getting accomodation types"));
                campgroundDetails['accomodation_types'] = await page.evaluate(funaccomodationtypes);

                for (var j = 0; j < campgroundDetails['accomodation_types'].length; j++) {
                    if (campgroundDetails['accomodation_types'][j]['data'].length == 0) {
                        await page.goto(campgroundDetails['accomodation_types'][j]['url'], {waitUntil: 'networkidle2'});
                        campgroundDetails['accomodation_types'][j]['data'] = await page.evaluate(funaccomodationdetails);
                    }
                }
            }



        }
//



        //  console.log(JSON.stringify(campgroundDetails));
        // console.log(campgroundDetails);

        fs.writeFileSync("scrapping/sun_campgrounds/" + campgroundDetails['slug'] + ".json", JSON.stringify(campgroundDetails));


    }
    return result;
}

async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

//});

//app.listen('8001');
//
//console.log('Your node server start successfully....');
//
//exports = module.exports = app;