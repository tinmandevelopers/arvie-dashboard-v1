const puppeteer = require("puppeteer");
var express = require('express');
var fs = require('fs');
const args = require('yargs').argv;
const path = require('path')

var configPath = '../scrapping/temp/';
var filePathStore = 'scrapping/';

var url = args.url;
var filename = args.file_name;

var form_siteCategory = '';
var bookignStatus = false;
var form_checkIn = '';
var form_checkOut = '';
var form_adults = '';
var form_petGroup = '';
var form_equipmentType = '';
var form_equipmentLength = '';
var form_slideOutsGroup = '';
var debugLog = [];
var details = [];

async function dateTime() {
    try {
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var val = date + ' ' + time;
        return val;
    } catch (e) {
        crawlerstatus = false;
        debugLog.push(await dateTime() + ":" + 'Error in Fetching DateTime Details');
    }
}

async function getEnquiryData() {
    let rawdata = fs.readFileSync(configPath + filename);
    var data = JSON.parse(rawdata);
    form_siteCategory = data['details']['reservation_type'];
    form_checkIn = data['check_in_date'].replace(/-/g, '/.');
    form_checkOut = data['check_out_date'].replace(/-/g, '/');
    form_adults = data['details']['adults'];
    form_petGroup = data['details']['pet_friendly'];
    form_equipmentType = data['details']['equipment_type'];
    form_equipmentLength = data['details']['length'];
    form_slideOutsGroup = data['details']['slide_out'];
}

(async () => {
    try {

        await getEnquiryData();

        // for (var p = 0; p < campgrounds.length; p++) {

        var browser = await puppeteer.launch({
            headless: true,
            args: [
                '--start-maximized' // you can also use '--start-fullscreen'
            ]
            // executablePath: './node_modules/puppeteer/.local-chromium/linux-818858/chrome-linux/chrome'
        });

        debugLog.push('*************************');
        debugLog.push(await dateTime() + ":" + await browser.version());
        // open a new page
        var page = await browser.newPage();

        try {
            // console.log('Nevigating : ' + campgrounds[p].title)

            debugLog.push(await dateTime() + ":" + 'Nevigating : ' + url);
            await page.setViewport({ width: 1366, height: 768 });

            await page.setDefaultNavigationTimeout(0);

            var browseUrl = url + 'reserve';
            // var browseUrl = campgrounds[p].link + 'reserve';
            debugLog.push(await dateTime() + ":" + 'Url is : ' + browseUrl);

            await page.goto(browseUrl, { waitUntil: 'networkidle2' });
            if (await page.$('#reserveForm') !== null) {

                debugLog.push(await dateTime() + ":" + "Scrolling page");
                await page.evaluate(() => {
                    document.querySelector('.nav-steps').scrollIntoView()
                })

                // await page.click("#Reservation_CheckInDate");
                // // await sleep(1000)
                // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(2) > td:nth-child(4)");
                // await sleep(1000)

                // await page.click("#Reservation_CheckOutDate");
                // // await sleep(1000)
                // await page.click("#ui-datepicker-div > div.ui-datepicker-group.ui-datepicker-group-first > table > tbody > tr:nth-child(4) > td:nth-child(4) > a");
                // await sleep(1000)
                // if (await page.$('#Reservation_SiteCategory') !== null) {
                //     debugLog.push(await dateTime() + ":" + "Selecting Reservation SiteCategory");
                //     await page.select('#Reservation_SiteCategory', form_siteCategory)
                //     await sleep(1000)
                // }

                if (await page.$('#Reservation_CheckInDate') !== null) {
                    debugLog.push(await dateTime() + ":" + "Storing Check In Date");
                    await page.click("#Reservation_CheckInDate");
                    await page.type("#Reservation_CheckInDate", form_checkIn);
                    await sleep(1000)
                }


                if (await page.$('#Reservation_CheckOutDate') !== null) {
                    debugLog.push(await dateTime() + ":" + "Storing Check Out Date");
                    await page.click("#Reservation_CheckOutDate");
                    await page.type("#Reservation_CheckOutDate", form_checkOut);
                    await page.click(".reserve-top-text");
                    await sleep(1000);
                }


                if (await page.$('#Reservation_Adults') !== null) {
                    debugLog.push(await dateTime() + ":" + "Storing Adult");
                    await page.click("#Reservation_Adults");
                    // console.log('adult is '+form_adults)
                    await page.type("#Reservation_Adults", form_adults);
                    // await sleep(1000)
                }


                if (await page.$('#Reservation_Pets_Group') !== null) {
                    debugLog.push(await dateTime() + ":" + "Selecting Pet Group");
                    await page.click('#Reservation_Pets_Group > label:nth-child(' + form_petGroup + ')');
                    // await sleep(1000)
                }

                if (await page.$('#Reservation_EquipmentType') !== null) {
                    debugLog.push(await dateTime() + ":" + "Selecting Equipment Type");
                    await page.select('#Reservation_EquipmentType', form_equipmentType)
                    await sleep(1000)
                }

                if (await page.$('#Reservation_EquipmentLength') !== null) {
                    // debugLog.push(await dateTime() + ":" + "Storing Equipment Length");
                    // await page.click("#Reservation_EquipmentLength");
                    // await page.type('#Reservation_EquipmentLength', form_equipmentLength)
                    // await sleep(1000)
                    var length = await isLocatorReady('#reserve-equipment-length', page);
                    if (length) {

                        await page.click("#Reservation_EquipmentLength");
                        await page.type('#Reservation_EquipmentLength', form_equipmentLength)
                    }
                }

                if (await page.$('#Reservation_SlideOuts_Group') !== null) {
                    // debugLog.push(await dateTime() + ":" + "Selecting Slideouts Group");
                    // await page.click('#Reservation_SlideOuts_Group > label:nth-child(' + form_slideOutsGroup + ')');
                    // await sleep(1000)
                    var sildeout = await isLocatorReady('#reserve-equipment-slideouts', page);
                    if (sildeout) {
                        await page.click('#Reservation_SlideOuts_Group > label:nth-child(' + form_slideOutsGroup + ')');
                    }
                }

                await page.evaluate(() => {
                    document.querySelector('#reserve-step-vkr').scrollIntoView()
                })

                debugLog.push(await dateTime() + ":" + "Clicking on next Button");
                await page.click('#nextButton');
                await sleep(5000)

                if (await page.$(".validation-summary-errors") !== null) {
                    debugLog.push(await dateTime() + ":" + "Fetching Validation errors ");

                    let element = await page.$('.validation-summary-errors');
                    let errorMsg = await page.evaluate(el => el.textContent, element)
                    comment = errorMsg;
                    bookignStatus = false;

                } else {
                    comment = "Available";
                    //     debugLog.push(await dateTime() + ":" + "Waiting For next Page ");
                    //     await sleep(1000)
                        if (await page.$("#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button") !== null) {
                            let elements = await page.$$("#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button");
                            var count = 2;
                            for (const el of elements) {
                                count++;
    
                                if (await page.$('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div') !== null) {
    
                                    var element = await page.$('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div');
                                    var titleInfo = '';
                                    var priceInfo = '';
                                    var otherInfo = '';
                                    var offerInfo = '';
                                    var capacityInfo = '';
                                    var descInfo = '';
    
                                    if (await element.$('div:nth-child(2) > div > h4') !== null) {
                                        var title = await element.$('div:nth-child(2) > div > h4');
                                        titleInfo = await page.evaluate(el => el.textContent, title);
                                    }
                                    if (await element.$('div:nth-child(2) > div:nth-child(2) > div:nth-child(2) .reserve-quote-per-night') !== null) {
                                        var price = await element.$('div:nth-child(2) > div:nth-child(2) > div:nth-child(2) .reserve-quote-per-night');
                                        priceInfo = await page.evaluate(el => el.textContent, price);
                                        priceInfo = priceInfo.replace(/\s+/g, ' ');
                                    }
                                    if (await element.$('div:nth-child(3) > div > div.details-first > div.row') !== null) {
                                        var detail = await element.$('div:nth-child(3) > div > div.details-first > div.row');
                                        otherInfo = await page.evaluate(el => el.textContent, detail);
                                        otherInfo = otherInfo.replace(/\s+/g, ' ').trim();
                                    }
                                    if (await element.$('div:nth-child(3) > div:nth-child(2)> div >div') !== null) {
                                        var offer = await element.$('div:nth-child(3) >div:nth-child(2)>div >div');
                                        offerInfo = await page.evaluate(el => el.textContent, offer);
                                        offerInfo = offerInfo.replace(/\s+/g, ' ').trim();
                                    }
                                    if (await element.$('div:nth-child(3) > div > div.details-first >.capacity-notice') !== null) {
                                        var capacity = await element.$('div:nth-child(3) >div>div.details-first>.capacity-notice');
                                        capacityInfo = await page.evaluate(el => el.textContent, capacity);
                                        capacityInfo = capacityInfo.replace(/\s+/g, ' ').trim()
                                    }
                                    if (await element.$('#reserve-container > div:nth-child(4) > div > form:nth-child(' + count + ') > div > div:nth-child(5) > div') !== null) {
                                        var desc = await page.$('#reserve-container > div:nth-child(4) > div >form:nth-child(' + count + ') >div >div:nth-child(5) >div');
                                        descInfo = await page.evaluate(el => el.textContent, desc);
                                        descInfo = descInfo.replace(/\s+/g, ' ').trim();
                                    }
    
                                    details.push({
                                        'title': titleInfo,
                                        'price': priceInfo,
                                        'other': otherInfo,
                                        'offer': offerInfo,
                                        'capacity': capacityInfo,
                                        'description': descInfo,
                                    });
                                }
                            }
                            comment = "Available";
                            bookignStatus = true;
                        }else{
                            comment = "Not Available";
                            bookignStatus = false;
                        }
                    //         debugLog.push(await dateTime() + ":" + "Scrolling Page ");
                    //         await page.evaluate(() => {
                    //             document.querySelector('#site-types > div.row.no-padding > div > div').scrollIntoView()
                    //         })
                    //         await sleep(2000)

                    //         debugLog.push(await dateTime() + ":" + "Clicking on reserve Button ");
                    //         await page.click('#site-types > div.row.reserve-sitetype-main-row > div > div:nth-child(1) > div > button');

                    //         await sleep(5000)

                    //         debugLog.push(await dateTime() + ":" + "Waiting For Next Page")

                    //         await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                    //         await page.evaluate(() => {
                    //             document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                    //         })

                    //         debugLog.push(await dateTime() + ":" + "In Step 3")
                    //         await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing First Name")
                    //         const firstname = await page.waitForSelector("#Reservation_FirstName");
                    //         await page.click("#Reservation_FirstName");
                    //         await firstname.type(form_firstName)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Last Name")
                    //         const lastname = await page.waitForSelector("#Reservation_LastName");
                    //         await page.click("#Reservation_LastName");
                    //         await lastname.type(form_lastName)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Address 1")
                    //         const address1 = await page.waitForSelector("#Reservation_Address1");
                    //         await page.click("#Reservation_Address1");
                    //         await address1.type(form_address1)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Address 2")
                    //         const address2 = await page.waitForSelector("#Reservation_Address2");
                    //         await page.click("#Reservation_Address2");
                    //         await address2.type(form_address2)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Coontry Code")
                    //         await page.select('#Reservation_CountryCode', form_countryCode)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Phone Number")
                    //         const phone = await page.waitForSelector("#Reservation_PhoneNumber");
                    //         await page.click("#Reservation_PhoneNumber");
                    //         await phone.type(form_phone)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing City")
                    //         const city = await page.waitForSelector("#Reservation_City");
                    //         await page.click("#Reservation_City");
                    //         await city.type(form_city)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Scrolling Page")
                    //         await page.waitForSelector("#reserve-step3-form");
                    //         await page.evaluate(() => {
                    //             document.querySelector('#reserve-step3-form').scrollIntoView()
                    //         })


                    //         debugLog.push(await dateTime() + ":" + "Storing State")
                    //         const state = await page.waitForSelector("#Reservation_StateProvinceCode");
                    //         await page.click("#Reservation_StateProvinceCode");
                    //         await state.type(form_state)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Postal Code")
                    //         const postcode = await page.waitForSelector("#Reservation_PostalCode");
                    //         await page.click("#Reservation_PostalCode");
                    //         await postcode.type(form_postcode)
                    //         // await sleep(1000)


                    //         debugLog.push(await dateTime() + ":" + "Storing Reservastion Email")
                    //         const resemail = await page.waitForSelector("#Reservation_EmailAddress");
                    //         await page.click("#Reservation_EmailAddress");
                    //         await resemail.type(form_email)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Storing Reservastion Confirm Email");
                    //         const resconfemail = await page.waitForSelector("#Reservation_ConfirmEmailAddress");
                    //         await page.click("#Reservation_ConfirmEmailAddress");
                    //         await resconfemail.type(form_confirmEmail)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Scrolling Page");
                    //         await page.waitForSelector("#reserve-step3-rates > div > div > div:nth-child(3) > div > div");
                    //         await page.evaluate(() => {
                    //             document.querySelector('#reserve-step3-rates > div > div > div:nth-child(3) > div > div').scrollIntoView()
                    //         })

                    //         debugLog.push(await dateTime() + ":" + "Storign Credit Card Number");
                    //         const creditcard = await page.waitForSelector("#Reservation_CreditCardNumber");
                    //         await page.click("#Reservation_CreditCardNumber");
                    //         await creditcard.type(form_creditCard)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Selecting Card Type");
                    //         await page.select('#Reservation_CreditCardType', form_creditCardType)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Selecting Card Expiration Month");
                    //         await page.select('#Reservation_CreditCardExpMonth', form_creditCardExpMonth)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Selecting Card Expiration Year");
                    //         await page.select('#Reservation_CreditCardExpYear', form_creditCardExpYear)
                    //         // await sleep(1000)


                    //         debugLog.push(await dateTime() + ":" + "Selecting Card Security Code");
                    //         const cardcode = await page.waitForSelector("#Reservation_CreditCardSecurityCode");
                    //         await page.click("#Reservation_CreditCardSecurityCode");
                    //         await cardcode.type(form_creditCardCode)
                    //         // await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Scrolling the page");
                    //         await page.waitForSelector("#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox");
                    //         await page.evaluate(() => {
                    //             document.querySelector('#reserve-step3-terms > div.custom-control.custom-checkbox.terms-checkbox').scrollIntoView()
                    //         })

                    //         debugLog.push(await dateTime() + ":" + "Selecting Tearms and Condition");
                    //         await page.click("#Reservation_TermsAgree");
                    //         await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Clicking on Continue buttom");
                    //         await page.click('#continueButton')
                    //         await sleep(3000)

                    //         if (await page.$(".validation-summary-errors") !== null) {
                    //             debugLog.push(await dateTime() + ":" + "Fetching Validation errors ");

                    //             let element = await page.$('.validation-summary-errors');
                    //             let errorMsg = await page.evaluate(el => el.textContent, element)
                    //             comment=errorMsg;
                    //             bookignStatus=false;
                    //             console.log(errorMsg);
                    //         } else{
                    //             await sleep(3000)
                    //             debugLog.push(await dateTime() + ":" + "Scrolling Page");

                    //             await page.waitForSelector("#bookButton");
                    //             await page.evaluate(() => {
                    //                 document.querySelector('#bookButton').scrollIntoView()
                    //             })

                    //             await sleep(1000)
                    //             debugLog.push(await dateTime() + ":" + "Clicking on Booking Button");
                    //             await page.click('#bookButton')
                    //             await sleep(1000)
                    //             debugLog.push(await dateTime() + ":" + "Booking is done for this campground");

                    //             console.log('Booking is Done for this campground')
                    //             comment='Booking is done for this campground';
                    //             bookignStatus=true;
                    //             browseClose=false;
                    //         }


                    //     } else {
                    //         var firstBlockSelector = '';

                    //         debugLog.push(await dateTime() + ":" + "checking Site availability");
                    //         if (await page.$(".reserve-sitetype-unavailable") !== null) {
                    //             firstBlockSelector = '.reserve-sitetype-unavailable';
                    //         } else {
                    //             firstBlockSelector = '.reserve-sitetype-special';
                    //         }
                    //         debugLog.push(await dateTime() + ":" + "scrolling the Page");
                    //         await page.waitForSelector(firstBlockSelector);
                    //         await page.evaluate((firstBlockSelector) => {
                    //             document.querySelector(firstBlockSelector + '> div:nth-child(2)').scrollIntoView()
                    //         }, firstBlockSelector)
                    //         await sleep(1000)

                    //         debugLog.push(await dateTime() + ":" + "Clicking on Avaiblity button");

                    //         await page.click(firstBlockSelector + ' > div:nth-child(2) > div >div:nth-child(1) > div >div:nth-child(2) > a')
                    //         await page.waitForSelector("#modal-window-iframe-lg-title");

                    //         const elementHandle = await page.$('#modal-window-iframe-lg > div > div > iframe');
                    //         const frame = await elementHandle.contentFrame();
                    //         // console.log('frame')
                    //         // console.log(frame)

                    //         debugLog.push(await dateTime() + ":" + "Popup open with dates");
                    //         await frame.waitForSelector('#availability-form-wrapper'),
                    //             await sleep(5000)

                    //         var firstavailableDate = '#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability';
                    //         var flag = false;
                    //         if (await frame.$(".validation-summary-errors") !== null) {

                    //             debugLog.push(await dateTime() + ":" + "Fetching Validation Errors");

                    //             let element = await frame.$('.validation-summary-errors');
                    //             let errorMsgPopup = await frame.evaluate(el => el.textContent, element)
                    //             console.log(errorMsgPopup);
                    //             comment=errorMsgPopup;
                    //             bookignStatus=false;
                    //         }
                    //         while (flag == false) {
                    //             if (await frame.$(firstavailableDate) != null) {
                    //                 flag = true;
                    //                 await frame.waitForSelector(firstavailableDate)
                    //                 debugLog.push(await dateTime() + ":" + "Fetching Date Where campground Available from")
                    //                 const dateValue = await frame.evaluate('document.querySelector("#calendar > div.fc-view-container > div > table > tbody > tr > td > div > div.fc-day-grid >div.fc-widget-content > div >table> tbody > tr >td.fc-event-availability").getAttribute("data-date")')
                    //                 console.log("This CampGroud is now available from " + dateValue);
                    //                 comment="This CampGroud is now available from " + dateValue;
                    //                 bookignStatus=false;
                    //             } else {


                    //                 if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button') != null) {
                    //                     if (await frame.$('#calendar > div.fc-toolbar > div.fc-right > button.fc-state-disabled') != null) {
                    //                         debugLog.push(await dateTime() + ":" + "No Campgrounds are Availabal for this year")
                    //                         console.log('No campgrounds are available for this Year');
                    //                         flag = true;
                    //                         bookignStatus=false;
                    //                         comment='No campgrounds are available for this Year';
                    //                     } else {
                    //                         debugLog.push(await dateTime() + ":" + "No Campgrounds are Available for this then clicking on next month button")
                    //                         await frame.waitForSelector('#calendar > div.fc-toolbar > div.fc-right > button');
                    //                         await frame.click('#calendar > div.fc-toolbar > div.fc-right > button');
                    //                     }
                    //                     await sleep(5000);
                    //                 } else {
                    //                     debugLog.push(await dateTime() + ":" + "No Campgrounds are Available")

                    //                     console.log('No campgrounds are available');
                    //                     comment="No campgrounds are available";
                    //                     bookignStatus=false;
                    //                     flag = true;
                    //                 }
                    //             }
                    //         }
                    //     }
                }
            } else {
                debugLog.push(await dateTime() + ":" + "Fetching Error Message")

                // let element = await page.$('#mainContent > div > div > div:nth-child(2) > div > div');
                // let element = await page.$('.maintenance-container');

                // let errorMsgCamp = await page.evaluate(el => el.textContent, element)
                let errorMsgCamp = 'Site is Under Maintenance'
                bookignStatus = false;
                comment = errorMsgCamp;
            }
            // await sleep(5000)
            // await browser.close();
        } catch (err) {
            // console.log(err)
            debugLog.push(await dateTime() + ":" + "Sorry, an error has ocurred ");
            comment = 'Sorry, an error has ocurred';
            bookignStatus = false;
        }

        await browser.close();
        // }

        // }
    } catch (err) {
        bookignStatus = true;
        debugLog.push(await dateTime() + ":" + "Sometghing Went Wrong")
        comment = err.message;
    }

    var result={
        'availability':bookignStatus,
        'message':comment,
        'details': details
    };

    console.log(JSON.stringify(result))
    // await browser.close();
})();


async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}


async function isLocatorReady(element, page) {
    const isVisibleHandle = await page.evaluateHandle((e) => {
        const style = window.getComputedStyle(document.querySelector(e))
        //   const style = window.getComputedStyle(e);
        return (style && style.display !== 'none' &&
            style.visibility !== 'hidden' && style.opacity !== '0');
    }, element);
    var visible = await isVisibleHandle.jsonValue();

    if (visible) {
        return true;
    }
    return false;
}